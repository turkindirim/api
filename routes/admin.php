<?php

Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login')->middleware('guest:admin');
Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit')->middleware('guest:admin');

Route::post('password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('password/reset', 'Auth\AdminResetPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('password/reset', 'Auth\AdminResetPasswordController@reset')->name('admin.password.update');
Route::get('password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

Route::group(['middleware' => 'auth:admin'], function () {

    // AJAX
    Route::get('/getOffer', 'AdminController@getOffer');
    Route::get('/getUser', 'AdminController@getUser');
    Route::get('/getDistributor', 'AdminController@getDistributor');
    Route::get('/getBranch', 'AdminController@getBranch');
    Route::get('/getOfferMedia', 'OfferController@getOfferMedia');
    Route::get('/deleteOfferBranches', 'OfferController@deleteOfferBranches');

    // Dashboard
    Route::get('/', 'AdminController@dashboard')->name('admin.dashboard');

    // Logout
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    
    // Sellers
    Route::get('sellers', 'SellerController@index')->name('seller.index');
    Route::post('sellers/filter', 'SellerController@filter')->name('seller.filter');
    Route::post('sellers', 'SellerController@store')->name('seller.store');
    Route::get('sellers/create', 'SellerController@create')->name('seller.create');
    Route::get('sellers/delete/{id}', 'SellerController@destroy')->name('seller.destroy');
    Route::get('sellers/edit/{id}', 'SellerController@edit')->name('seller.edit');
    Route::put('sellers/{id}', 'SellerController@update')->name('seller.update');
    Route::get('sellers/permenant/{id}', 'SellerController@forcedelete')->name('seller.forcedelete');
    Route::get('sellers/restore/{id}', 'SellerController@restore')->name('seller.restore');

    // Distributors
    Route::get('distributors', 'DistributorController@index')->name('distributor.index');
    Route::post('distributors/filter', 'DistributorController@filter')->name('distributor.filter');
    Route::get('distributors/create', 'DistributorController@create')->name('distributor.create');
    Route::post('distributors', 'DistributorController@store')->name('distributor.store');
    Route::get('distributors/delete/{id}', 'DistributorController@destroy')->name('distributor.destroy');
    Route::get('distributors/edit/{id}', 'DistributorController@edit')->name('distributor.edit');
    Route::put('distributors/{id}', 'DistributorController@update')->name('distributor.update');

    Route::get('distributors/permenant/{id}', 'DistributorController@forcedelete')->name('distributor.forcedelete');
    Route::get('distributors/restore/{id}', 'DistributorController@restore')->name('distributor.restore');

    // Branches
    Route::get('branches', 'BranchController@index')->name('branch.index');
    Route::post('branches/filter', 'BranchController@filter')->name('branch.filter');
    Route::get('branches/create', 'BranchController@create')->name('branch.create');
    Route::post('branches', 'BranchController@store')->name('branch.store');
    Route::get('branches/delete/{id}', 'BranchController@destroy')->name('branch.destroy');
    Route::get('branches/edit/{id}', 'BranchController@edit')->name('branch.edit');
    Route::get('branches/reviews/{id}', 'BranchController@reviews')->name('branch.reviews');
    Route::put('branches/{id}', 'BranchController@update')->name('branch.update');
    Route::get('branches/permenant/{id}', 'BranchController@forcedelete')->name('branch.forcedelete');
    Route::get('branches/restore/{id}', 'BranchController@restore')->name('branch.restore');
    
    // Users
    Route::get('users', 'UserController@index')->name('users.index');
    Route::post('users/filter', 'UserController@filter')->name('users.filter');
    Route::get('users/search', 'UserController@search')->name('users.search');
    Route::get('users/create', 'UserController@create')->name('users.create');
    Route::post('users', 'UserController@store')->name('users.store');
    Route::get('users/delete/{id}', 'UserController@destroy')->name('users.destroy');
    Route::get('users/edit/{id}', 'UserController@edit')->name('users.edit');
    Route::put('users/{id}', 'UserController@update')->name('users.update');
    Route::get('users/permenant/{id}', 'UserController@forcedelete')->name('users.forcedelete');
    Route::get('users/restore/{id}', 'UserController@restore')->name('users.restore');

    // Admins
    Route::get('admins', 'AdminController@index')->name('admins.index');
    Route::get('admins/create', 'AdminController@create')->name('admins.create');
    Route::post('admins', 'AdminController@store')->name('admins.store');
    Route::get('admins/delete/{id}', 'AdminController@destroy')->name('admins.destroy');
    Route::get('admins/edit/{id}', 'AdminController@edit')->name('admins.edit');
    Route::put('admins/{id}', 'AdminController@update')->name('admins.update');
    Route::get('admins/permenant/{id}', 'AdminController@forcedelete')->name('admins.forcedelete');
    Route::get('admins/restore/{id}', 'AdminController@restore')->name('admins.restore');

    // Languages
    Route::get('languages', 'LanguageController@index')->name('language.index');
    Route::get('languages/create', 'LanguageController@create')->name('language.create');
    Route::post('languages', 'LanguageController@store')->name('language.store');
    Route::get('languages/delete/{id}', 'LanguageController@destroy')->name('language.destroy');
    Route::get('languages/edit/{id}', 'LanguageController@edit')->name('language.edit');
    Route::put('languages/{id}', 'LanguageController@update')->name('language.update');
    Route::get('languages/permenant/{id}', 'LanguageController@forcedelete')->name('language.forcedelete');
    Route::get('languages/restore/{id}', 'LanguageController@restore')->name('language.restore');
    
    // Countries
    Route::get('countries', 'CountryController@index')->name('country.index');
    Route::get('countries/create', 'CountryController@create')->name('country.create');
    Route::post('countries', 'CountryController@store')->name('country.store');
    Route::get('countries/delete/{id}', 'CountryController@destroy')->name('country.destroy');
    Route::get('countries/edit/{id}', 'CountryController@edit')->name('country.edit');
    Route::put('countries/{id}', 'CountryController@update')->name('country.update');
    Route::get('countries/permenant/{id}', 'CountryController@forcedelete')->name('country.forcedelete');
    Route::get('countries/restore/{id}', 'CountryController@restore')->name('country.restore');

    // Provinces
    Route::get('provinces', 'ProvinceController@index')->name('province.index');
    Route::get('provinces/create', 'ProvinceController@create')->name('province.create');
    Route::post('provinces', 'ProvinceController@store')->name('province.store');
    Route::get('provinces/delete/{id}', 'ProvinceController@destroy')->name('province.destroy');
    Route::get('provinces/edit/{id}', 'ProvinceController@edit')->name('province.edit');
    Route::put('provinces/{id}', 'ProvinceController@update')->name('province.update');
    Route::get('provinces/permenant/{id}', 'ProvinceController@forcedelete')->name('province.forcedelete');
    Route::get('provinces/restore/{id}', 'ProvinceController@restore')->name('province.restore');

    // Cities
    Route::get('cities', 'CityController@index')->name('city.index');
    Route::get('cities/create', 'CityController@create')->name('city.create');
    Route::post('cities', 'CityController@store')->name('city.store');
    Route::get('cities/delete/{id}', 'CityController@destroy')->name('city.destroy');
    Route::get('cities/edit/{id}', 'CityController@edit')->name('city.edit');
    Route::put('cities/{id}', 'CityController@update')->name('city.update');
    Route::get('cities/permenant/{id}', 'CityController@forcedelete')->name('city.forcedelete');
    Route::get('cities/restore/{id}', 'CityController@restore')->name('city.restore');
    
    // Districts
    Route::get('districts', 'DistrictController@index')->name('district.index');
    Route::get('districts/create', 'DistrictController@create')->name('district.create');
    Route::post('districts', 'DistrictController@store')->name('district.store');
    Route::get('districts/delete/{id}', 'DistrictController@destroy')->name('district.destroy');
    Route::get('districts/edit/{id}', 'DistrictController@edit')->name('district.edit');
    Route::put('districts/{id}', 'DistrictController@update')->name('district.update');
    Route::get('districts/permenant/{id}', 'DistrictController@forcedelete')->name('district.forcedelete');
    Route::get('districts/restore/{id}', 'DistrictController@restore')->name('district.restore');

    // Card Types
    Route::get('types', 'TypeController@index')->name('types.index');
    Route::get('types/create', 'TypeController@create')->name('types.create');
    Route::post('types', 'TypeController@store')->name('types.store');
    Route::get('types/delete/{id}', 'TypeController@destroy')->name('types.destroy');
    Route::get('types/edit/{id}', 'TypeController@edit')->name('types.edit');
    Route::put('types/{id}', 'TypeController@update')->name('types.update');
    Route::get('types/permenant/{id}', 'TypeController@forcedelete')->name('types.forcedelete');
    Route::get('types/restore/{id}', 'TypeController@restore')->name('types.restore');

    // Offer Categories
    Route::get('categories', 'CategoryController@index')->name('categories.index');
    Route::get('categories/create', 'CategoryController@create')->name('categories.create');
    Route::post('categories', 'CategoryController@store')->name('categories.store');
    Route::get('categories/delete/{id}', 'CategoryController@destroy')->name('categories.destroy');
    Route::get('categories/edit/{id}', 'CategoryController@edit')->name('categories.edit');
    Route::put('categories/{id}', 'CategoryController@update')->name('categories.update');
    Route::get('categories/permenant/{id}', 'CategoryController@forcedelete')->name('categories.forcedelete');
    Route::get('categories/restore/{id}', 'CategoryController@restore')->name('categories.restore');
    
    
    // Tags
    Route::get('tags', 'TagController@index')->name('tags.index');
    Route::get('tags/create', 'TagController@create')->name('tags.create');
    Route::post('tags', 'TagController@store')->name('tags.store');
    Route::get('tags/delete/{id}', 'TagController@destroy')->name('tags.destroy');
    Route::get('tags/edit/{id}', 'TagController@edit')->name('tags.edit');
    Route::put('tags/{id}', 'TagController@update')->name('tags.update');
    Route::get('tags/permenant/{id}', 'TagController@forcedelete')->name('tags.forcedelete');
    Route::get('tags/restore/{id}', 'TagController@restore')->name('tags.restore');

    // Cards
    Route::get('cards', 'CardController@index')->name('cards.index');
    Route::get('cards/barcode', 'CardController@barcode')->name('cards.barcode');
    Route::post('cards/setDistributor', 'CardController@setDistributor')->name('cards.setDistributor');
    Route::get('cards/search', 'CardController@search')->name('cards.search');
    Route::get('cards/create', 'CardController@create')->name('cards.create');
    
    Route::get('cards/{id}', 'CardController@show');

    
    Route::get('cards/log/{id}', 'CardController@log')->name('cards.log');
    Route::post('cards', 'CardController@store')->name('cards.store');
    Route::post('cards/bulk', 'CardController@bulk')->name('cards.bulk');
    Route::get('cards/delete/{id}', 'CardController@destroy')->name('cards.destroy');
    Route::get('cards/edit/{id}', 'CardController@edit')->name('cards.edit');
    Route::get('cards/permenant/{id}', 'CardController@forcedelete')->name('cards.forcedelete');
    Route::get('cards/restore/{id}', 'CardController@restore')->name('cards.restore');
    Route::put('cards/{id}', 'CardController@update')->name('cards.update');

    // Offers
    Route::get('offers', 'OfferController@index')->name('offers.index');
    Route::get('offers/filter', 'OfferController@index');
    Route::post('offers/filter', 'OfferController@index')->name('offers.filter');
    Route::get('offers/create', 'OfferController@create')->name('offers.create');
    Route::post('offers', 'OfferController@store')->name('offers.store');
    Route::get('offers/delete/{id}', 'OfferController@destroy')->name('offers.destroy');
    Route::get('offers/edit/{id}', 'OfferController@edit')->name('offers.edit');
    Route::get('offers/reviews/{id}', 'OfferController@reviews')->name('offers.reviews');
    Route::get('offers/permenant/{id}', 'OfferController@forcedelete')->name('offers.forcedelete');
    Route::get('offers/restore/{id}', 'OfferController@restore')->name('offers.restore');
    Route::put('offers/{id}', 'OfferController@update')->name('offers.update');

    // Account
    Route::get('account', 'AdminController@account')->name('admin.account');
    Route::post('account', 'AdminController@updateAccount')->name('admin.account.update');

    // Newsfeed
    Route::get('newsfeed', 'NewsfeedController@index')->name('newsfeed.index');
    Route::get('newsfeed/create', 'NewsfeedController@create')->name('newsfeed.create');
    Route::post('newsfeed', 'NewsfeedController@store')->name('newsfeed.store');
    Route::get('newsfeed/delete/{id}', 'NewsfeedController@destroy')->name('newsfeed.destroy');
    Route::get('newsfeed/edit/{id}', 'NewsfeedController@edit')->name('newsfeed.edit');
    Route::get('newsfeed/permenant/{id}', 'NewsfeedController@forcedelete')->name('newsfeed.forcedelete');
    Route::get('newsfeed/restore/{id}', 'NewsfeedController@restore')->name('newsfeed.restore');
    Route::put('newsfeed/{id}', 'NewsfeedController@update')->name('newsfeed.update');
    // Post
    Route::get('post', 'PostController@index')->name('post.index');
    Route::get('post/create', 'PostController@create')->name('post.create');
    Route::post('post', 'PostController@store')->name('post.store');
    Route::get('post/delete/{id}', 'PostController@destroy')->name('post.destroy');
    Route::get('post/reviews/{id}', 'PostController@reviews')->name('post.reviews');
    Route::get('post/edit/{id}', 'PostController@edit')->name('post.edit');
    Route::get('post/permenant/{id}', 'PostController@forcedelete')->name('post.forcedelete');
    Route::get('post/restore/{id}', 'PostController@restore')->name('post.restore');
    Route::put('post/{id}', 'PostController@update')->name('post.update');


    // Trash
    Route::get('trash', function(){
        return redirect()->route('admin.dashboard');
    });
    Route::get('trash/sellers', 'TrashController@sellers')->name('sellers.trash');
    Route::get('trash/branches', 'TrashController@branches')->name('branches.trash');
    Route::get('trash/distributors', 'TrashController@distributors')->name('distributors.trash');
    Route::get('trash/users', 'TrashController@users')->name('users.trash');
    Route::get('trash/newsfeed', 'TrashController@newsfeed')->name('newsfeed.trash');
    Route::get('trash/languages', 'TrashController@languages')->name('languages.trash');
    Route::get('trash/cards', 'TrashController@cards')->name('cards.trash');
    Route::get('trash/types', 'TrashController@types')->name('types.trash');
    Route::get('trash/offers', 'TrashController@offers')->name('offers.trash');
    Route::get('trash/categories', 'TrashController@categories')->name('categories.trash');
    Route::get('trash/countries', 'TrashController@countries')->name('countries.trash');
    Route::get('trash/provinces', 'TrashController@provinces')->name('provinces.trash');
    Route::get('trash/cities', 'TrashController@cities')->name('cities.trash');
    Route::get('trash/districts', 'TrashController@districts')->name('districts.trash');

    // Settings
    Route::get('settings', 'AdminController@settings')->name('settings');
    Route::post('settings', 'AdminController@update_settings')->name('settings.update');
    Route::post('setsort/{model}', 'AdminController@setsort')->name('model.sort');

    // Slides
    Route::get('slides', 'SlideController@index')->name('slides.index');
    Route::get('slides/create', 'SlideController@create')->name('slides.create');
    Route::post('slides', 'SlideController@store')->name('slides.store');
    Route::get('slides/delete/{id}', 'SlideController@destroy')->name('slides.destroy');
    Route::get('slides/edit/{id}', 'SlideController@edit')->name('slides.edit');
    Route::put('slides/{id}', 'SlideController@update')->name('slides.update');
    Route::get('slides/permenant/{id}', 'SlideController@forcedelete')->name('slides.forcedelete');
    Route::get('slides/restore/{id}', 'SlideController@restore')->name('slides.restore');


    Route::get('reviews/delete/{id}', 'ReviewController@destroy')->name('reviews.destroy');


    // Log
    Route::get('log', function(){
        return redirect()->route('admin.dashboard');
    });
    Route::get('log/admins', 'LogController@admins')->name('log.admins');
    Route::get('log/branches', 'LogController@branches')->name('log.branches');
    Route::get('log/distributors', 'LogController@distributors')->name('log.distributors');
    Route::get('log/users', 'LogController@users')->name('log.users');
    Route::post('log/admins', 'LogController@admins')->name('log.admins');
    Route::post('log/branches', 'LogController@branches')->name('log.branches');
    Route::post('log/distributors', 'LogController@distributors')->name('log.distributors');
    Route::post('log/users', 'LogController@users')->name('log.users');

    // Media
    Route::get('media/delete/{id}', 'MediaController@destroy')->name('media.destroy');

    // Transactions
    Route::get('transactions', 'TransactionController@index')->name('transactions.index');
    Route::get('transactions/search', 'TransactionController@search')->name('transactions.search');

});


