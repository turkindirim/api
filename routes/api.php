<?php

use Illuminate\Http\Request;


Route::post('login', 'Auth\ApiLoginController@login');
Route::post('register', 'UserController@store');

// Home
Route::get('/home', 'HomeController@homeApi');

// Sellers
Route::get('sellers', 'SellerController@index');
Route::get('sellers/{id}', 'SellerController@show');
// Distributors
Route::get('distributors', 'DistributorController@index');
Route::get('distributors/{id}', 'DistributorController@show');
// Branches
Route::get('branches', 'BranchController@index');
Route::get('branches/{id}', 'BranchController@show');
// Languages
Route::get('languages', 'LanguageController@index');
// Countries
Route::get('countries', 'CountryController@index');
// Provinces
Route::get('provinces', 'ProvinceController@index');
Route::get('provinces/{id}', 'ProvinceController@show');
// Cities
Route::get('cities', 'CityController@index');
Route::get('cities/{id}', 'CityController@show');
// Districts
Route::get('districts', 'DistrictController@index');
Route::get('districts/{id}', 'DistrictController@show');
// Types
Route::get('types', 'TypeController@index');
Route::get('types/{id}', 'TypeController@show');
// Categories
Route::get('categories', 'CategoryController@index');
Route::get('categories/{id}/{title?}', 'CategoryController@show');
// Newsfeed
Route::get('newsfeed', 'NewsfeedController@index');
Route::get('newsfeed/{id}/{title?}', 'NewsfeedController@show');
// Post
Route::get('posts', 'PostController@index');
Route::get('post/{id}', 'PostController@showApi');
// Offers
Route::get('offers', 'OfferController@index');
Route::get('offers/{id}', 'OfferController@show');



Route::group(['prefix' => 'admin', 'middleware' => 'auth:api-admin'], function() {
    // Controlling Sellers
    Route::post('sellers', 'SellerController@store');
    Route::delete('sellers/{id}', 'SellerController@destroy');
    Route::put('sellers/{id}', 'SellerController@update');
    // Controlling Distributors
    Route::post('distributors', 'DistributorController@store');
    Route::delete('distributors/{id}', 'DistributorController@destroy');
    Route::put('distributors/{id}', 'DistributorController@update');
    // Controlling Branches
    Route::post('branches', 'BranchController@store');
    Route::delete('branches/{id}', 'BranchController@destroy');
    Route::put('branches/{id}', 'BranchController@update');
    // Langauges
    Route::post('languages', 'LanguageController@store');
    Route::delete('languages/{id}', 'LanguageController@destroy');
    Route::put('languages/{id}', 'LanguageController@update');
    // Provinces
    Route::post('provinces', 'ProvinceController@store');
    Route::delete('provinces/{id}', 'ProvinceController@destroy');
    Route::put('provinces/{id}', 'ProvinceController@update');
    // Cities
    Route::post('cities', 'CityController@store');
    Route::delete('cities/{id}', 'CityController@destroy');
    Route::put('cities/{id}', 'CityController@update');
    // Districts
    Route::post('districts', 'DistrictController@store');
    Route::delete('districts/{id}', 'DistrictController@destroy');
    Route::put('districts/{id}', 'DistrictController@update');
    // Types
    Route::post('types', 'TypeController@store');
    Route::delete('types/{id}', 'TypeController@destroy');
    Route::put('types/{id}', 'TypeController@update');
    // Categories
    Route::post('categories', 'CategoryController@store');
    Route::delete('categories/{id}', 'CategoryController@destroy');
    Route::put('categories/{id}', 'CategoryController@update');
    // Newsfeed
    Route::post('newsfeed', 'NewsfeedController@store');
    Route::get('newsfeed/delete/{id}', 'NewsfeedController@destroy');
    Route::put('newsfeed/{id}', 'NewsfeedController@update');
    // Offers
    Route::post('offers', 'OfferController@store');
    Route::get('offers/delete/{id}', 'OfferController@destroy');
    Route::put('offers/{id}', 'OfferController@update');
    // Cards
    Route::get('card/{id}', 'CardController@show');
});
    

Route::group(['prefix' => 'branch', 'middleware' => 'auth:api-branch'], function() {
    // Offers
    Route::post('offers', 'OfferController@store');
    Route::delete('offers/{id}', 'OfferController@destroy');
    Route::put('offers/{id}', 'OfferController@update');
    // Cards
    Route::get('card/{id}', 'CardController@show');
    // Transactions
    Route::get('transactions', 'BranchController@transactions');
    Route::delete('transactions/{id}', 'TransactionController@destroy');
    // Sell (Make transaction)
    Route::post('sell', 'BranchController@sell');
    // Account
    Route::get('account', 'BranchController@account');
    Route::post('account', 'BranchController@api_update');
});


Route::group(['prefix' => 'distributor', 'middleware' => 'auth:api-distributor'], function() {
    // Cards
    Route::get('card/{id}', 'CardController@show');
    Route::get('cards', 'DistributorController@cards');
    Route::post('sell/{number}', 'DistributorController@sellCard');
    // Account
    Route::get('account', 'DistributorController@account');
    Route::post('account', 'DistributorController@api_update');
    
});

Route::group(['middleware' => 'auth:api'], function() {
    // Cards
    Route::get('cards', 'UserController@cards');
    Route::post('activate', 'UserController@activate');
    // Transactions
    Route::get('transactions', 'UserController@transactions');
    // Account
    Route::get('account', 'UserController@account');
    Route::post('account', 'UserController@api_update');
    Route::post('/account/password', 'User\AccountController@updatePasswordWithVerification');
    

    // Reviews
    Route::post('/review/post', 'ReviewController@reviewPost');
    Route::post('/review/offer', 'ReviewController@reviewOffer');
    Route::post('/review/branch', 'ReviewController@reviewBranch');

});
