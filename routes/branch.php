<?php

Route::get('/login', 'Auth\BranchLoginController@showLoginForm')->name('branch.login')->middleware('guest:branch');
Route::post('/login', 'Auth\BranchLoginController@login')->name('branch.login.submit')->middleware('guest:branch');

Route::post('password/email', 'Auth\BranchForgotPasswordController@sendResetLinkEmail')->name('branch.password.email');
Route::get('password/reset', 'Auth\BranchResetPasswordController@showLinkRequestForm')->name('branch.password.request');
Route::post('password/reset', 'Auth\BranchResetPasswordController@reset')->name('branch.password.update');
Route::get('password/reset/{token}', 'Auth\BranchResetPasswordController@showResetForm')->name('branch.password.reset');

Route::group(['middleware' => 'auth:branch'], function () {
    Route::get('/', 'BranchController@dashboard')->name('branch.dashboard');
    // Offers
    Route::post('offers', 'OfferController@store');
    Route::delete('offers/{id}', 'OfferController@destroy');
    Route::put('offers/{id}', 'OfferController@update');
    // Cards
    Route::get('card/{id}', 'CardController@show');
    // Transactions
    Route::get('transactions', 'BranchController@transactions');
    Route::delete('transactions/{id}', 'TransactionController@destroy');
    // Sell (Make transaction)
    Route::post('sell', 'BranchController@sell');
    // Account
    Route::get('account', 'BranchController@account');
    Route::post('account', 'BranchController@api_update');
});