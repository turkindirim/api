<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('route:clear');

    // return what you want
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('/companies', 'User\BranchController@index');
Route::get('/company/{id}/{title?}', 'BranchController@show');
Route::get('/distributors', 'User\DistributorController@index');
Route::get('/distributor/{id}/{title?}', 'DistributorController@show');
Route::get('/offers', 'OfferController@indexFront');
Route::get('/offers-map', 'OfferController@indexMap');
Route::get('/tags/posts/{id}/{title?}', 'PostController@showPostsByTag');
Route::get('/tags/offers/{id}/{title?}', 'OfferController@showOffersByTag');
Route::get('/category/posts/{id}/{title?}', 'PostController@showPostsBycategory');
Route::get('/category/{id}/{title?}', 'CategoryController@show');
Route::get('/offer/{id}/{title?}', 'OfferController@show');
Route::get('/feed/{id}/{title?}', 'NewsfeedController@show');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/about', 'PostController@aboutPage')->name('about');
Route::get('/privacy', 'PostController@privacyPage')->name('privacy');
Route::get('/terms', 'PostController@termsPage')->name('terms');
Route::get('/support', 'PostController@supportPage')->name('support');
Route::get('/cards', 'HomeController@cards')->name('cards');
Route::post('/contactus', 'HomeController@contactus')->name('contactus');
Route::post('/order', 'HomeController@order')->name('order');

Route::get('/language/{language}', 'LanguageController@switchLanguage');

Route::get('/app','DetectedController@app');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/account', 'User\AccountController@cards')->name('user.cards.index');
    Route::post('/account', 'User\AccountController@updateAccount')->name('updateAccount');
    Route::get('/account/edit', 'User\AccountController@account')->name('account');
    Route::post('/account/cards', 'UserController@activate')->name('user.cards.store');
    Route::post('/account/password', 'User\AccountController@updatePasswordWithVerification');
    Route::get('logout', 'Auth\LoginController@logout')->name('user.logout');
    Route::post('/review/post', 'ReviewController@reviewPost');
    Route::post('/review/offer', 'ReviewController@reviewOffer');
    Route::post('/review/branch', 'ReviewController@reviewBranch');
});
Route::group(['middleware' => 'guest'], function () {
    Route::post('register', 'User\AccountController@register')->name('user.register');
    Route::get('/redirect/facebook/', 'Auth\SocialAuthFacebookController@redirectFacebook');
    Route::get('/callback/facebook/', 'Auth\SocialAuthFacebookController@callbackFacebook');
    Route::get('/redirect/twitter/', 'Auth\SocialAuthFacebookController@redirectTwitter');
    Route::get('/callback/twitter/', 'Auth\SocialAuthFacebookController@callbackTwitter');
});
Route::get('/posts', 'PostController@indexFront')->name('posts');
Route::get('{title}', 'PostController@show')->name('post.show');