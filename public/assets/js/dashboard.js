$(document).ready(function() {
    $('.maxFileSize').change(function() {
        var max;
        max = $(this).attr('max-size');
        if (!max) {
            max = 2048;
        }
        if (((this.files[0].size) / 1024) > max) {
            alert('No more than ' + max + ' KB please');
            $(this).val('');
        }        
    });
    $('body').on('blur', '.sort_field', function (e) {
        var model = $(this).data('model');
        var id = $(this).data('id');
        var sort = $(this).val();
        $(this).parents('td').find('.real_sort').html(sort);
        fetch('/setsort/' + model, {
            method: 'POST',
            headers: {
                'Accept-Language': AcceptLanguage,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: JSON.stringify({
                id: id,
                sort: sort
            })
        });
    });
    if (document.getElementById('setDistributorForm')) {
        $('#setDistributorForm').submit(e => {
            e.preventDefault();
            var cardNumber = $('#cardNumber').val();
            if (cardNumber.length != 16) {
                UIkit.notify({
                    message: InvalidCardNumber,
                    pos: 'bottom-center',
                    status: 'danger'
                });
                return;
            }
            fetch('/cards/' + cardNumber, {
                headers: {
                    'Accept-Language': AcceptLanguage,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then(response => {
                response.text().then(text => {
                    text = JSON.parse(text);
                    if (text.data != null) {
                        if (text.data.distributor != null || text.data.user != null) {
                            UIkit.notify({
                                message: cardAlreadyUsedOrSetToDistributor,
                                pos: 'bottom-center',
                                status: 'warning'
                            });
                            return;
                        }
                        $('#cards_list').prepend('<tr><input type="hidden" name="cards[]" value="'+text.data.id+'"><td>'+text.data.id+'</td><td>'+text.data.number+'</td><td>'+text.data.created_at_human+'</td><td><button type="button" class="md-btn md-btn-danger delete_parent_tr"><i class="uk-icon-close"></i></button></td></tr>');
                        $('#cardNumber').val('');
                    } else {
                        UIkit.notify({
                            message: text.meta.messages[0],
                            pos: 'bottom-center',
                            status: 'danger'
                        });
                    }
                });
            });
        });
    }
    
    $('.replace_image').each(function(i, e) {
        $(e).attr('src', $(e).data('src'));
    });
    
    $('body').on('click', '.delete_parent_tr', function(){
        $(this).parents('tr').remove();
    });
    $('body').on('keyup', '.userSelector', function(){
        var field = $(this);
        var hiddenField = $(field).data('id');
        var userType = $(field).data('user');
        var field_value = $(field).val();
        if (field_value.length <= 1) {
            $('#'+hiddenField).val('');
        }
        if ($(field).val().length > 1) {
            fetch('/get'+userType.charAt(0).toUpperCase() + userType.slice(1)+'?'+userType+'=' + field.val(), {
                headers: {
                    'Accept-Language': AcceptLanguage,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then((response) => {
                response.text().then((text) => {
                    text = JSON.parse(text);
                    var autoCompleteElement = $(field).parents('.autocomplete-wrapper').find('.uk-dropdown');
                    var ulElement = autoCompleteElement.find('ul');
                    
                    if (text.meta.error == false) {
                        $(ulElement).html('');
                        $('#'+hiddenField).val('');
                        text.data.forEach(user => {
                            var prefix = user.prefix ? user.prefix + ' - ' : '', line1, line2, logo;
                            switch (userType) {
                                case 'distributor':
                                case 'branch':
                                case 'user':
                                    line1 = prefix + user.name;
                                    line2 = user.email;
                                    logo = user.logo;
                                    break;
                                case 'offer':
                                    line1 = user.name;
                                    line2 = user.valid_from + ' | ' + user.valid_to;
                                    logo = user.media.data[0].url;
                                    break;
                            }

                            $(ulElement).append(
                            '<li data-id="'+user.id+'" class="md-list-li-item">'+
                                '<div class="md-list-addon-element">'+
                                    '<img class="md-user-image md-list-addon-avatar" src="'+logo+'">'+
                                '</div>'+
                                '<div class="md-list-content">'+
                                    '<span class="md-list-heading">'+line1+'</span>'+
                                    '<span class="uk-text-small uk-text-muted">'+line2+'</span>'+
                                '</div>'+
                            '</li>');
                        });
                        $(autoCompleteElement).css({
                            'transform':'scale(1)',
                            'opacity': 1,
                            'display':'block'
                        });

                        $(ulElement).find('li').click(function(){
                            var name = $(this).find('span.md-list-heading').html();
                            var id = $(this).data('id');
                            $('#'+hiddenField).val(id);
                            $(field).val(name);
                            $(autoCompleteElement).css({
                                'transform':'scale(0.25)',
                                'opacity': 0,
                                'display':'none'
                            });
                        });
                    } else {
                        UIkit.notify({
                            message: text.meta.messages[0],
                            pos: 'bottom-center',
                            status: 'warning'
                        });
                        $(autoCompleteElement).css({
                            'transform':'scale(0.25)',
                            'opacity': 0,
                            'display':'none'
                        });
                    }
                });
            });
        }
    });

   
    var bulkSubmit = $('.bulk-submit').get();
    if (bulkSubmit.length) {
        bulkSubmit.forEach(form => {
            $(form).submit((e) => {
                var ids = $(form).find('.ids');
                if ($(ids).val().length) {
                    return true;
                }
                e.preventDefault();
                var rows = $('#' + $(form).data('table')).find('tbody').find('tr').get();
                var checkedRows = [];
                rows.forEach((row) => {
                    var theCheckBox = $(row).find('td:first').find('input[type=checkbox]');
                    if ($(theCheckBox).is(":checked")) {
                        checkedRows.push($(theCheckBox).data('id'));
                    }
                });
                if (checkedRows.length) {
                    $(ids).val(checkedRows.join());
                    $(form).submit();
                } else {
                    UIkit.notify({
                        message: NoRowSelected,
                        pos: 'bottom-center',
                        status: 'danger',
                        timeout: 2000
                    });
                }
                
            })
        })
    }
    var zoomLevel, mapCenter;
    if (typeof showMapMarker != 'undefined') {
        zoomLevel = 12;
        mapCenter = {lat: branchLat, lng: branchLng}
    } else {
        zoomLevel = 6;
        mapCenter = {lat:39.11050558848269, lng:35.46138046102419}
    }
    var map = document.getElementById('map');
    if (map) {
        var map = new google.maps.Map(document.getElementById('map'), {
            center:mapCenter,
            zoom: zoomLevel
        });
        if (typeof showMapMarker != 'undefined') {
            var marker = new google.maps.Marker({
                position: {lat: branchLat, lng: branchLng},
                map: map,
                title: branchName
            });
        }
        google.maps.event.addListener(map, "click", function (e) {
            var latLng = e.latLng;
            marker.setPosition(latLng);
            $('#latitude').val(latLng.lat().toString()).focus();
            $('#longitude').val(latLng.lng().toString()).focus();
        });
     }
     $('.lowercase').keyup(function(){
         $(this).val($(this).val().toLowerCase());
     });
});
function getOfferMedia(id)
{
    $('#media_modal').html('');
    fetch('/getOfferMedia?offer_id=' + id, {
        headers: {
            'Accept-Language': AcceptLanguage,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then(reponse => {
        reponse.text().then(text => {
            if (text.length) {
                var media = JSON.parse(text),
                images = '<div class="uk-grid uk-grid-divider">',
                videos = '<div class="uk-grid uk-grid-divider">';
                media.forEach(item => {
                    switch (item.extension) {
                        case 'jpg':
                        case 'png':
                        case 'gif':
                        case 'bmp':
                            images += '<div class="uk-width-medium-1-4"><img src="'+uploadURL+'/'+item.name+'" style="max-width:100%"></div>';
                            break;
                        case 'mp4':
                            videos += '';
                            break;
                    }
                });
                images += '</div>';
                videos += '</div>';
                $('#media_modal').html('<div class="md-card"><div class="md-card-content">' + images + videos + '</div></div>');
            }
        });
    });
}
function mediaDestroy(url, item)
{
    fetch(url, {
        headers: {
            'Accept-Language': AcceptLanguage,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then(response => {
        response.text().then(text => {
            var result = JSON.parse(text);
            if (result.meta.error == false) {
                $('#' + item).remove();
                UIkit.notify({
                    message: Success,
                    pos: 'bottom-center',
                    status: 'success',
                    timeout: 2000
                });
            } else {
                UIkit.notify({
                    message: result.meta.messages[0],
                    pos: 'bottom-center',
                    status: 'danger',
                    timeout: 2000
                });
            }
        });
    });
}
function deleteOfferBranches(offer)
{    
    var rows = $('.toDeleteBranches').find('tr').get();
    var checkedRows = [];
    rows.forEach((row) => {
        if ($(row).find('td:first').find('input[type=checkbox]').is(":checked")) {
            checkedRows.push($(row).data('id'));
        }
    });
    if (checkedRows.length) {
        var ids = checkedRows.join();
        fetch('/deleteOfferBranches?offer='+offer+'&ids=' + ids, {
            headers: {
                'Accept-Language': AcceptLanguage,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(response => {
            response.text().then(text => {
                var result = JSON.parse(text);
                if (result.meta.error == false) {
                    UIkit.notify({
                        message: Success,
                        pos: 'bottom-center',
                        status: 'success',
                        timeout: 2000
                    });
                    checkedRows.forEach(row => {
                        $('#row' + row).remove();
                    });
                } else {
                    UIkit.notify({
                        message: result.meta.messages[0],
                        pos: 'bottom-center',
                        status: 'danger',
                        timeout: 2000
                    });
                }
            });
        });
    } else {
        UIkit.notify({
            message: NoRowSelected,
            pos: 'bottom-center',
            status: 'danger',
            timeout: 2000
        });
    }
}
function countryChanged(country_id, city = false, district = false)
{
    var provinceSelector = document.getElementById('province'), provinces;
    $(provinceSelector).html('');
    if (city) {
        citySelector = document.getElementById('city');
        $(citySelector).html('');
    }
    if (district) {
        districtSelector = document.getElementById('district');
        $(districtSelector).html('');
    }
        

    fetch(API + '/provinces?country_id=' + country_id, {
        headers: {
            'Accept-Language': AcceptLanguage,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then((response) => {
        response.text().then((text) => {
            if (text.length) {
                $(provinceSelector).html('<option value="">'+selectProvince+'</option>');
                provinces = JSON.parse(text);
                provinces['data'].forEach(city => {
                    $(provinceSelector).append('<option value="'+city.id+'">'+city.name+'</option>');
                });
                if (provinces['data'].length == 1 && city) {
                    provinceChanged(provinces['data'][0].id);
                }
            }
        }).catch((error) => {
            console.log(error)
        });
    }).catch((error) => {
        console.log(error)
    });
}
function provinceChanged(province_id)
{
    var citySelector = document.getElementById('city'),
        districtSelector = document.getElementById('district'),
        cities;
    $(citySelector).html('');
    $(districtSelector).html('');

    fetch(API + '/cities?province_id=' + province_id, {
        headers: {
            'Accept-Language': AcceptLanguage,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then((response) => {
        response.text().then((text) => {
            if (text.length) {
                $(citySelector).html('<option value="">'+selectCity+'</option>');
                cities = JSON.parse(text);
                cities['data'].forEach(city => {
                    $(citySelector).append('<option value="'+city.id+'">'+city.name+'</option>');
                });
                if (cities['data'].length == 1) {
                    cityChanged(cities['data'][0].id);
                }
            }
        }).catch((error) => {
            console.log(error)
        });
    }).catch((error) => {
        console.log(error)
    });
}
function cityChanged(city_id)
{
    var districtSelector = document.getElementById('district'),
        districts;
    $(districtSelector).html('');

    fetch(API + '/districts?city_id=' + city_id, {
        headers: {
            'Accept-Language': AcceptLanguage,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then((response) => {
        response.text().then((text) => {
            if (text.length) {
                $(districtSelector).html('<option value="">'+selectDistrict+'</option>');
                districts = JSON.parse(text);
                districts['data'].forEach(district => {
                    $(districtSelector).append('<option value="'+district.id+'">'+district.name+'</option>');
                });
            }
        }).catch((error) => {
            console.log(error)
        });
    }).catch((error) => {
        console.log(error)
    });
}