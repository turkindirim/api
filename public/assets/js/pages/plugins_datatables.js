$(function() {
    // datatables
    altair_datatables.dt_default();
    altair_datatables.dt_scroll();
    altair_datatables.dt_individual_search();
    altair_datatables.dt_colVis();
    altair_datatables.dt_tableExport();
    altair_datatables.dt_tableAjax();
});

altair_datatables = {
    dt_default: function() {
        var $dt_default = $('#dt_default');
        if($dt_default.length) {
            $dt_default.DataTable({pageLength:50});
        }
    },
    dt_scroll: function() {
        var $dt_scroll = $('#dt_scroll');
        if($dt_scroll.length) {
            $dt_scroll.DataTable({
                "scrollY": "200px",
                "scrollCollapse": false,
                "paging": false
            });
        }
    },
    dt_individual_search: function() {
        var $dt_individual_search = $('#dt_individual_search');
        if($dt_individual_search.length) {

            // Setup - add a text input to each footer cell
            $dt_individual_search.find('tfoot th').each( function() {
                var title = $dt_individual_search.find('tfoot th').eq( $(this).index() ).text();
                $(this).html('<input type="text" class="md-input" placeholder="' + title + '" />');
            } );

            // reinitialize md inputs
            altair_md.inputs();

            // DataTable
            var individual_search_table = $dt_individual_search.DataTable();

            // Apply the search
            individual_search_table.columns().every(function() {
                var that = this;

                $('input', this.footer()).on('keyup change', function() {
                    that
                        .search( this.value )
                        .draw();
                } );
            });

        }
    },
    dt_colVis: function() {
        var $dt_colVis = $('#dt_colVis'),
            $dt_buttons = $dt_colVis.prev('.dt_colVis_buttons');

        if($dt_colVis.length) {

            // init datatables
            var colVis_table = $dt_colVis.DataTable({
                buttons: [
                    {
                        extend: 'colvis',
                        fade: 0
                    }
                ]
            });

            colVis_table.buttons().container().appendTo( $dt_buttons );

        }
    },
    dt_tableExport: function() {
        var $dt_tableExport = $('#dt_tableExport'),
            $dt_buttons = $dt_tableExport.prev('.dt_colVis_buttons');

        if($dt_tableExport.length) {
            var table_export = $dt_tableExport.DataTable({
                scrollX: true,
                buttons: [
                    {
                        extend:    'copyHtml5',
                        text:      '<i class="uk-icon-files-o"></i> Copy',
                        titleAttr: 'Copy'
                    },
                    {
                        extend:    'print',
                        text:      '<i class="uk-icon-print"></i> Print',
                        titleAttr: 'Print'
                    },
                    {
                        extend:    'excelHtml5',
                        text:      '<i class="uk-icon-file-excel-o"></i> XLSX',
                        titleAttr: '',
                        customize: function( xlsx ) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            $('row c[r^="B"]', sheet).attr( 't', 's' );
                        }
                    },
                    {
                        extend:    'csvHtml5',
                        text:      '<i class="uk-icon-file-text-o"></i> CSV',
                        titleAttr: 'CSV'
                    },
                    {
                        extend: 'colvis',
                        fade: 1
                    }
                ]
                
            });

            table_export.buttons().container().appendTo( $dt_buttons );

        }
    },
    dt_tableAjax: function ()
    {
        var $dt_tableAjax = $('#dt_tableAjax'),
            $dt_buttons = $dt_tableAjax.prev('.dt_colVis_buttons');
        
        if($dt_tableAjax.length) {
            var tableAjax = $dt_tableAjax
                .on('draw.dt', function ( e, settings, json, xhr ) {
                    window.setTimeout(() => {
                        altair_md.init();
                        $table_check = $('.table_check');
                        $table_check.each(function() {
                            console.log('a');
                            var $this = $(this),
                                $checkAll = $this.find('.check_all'),
                                $checkRow = $('.check_row');

                            $checkAll
                                .on('ifChecked',function() {
                                    $checkRow.iCheck('check');
                                })
                                .on('ifUnchecked',function() {
                                    $checkRow.iCheck('uncheck');
                                });

                            $checkRow
                                .on('ifChecked',function(event) {
                                    $(event.currentTarget).closest('tr').addClass('row_checked');
                                })
                                .on('ifUnchecked',function(event) {
                                    $(event.currentTarget).closest('tr').removeClass('row_checked');
                                })
                        });
                    }, 1);
                }).DataTable({
                    "scrollX": true,
                "lengthMenu": [[10, 25, 50, 100, 250, 500, 1000, 5000, -1], [10, 25, 50, 100, 250, 500, 1000, 5000, "All"]],
                'processing': true,
                'serverSide': true,
                'ajax': {
                    'url' : DT_Ajax_URL,
                    'data' : function ( d ) {
                        return {...d, ...extra_data()};
                    }
                },
                "dom": 'lBfrtip',
                'columns': DT_Cols_Arr,
                'buttons': [
                    {
                        'extend':    'copyHtml5',
                        'text':      '<i class="uk-icon-files-o"></i> Copy',
                        'titleAttr': 'Copy'
                    },
                    {
                        'extend':    'print',
                        'text':      '<i class="uk-icon-print"></i> Print',
                        'titleAttr': 'Print'
                    },
                    {
                        'extend':    'excelHtml5',
                        'text':      '<i class="uk-icon-file-excel-o"></i> XLSX',
                        'titleAttr': '',
                        customize: function( xlsx ) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            $('row c[r^="C"]', sheet).attr( 't', 's' );
                        }
                    },
                    {
                        'extend':    'csvHtml5',
                        'text':      '<i class="uk-icon-file-text-o"></i> CSV',
                        'titleAttr': 'CSV'
                    },
                    {
                        'extend': 'colvis',
                        'fade': 1
                    }
                ],
                "drawCallback": function( settings ) {
                    $('.updateAjaxTable').removeClass('disabled');

                }
                
            });

            tableAjax.buttons().container().appendTo( $dt_buttons );

            $('.updateAjaxTable').click(function () {
                $(this).addClass('disabled');
                tableAjax.draw();
            });

        }
    }
};