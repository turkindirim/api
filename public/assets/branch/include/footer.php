<footer class="main-footer dark-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="footer-widget fl-wrap">
                                <h3>Contact details</h3>
                                <div class="footer-contacts-widget fl-wrap">
                                    <!--<p> </p>-->
                                    <ul  class="footer-contacts ">
                                        <li><span><i class="fa fa-envelope-o"></i> Mail :</span><a href="mailto:info@turkindirim.com.tr" target="_blank">info@turkindirim.com.tr</a></li>
                                        <li><span><i class="fa fa-phone"></i> Phone :</span><a href="tel:00908502410444">+90 850 241 04 44</a></li>
                                        <li> <span><i class="fa fa-map-marker"></i> Adress :</span><a href="#" target="_blank">19Mayis Mah Gazi Berkay sok - No:6/3 şişle/istanbul</a></li>
                                    </ul>
                                </div>
                                <div class="footer-social">
                                    <ul>
                                        <li><a href="https://www.facebook.com/turkindirimtr" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/turkindirimtr" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="https://www.instagram.com/turkindirimtr/" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank" ><i class="fa fa-youtube"></i></a></li>
                                        <li><a href="#" target="_blank" ><i class="fa fa-snapchat"></i></a></li>
                                        <li><a href="#" target="_blank" ><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                                <div class="footer-widget fl-wrap">
                                    <h3>Quick Links</h3>
                                    <div class="widget-posts fl-wrap">
                                        <ul class="links-menu">
                                            <li class="link-item"><a href="index.php" >Home </a></li>
                                            <li class="link-item"><a href="#">Blog </a></li>
                                            <li class="link-item"><a href="#">F A Q</a></li> 
                                            <li class="link-item"><a href="#">Privcy Policy</a></li>
                                            <li class="link-item"><a href="#">Terms & Conditions</a></li> 
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <div class="col-md-3">
                            <div class="footer-widget fl-wrap">
                                <h3>Our Cards</h3>
                                    <ul class="footer-cards">
                                        
                                        <li class="clearfix">
                                            <a href="#"  class="widget-cards-img"><img src="images/cards/turk.jpg" class="respimg" alt=""></a>
                                            <div class="widget-cards-descr">
                                                <a href="#" title=""> Turk Kart</a>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <a href="#"  class="widget-cards-img"><img src="images/cards/tatil.jpg" class="respimg" alt=""></a>
                                            <div class="widget-cards-descr">
                                                <a href="#" title="">Tatil Kart</a>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <a href="#"  class="widget-cards-img"><img src="images/cards/ozel.jpg" class="respimg" alt=""></a>
                                            <div class="widget-cards-descr">
                                                <a href="#" title="">Ozel Kart</a>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <a href="#"  class="widget-cards-img"><img src="images/cards/ogrenci.jpg" class="respimg" alt=""></a>
                                            <div class="widget-cards-descr">
                                                <a href="#" title="">Ogrenci Kart</a>
                                            </div>
                                        </li>
                                    </ul>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="footer-widget fl-wrap">
                            <h3>Download Our App</h3>
                                <div class="subscribe-widget fl-wrap">
                                    <p>Download our application and stay up to date about the offers</p>
                                    <a href="#"  class="widget-download-img"><img src="images/google-store.png" class="respimg" alt=""></a>
                                    <a href="#"  class="widget-download-img"><img src="images/apple-store.png" class="respimg" alt=""></a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sub-footer fl-wrap">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                        
                                </div>
                                <div class="col-md-6">
                                        <div class="copyright"> Türk indirim © 2010 - <script>document.write(new Date().getFullYear());</script>| All Rights Reserved | Powered by <a href="nexoajans.com" target="_blank">Nexo Ajans</a>	</div>
                                </div>
                        </div>
                    </div>
                </div>
            </footer>