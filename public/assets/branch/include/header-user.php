            <header class="main-header dark-header fs-header sticky">
                <div class="header-inner">
                    <div class="logo-holder">
                        <a href="index.php"><img src="images/logo.png" alt=""></a>
                    </div>
                    <div class="header-search vis-header-search">
                        <div class="header-search-input-item">
                            <input type="text" placeholder="Search" value=""/>
                        </div>
                        <div class="header-search-select-item">
                            <select data-placeholder="All Categories" class="chosen-select" >
                                <option>Categories</option>
                                <option>Shops</option>
                                <option>Hotels</option>
                                <option>Restaurants</option>
                                <option>Fitness</option>
                                <option>Events</option>
                            </select>
                        </div>
                        <div class="header-search-select-item mobile-hide">
                            <select data-placeholder="All Categories" class="chosen-select" >
                                <option>Locations</option>
                                <option>Istanbul</option>
                                <option>Antalya</option>
                                <option>Bursa</option>
                                <option>Mirsen</option>
                                <option>trabzon</option>
                            </select>
                        </div>
                        <button class="header-search-button" onclick="window.location.href='offers.php'"><i class="fa fa-search"></i></button>
                    </div>
                    <div class="show-search-button"><i class="fa fa-search"></i> <span>Search</span></div>
                    <a href="add-offer.php" class="add-list">Add Offer <span><i class="fa fa-plus"></i></span></a>
                    <div class="header-user-menu">
                        <div class="header-user-name">
                            <span><img src="images/avatar/clients(4).jpg" alt=""></span>
                            Hello , Osama
                        </div>
                        <ul>
                            <li><a href="dashboard-myprofile.php"> Edit profile</a></li>
                            <li><a href="dashboard-myprofile.php"> Add Offer</a></li>
                            <li><a href="index.php">Log Out</a></li>
                        </ul>
                    </div>
                    <!-- nav-button-wrap--> 
                    <div class="nav-button-wrap color-bg">
                        <div class="nav-button">
                            <span></span><span></span><span></span>
                        </div>
                    </div>
                    <!-- nav-button-wrap end-->
                    <!--  navigation --> 
                    <div class="nav-holder main-menu">
                        <nav>
                            <ul>
                                <li><a href="index.php" >Home </a></li>
                                <li><a href="#">Offers </a></li>
                                <li><a href="#">Cards</a></li> 
                                <li><a href="#">About</a></li>
                                <li><a href="#">Contact</a></li> 
                                <li><a href="#">ع</a></li> 
                            <ul>    
                        </nav>
                    </div>
                    <!-- navigation  end -->
                </div>
            </header>