(function($){
    $('.changePassword').click(function(){
        var pwdChanged = false;
        Swal.fire({
            confirmButtonText:ChangePassword,
            title: ChangePassword,
            confirmButtonClass:'btn big-btn color-bg flat-btn',
            buttonsStyling: false,
            html:
              '<input id="old_password" type="password" placeholder="'+OldPassword+'" class="swal2-input">' +
              '<input id="newpassword" type="password" placeholder="'+NewPassword+'" class="swal2-input">' +
              '<input id="newpassword_confirmation" type="password" placeholder="'+NewPasswordConfirmation+'" class="swal2-input">',
            focusConfirm:false,
            preConfirm: () => {
                return fetch('/account/password', {
                    method: 'POST',
                    body: JSON.stringify({
                        oldPassword: document.getElementById('old_password').value,
                        password: document.getElementById('newpassword').value,
                        password_confirmation: document.getElementById('newpassword_confirmation').value,
                    }),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Accept':'application/json',
                        'Content-Type':'application/json'
                    },
                })
                .then(response => {
                    return response.json().then(json => {
                        if (!response.ok) {
                            throw new Error(json.meta.messages[0]);
                        } else {
                            if (json.data.id != null) {
                                pwdChanged = true;
                            }
                        }
                    })
                })
                .catch((error) => {
                    Swal.showValidationMessage(
                    `${error}`
                    )
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then(r => {
            if (pwdChanged) {
                Swal.fire({
                    icon: "success",
                    type: "success",
                    html: PasswordChanged,
                    confirmButtonClass:'btn big-btn color-bg flat-btn',
                });
            }
        });
    });



    $('#addNewCard').click(function(e){
        e.preventDefault();
        var succ = false;
        $('#addNewCard').prop('disabled', 'disabled').find('span').html(PleaseWait);
        var mob = $("#new_card_mobile").intlTelInput("getNumber");
        if (mob.length <= 4) {
            mob = '';
        }

        $.ajax({
            url: '/account/cards',
            method: 'POST',
            data: JSON.stringify({
                number: document.getElementById('new_card_number').value,
                activation_code: document.getElementById('new_Card_activation_code').value,
                mobile: mob
            }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Accept':'application/json',
                'Content-Type':'application/json'
            }
        }).complete(response => {
            var json = response.responseJSON;
            $('#addNewCard').prop('disabled', false).find('span').html(AddCard);
            $('#errors').html(errors).css('visibility', 'hidden');
            console.log(response);
            if (response.status != 200) {
                var errors = '';
                for (var er = 0; er < json.meta.messages.length; er++) {
                    errors += '<li>'+json.meta.messages[er]+'</li>';
                }
                $('#errors').html(errors).css('visibility', 'visible');
                
            } else {
                if (json.data.id != null) {
                    var card_info = '';
                    card_info += '<img src="'+json.data.logo+'" style="max-width: 96%;margin:10px 2%">';
                    $('#card_image').html(card_info);
                    $('#card_titla').html(json.data.type.name);
                    $('#valid_days').html(json.data.period);
                    $('#add_card_1').slideUp('fast', '', function() {
                        $('#add_card_2').fadeIn('fast', function () {
                            window.setTimeout(function() {
                                $('#add_card_2').slideUp('fast', function () {
                                    $('#add_card_3').fadeIn('fast');
                                    window.setTimeout(function() {
                                        $('#add_card_3').slideUp('fast', function() {
                                            $('#add_card_1').fadeIn('fast');
                                            $('#new_card_number').val('');
                                            $('#new_Card_activation_code').val('');
                                            $('#new_card_mobile').val('');
                                            $('.activateCardModal').fadeOut();
                                            window.location.href = "/account";
                                        });
                                    }, 3000);
                                });
                            }, 2000);
                        });
                    });
                    
                }
            }
            
        });


        /*
        Swal.fire({
            title: CardInfo,
            confirmButtonClass:'btn big-btn color-bg flat-btn',
            buttonsStyling: false,
            html:
              '<input id="new_card_number"" maxlength="16" placeholder="'+CardNumber+'" class="swal2-input">' +
              '<input id="new_Card_activation_code"" maxlength="5" placeholder="'+ActivationCode+'" class="swal2-input">',
            focusConfirm: false,
            preConfirm: () => {
                return 
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then(r => {
            if (succ) {
                Swal.fire({
                    icon: "success",
                    type: "success",
                    html: CardActivated,
                    confirmButtonClass:'btn big-btn color-bg flat-btn',
                });
            }
        });*/
    });
    $('.activateCard').click(function(e){
        e.preventDefault();
        $('.activateCardModal').fadeIn();
        $('.phone').intlTelInput({
            autoPlaceholder:'off',
            initialCountry: "auto",
            geoIpLookup: function(success, failure) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                success(countryCode);
                });
            },
            utilsScript:'/front/js/utils.js',
            formatOnDisplay:true,
            nationalMode:true,
            separateDialCode:true,
            preferredCountries:[],
            hiddenInput:"mobile"
        });
        $("html, body").addClass("hid-body");
    });
    $('.missingDataError').click(function(){
        Swal.fire({
            icon: "error",
            type: "error",
            html: ProfileInfoIncomplete
        });
    });

    $( ".jqdatepicker" ).datepicker({
        format:'yyyy-mm-dd'
    });
    $('.mobile').intlTelInput({
        autoPlaceholder:'off',
        geoIpLookup:function(success, failure) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              success(countryCode);
            });
        },
        utilsScript:'/front/js/utils.js',
        formatOnDisplay:true,
        nationalMode:true,
        separateDialCode:true,
        preferredCountries:[],
        hiddenInput:"mobile"
    });
	$.fn.serializeObject = function () {
		"use strict";
		var result = {};
		var extend = function (i, element) {
			var node = result[element.name];
			if ('undefined' !== typeof node && node !== null) {
				if ($.isArray(node)) {
					node.push(element.value);
				} else {
					result[element.name] = [node, element.value];
				}
			} else {
				result[element.name] = element.value;
			}
		};
		$.each(this.serializeArray(), extend);
		return result;
	};
})(jQuery);

$(document).ready(() => {
    $('#contactusform').submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: '/contactus',
            type: 'POST',
            data: $('#contactusform').serializeObject(),
            success: function (data) {
                Swal.fire({
                    icon: "success",
                    type: "success",
                    html: ''
                });
                $('#contactusform').trigger('reset');
            },
            error: function(xhr,status,error) {
                console.log(xhr, status, error);
                Swal.fire({
                    icon: "error",
                    type: "error",
                    html: xhr.responseJSON.meta.messages[0]
                });
            }
        })
    });
    if (document.getElementById('error-message')) {
        Swal.fire({
            icon: "error",
            type: "error",
            html: $('#error-message').html()
        });
    }
    if (document.getElementById('error-success')) {
        Swal.fire({
            icon: "success",
            type: "success",
            html: $('#success-message').html()
        });
    }

    var formIsValid = false;
    $('#main-register-form2').submit(function(e) {
        if (!formIsValid) {
            e.preventDefault();
            var firstname = String($('#firstname').val()),
            lastname = String($('#lastname').val()),
            email = $('#email').val(),
            password = $('#password').val(),
            password_confirmation = $('#password_confirmation').val();
            
            var fnValid = validator(firstname, {type: 'minLength', minLength:2});
            var lnValid = validator(lastname, {type: 'minLength', minLength:2});
            var emailValid = validator(email, {type: 'email'});
            var pwdValid = validator(password, {type: 'minLength', minLength:6});
            var pwd2Valid = validator(password_confirmation, {type: 'equalto', equalTo:password});
            pwd2Valid = (pwd2Valid && validator(password_confirmation, {type: 'minLength', minLength:6}));

            if (!fnValid) { $('#firstname').addClass('is-invalid') } else { $('#firstname').removeClass('is-invalid') }
            if (!lnValid) { $('#lastname').addClass('is-invalid') } else { $('#lastname').removeClass('is-invalid') }
            if (!emailValid) { $('#email').addClass('is-invalid') } else { $('#email').removeClass('is-invalid') }
            if (!pwdValid) { $('#password').addClass('is-invalid') } else { $('#password').removeClass('is-invalid') }
            if (!pwd2Valid) { $('#password_confirmation').addClass('is-invalid') } else { $('#password_confirmation').removeClass('is-invalid') }

            if (fnValid && lnValid && emailValid && pwdValid && pwd2Valid) {
                formIsValid = true;
                $('#main-register-form2').submit();
            }
        }
    });
});

function validator(string, params)
{
    switch (params.type) {
        case 'email':
            return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(string);
        case 'minLength':
            return string.length >= params.minLength;
        case 'equalto':
            return string == params.equalTo
    }
    return false;
}