<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardTypeTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_type_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('card_type_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name');
            $table->mediumText('description')->nullable();
            $table->unique(['card_type_id','locale']);
            $table->engine = 'InnoDB';
            $table->foreign('card_type_id')->references('id')->on('card_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_type_translations');
    }
}
