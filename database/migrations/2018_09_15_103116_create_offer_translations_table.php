<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offer_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name');
            $table->mediumText('description')->nullable();
            $table->text('excerpt')->nullable();
            $table->string('thumb', 36)->nullable();
            $table->string('background', 36)->nullable();
            $table->string('header', 36)->nullable();
            $table->unique(['offer_id','locale']);
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_translations');
    }
}
