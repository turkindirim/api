<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsfeedTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsfeed_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('newsfeed_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name');
            $table->mediumText('description')->nullable();
            $table->string('logo', 29)->nullable()->default(null);
            $table->unique(['newsfeed_id','locale']);
            $table->engine = 'InnoDB';
            $table->foreign('newsfeed_id')->references('id')->on('newsfeeds')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsfeed_translations');
    }
}
