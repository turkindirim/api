<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone', 15)->unique();
            $table->string('mobile', 15)->unique();
            $table->string('password');
            $table->rememberToken();
            $table->string('default_language', 2);
            $table->string('logo', 40)->nullable();
            $table->string('website', 60)->nullable();
            $table->mediumInteger('province_id')->unsigned();
            $table->unsignedSmallInteger('city_id');
            $table->unsignedInteger('district_id');
            $table->string('address1', 255);
            $table->string('address2', 255)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
            $table->decimal('latitude', 10, 8)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->engine = 'InnoDB';
            
            Schema::table('addresses', function($table) {
                $table->foreign('province_id')->references('id')->on('provinces')->onDelete('cascade');
                $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
                $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributors');
    }
}
