<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('type_id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('distributor_id')->unsigned()->nullable();
            $table->string('number', 16)->unique();
            $table->string('flag', 32);
            $table->string('activation_code', 5);
            $table->unsignedSmallInteger('period');
            $table->timestamp('sold_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('used_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';

        });

        Schema::table('cards', function ($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('distributor_id')->references('id')->on('distributors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
