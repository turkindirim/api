<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 32);
            $table->string('lastname', 32);
            $table->string('name', 64);
            $table->string('logo', 29);
            $table->tinyInteger('nationality')->unsigned();
            $table->string('passport', 29)->nullable();
            $table->string('passport_number', 32)->nullable();
            $table->string('mobile', 15)->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->tinyInteger('country_id');
            $table->mediumInteger('province_id');
            $table->string('address1', 255);
            $table->string('address2', 255)->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('default_language', 2);
            $table->timestamps();
            $table->softDeletes();
            
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
