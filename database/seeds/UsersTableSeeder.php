<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 1; $i++) { 
            DB::table('users')->insert([
                "firstname" => "Feras",
                "lastname" => "Jobeir",
                "nationality" => "2",
                "mobile" => "00905384119984",
                "country_id" => "1",
                "province_id" => "1",
                "address1" => "Address One goes here",
                "email" => "fjobeir@yahoo.com",
                "password" => '$2y$10$L0xtZA80msAJRE8rQ9qCxOUZY6uHdjIMrlGrDbKgNUKSVaIC7IwT.',
                "name" => "Feras Jobeir",
                "logo" => null
            ]);
        }
        
    }
}
