@extends('/frontend/layout')

@section('content')

<div id="wrapper">  
    <div class="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class=" col-md-offset-3 col-md-6 col-xs-12">
                    <div class="main-register fl-wrap" style="margin:30px auto">
                        <div id="tabs-container">
                            <div class="custom-form">
                                @if (session('resent'))
                                    <div class="alert alert-success" role="alert">
                                        {{ __('front.A verification link has been sent to your email address, If you do not receive the confirmation message within a few minutes of signing up, please check your spam or junk folder.') }}
                                    </div>
                                @endif
                                {{ __('front.Before proceeding, please check your email for a verification link.') }}
                                {{ __('front.If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('front.click here to request another') }}</a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
