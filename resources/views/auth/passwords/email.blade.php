@extends('/frontend/layout')

@section('content')

<div id="wrapper">  
    <div class="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-xs-12">
                    <div class="main-register fl-wrap" style="margin:30px auto">
                        <h3>{{ __('Reset Password') }}</h3>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div id="tabs-container">
                            <div class="custom-form">
                                <form method="post" action="/password/email" name="registerform">
                                    @csrf
                                    <label>{{ __('front.Email') }} * </label>
                                    <input name="email" type="text"  value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    <button type="submit" class="log-submit-btn"><span>{{ __('Send Password Reset Link') }}</span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

