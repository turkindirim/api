@extends('/frontend/layout')

@section('content')

<div id="wrapper">  
    <div class="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-xs-12">
                    <div class="main-register fl-wrap" style="margin:30px auto">
                        <h3>{{ __('front.Sign In') }} <span>{{ __('front.Turk') }}<strong>{{ __('front.indirim') }}</strong></span></h3>
                        <div class="soc-log fl-wrap">
                            <a href="#" class="facebook-log"><i class="fa fa-facebook-official"></i>Log in with Facebook</a>
                            <a href="#" class="twitter-log"><i class="fa fa-twitter"></i> Log in with Twitter</a>
                        </div>
                        <div class="log-separator fl-wrap"><span>{{ __('or') }}</span></div>
                        <div id="tabs-container">
                            <div class="custom-form">
                                <form method="post" name="registerform" action="/login">
                                    @csrf
                                    <label>{{ __('front.Email') }} * </label>
                                    <input name="email" type="text"  value="{{ old('email') }}">
        
                                    <label>{{ __('front.Password') }} * </label>
                                    <input name="password" type="password">

                                    <button type="submit" class="log-submit-btn"><span>{{ __('front.Log in') }}</span></button>
                                    <div class="clearfix"></div>
                                    <div class="filter-tags">
                                        <input id="check-a" type="checkbox" name="check">
                                        <label for="check-a">{{ __('front.Remember me') }}</label>
                                    </div>
                                </form>
                                <div class="lost_password">
                                    <a href="/password/reset">{{ __('front.Forgot Password') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
