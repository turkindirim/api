@extends('/frontend/layout')

@section('title')
{{ __('front.Home') }}
@endsection

@section('content')

            <div id="wrapper">
                <!-- Content-->   
                <div class="content">
                    <!--section -->
                    <section class="hero-section no-dadding"  id="sec1">
                        <div class="slider-container-wrap fl-wrap">
                            <div class="slider-container">
                                
                                <!-- slideshow-item-2-->	
                                <div class="slider-item fl-wrap slider-h">
                                    <div class="bg bg-ser" data-bg="images/bg/slider/slider002.jpg" style="background-image: url(images/bg/slider/slider002.jpg);"></div>
                                    
                                    <div class="hero-section-wrap fl-wrap">
                                        <div class="container">
                                            <div class="intro-item fl-wrap">
                                                <h3></h3>
                                                <h2></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  slideshow-item end  -->
                                <!-- slideshow-item-2-->	
                                <div class="slider-item fl-wrap slider-h">
                                    <div class="bg bg-ser" data-bg="images/bg/slider/slider003.jpg" style="background-image: url(images/bg/slider/slider003.jpg);"></div>
                                    
                                    <div class="hero-section-wrap fl-wrap">
                                        <div class="container">
                                            <div class="intro-item fl-wrap">
                                                <h3></h3>
                                                <h2></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  slideshow-item end  -->
                                <!-- slideshow-item-2-->	
                                <div class="slider-item fl-wrap slider-h">
                                    <div class="bg bg-ser" data-bg="images/bg/slider/slider004.jpg" style="background-image: url(images/bg/slider/slider004.jpg);"></div>
                                    
                                    <div class="hero-section-wrap fl-wrap">
                                        <div class="container">
                                            <div class="intro-item fl-wrap">
                                                <h3></h3>
                                                <h2></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  slideshow-item end  -->
                                <!-- slideshow-item-2-->	
                                <div class="slider-item fl-wrap slider-h">
                                    <div class="bg bg-ser" data-bg="images/bg/slider/slider005.jpg" style="background-image: url(images/bg/slider/slider005.jpg);"></div>
                                    
                                    <div class="hero-section-wrap fl-wrap">
                                        <div class="container">
                                            <div class="intro-item fl-wrap">
                                                <h3></h3>
                                                <h2></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  slideshow-item end  -->
                            </div>
                            <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                            <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section -->
                    <section id="sec2">
                        <div class="container">
                            <div class="section-title">
                                <h2>{{__('front.Browse our categories') }}</h2>
                                <span class="section-separator"></span>
                            </div>
                            <!-- portfolio start -->
                                
                                <section class="cate-section">
                                    <div class="container">
                                        <div class="fl-wrap spons-list">
                                            <ul class="client-carousel">
                                                @foreach ($global['categories'] as $category)
                                                <li><a href="{{ url('category/' . $category->id . '/' . urlencode($category->name)) }}"><img src="{{ url('uploads/' . $category->logo) }}" alt="{{ $category->name }}"><br> <h4>{{ $category->name }}</h4></a></li>
                                                @endforeach
                                            </ul>
                                            <div class="sp-cont sp-cont-prev"><i class="fa fa-angle-left"></i></div>
                                            <div class="sp-cont sp-cont-next"><i class="fa fa-angle-right"></i></div>
                                        </div>
                                    </div>
                                </section>
                            <!-- portfolio end -->
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section -->
                    <section class="gray-section">
                        <div class="container">
                            <div class="section-title">
                                <h2>{{__('front.Popular offers') }}</h2>
                                <span class="section-separator"></span>
                                <p>{{__('front.Browse our most popular offers') }} </p>
                            </div>
                        </div>
                        <!-- carousel --> 
                        <div class="list-carousel fl-wrap card-listing ">
                            <!--listing-carousel-->
                            <div class="listing-carousel  fl-wrap ">
                                 
                                                                                           
                                <!--slick-slide-item-->
                                        @foreach ($offers as $offer)
                                        
                                <div class="slick-slide-item">
                                        <!-- listing-item -->
                                        <div class="listing-item">
                                            <article class="geodir-category-listing fl-wrap">
                                                <div class="geodir-category-img">
                                                    
                                                    <a href="{{ url('offer/' . $offer->id) }}" title="{{ $offer->name }}"><img src="{{ url('uploads/' . $offer->thumb) }}" alt=""></a>
                                                    <a href="{{ url('offer/' . $offer->id) }}" title="{{ $offer->name }}"><div class="overlay"></div></a>
                                                    
                                                </div>
                                                <div class="geodir-category-content fl-wrap">
                                                    <a class="listing-geodir-category" href="{{url('category/'. $offer->categories[0]->id)}}"><i class="{{ $offer->categories[0]->code }}"></i></a>
                                                    <div class="listing-avatar"><a href="{{url('company/'. $offer->branches[0]->id)}}"><img src="{{ url('uploads/'. $offer->branches[0]->seller->logo) }}" alt=""></a>
                                                        <span class="avatar-tooltip"><strong>{{ $offer->branches[0]->name }}</strong></span>
                                                    </div>
                                                
                                                    <h3><a href="{{ url('offer/' . $offer->id) }}" title="{{ $offer->name }}">{{ $offer->name }}</a></h3>
                                                    <p>{!! $offer->excerpt !!}</p>
                                                    <div class="geodir-category-options fl-wrap">
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5">
                                                            <span>{{$offer->views. " " .__('front.Views') }}</span>
                                                        </div>
                                                        <div class="geodir-category-location"><a href="{{ url('offer/' . $offer->id) }}"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $offer->branches[0]->city['name'] }} - {{ $offer->branches[0]->province['name'] }}</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <!-- listing-item end--> 
                                </div> 
                                        @endforeach  

                                <!--slick-slide-item end-->                              
                            </div>
                            <!--listing-carousel end-->
                            <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                            <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
                        </div>
                        <!--  carousel end--> 
                        <a href="/offers" class="btn  big-btn circle-btn dec-btn  color-bg flat-btn offer-btn">{{__('front.View All') }}<i class="fa fa-eye"></i></a>

                    </section>
                    
                    <!-- section end -->
                    <!--section --><!--
                    <section class="color-bg padding-30 gradient-bg">
                        <div class="shapes-bg-big"></div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="color-bg-text">
                                        <h3>{{__('front.Download our application') }}</h3>
                                        <p>{{__('front.Download our application and stay up to date about the offers') }}</p>
                                        <a href="#"><img src="images/google-store.png" alt=""></a><a href="#"> <img src="images/apple-store.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="images-app fl-wrap">
                                        <img src="images/app.png" alt="">
                                     
                                </div>
                                
                            </div>
                        </div>
                    </section>-->
                    <!--section   end -->  

                    <!--section- cards -->  
                    <section id="cards" class="hide-mobile-card">
                        <div class="container">
                            <div class="section-title">
                                <h2>{{__('front.Buy turk indirim cards') }} </h2>
                                <span class="section-separator"></span>
                                <p>{{__('front.Buy our cards online and enjoy unlimited discounts and offers') }}</p>
                            </div>
                            <div class="pricing-wrap fl-wrap">
                                
                                <!-- price-item-->
                                <div class="price-item">
                                        <div class="price-head op2">
                                            <img src="images/cards/tatil.jpg" alt="">
                                        </div>
                                        <div class="price-content fl-wrap">
                                            <div class="price-num fl-wrap">
                                                
                                                <img class="price-img" src="images/cards/price-2.png" alt="">
                                                <div class="price-num-desc">{{__('front.3 Months') }}</div>
                                            </div>
                                            <div class="price-desc fl-wrap">
                                                <ul>
                                                    <li>{{__('front.For tourists') }}</li>
                                                    <li>{{__('front.Three months active time from the first date use') }}</li>
                                                    <li>{{__('front.All categories included') }}</li>
                                                    <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                </ul>
                                                <a href="#" class="price-link modal-open2" >{{__('front.Order the card now') }}</a>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- price-item end-->
                                
                                <!-- price-item-->
                                <div class="price-item">
                                    <div class="price-head">
                                        <img src="images/cards/turk-kart.jpg" alt="">
                                    </div>
                                    <div class="price-content fl-wrap">
                                        <div class="price-num fl-wrap">
                                            <img class="price-img" src="images/cards/price-3.png" alt="">
                                            <div class="price-num-desc">{{__('front.Per year') }}</div>
                                        </div>
                                        <div class="price-desc fl-wrap">
                                            <ul>
                                                <li>{{__('front.For residence in Turkey') }}</li>
                                                <li>{{__('front.One year active time from the first use date') }}<br></li>
                                                <li>{{__('front.All categories included') }}</li>
                                                <li>{{__('front.Unlimited discounts and offers') }}</li>
                                            </ul>
                                            <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- price-item end--> 
                                <!-- price-item-->
                                <div class="price-item">
                                        <div class="price-head op1">
                                            <img src="images/cards/ogrenci.jpg" alt="">
                                        </div>
                                        <div class="price-content fl-wrap">
                                            <div class="price-num fl-wrap">
                                                <img class="price-img" src="images/cards/price-1.png" alt="">
                                                <div class="price-num-desc">{{__('front.Per year') }}</div>
                                            </div>
                                            <div class="price-desc fl-wrap">
                                                <ul>
                                                    <li>{{__('front.For students') }}</li>
                                                    <li>{{__('front.One year active time from the first use date') }}</li>
                                                    <li>{{__('front.All categories included') }}</li>
                                                    <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                </ul>
                                                <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- price-item end-->
                                    <!-- price-item-->
                                    <div class="price-item">
                                            <div class="price-head">
                                                <img src="images/cards/ozel.jpg" alt="">
                                            </div>
                                            <div class="price-content fl-wrap">
                                                <div class="price-num fl-wrap">
                                                    <img class="price-img" src="images/cards/price-4.png" alt="">
                                                    <div class="price-num-desc">{{__('front.Per year') }}</div>
                                                </div>
                                                <div class="price-desc fl-wrap">
                                                    <ul>
                                                        <li>{{__('front.For residence in Turkey') }}</li>
                                                        <li>{{__('front.One year active time from the first use date') }}<br></li>
                                                        <li>{{__('front.All categories included') }}</li>
                                                        <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                    </ul>
                                                    <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- price-item end-->                                                   
                            </div>
                            <!-- about-wrap end  --> 
                        </div>
                    </section>
                    <!-----mobile-cards----->
                    <section id="cards" class="hide-desktop-card">
                        <div class="container">
                            <div class="section-title">
                                <h2>{{__('front.Buy turk indirim cards') }} </h2>
                                <span class="section-separator"></span>
                                <p>{{__('front.Buy our cards online and enjoy unlimited discounts and offers') }}</p>
                            </div>
                            <div class="pricing-wrap fl-wrap">
                                
                                <!-- price-item-->
                                <div class="price-item">
                                        <div class="price-head op2">
                                            <img src="images/cards/tatil.jpg" alt="">
                                        </div>
                                        <div class="price-content fl-wrap">
                                            <div class="price-num fl-wrap">
                                                <img class="price-img" src="images/cards/price-2.png" alt="">
                                                <div class="price-num-desc">{{__('front.3 Months') }}</div>
                                            </div>
                                            <div class="price-desc fl-wrap">
                                                <div class="accordion">
                                                    <a class="toggle act-accordion" href="#"> {{__('front.For tourists') }} <i class="fa fa-angle-down"></i></a>
                                                    
                                                    <div class="accordion-inner" aria-expanded="false">
                                                            <ul>
                                                                <li>3{{__('front.Months active time') }}</li>
                                                                <li>{{__('front.All categories included') }}</li>
                                                                <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                            </ul>
                                                    </div>
                                                </div>
                                                
                                                <a href="#" class="price-link">{{__('front.Order the card now') }}</a>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- price-item end-->
                                
                               
                                <!-- price-item-->
                                <div class="price-item">
                                    <div class="price-head">
                                        <img src="images/cards/turk-kart.jpg" alt="">
                                    </div>
                                    <div class="price-content fl-wrap">
                                        <div class="price-num fl-wrap">
                                            <img class="price-img" src="images/cards/price-3.png" alt="">
                                            <div class="price-num-desc">{{__('front.Per year') }}</div>
                                        </div>
                                        <div class="price-desc fl-wrap">
                                                <div class="accordion">
                                                    <a class="toggle act-accordion" href="#"> {{__('front.For residence in Turkey') }} <i class="fa fa-angle-down"></i></a>
                                                    
                                                    <div class="accordion-inner" aria-expanded="false">
                                                            <ul>
                                                                <li>{{__('front.One year active time from the first use date') }}</li>
                                                                <li>{{__('front.All categories included') }}</li>
                                                                <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                            </ul>
                                                    </div>
                                                </div>
                                            
                                            <a href="#" class="price-link">{{__('front.Order the card now') }}</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- price-item end-->  
                                <!-- price-item-->
                                <div class="price-item">
                                        <div class="price-head op1">
                                            <img src="images/cards/ogrenci.jpg" alt="">
                                        </div>
                                        <div class="price-content fl-wrap">
                                            <div class="price-num fl-wrap">
                                                <img class="price-img" src="images/cards/price-1.png" alt="">
                                                <div class="price-num-desc">{{__('front.Per year') }}</div>
                                            </div>
                                            <div class="price-desc fl-wrap">
                                                    <div class="accordion">
                                                        <a class="toggle act-accordion" href="#"> {{__('front.For students') }} <i class="fa fa-angle-down"></i></a>
                                                        
                                                        <div class="accordion-inner" aria-expanded="false">
                                                                <ul>
                                                                    <li>{{__('front.One year active time from the first use date') }}</li>
                                                                    <li>{{__('front.All categories included') }}</li>
                                                                    <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                                </ul>
                                                        </div>
                                                    </div>
                                                
                                                <a href="#" class="price-link">{{__('front.Order the card now') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- price-item end-->
                                    <!-- price-item-->
                                    <div class="price-item">
                                            <div class="price-head">
                                                <img src="images/cards/ozel.jpg" alt="">
                                            </div>
                                            <div class="price-content fl-wrap">
                                                <div class="price-num fl-wrap">
                                                    <img class="price-img" src="images/cards/price-4.png" alt="">
                                                    <div class="price-num-desc">{{__('front.Per year') }}</div>
                                                </div>
                                                <div class="price-desc fl-wrap">
                                                        <div class="accordion">
                                                            <a class="toggle act-accordion" href="#"> {{__('front.For residence in Turkey') }} <i class="fa fa-angle-down"></i></a>
                                                            
                                                            <div class="accordion-inner" aria-expanded="false">
                                                                    <ul>
                                                                        <li>{{__('front.One year active time from the first use date') }}</li>
                                                                        <li>{{__('front.All categories included') }}</li>
                                                                        <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                                    </ul>
                                                            </div>
                                                        </div>
                                                    
                                                    <a href="#" class="price-link">{{__('front.Order the card now') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- price-item end-->                                                   
                            </div>
                            <!-- about-wrap end  --> 
                        </div>
                    </section>
                    <section class="parallax-section call-to-action-back">
                        <!--container-->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 intro-item ">
                                    <img src="images/bg/cta.jpg" alt="">
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section -->
                    
                    <section>
                        <div class="container">
                            <div class="section-title">
                                <h2>{{__('front.How it works?') }}</h2>
                                <span class="section-separator"></span>
                            </div>
                            <!--process-wrap  -->
                            <div class="process-wrap fl-wrap">
                                <ul>
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">01 . </span>
                                            <div class="time-line-icon"><i class="fa fa-map-o"></i></div>
                                            <h4>{{__('front.Check our offers') }} </h4>
                                            <p>{{__('front.Stay always tuned about our offers by checking the website of mobile application') }}</p>
                                        </div>
                                        <span class="pr-dec"></span>
                                    </li>
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">02 .</span>
                                            <div class="time-line-icon"><i class="fa fa-envelope-open-o"></i></div>
                                            <h4>{{__('front.Use Turk Indirim card') }} </h4>
                                            <p>{{__('front.Show Turk Indirim cards in the stores to get the discounts') }}</p>
                                        </div>
                                        <span class="pr-dec"></span>
                                    </li>
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">03 .</span>
                                            <div class="time-line-icon"><i class="fa fa-hand-peace-o"></i></div>
                                            <h4>{{__('front.Enjoy the discount') }} </h4>
                                            <p>{{__('front.Unlimited offers and discounts are available every time') }}</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!--process-wrap   end-->
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section -->
                    <!--<section class="color-bg-purpel">
                        <div class="shapes-bg-big"></div>
                        <div class="container">
                            <div class=" single-facts fl-wrap">
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="1054">1054</div>
                                            </div>
                                        </div>
                                        <h6>{{__('front.Available offers') }}</h6>
                                    </div>
                                </div>
                               
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="2168">2168</div>
                                            </div>
                                        </div>
                                        <h6>{{__('front.New Visitors Every Week') }}</h6>
                                    </div>
                                </div>
                                
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="1172">1172</div>
                                            </div>
                                        </div>
                                        <h6>{{__('front.Happy customers every year') }}</h6>
                                    </div>
                                </div>
                                
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="4">4</div>
                                            </div>
                                        </div>
                                        <h6>{{__('front.Cards available') }}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>-->
                    <!-- section end -->
                    
                    <!--section -->
                    <section class="gray-section clients">
                        <div class="container">
                            <div class="fl-wrap spons-list">
                                <ul class="client-carousel">
                                    @foreach ($featured_sellers as $seller)
                                        <li><a href="#"><img src="{{ url('uploads/'.$seller->logo) }}" alt="{{ $seller->name }}"></a></li>
                                    @endforeach
                                </ul>
                                <div class="sp-cont sp-cont-prev"><i class="fa fa-angle-left"></i></div>
                                <div class="sp-cont sp-cont-next"><i class="fa fa-angle-right"></i></div>
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                    
                    <!--section -->
                    @include('frontend._partials.getintouch')
                </div>
                <!-- Content end -->      
            </div>
            <!-- wrapper end -->
@endsection