<footer class="main-footer dark-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="footer-widget fl-wrap">
                        <h3>{{__('front.Contact details') }}</h3>
                        <div class="footer-contacts-widget fl-wrap">
                            <ul  class="footer-contacts fl-wrap">
                                <li><span><i class="fa fa-envelope-o"> </i></span><a href="mailto:info@turkindirim.com.tr" target="_blank">info@turkindirim.com.tr</a></li>
                                <li><span><i class="fa fa-phone"></i> </span><a href="tel:00908502410444">+90 850 241 0 444</a></li>
                                <li> <span><i class="fa fa-map-marker"></i> </span><a href="#" target="_blank">19 Mayis Mah. Gazi Berkay Sok. - No:6/3 Şişli/İstanbul</a></li>
                            </ul>
                        </div>
                        <div class="footer-social">
                            <ul>
                                <li><a href="https://www.facebook.com/turkindirimtr" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/turkindirimtr" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://www.instagram.com/turkindirimtr/" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://www.youtube.com/channel/UCuf9PVvbmohBZDf3KosiTDQ/featured?disable_polymer=1" target="_blank" ><i class="fab fa-youtube"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/turkindirimtr/" target="_blank" ><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="https://wa.me/905351001800" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                        <div class="footer-widget fl-wrap">
                            <h3>{{__('front.Quick Links') }}</h3>
                            <div class="widget-posts fl-wrap">
                                <ul class="links-menu">
                                    <li class="link-item"><a href="/" >{{__('front.Home') }} </a></li>
                                    <li class="link-item"><a href="" >{{__('front.Blog') }} </a></li>
                                    <li class="link-item"><a href="">{{__('front.F A Q') }}</a></li> 
                                    <li class="link-item"><a href="/privacy">{{__('front.Privcy Policy') }}</a></li>
                                    <li class="link-item"><a href="/terms">{{__('front.Terms & Conditions') }}</a></li> 
                                </ul>
                            </div>
                        </div>
                    </div>
                <div class="col-md-3">
                    <div class="footer-widget fl-wrap">
                        <h3>{{__('front.Our Cards') }}</h3>
                            <ul class="footer-cards">
                                <li class="clearfix">
                                    <a href="#"  class="widget-cards-img"><img src="{{ asset('images/cards/tatil.jpg')}}" class="respimg" alt=""></a>
                                    <div class="widget-cards-descr">
                                        <a href="#" title="">Tatil Kart</a>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <a href="#"  class="widget-cards-img"><img src="{{ asset('images/cards/turk-kart.jpg')}}" class="respimg" alt=""></a>
                                    <div class="widget-cards-descr">
                                        <a href="#" title=""> Turk Kart </a>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <a href="#"  class="widget-cards-img"><img src="{{ asset('images/cards/ogrenci.jpg')}}" class="respimg" alt=""></a>
                                    <div class="widget-cards-descr">
                                        <a href="#" title="">Öğrenci Kart</a>
                                    </div>
                                </li>
                                
                                <li class="clearfix">
                                    <a href="#"  class="widget-cards-img"><img src="{{ asset('images/cards/ozel.jpg')}}" class="respimg" alt=""></a>
                                    <div class="widget-cards-descr">
                                        <a href="#" title="">Özel Kart</a>
                                    </div>
                                </li>
                            </ul>
                    </div>
                </div>
                <div class="col-md-3 kart-footer">
                    <div class="footer-widget fl-wrap">
                        <h3>{{__('front.Download Our App') }}</h3>
                            <ul class="footer-cards">
                             <p>{{__('front.Download our application and stay up to date about all offers') }}</p>   
                             <a href="#"  class="widget-download-img"><img src="{{ asset('images/google-store.png')}}" alt=""></a>
                             <a href="#"  class="widget-download-img"><img src="{{ asset('images/apple-store.png')}}" alt=""></a>
                            </ul>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="sub-footer fl-wrap">
            <div class="container">
                <div class="row">
                        <div class="col-md-6">
                                
                        </div>
                        <div class="col-md-6">
                                <div class="copyright"> Türk indirim © 2010 - <script>document.write(new Date().getFullYear());</script> &nbsp;| All Rights Reserved | Powered by <a href="https://nexoajans.com" target="_blank">Nexo Ajans</a>	</div>
                        </div>
                </div>
            </div>
        </div>
    </footer>            <!--footer end  -->
    <!--register form -->
    <div class="main-register-wrap modal">
        <div class="main-overlay"></div>
        <div class="main-register-holder">
            <div class="main-register fl-wrap">
                <div class="close-reg"><i class="fa fa-times"></i></div>
                <h3>{{ __('front.Sign In') }} <span>Türk<strong>İndirim</strong></span></h3>
                <!--
                <div class="soc-log fl-wrap">
                    <p>For faster login or register use your social account.</p>
                    <a href="#" class="facebook-log"><i class="fa fa-facebook-official"></i>Log in with Facebook</a>
                    <a href="#" class="twitter-log"><i class="fa fa-twitter"></i> Log in with Twitter</a>
                </div>
                <div class="log-separator fl-wrap"><span>{{ __('front.or') }}</span></div>
                //-->
                <div id="tabs-container">
                    <ul class="tabs-menu">
                        <li class="current"><a href="#tab-1">{{ __('front.Login') }}</a></li>
                        <li><a href="#tab-2">{{ __('front.Register') }}</a></li>
                        <li><a href="#tab-3">{{ __('front.Instructions') }}</a></li>
                    </ul>
                    <div class="tab">
                        <div id="tab-1" class="tab-content">
                            <div class="custom-form">
                                <form method="post" action="/login" name="registerform">
                                    @csrf
                                    <label>{{ __('front.Email') }} </label>
                                    <input id="login-email" name="email" type="text" value="{{ old('email') }}" required> 
                                    <label >{{ __('front.Password') }} * </label>
                                    <input id="login-password" name="password" type="password"   value=""  required> 
                                    <button type="submit"  class="log-submit-btn"><span>{{ __('front.Log in') }}</span></button> 
                                    <div class="clearfix"></div>
                                    <div class="filter-tags">
                                        <input id="check-a" type="checkbox" name="check">
                                        <label for="check-a">{{ __('front.Remember me') }}</label>
                                    </div>
                                </form>
                                <div class="lost_password">
                                    <a href="/password/reset">{{ __('front.Forgot Password') }}</a>
                                </div>
                            </div>
                        </div>
                        <div class="tab">
                            <div id="tab-2" class="tab-content">
                                <div class="custom-form">

                                    <form method="post" action="/register" name="registerform" class="main-register-form" id="main-register-form2">
                                        @csrf
                                        <label >{{ __('front.Name')}} * </label>
                                        <input type="text" id="firstname" name="firstname" value="{{ old('firstname') }}" > 
                                     
                                        <label>{{ __('front.Email')}} *</label>
                                        <input type="text" id="email" name="email" value="{{ old('email') }}" >                                              
                                        <label >{{ __('front.Password')}} *</label>
                                        <input type="password" id="password" name="password"  value=""  >                                 
                                        <label >{{ __('front.Password Confirmation')}} *</label>
                                        <input type="password" id="password_confirmation" name="password_confirmation"  value=""> 
                                        <div class="clearfix"></div>
                                        <div style="margin:6px 0" class="filter-tags">
                                            <input type="checkbox" name="" id="" checked onchange="$('.register_btn').prop('disabled', !$(this).is(':checked'))">&nbsp;&nbsp;&nbsp;{{ __('front.I agree to the') }}
                                            <a target="_blank" href="/terms">{{ __('front.Terms & Conditions') }}</a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <button type="submit" class="log-submit-btn signin_button register_btn"  ><span>{{ __('front.Register')}}</span></button> 
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab">
                                <div id="tab-3" class="tab-content">
                                    <div class="instruction">
                                            <p>{{__('front.how to activate the card') }}</p>
                                            <a href="#" class="price-link">{{__('front.download PDF file') }}</a>
                                            <p>{{__('front.how to register in the website') }}</p>
                                            <a href="#" class="price-link">{{__('front.download PDF file') }}</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>            
    <!--register form end -->


    <!--modal-open -->
    <div class="main-register-wrap modal2">
        <div class="main-overlay"></div>
        <div class="main-register-holder">
            <div class="main-register fl-wrap">
                <div class="close-reg"><i class="fa fa-times"></i></div>
                <h3>{{ __('front.Order Card') }}</h3>
                    <div id="contact-form" class="order-style">
                        <div id="message"></div>
                        <form  class="custom-form" method="post" action="/order" name="orderform" id="orderform">
                            @csrf
                            <fieldset>
                                <input type="text" name="name" id="name" placeholder="{{__('front.Your Name') }} *" value=""/>
                                <div class="clearfix"></div>
                                <input type="text"  name="email" id="email" placeholder="{{__('front.Email Adress') }}*" value=""/>
                                <div class="clearfix"></div>
                                <input type="text"  name="phone" id="phone" placeholder="{{__('front.Phone') }} *" value=""/>
                                <textarea name="message"  id="comments" onClick="this.select()" >{{__('front.Message') }}</textarea>
                                
                            </fieldset>
                            <button type="submit" class="btn  big-btn  color-bg flat-btn" id="submit">{{__('front.Send') }}<i class="fa fa-angle-right"></i></button>
                        </form>
                    </div>
            </div>
        </div>
    </div>



    <!--order card form 
    <div class="order-card modal2">
        <div class="list-single-main-item-title fl-wrap">
            <h3>{{__('front.Get In Touch') }}</h3>
        </div>
        <div id="message"></div>
        <form  class="custom-form" method="post" action="/order-card" name="contactform" id="contactusform">
            @csrf
            <fieldset>
                <label><i class="fa fa-user-o"></i></label>
                <input type="text" name="name" id="name" placeholder="{{__('front.Your Name') }} *" value=""/>
                <div class="clearfix"></div>
                <label><i class="fa fa-envelope-o"></i>  </label>
                <input type="text"  name="email" id="emailaddress" placeholder="{{__('front.Email Adress') }}*" value=""/>
                <div class="clearfix"></div>
                <label><i class="fa fa-phone"></i>  </label>
                <input type="text"  name="phone" id="phone" placeholder="{{__('front.Phone') }} *" value=""/>
                <textarea name="message"  id="comments" onClick="this.select()" >{{__('front.Message') }}</textarea>
                
            </fieldset>
            <button class="btn  big-btn  color-bg flat-btn" id="submit">{{__('front.Send') }}<i class="fa fa-angle-right"></i></button>
        </form>
    </div>
    order card form end -->
    <a class="to-top"><i class="fa fa-angle-up"></i></a>
        
</div>
<!-- Main end -->
<!--=============== scripts  ===============-->

@yield('before_script')
<script type="text/javascript" src="{{ asset('front/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/plugins.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/scripts.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('front/js/intlTelInput-jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/custom.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIMjfEw0gDpISenqbXPS5oepcPot5muRk&libraries=places"></script>
<script type="text/javascript" src="{{ asset('front/js/map_infobox.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/markerclusterer.js') }}"></script>
<script>
function showMap(theLat, theLng, title) {
    if (!document.getElementById('map-main')) {return;}
    var map = new google.maps.Map(document.getElementById('map-main'), {
        zoom: 15,
        scrollwheel: false,
        center: new google.maps.LatLng( theLat, theLng),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        panControl: false,
        fullscreenControl: true,
        navigationControl: false,
        streetViewControl: false,
        animation: google.maps.Animation.BOUNCE,
        gestureHandling: 'cooperative',
        styles: [{
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#444444"
            }]
        }]
    });
    var marker = new google.maps.Marker({
        position: {lat: theLat, lng: theLng},
        map: map,
        title: title
    });
}
</script>

@yield('after_script')
 
</body>
</html>