@extends('/frontend/layout')

@section('title')
{{ __('front.Contact') }}
@endsection

@section('content')

<div id="wrapper">
                <!--content-->  
                <div class="content">
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="images/bg/contact.png" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span>{{__('front.Contact us') }}</span></h2>
                            </div>
                        </div>
                        
                    </section>
                    <!-- section end -->
                    <!--section -->  
                    <section id="sec1" class="map-contact">
                            <div class="row">
                                <div class="col-md-12">
                                        <div class="map-container">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3008.4484901173687!2d28.98929075942233!3d41.05918824114341!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab7fff621c563%3A0x1a4f47e96c649b65!2sTurk+indirim!5e0!3m2!1sen!2str!4v1543524228198" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>
                                </div>
                            </div>
                    </section>

                    <section id="sec1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="list-single-main-item fl-wrap">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>{{__('front.Contact') }} <span>{{__('front.Details') }} </span></h3>
                                        </div>
                                        
                                        <p>{{__('front.Drop us an email and we will be back') }}</p>
                                        <div class="list-author-widget-contacts">
                                            <ul>
                                                <li><span><i class="fa fa-envelope-o"></i> </span><a href="mailto:info@turkindirim.com.tr" target="_blank">info@turkindirim.com.tr</a></li>
                                                <li><span><i class="fa fa-mobile"></i> </span><a href="tel:‏00905351001800‏">+90 535 100 18 00</a></li>
                                                <li><span><i class="fa fa-phone"></i> </span><a href="tel:00908502410444">+90 850 241 0 444</a></li>
                                                <li> <span><i class="fa fa-map-marker"></i> </span><a href="#" target="_blank">19 Mayıs Mah. Gazi Berkay Sok. - No:6/3 Şişli/İstanbul</a></li>
                                            </ul>
                                        </div>
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3 class="social-title">{{__('front.Social Media Links') }}</h3>
                                        </div>
                                        <div class="list-widget-social">
                                            <ul>
                                                <li><a href="https://www.facebook.com/turkindirimtr" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="https://twitter.com/turkindirimtr" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="https://www.instagram.com/turkindirimtr/" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                                                <li><a href="https://www.youtube.com/channel/UCuf9PVvbmohBZDf3KosiTDQ/featured?disable_polymer=1" target="_blank" ><i class="fab fa-youtube"></i></a></li>
                                                <li><a href="https://www.linkedin.com/company/turkindirimtr/" target="_blank" ><i class="fab fa-linkedin-in"></i></a></li>
                                                <li><a href="https://wa.me/905351001800" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="list-single-main-wrapper fl-wrap border-rad">
                                        
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>{{__('front.Get In Touch') }}</h3>
                                        </div>
                                        <div id="contact-form">
                                            <div id="message"></div>
                                            <form  class="custom-form" method="post" action="/contactus" name="contactform" id="contactusform">
                                                @csrf
                                                <fieldset>
                                                    <label><i class="fa fa-user-o"></i></label>
                                                    <input type="text" name="name" id="name" placeholder="{{__('front.Your Name') }} *" value=""/>
                                                    <div class="clearfix"></div>
                                                    <label><i class="fa fa-envelope-o"></i>  </label>
                                                    <input type="text"  name="email" id="emailaddress" placeholder="{{__('front.Email Adress') }}*" value=""/>
													<div class="clearfix"></div>
													<label><i class="fa fa-phone"></i>  </label>
                                                    <input type="text"  name="phone" id="phone" placeholder="{{__('front.Phone') }} *" value=""/>
                                                    <textarea name="message"  id="comments" onClick="this.select()" >{{__('front.Message') }}</textarea>
                                                    
                                                </fieldset>
                                                <button class="btn  big-btn  color-bg flat-btn" id="submit">{{__('front.Send') }}<i class="fa fa-angle-right"></i></button>
                                            </form>
                                        </div>
                                        <!-- contact form  end--> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                    <div class="limit-box fl-wrap"></div>
                    <!--section -->  
                    
                </div>
                <!-- contentend -->
            </div>

@endsection