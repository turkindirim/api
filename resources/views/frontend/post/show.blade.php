@extends('/frontend/layout')

@section('title')
{{ $post->name }}
@endsection

@section('content')	
<div id="wrapper" class="blog">  
    <div class="content">
    <div id="wrapper">
                <!-- content -->
                <div class="content">
                    <!--section -->
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="overlay"></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span>{{ $post->name }}</span></h2>
                                
                            </div>
                        </div>
                       
                    </section>
                    <!-- section end -->
                    <!--section -->   
                    <section class="gray-section" id="sec1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="list-single-main-wrapper fl-wrap" id="sec2">
                                        <!-- article> --> 
                                        <article>
                                            <div class="list-single-main-media fl-wrap">
                                                <div class="single-slider-wrapper fl-wrap">
                                                    <div class="single-slider fl-wrap"  >
                                                        <div class="slick-slide-item"><img src="{{ url('uploads/'.$post->logo) }}" alt=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-single-main-item fl-wrap">
                                                <div class="list-single-main-item-title fl-wrap">
                                                </div>
                                                
                                                <p>{!! $post->description !!}</p>
                                                <div class="post-opt">
                                                    <ul>
                                                        <li><i class="fa fa-calendar-check-o"></i> <span>{{ substr($post->created_at, 0, 10) }}</span></li>
                                                        <li><i class="fa fa-eye"></i> <span>{{ $post->views }}</span></li>
                                                        <li><i class="fa fa-tags"></i> <a href="#">Photography</a> , <a href="#">Design</a> </li>
                                                    </ul>
                                                </div>
                                                <span class="fw-separator"></span>
                                                <div class="list-single-main-item-title fl-wrap">
                                                    <h3>{{ __('front.Tags') }}</h3>
                                                </div>
                                                <div class="list-single-tags tags-stylwrap blog-tags">
                                                    <a href="#">Event</a>
                                                    <a href="#">Conference</a>
                                                    
                                                </div>
                                                <div class="share-holder hid-share">
                                                    <div class="showshare"><span>{{ __('front.Share') }} </span><i class="fa fa-share"></i></div>
                                                    <div class="share-container  isShare"></div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- article end -->       
                                        
                                                                      
                                    </div>
                                </div>
                                
                                @include('/frontend/post/sidebar')

                            </div>
                        </div>
                    </section>
                    <!--section end -->
                    
                </div>
                <!--content end --> 
            </div>
        
    </div>
</div>
@endsection