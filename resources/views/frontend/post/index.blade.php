@extends('/frontend/layout')

@section('title')
{{ __('front.Posts') }}
@endsection
@section('content')	

            <div id="wrapper">
                <!-- content -->
                <div class="content">
                    <!--section -->
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{ asset('images/banner.png') }}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span>{{ __('front.Posts') }}</span></h2>
                            </div>
                        </div>
                        
                    </section>
                    <!-- section end -->
                    <!--section -->   
                    <section class="gray-section" id="sec1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="list-single-main-wrapper fl-wrap" id="sec2">
                                        <!-- article> --> 
                                        <article>
                                                @foreach ($posts as $post)
                                            
                                            <div class="list-single-main-media fl-wrap">
                                                <div class="single-slider-wrapper fl-wrap">
                                                    <div class="single-slider fl-wrap"  >
                                                        <div class="slick-slide-item"><a href="/{{ urlencode($post->name) }}"><img src="{{ url('uploads/'.$post->logo) }}"  alt=""></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-single-main-item fl-wrap">
                                                <div class="list-single-main-item-title fl-wrap">
                                                    <h3><a href="/{{ urlencode($post->name) }}">{{ $post->name }}</a></h3>
                                                </div>
                                                <p>{!! $post->excerpt !!}</p>
                                                <div class="post-opt">
                                                    <ul>
                                                        <li><i class="fa fa-calendar-check-o"></i> <span>{{ substr($post->created_at, 0, 10) }}</span></li>
                                                        <li><i class="fa fa-eye"></i> <span>{{ $post->views ? $post->views : 1 }}</span></li>
                                                        <li><i class="fa fa-tags"></i>
                                                        @foreach ($post->tags as $tag)
                                                            <a href="/tags/posts/{{ $tag->id }}/{{ urlencode($tag->name) }}">{{ $tag->name }}</a>
                                                            @if (!$loop->last)
                                                                ,  
                                                            @endif
                                                        @endforeach
                                                        </li>
                                                    </ul>
                                                </div>
                                                <span class="fw-separator"></span>
                                                <a href="/{{ urlencode($post->name) }}" class="btn transparent-btn float-btn">{{ __('front.Read more') }}<i class="fa fa-eye"></i></a>
                                            </div>
                                            @endforeach
                                        </article>
                                        <!-- article end -->       
                                                                        
                                        @if ($total > 1)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="pagination">
                                                        @include('frontend._partials.pagination')
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <!--box-widget-wrap -->
                                @include('/frontend/post/sidebar')
                                <!--box-widget-wrap end -->
                            </div>
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                    
                    <!-- section end -->
                </div>
                <!-- content end -->
            </div>
        @endsection