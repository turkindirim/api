<div class="col-md-4">
    <div class="box-widget-wrap">
        <div class="box-widget-item fl-wrap">
            <div class="box-widget-item-header">
                <h3>{{ __('front.Recent posts') }}: </h3>
            </div>
            <div class="box-widget widget-posts blog-widgets">
                <div class="box-widget-content">
                    <ul>
                        @foreach ($recentPosts as $post)
                        <li class="clearfix">
                            <a href="/{{ urlencode($post->name) }}"  class="widget-posts-img"><img src="{{ url('uploads/'.$post->logo) }}"  alt=""></a>
                            <div class="widget-posts-descr">
                                <a href="/{{ urlencode($post->name) }}" title="">{{ $post->name }}</a>
                                <span class="widget-posts-date"><i class="fa fa-calendar-check-o"></i> {{ substr($post->created_at, 0, 10 ) }} </span>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="box-widget-item fl-wrap">
            <div class="box-widget-item-header">
                <h3>{{ __('front.Tags') }}: </h3>
            </div>
            <div class="list-single-tags tags-stylwrap">
                @foreach ($tags as $tag)
                <a href="/tags/posts/{{ $tag->id }}/{{ urlencode($tag->name) }}">{{ $tag->name }}</a>
                @endforeach                                                                             
            </div>
        </div>                   
    </div>
</div>