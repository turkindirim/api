<section class="gradient-bg">
    <div class="cirle-bg">
        <div class="bg" data-bg="{{ url('images/bg/circle.png') }}"></div>
    </div>
    <div class="container">
        <div class="join-wrap fl-wrap">
            <div class="row">
                <div class="col-md-8">
                    <h3>{{__('front.Do You Have Questions ?') }}</h3>
                    <p>{{__('front.Drop us an email and we will be back') }}</p>
                </div>
                <div class="col-md-4"><a href="/contact" class="join-wrap-btn">{{__('front.Get in touch') }} <i class="fa fa-envelope-o"></i></a></div>
            </div>
        </div>
    </div>
</section>