<style>
.more-dots {
    display: inline-block;
    width: 44px;
    height: 44px;
    line-height: 44px;
    border-radius: 6px;
    border: 1px solid #eee;
    box-sizing: border-box;
    position: relative;
    font-size: 10px;
    color: #888DA0;
    background: #fff;
    letter-spacing: 1px;
}
</style>
<a href="{{ $previousPageUrl }}" class="prevposts-link"><i class="fa fa-caret-left"></i></a>
@if ($last_page <= 5)
    @for ($i = 1; $i <= $last_page; $i++)
        <a href="?page={{ $i }}" class="{{ $i == $current_page ? 'current-page' :  ''}}">{{ $i }}</a>
    @endfor
@else
    <a href="?page=1" class="{{ 1 == $current_page ? 'current-page' :  ''}}">1</a>
    @if ($current_page > 2)
        <span class="more-dots">&#9679; &#9679;</span>
    @endif
    @if ($current_page > 1 && $current_page < $last_page)
        <a href="?page={{ $current_page }}" class="current-page">{{ $current_page }}</a>    
    @endif
    @if (($last_page - $current_page) > 1)
        <span class="more-dots">&#9679; &#9679;</span>
    @endif
    <a href="?page={{ $last_page }}" class="{{ $last_page == $current_page ? 'current-page' :  ''}}">{{ $last_page }}</a>
@endif
<a href="{{ $nextPageUrl }}" class="nextposts-link"><i class="fa fa-caret-right"></i></a>