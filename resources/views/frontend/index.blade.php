@extends('/frontend/layout')

@section('title')
{{ __('front.Posts') }}
@endsection
@section('content')	
<div id="wrapper">  
    <div class="content">
        @foreach ($posts as $post)
            {{ $post->name }}

            {!! $post->description !!}

            {{ $post->logo }}
        @endforeach


        @if ($total > 1)
            <div class="row">
                <div class="col-md-12">
                    <div class="pagination">
                        <a href="{{ $previousPageUrl }}" class="prevposts-link"><i class="fa fa-caret-left"></i></a>
                        @for ($i = 1; $i <= $last_page; $i++)
                        <a href="?page={{ $i }}" class="{{ $i == $current_page ? 'current-page' :  ''}}">{{ $i }}</a>
                        @endfor
                        <a href="{{ $nextPageUrl }}" class="nextposts-link"><i class="fa fa-caret-right"></i></a>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection