@extends('/frontend/layout')

@section('title')
{{__('front.Support') }}
@endsection

@section('content')

    <div id="wrapper">
        <!--content-->  
        <div class="content">
            <!--  section  --> 
            <section class="parallax-section" data-scrollax-parent="true">
                <div class="bg par-elem "  data-bg="{{ asset('images/banner.png') }}" data-scrollax="properties: { translateY: '30%' }"></div>
                <div class="container">
                    <div class="section-title center-align">
                        <h2><span>{{__('front.Support') }}</span></h2>
                    </div>
                </div>
                
            </section>
            <!--  section  end--> 
            <div class="container about-discription">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-10">
                            <br><br><br>
                            <p></p>
                    
                    </div>
                    <!--box-widget-wrap -->
                    <div class="col-md-1">
                    </div>
                    <!--box-widget-wrap end -->
                </div>
            </div>
        <!--  section  --> 
        @include('frontend._partials.getintouch')
        <!--  section  end--> 
    </div>
@endsection