@extends('/frontend/layout')

@section('title')
    {{ $post->name }}
@endsection

@section('content')	
<div id="wrapper" class="blog">  
    <div class="content">
    <div id="wrapper">
                <!-- content -->
                <div class="content">
                    <!--section -->
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="overlay"></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span>{{ $post->name }}</span></h2>
                                
                            </div>
                        </div>
                       
                    </section>
                    <!-- section end -->
                    <!--section -->   
                    <section class="gray-section" id="sec1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="list-single-main-wrapper fl-wrap" id="sec2">
                                        <!-- article> --> 
                                        <article>
                                            <div class="list-single-main-media fl-wrap">
                                                <div class="single-slider-wrapper fl-wrap">
                                                    <div class="single-slider fl-wrap"  >
                                                        <div class="slick-slide-item"><img src="{{ url('uploads/'.$post->logo) }}" alt=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-single-main-item fl-wrap">
                                                <div class="list-single-main-item-title fl-wrap">
                                                </div>
                                                
                                                <p>{!! $post->description !!}</p>
                                                <div class="post-opt">
                                                    <ul>
                                                        <li><i class="fa fa-calendar-check-o"></i> <span>{{ substr($post->created_at, 0, 10) }}</span></li>
                                                        <li><i class="fa fa-eye"></i> <span>{{ $post->views }}</span></li>
                                                        <li><i class="fa fa-tags"></i> <a href="#">Photography</a> , <a href="#">Design</a> </li>
                                                    </ul>
                                                </div>
                                                <span class="fw-separator"></span>
                                                <div class="list-single-main-item-title fl-wrap">
                                                    <h3>{{ __('front.Tags') }}</h3>
                                                </div>
                                                <div class="list-single-tags tags-stylwrap blog-tags">
                                                    <a href="#">Event</a>
                                                    <a href="#">Conference</a>
                                                    
                                                </div>
                                                <div class="share-holder hid-share">
                                                    <div class="showshare"><span>{{ __('front.Share') }} </span><i class="fa fa-share"></i></div>
                                                    <div class="share-container  isShare"></div>
                                                </div>
                                                <span class="fw-separator"></span>
                                                <div class="post-nav fl-wrap">
                                                    <a href="#" class="post-link prev-post-link"><i class="fa fa-angle-left"></i>{{ __('front.Prev') }} <span class="clearfix">The Sign of Experience</span></a>
                                                    <a href="#" class="post-link next-post-link"><i class="fa fa-angle-right"></i>{{ __('front.Next') }}<span class="clearfix">Dedicated to Results</span></a>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- article end -->       
                                        
                                                                      
                                    </div>
                                </div>
                                <!--box-widget-wrap -->
                                <div class="col-md-4">
                                    <div class="box-widget-wrap">
                                          
                                        
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap">
                                            <div class="box-widget-item-header">
                                                <h3>{{ __('front.Recent posts') }}: </h3>
                                            </div>
                                            <div class="box-widget widget-posts blog-widgets">
                                                <div class="box-widget-content">
                                                    <ul>
                                                        <li class="clearfix">
                                                            <a href="#"  class="widget-posts-img"><img src="images/blog/10.jpg"  alt=""></a>
                                                            <div class="widget-posts-descr">
                                                                <a href="#" title="">Event in City Mol</a>
                                                                <span class="widget-posts-date"><i class="fa fa-calendar-check-o"></i> 7 Mar 2017 </span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->     
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap">
                                            <div class="box-widget-item-header">
                                                <h3>{{ __('front.Tags') }}: </h3>
                                            </div>
                                            <div class="list-single-tags tags-stylwrap">
                                                <a href="#">Event</a>
                                                <a href="#">Design</a>                                                                              
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->     
                                                                            
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap">
                                            <div class="box-widget-item-header">
                                                <h3>{{ __('front.Categories') }} : </h3>
                                            </div>
                                            <div class="box-widget">
                                                <div class="box-widget-content">
                                                    <ul class="cat-item">
                                                        <li><a href="#">Standard</a> <span>(3)</span></li>
                                                        <li><a href="#">Video</a> <span>(6) </span></li>
                                                        <li><a href="#">Gallery</a> <span>(12) </span></li>
                                                        <li><a href="#">Quotes</a> <span>(4)</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->                    
                                    </div>
                                </div>
                                <!--box-widget-wrap end -->
                            </div>
                        </div>
                    </section>
                    <!--section end -->
                    
                </div>
                <!--content end --> 
            </div>
        
    </div>
</div>
@endsection