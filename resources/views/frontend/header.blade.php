<!DOCTYPE HTML>

<html lang="en">

    <head>
    <!--=============== basic  ===============-->
    <meta charset="UTF-8">
    <title>{{ __('front.Türk indirim')}}  | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--=============== css  ===============-->	
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/reset.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/plugins.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/style.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/color.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/datepicker.min.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('front/css/intlTelInput.css') }}">

    <!--=============== favicons ===============-->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
    <style>
    .list-author-widget-contacts  li span {
        min-width:unset;
    }
    .list-author-widget-header a {
        display: block
    }
    .list-author-widget-header img {
        bottom:unset
    }
    .custom-form textarea.is-invalid, .custom-form input[type="text"].is-invalid, .custom-form input[type=email].is-invalid, .custom-form input[type=password].is-invalid {
        border:1px solid #dc3545;
    }
    .card-listing .geodir-category-content p { 
        max-height: 90px; height:90px; width: 100%; overflow: hidden;
    }
    #error-message, #success-message {
        max-width: 0; max-height: 0;   overflow: hidden;  z-index:-1;   opacity: 0;
    }
    .custom-select {
        float: left;
        border: 1px solid #eee;
        background: #f9f9f9;
        width: 100%;
        padding: 15px 20px 15px 55px;
        border-radius: 6px;
        color: #666;
        font-size: 13px;
        -webkit-appearance: none;
        margin-bottom:20px;
    }
    </style>
    <script>
    var API = '{{ env("API_URL") }}',
    CardInfo = "{{ __('front.Card Info') }}",
    CardActivated = "{{ __('front.Card activated successfully') }}",
    CardNumber = "{{ __('front.Card Number') }}",
    ActivationCode = "{{ __('front.Activation Code') }}"
    ProfileInfoIncomplete = "{{ __('front.Please complete your profile information') }}",
    ChangePassword = "{{ __('front.Change Password') }}",
    OldPassword = "{{ __('front.Old Password') }}",
    NewPassword = "{{ __('front.New Password') }}",
    NewPasswordConfirmation = "{{ __('front.New Password Confirmation') }}",
    PasswordChanged = "{{ __('front.Password Changed') }}",
    PleaseWait = "{{ __('front.Please Wait') }}",
    AddCard = "{{ __('front.Add Card') }}";
    </script>
</head>

<body class="{{ config('app.site_language') ? config('app.site_language')->direction == 0 ? 'rtl' : 'ltr' : '' }}">

    @if ($errors->any())
    <div id="error-message">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

    @endif



    @if (\Session::has('success'))
    <div id="success-message">
        <span>{!! \Session::get('success') !!}</span>
    </div>

    @endif

    <!--loader-->

    <div class="loader-wrap">
    <!--   <div class="pin"></div>
    <div class="pulse"></div>-->

    </div>

    <!--loader end-->

    <!-- Main  -->

    <div id="main">
    <!-- header-->
    <header class="main-header dark-header fs-header sticky">
        <div class="header-inner">
            <div class="logo-holder">
                <a href="/"><img src="{{ asset('images/logo.png') }}" alt=""></a>
            </div>
            <form action="/offers" class="header-search vis-header-search">
                <div class="header-search-input-item">
                    <input name="term" type="text" placeholder="{{ __('front.Search')}}" value="{{ Request::input('term') ? Request::input('term'): ''}}"/>
                </div>
                <div class="header-search-select-item">
                    <select data-placeholder="All Categories" class="chosen-select" name="category">
                        <option value="">{{ __('front.Categories')}} </option>
                        @foreach ($global['categories'] as $category)
                            <option value="{{ $category->id }}" {{ Request::input('category') == $category->id ? 'selected="selected"':''}}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="header-search-select-item mobile-hide">
                    <select data-placeholder="All locations" class="chosen-select" name="city">
                        <option value="">{{ __('front.Locations')}} </option>
                        @foreach ($global['cities'] as $city) 
                            <option value="{{ $city->id }}" {{ Request::input('city') == $city->id ? 'selected="selected"':''}}>{{ $city->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button class="header-search-button" type="submit"><i class="fa fa-search"></i></button>
            </form>
            <div class="show-search-button"><i class="fa fa-search"></i> <span>{{ __('front.Search')}} </span></div>
            
            @guest
            <div class="show-reg-form modal-open"><i class="fa fa-sign-in"></i>{{ __('front.Sign In')}} </div>
            @endguest
            
            <!-- nav-button-wrap--> 
            <div class="nav-button-wrap color-bg">
                <div class="nav-button">
                    <span></span><span></span><span></span>
                </div>
            </div>
            <!-- nav-button-wrap end-->
            <!--  navigation --> 
            <div class="nav-holder main-menu">
                <nav>
                    <ul>
                        <li><a href="/" >{{ __('front.Home')}}  </a></li>
                        <li><a href="/companies">{{ __('front.Companies')}}</a></li>
                        <li><a href="/offers">{{ __('front.Offers')}}</a></li>
                        <li><a href="/cards">{{ __('front.Cards')}}</a></li> 
                        <li><a href="/about">{{ __('front.About')}}</a></li>
                        <li><a href="/contact">{{ __('front.Contact')}} </a></li>
                        @auth
                        <li><a href="/account">{{ __('front.My account')}} </a></li>
                        @endauth 
                        <li><a href="#"><i class="fas fa-globe-africa"></i>
                            <ul>
                                @foreach ($global['languages'] as $language)


                                <a href="{{ url('/language/' . $language->code) }}">{{ $language->name }}</a>
                                @endforeach 
                            </ul>
                            </a>
                        </li>
                    <ul>    
                </nav>
            </div>
            <!-- navigation  end -->
        </div>
    </header>