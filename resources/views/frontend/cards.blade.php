@extends('/frontend/layout')

@section('title')
{{ __('front.Cards') }}
@endsection

@section('content')	
            <!-- wrapper -->	
            <div id="wrapper">
                <!--  content -->	
                <div class="content">
                    <!--  section  --> 
                    <section class="parallax-section" data-scrollax-parent="true" id="sec1">
                        <div class="bg par-elem "  data-bg="images/cards.png" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span>{{__('front.Our Cards') }}</span></h2>
                            </div>
                        </div>
                        
                    </section>
                    <!--  section end --> 
                    <!--section- cards -->  
                     <!--section- cards -->  
                        <section id="cards" class="hide-mobile-card">
                            <div class="container">
                                <div class="section-title">
                                    <h2>{{__('front.Buy turk indirim cards') }} </h2>
                                    <span class="section-separator"></span>
                                    <p>{{__('front.Buy our cards online and enjoy unlimited discounts and offers') }}</p>
                                </div>
                                <div class="pricing-wrap fl-wrap">
                                    
                                    <!-- price-item-->
                                    <div class="price-item">
                                            <div class="price-head op2">
                                                <img src="images/cards/tatil.jpg" alt="">
                                            </div>
                                            <div class="price-content fl-wrap">
                                                <div class="price-num fl-wrap">
                                                    
                                                    <img class="price-img" src="images/cards/price-2.png" alt="">
                                                    <div class="price-num-desc">{{__('front.3 Months') }}</div>
                                                </div>
                                                <div class="price-desc fl-wrap">
                                                    <ul>
                                                        <li>{{__('front.For tourists') }}</li>
                                                        <li>{{__('front.Three months active time from the first date use') }}</li>
                                                        <li>{{__('front.All categories included') }}</li>
                                                        <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                    </ul>
                                                    <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <!-- price-item end-->
                                    
                                    <!-- price-item-->
                                    <div class="price-item">
                                        <div class="price-head">
                                            <img src="images/cards/turk-kart.jpg" alt="">
                                        </div>
                                        <div class="price-content fl-wrap">
                                            <div class="price-num fl-wrap">
                                                <img class="price-img" src="images/cards/price-3.png" alt="">
                                                <div class="price-num-desc">{{__('front.Per year') }}</div>
                                            </div>
                                            <div class="price-desc fl-wrap">
                                                <ul>
                                                    <li>{{__('front.For residence in Turkey') }}</li>
                                                    <li>{{__('front.One year active time from the first use date') }}</li>
                                                    <li>{{__('front.All categories included') }}</li>
                                                    <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                </ul>
                                                <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- price-item end--> 
                                    <!-- price-item-->
                                    <div class="price-item">
                                            <div class="price-head op1">
                                                <img src="images/cards/ogrenci.jpg" alt="">
                                            </div>
                                            <div class="price-content fl-wrap">
                                                <div class="price-num fl-wrap">
                                                    <img class="price-img" src="images/cards/price-1.png" alt="">
                                                    <div class="price-num-desc">{{__('front.Per year') }}</div>
                                                </div>
                                                <div class="price-desc fl-wrap">
                                                    <ul>
                                                        <li>{{__('front.For students') }}</li>
                                                        <li>{{__('front.One year active time from the first use date') }}</li>
                                                        <li>{{__('front.All categories included') }}</li>
                                                        <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                    </ul>
                                                    <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- price-item end-->
                                        <!-- price-item-->
                                        <div class="price-item">
                                            <div class="price-head">
                                                <img src="images/cards/ozel.jpg" alt="">
                                            </div>
                                            <div class="price-content fl-wrap">
                                                <div class="price-num fl-wrap">
                                                    <img class="price-img" src="images/cards/price-4.png" alt="">
                                                    <div class="price-num-desc">{{__('front.Per year') }}</div>
                                                </div>
                                                <div class="price-desc fl-wrap">
                                                    <ul>
                                                        <li>{{__('front.For martyrs families and people  with disabilities') }}</li>
                                                        <li>{{__('front.One year active time from the first use date') }}</li>
                                                        <li>{{__('front.All categories included') }}</li>
                                                        <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                    </ul>
                                                    <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- price-item end--> 
                                        
                                </div>
                                <div class="instruction">
                                    <p>{{__('front.how to activate the card') }}</p> <a href="#" class="price-link">{{__('front.download PDF file') }}</a>
                                </div>
                                <!-- about-wrap end  --> 
                            </div>
                        </section>
                        <!-----mobile-cards----->
                        <section id="cards" class="hide-desktop-card">
                            <div class="container">
                                <div class="section-title">
                                    <h2>{{__('front.Buy turk indirim cards') }} </h2>
                                    <span class="section-separator"></span>
                                    <p>{{__('front.Buy our cards online and enjoy unlimited discounts and offers') }}</p>
                                </div>
                                <div class="pricing-wrap fl-wrap">
                                    
                                    <!-- price-item-1-->
                                    <div class="price-item">
                                            <div class="price-head op2">
                                                <img src="images/cards/tatil.jpg" alt="">
                                            </div>
                                            <div class="price-content fl-wrap">
                                                <div class="price-num fl-wrap">
                                                    <img class="price-img" src="images/cards/price-2.png" alt="">
                                                    <div class="price-num-desc">{{__('front.3 Months') }}</div>
                                                </div>
                                                <div class="price-desc fl-wrap">
                                                    <div class="accordion">
                                                        <a class="toggle act-accordion" href="#"> {{__('front.For tourists') }} <i class="fa fa-angle-down"></i></a>
                                                        
                                                        <div class="accordion-inner" aria-expanded="false">
                                                                <ul>
                                                                    <li>3{{__('front.Three months active time from the first date use') }}</li>
                                                                    <li>{{__('front.All categories included') }}</li>
                                                                    <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                                </ul>
                                                        </div>
                                                    </div>
                                                    
                                                    <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <!-- price-item end-->
                                    
                                   
                                    <!-- price-item-2-->
                                    <div class="price-item">
                                        <div class="price-head">
                                            <img src="images/cards/turk-kart.jpg" alt="">
                                        </div>
                                        <div class="price-content fl-wrap">
                                            <div class="price-num fl-wrap">
                                                <img class="price-img" src="images/cards/price-3.png" alt="">
                                                <div class="price-num-desc">{{__('front.Per year') }}</div>
                                            </div>
                                            <div class="price-desc fl-wrap">
                                                    <div class="accordion">
                                                        <a class="toggle act-accordion" href="#"> {{__('front.For residence in Turkey') }} <i class="fa fa-angle-down"></i></a>
                                                        
                                                        <div class="accordion-inner" aria-expanded="false">
                                                                <ul>
                                                                    <li>{{__('front.One year active time from the first use date') }}</li>
                                                                    <li>{{__('front.All categories included') }}</li>
                                                                    <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                                </ul>
                                                        </div>
                                                    </div>
                                                
                                                <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- price-item end-->  
                                    <!-- price-item-3-->
                                    <div class="price-item">
                                            <div class="price-head op1">
                                                <img src="images/cards/ogrenci.jpg" alt="">
                                            </div>
                                            <div class="price-content fl-wrap">
                                                <div class="price-num fl-wrap">
                                                    <img class="price-img" src="images/cards/price-1.png" alt="">
                                                    <div class="price-num-desc">{{__('front.Per year') }}</div>
                                                </div>
                                                <div class="price-desc fl-wrap">
                                                        <div class="accordion">
                                                            <a class="toggle act-accordion" href="#"> {{__('front.For students') }} <i class="fa fa-angle-down"></i></a>
                                                            
                                                            <div class="accordion-inner" aria-expanded="false">
                                                                    <ul>
                                                                        <li>{{__('front.One year active time from the first use date') }}</li>
                                                                        <li>{{__('front.All categories included') }}</li>
                                                                        <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                                    </ul>
                                                            </div>
                                                        </div>
                                                    
                                                    <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- price-item end-->
                                        <!-- price-item-4-->
                                        <div class="price-item">
                                            <div class="price-head">
                                                <img src="images/cards/ozel.jpg" alt="">
                                            </div>
                                            <div class="price-content fl-wrap">
                                                <div class="price-num fl-wrap">
                                                    <img class="price-img" src="images/cards/price-4.png" alt="">
                                                    <div class="price-num-desc">{{__('front.Per year') }}</div>
                                                </div>
                                                <div class="price-desc fl-wrap">
                                                        <div class="accordion">
                                                            <a class="toggle act-accordion" href="#"> {{__('front.For martyrs families and people  with disabilities') }} <i class="fa fa-angle-down"></i></a>
                                                            
                                                            <div class="accordion-inner" aria-expanded="false">
                                                                    <ul>
                                                                        <li>{{__('front.One year active time from the first use date') }}</li>
                                                                        <li>{{__('front.All categories included') }}</li>
                                                                        <li>{{__('front.Unlimited discounts and offers') }}</li>
                                                                    </ul>
                                                            </div>
                                                        </div>
                                                    
                                                    <a href="#" class="price-link modal-open2">{{__('front.Order the card now') }}</a>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- price-item end-->  
                                                                                        
                                </div>
                                <div class="instruction-activate">
                                    <p>{{__('front.how to activate card') }}</p>
                                    <a href="#" class="price-link">{{__('front.download PDF file') }}</a>
                                </div> 
                                <!-- about-wrap end  --> 
                            </div>
                        </section>
                    <!--section -->
                    @include('frontend._partials.getintouch')
                    <!--  section end --> 
                    <div class="limit-box"></div>
                </div>
                <!--  content end -->  
            </div>
            <!-- wrapper end -->
@endsection