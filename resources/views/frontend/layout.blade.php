@include('/frontend/header')

    @if ($errors->any())

        <div class="">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (\Session::has('success'))
        <div class="">
            <span>{!! \Session::get('success') !!}</span>
        </div>
    @endif

    @yield('content')


@include('frontend/footer')