@extends('/frontend/layout')

@section('content')
    <div id="wrapper">
        <div class="content">
            <section class="parallax-section small-par color-bg">
                <div class="shapes-bg-big"></div>
                <div class="container">
                    <div class="section-title center-align">
                        <div class="breadcrumbs fl-wrap"><a href="#">{{__('front.Home') }}</a><a href="#">{{__('front.Companies') }}</a><span>{{ $branch->name }}</span></div>
                        <h2><span>{{__('front.Company') }} : {{ $branch->name }}</span></h2>
                        <div class="user-profile-avatar"><img src="{{ url('uploads/' . $branch->logo) }}" alt=""></div>
                        <div class="user-profile-rating clearfix">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5">
                                <span>(37 {{__('front.Reviews') }})</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-sec-link">
                    <div class="container"><a href="#sec1" class="custom-scroll-link">{{__('front.About Company') }}</a></div>
                </div>
            </section>
            <section class="gray-bg" id="sec1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="list-single-main-item fl-wrap">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>{{__('front.About') }} <span> {{ $branch->name }}</span></h3>
                                </div>
                                <div>
                                    {!! $branch->description !!}
                                </div>
                                
                                
                                <a href="{{ $branch->seller->website }}" target="_blank" class="btn transparent-btn float-btn">{{__('front.Visit Website') }} <i class="fa fa-angle-right"></i></a>
                            </div>
                            <div class="listsearch-header fl-wrap">
                                <h3>{{ $branch->name }} {{__('front.Offers') }}</h3>
                                <div class="listing-view-layout">
                                    <ul>
                                        <li><a class="grid active" href="#"><i class="fa fa-th-large"></i></a></li>
                                        <li><a class="list" href="#"><i class="fa fa-list-ul"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="list-main-wrap fl-wrap card-listing ">
                                @foreach ($branch->offers as $offer)
                                <div class="listing-item">
                                    <article class="geodir-category-listing fl-wrap">
                                        <div class="geodir-category-img">
                                            
                                            <a href="{{url('/offer/' . $offer->id)}}"><img src="{{ $offer->thumb ? url('uploads/' . $offer->thumb) : '' }}" alt=""></a>
                                            <a href="#"><div class="overlay"></div></a>
                                            
                                        </div>
                                        <div class="geodir-category-content fl-wrap">
                                            <a class="listing-geodir-category" href="{{url('/category/'. $offer->categories[0]->id)}}"><i class="{{ $offer->categories[0]->code }}"></i></a>
                                            <div class="listing-avatar"><a href="{{url('/company/'. $offer->branches[0]->id)}}"><img src="{{ url('uploads/'. $branch->seller->logo) }}" alt=""></a>
                                                <span class="avatar-tooltip"><strong>{{ $branch->name }}</strong></span>
                                            </div>
                                            <h3><a href="#">{{ $offer['name'] }}</a></h3>
                                            <p>{!! $offer['excerpt'] !!}</p>
                                            <div class="geodir-category-options fl-wrap">
                                                <div class="listing-rating card-popup-rainingvis" data-starrating2="5">
                                                    <span>{{ $offer->views}}</span>
                                                </div>
                                                <div class="geodir-category-location"><a href="{{ url('/offer/' . $offer['id']) }}"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $branch->city['name'] }} - {{ $branch->province['name'] }}</a></div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                @endforeach                         
                                <div class="clearfix"></div>
                            </div>                        
                        </div>
                        <div class="col-md-4">
                            <div class="fl-wrap">
                                <!--box-widget-item -->
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget-item-header">
                                        <h3>{{__('front.User Contacts') }} : </h3>
                                    </div>
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="list-author-widget-contacts list-item-widget-contacts">
                                                <ul>
                                                    <li><span><i class="fa fa-phone"></i></span> <a href="tel:{{ $branch->phone }}"> {{ parsePhoneNumber($branch->phone) }}</a></li>
                                                    <li><span><i class="fa fa-mobile"></i></span> <a href="tel:{{ $branch->mobile }}"> {{ parsePhoneNumber($branch->mobile) }}</a></li>
                                                    <li><span><i class="fa fa-envelope-o"></i></span> <a href="mailto:{{ $branch->email }}">{{ $branch->email }}</a></li>
                                                    <li><span><i class="fa fa-globe"></i></span> <a href="{{ $branch->seller->website }}">{{ $branch->seller->website }}</a></li>
                                                    <li><span><i class="fa fa-map-marker"></i></span> <a href="#">{{ $branch->address1 }} {{ $branch->address2 }} {{ $branch->district['name'] }} {{ $branch->city['name'] }} - {{ $branch->province['name'] }}</a></li>

                                                </ul>
                                            </div>
                                            <div class="list-widget-social">
                                                <ul>
                                                    <li><a href="{{ $branch->seller->facebook }}" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="{{ $branch->seller->twitter }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="{{ $branch->seller->instagram }}" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                                                    <li><a href="{{ $branch->seller->youtube }}" target="_blank" ><i class="fab fa-youtube"></i></a></li>
                                                    <li><a href="https://wa.me/{{ $branch->mobile }}" target="_blank" ><i class="fa fa-whatsapp"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget-item-header">
                                        <h3>{{__('front.Get in Touch') }} : </h3>
                                    </div>
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <form id="add-comment" class="add-comment custom-form">
                                                <fieldset>
                                                    <label><i class="fa fa-user-o"></i></label>
                                                    <input type="text" placeholder="{{__('front.Your Name') }} *" value=""/>
                                                    <div class="clearfix"></div>
                                                    <label><i class="fa fa-envelope-o"></i>  </label>
                                                    <input type="text" placeholder="{{__('front.Email') }}*" value=""/>
                                                    <textarea cols="40" rows="3" placeholder="{{__('front.Your message') }}:"></textarea>
                                                </fieldset>
                                                <button class="btn  big-btn  color-bg flat-btn">{{__('front.Send Message') }}<i class="fa fa-angle-right"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--box-widget-item end -->                                             
                            </div>
                        </div>
                        <!--box-widget-wrap end-->
                    </div>
                </div>
            </section>
            <!-- section end -->
            <div class="limit-box fl-wrap"></div>
            <!--section -->
            @include('frontend._partials.getintouch')
            <!-- section end -->
        </div>
        <!-- content end -->
    </div>
    @endsection