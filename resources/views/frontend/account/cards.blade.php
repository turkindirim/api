@extends('/frontend/layout')

@section('title')
{{ __('front.My cards') }}
@endsection

@section('content')
<style>
.dashboard-listing-table-image {
    position: relative;
}
.card_number {
    position: absolute;
    bottom:0;
    width: 100%;
    text-align: center;
    font-weight: bold
}
</style>
<div id="wrapper">
    <!--content -->  
    <div class="content">
        <!--section --> 
        <section id="sec1">
            <!-- container -->
            <div class="container">
                <!-- profile-edit-wrap -->
                <div class="profile-edit-wrap">
                    <div class="row">
                        @include('frontend.account.sidebar')
                        <div class="col-md-9">
                            <div class="dashboard-list-box fl-wrap">
                                <div class="dashboard-header fl-wrap">
                                    <h3>{{ __('front.My Cards') }}</h3>
                                </div>
                                @foreach ($cards as $card)
                                    <div class="dashboard-list">
                                        <div class="dashboard-message">
                                            <div class="dashboard-listing-table-image">
                                                <a href="#"><img src="{{ url('uploads/' . $card->type->logo) }}" alt=""></a>
                                                <div class="card_number">{{ $card->number }}</div>
                                            </div>
                                            <div class="dashboard-listing-table-text">
                                                <h4><a href="#">{{ $card->type->name }}</a></h4>
                                            </div>
                                            <div class="dashboard-listing-table-text">
                                                <span class="dashboard-listing-table-address card-date"><b>{{ __('front.Activation date') }}: </b> {{ $card->activated_at ? $card->activated_at->format('Y-m-d') : '' }} </span>
                                            </div>
                                            <div class="dashboard-listing-table-text">
                                                <span class="dashboard-listing-table-address card-date"><b>{{ __('front.Valid until') }}: </b > {{ $card->activated_at ? substr($card->expired_at, 0, 10) : '' }} </span>
                                            </div>
                                            <div class="dashboard-listing-table-text">
                                                <span class="dashboard-listing-table-address card-date"><b>{{ __('front.Points') }}: </b> {{ $card->transactions->count() }} </span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!--profile-edit-wrap end -->
            </div>
            <!--container end -->
        </section>
        <!-- section end -->	
        <div class="limit-box fl-wrap"></div>
    </div>
</div>

@include('frontend.account.activatecard_modal')
@endsection