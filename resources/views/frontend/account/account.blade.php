@extends('/frontend/layout')

@section('title')
{{ __('front.My account') }}
@endsection

@section('content')
@php
    $user = auth()->user();
@endphp
<div id="wrapper">
    <!--content -->  
    <div class="content">
        <!--section --> 
        <section id="sec1">
            <!-- container -->
            <div class="container">
                <!-- profile-edit-wrap -->
                <div class="profile-edit-wrap">
                    <div class="row">
                        @include('frontend.account.sidebar')
                        <div class="col-md-9">
                            <form action="/account" method="post" class="custom-form">
                                @csrf
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <label>{{ __('front.First Name') }}</label>
                                        <input required="required" type="text" name="firstname" value="{{ $user->firstname }}">
                                        <label>{{ __('front.Last Name') }}</label>
                                        <input required="required" type="text" name="lastname" value="{{ $user->lastname }}">
                                        <label>{{ __('front.Email') }}</label>
                                        <input required="required" type="text" name="email" value="{{ $user->email }}">
                                        <label>{{ __('front.Mobile') }}</label>
                                        <input required="required" type="text" name="mobilell" value="{{ $user->mobile ?? '+90' }}" class="mobile">
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <label for="">{{ __('front.Nationality') }}</label>
                                        <select name="nationality" class="custom-select">
                                            @foreach ($countries as $country)
                                                <option value="{{ $country->id }}" {{ $country->id == $user->nationality ? 'selected="selected"' : '' }}>{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                        <label>{{ __('front.Address') }}</label>
                                        <input required="required" type="text" name="address1"  value="{{ $user->address1 }}">
                                        
                                        <label for="">{{ __('front.Gender') }}</label>
                                        <select name="gender" class="custom-select">
                                            <option value="0">{{ __('front.Female') }}</option>
                                            <option value="1" {{ $user->gender == '1' ? 'selected="selected"':'' }}>{{ __('front.Male') }}</option>
                                        </select>
                                        <label for="">{{ __('front.Birthdate') }}</label>
                                        <input type="text" class="jqdatepicker" name="dob" value="{{ $user->dob }}">
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button required="required" type="submit" class="btn big-btn color-bg flat-btn">{{ __('front.Submit') }}</button>
                                    </div>    
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
                <!--profile-edit-wrap end -->
            </div>
            <!--container end -->
        </section>
        <!-- section end -->	
        <div class="limit-box fl-wrap"></div>
    </div>
</div>

@include('frontend.account.activatecard_modal')

@endsection