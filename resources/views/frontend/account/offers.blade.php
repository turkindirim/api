
@extends('/frontend/layout')

@section('title')
{{ __('front.My Offers') }}
@endsection

@section('content')
<div id="wrapper">
    <!--content -->  
    <div class="content">
        <!--section --> 
        <section id="sec1">
            <!-- container -->
            <div class="container">
                <!-- profile-edit-wrap -->
                <div class="profile-edit-wrap">
                    <div class="row">
                        @include('frontend.account.sidebar')
                        <div class="col-md-9">
                            <!-- list-main-wrap-->
                            <div class="list-main-wrap fl-wrap card-listing">
                                
                            
                                <div class="slick-slide-item">
                                        @foreach ($offers as $offer)
                                        <!-- listing-item -->
                                        <div class="listing-item">
                                            <article class="geodir-category-listing fl-wrap">
                                                <div class="geodir-category-img">
                                                    
                                                    <a href="{{ url('offer/' . $offer->id . '/' . urlencode($offer->name)) }}" title="{{ $offer->name }}"><img src="{{ url('uploads/' . $offer->thumb) }}" alt=""></a>
                                                    <a href="{{ url('offer/' . $offer->id . '/' . urlencode($offer->name)) }}" title="{{ $offer->name }}"><div class="overlay"></div></a>
                                                    
                                                </div>
                                                <div class="geodir-category-content fl-wrap">
                                                    <a class="listing-geodir-category" href="{{url('category/'. $offer->categories[0]->id)}}"><i class="{{ $offer->categories[0]->code }}"></i></a>
                                                    <div class="listing-avatar"><a href="{{url('company/'. $offer->branches[0]->id)}}"><img src="{{ url('uploads/'. $offer->branches[0]->seller->logo) }}" alt=""></a>
                                                        <span class="avatar-tooltip"><strong>{{ $offer->branches[0]->name }}</strong></span>
                                                    </div>
                                                
                                                    <h3><a href="{{ url('offer/' . $offer->id) }}" title="{{ $offer->name }}">{{ $offer->name }}</a></h3>
                                                    <p>{!! $offer->excerpt !!}</p>
                                                    <div class="geodir-category-options fl-wrap">
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5">
                                                            <span>{{$offer->views. " " .__('front.Views') }}</span>
                                                        </div>
                                                        <div class="geodir-category-location"><a href="{{ url('offer/' . $offer->id) }}"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $offer->branches[0]->city['name'] }} - {{ $offer->branches[0]->province['name'] }}</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <!-- listing-item end-->
                                            @endforeach 
                                </div> 
                                @if ($total > 1)
                                @php
                                $queryParams = '';
                                if (Request::input('term')) {
                                    $queryParams .= '&term=' . urlencode(Request::input('term'));
                                }
                                if (Request::input('category')) {
                                    $queryParams .= '&category=' . (Request::input('category'));
                                }
                                if (Request::input('province')) {
                                    $queryParams .= '&province=' . urlencode(Request::input('province'));
                                }
                                @endphp
                                
                                @endif
                            </div>
                            <!-- list-main-wrap end-->  
                        </div>
                    </div>
                </div>
                <!--profile-edit-wrap end -->
            </div>
            <!--container end -->
        </section>
        <!-- section end -->	
        <div class="limit-box fl-wrap"></div>
    </div>
</div>

@endsection