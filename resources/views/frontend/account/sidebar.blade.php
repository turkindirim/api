@php
    $user = auth()->user();
@endphp
<div class="col-md-3">
    <div class="fixed-bar fl-wrap">
        <div class="user-profile-menu-wrap fl-wrap">
            <!-- user-profile-menu-->
            <div class="user-profile-menu">
                <h3><a href="/account">{{ __('front.Dashboard') }}</a></h3>
                <ul>
                    <li><a href="/account/edit"><i class="fa fa-user-o"></i> {{ __('front.Edit Profile') }}</a></li>
                    <li><a href="#" class="changePassword"><i class="fa fa-unlock-alt"></i> {{ __('front.Change Password') }}</a></li>
                </ul>
            </div>
            <div class="user-profile-menu">
                <ul>
                    <li><a href="#" class="activateCard show-reg-form" style="top:unset"><i class="fa fa-plus-square-o"></i> {{ __('front.Activate a card') }}</a></li>                    
                </ul>
            </div>
            <!-- user-profile-menu end-->                                        
            <a href="{{ route('user.logout') }}" class="log-out-btn">{{ __('front.Logout') }}</a>
        </div>
    </div>
</div>




