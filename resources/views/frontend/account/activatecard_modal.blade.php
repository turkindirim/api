
@php
$user = auth()->user();   
@endphp
<div class="activateCardModal modal">
    <div class="main-overlay"></div>
    <div class="main-register-holder">
        <div class="main-register fl-wrap">
            <div class="close-reg"><i class="fa fa-times"></i></div>
            <h3>{{ __('front.Activate a card') }}</strong></span></h3>
            <div id="tabs-container" style="margin-top:0">
                <div id="add_card_1">
                    <div id="errors" class="alert alert-danger"></div>
                        <div class="custom-form">
                            <form method="post" action="/account/cards" name="registerform" class="main-register-form">
                                @csrf
                                <label >{{ __('front.Card Number') }}</label>
                                <input type="text" id="new_card_number" name="number" value="" required> 
                                <label>{{ __('front.Activation Code') }}</label>
                                <input type="text" id="new_Card_activation_code" name="activation_code" value="" required>
                                <label>{{ __('front.Mobile') }}</label>
                                <input type="text" id="new_card_mobile" name="mobile" class="phone" value="{{ $user->mobile ? $user->mobile : '' }}" required>  
                                <button type="submit" class="log-submit-btn signin_button" id="addNewCard" ><span>{{ __('front.Add Card') }}</span></button> 
                            </form>
                        </div>
                    </div>
                <div id="add_card_2" style="display:none">
                    <svg width="90" height="90" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve" style="margin-top:30px">
                    <path style="fill:#6AC259;" d="M213.333,0C95.518,0,0,95.514,0,213.333s95.518,213.333,213.333,213.333
                        c117.828,0,213.333-95.514,213.333-213.333S331.157,0,213.333,0z M174.199,322.918l-93.935-93.931l31.309-31.309l62.626,62.622
                        l140.894-140.898l31.309,31.309L174.199,322.918z"/>
                    </svg>   
                    <h4>{{ __('front.Card activated successfully') }}</h4> 
                </div>    
                <div id="add_card_3" style="display:none">
                    <div id="card_image"></div>  
                    <h3 id="card_titla" style="margin 20px 0"></h3>
                    <h5><b>{{ __('front.Valid until') }}:</b> <span id="valid_days"></span> {{ __('front.days since first usage') }}</h5>
                    <h5><b>{{ __('front.Points') }}:</b> 0</h5>
                </div>        
            </div>
        </div>
    </div>
</div> 