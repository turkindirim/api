@extends('/frontend/layout')

@section('content')
<div id="wrapper">
    <!--content -->  
    <div class="content">
        <!--section --> 
        <section id="sec1">
            <!-- container -->
            <div class="container">
                <!-- profile-edit-wrap -->
                <div class="profile-edit-wrap">
                    <div class="row">
                        @include('frontend.account.sidebar')
                        <div class="col-md-9">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="profile-edit-container">
                                            <div class="custom-form">
                                                <label>{{ __('front.Card Number') }}</label>
                                                <input id="card_number" type="text" placeholder="{{ __('front.Card Number') }}" value="" maxlength="16"/>
                                                <label>{{ __('front.Activation Code') }}</label>
                                                <input id="activation_code" type="text" placeholder="{{ __('front.Activation Code') }}" value="" maxlength="5"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                            <button id="addCard" class="btn  big-btn  color-bg flat-btn">{{ __('front.Add Card') }}<i class="fa fa-angle-right"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--profile-edit-wrap end -->
            </div>
            <!--container end -->
        </section>
        <!-- section end -->	
        <div class="limit-box fl-wrap"></div>
    </div>
</div>

@endsection

@section('after_script')
    <script>
    $(document).ready(function(){
        $('#addCard').click(function(e) {
        // e.preventDefault();
        $.ajax({
            url: "{{ route('user.cards.store') }}",
            method: 'post',
            data: {
                number: $('#card_number').val(),
                activation_code: $('#activation_code').val(),
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(result) {
                window.alert('OK');
                $('#activation_code').val('');
                $('#card_number').val('');
            },
            error: function (jqXHR, exception) {
                window.alert(jqXHR.responseJSON.meta.messages);
            }
        });
    });
    });
    </script>
@endsection