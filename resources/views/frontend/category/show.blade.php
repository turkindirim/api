@extends('/frontend/layout')

@section('content')

<?php //dd($category); ?>

<div id="wrapper">
                <!--  content  --> 
                <div class="content">
                    <!--  section  end--> 
                    <!--  section  --> 
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{ url('uploads/' . $category->header) }}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay"></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span>{{$category->name}}</span></h2>
                            </div>
                        </div>
                        
                    </section>
                    <!--  section  end--> 
                    <!--  section  --> 
                    <section class="gray-bg no-pading no-top-padding" id="sec1">
                        <div class="col-list-wrap fh-col-list-wrap  left-list">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="listsearch-header fl-wrap">
                                            <h3>{{$category->name}}</h3>
                                            <div class="listing-view-layout">
                                                <ul>
                                                    <li><a class="grid active" href="#"><i class="fa fa-th-large"></i></a></li>
                                                    <li><a class="list" href="#"><i class="fa fa-list-ul"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- list-main-wrap-->
                                        <div class="list-main-wrap fl-wrap card-listing">
                                                <div class="slick-slide-item">
                                            @foreach ($category->offers as $offer)
                                                    <!-- listing-item -->
                                                    <div class="listing-item-3 listing-item">
                                                        <article class="geodir-category-listing fl-wrap">
                                                            <div class="geodir-category-img">
                                                                
                                                                <a href="{{ url('offer/' . $offer->id) }}" title="{{ $offer->name }}"><img src="{{ url('uploads/' . $offer->thumb) }}" alt=""></a>
                                                                <a href="{{ url('offer/' . $offer->id) }}" title="{{ $offer->name }}"><div class="overlay"></div></a>
                                                                
                                                            </div>
                                                            <div class="geodir-category-content fl-wrap">
                                                                <a class="listing-geodir-category" href="#"><i class="{{ $offer->categories[0] ? $offer->categories[0]->code : '' }}"></i></a>
                                                                <div class="listing-avatar"><a href="{{ count($offer->branches) ? url('company/' . $offer->branches[0]->id) : ''}}"><img src="{{ count($offer->branches) ? url('uploads/'. $offer->branches[0]->seller->logo) : '' }}" alt=""></a>
                                                                    <span class="avatar-tooltip"><strong>{{ count($offer->branches) ? $offer->branches[0]->name : '' }}</strong></span>
                                                                </div>
                                                            
                                                                <h3><a href="{{ url('offer/' . $offer->id) }}" title="{{ $offer->name }}">{{ $offer->name }}</a></h3>
                                                                <p>{!! $offer->excerpt !!}</p>
                                                                <div class="geodir-category-options fl-wrap">
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5">
                                                                        <span>{{$offer->views. " " .__('front.Views') }}</span>
                                                                    </div>
                                                                    <div class="geodir-category-location"><a href="{{ url('offer/' . $offer->id) }}"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ count($offer->branches) ? $offer->branches[0]->city['name'] : ''}} - {{ count($offer->branches) ? $offer->branches[0]->province['name'] : '' }}</a></div>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                    <!-- listing-item end--> 
                                                    @endforeach 
                                                </div> 
                                          
                                                
                                        </div>
                                        <!-- list-main-wrap end-->                           
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--  section  end--> 
                    <div class="limit-box fl-wrap"></div>
                    <!--  section  --> 
                    @include('frontend._partials.getintouch')
                    <!--  section  end--> 
                </div>
                <!-- content end--> 
            </div>



                                               
                                        

@endsection