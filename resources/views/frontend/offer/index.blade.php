@extends('/frontend/layout')

@section('title')
{{ __('front.Offers') }}
@endsection

@section('content')

<div id="wrapper">
    <div class="content">
        <section class="parallax-section" data-scrollax-parent="true">
            <div class="bg par-elem" data-bg="{{ asset('images/offers.png') }}" data-scrollax="properties: { translateY: '30%' }"></div>
            <div class="overlay"></div>
            <div class="container">
                <div class="section-title center-align">
                    <h2><span>{{__('front.Offers') }}</span></h2>
                </div>
            </div>
        </section>
        <section class="gray-bg no-pading no-top-padding" id="sec1">
            <div class="col-list-wrap fh-col-list-wrap  left-list">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="listsearch-header fl-wrap">
                                <h3>{{__('front.Results For :') }} <span>{{__('front.All Offers') }}</span></h3>
                                <div class="listing-view-layout">
                                    <ul>
                                        <li><a class="grid active" href="#"><i class="fa fa-th-large"></i></a></li>
                                        <li><a class="list" href="#"><i class="fa fa-list-ul"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- list-main-wrap-->
                            <div class="list-main-wrap fl-wrap card-listing">
                                
                            
                                <div class="slick-slide-item">
                                        @foreach ($offers as $offer)
                                        <!-- listing-item -->
                                        <div class="listing-item-3 listing-item">
                                            <article class="geodir-category-listing fl-wrap">
                                                <div class="geodir-category-img">
                                                    <a href="{{ url('offer/' . $offer->id . '/' . urlencode($offer->name)) }}" title="{{ $offer->name }}"><img src="{{ url('uploads/' . $offer->thumb) }}" alt=""></a>
                                                    <a href="{{ url('offer/' . $offer->id . '/' . urlencode($offer->name)) }}" title="{{ $offer->name }}"><div class="overlay"></div></a>
                                                </div>
                                                <div class="geodir-category-content fl-wrap">
                                                    <a class="listing-geodir-category" href="{{ $offer->categories->count() ? url('category/'. $offer->categories[0]->id) : '#'}}"><i class="{{ $offer->categories->count() ? $offer->categories[0]->code : '' }}"></i></a>
                                                    <div class="listing-avatar"><a href="{{ $offer->branches->count() ? url('company/'. $offer->branches[0]->id) : '' }}"><img src="{{ $offer->branches->count() ? url('uploads/'. $offer->branches[0]->seller->logo) : '' }}" alt=""></a>
                                                        <span class="avatar-tooltip"><strong>{{ $offer->branches->count() ? $offer->branches[0]->name : '' }}</strong></span>
                                                    </div>
                                                    <h3><a href="{{ url('offer/' . $offer->id) }}" title="{{ $offer->name }}">{{ $offer->name }}</a></h3>
                                                    <p>{!! $offer->excerpt !!}</p>
                                                    <div class="geodir-category-options fl-wrap">
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="{{ $offer->average_rate }}">
                                                            <span>{{$offer->views . ' ' . trans_choice('front.Views', $offer->views) }}</span>
                                                        </div>
                                                        <div class="geodir-category-location"><a href="{{ url('offer/' . $offer->id . '/' . urlencode($offer->name)) }}"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $offer->branches->count() ? $offer->branches[0]->city['name'] : '' }} - {{ $offer->branches->count() ? $offer->branches[0]->province['name'] : '' }}</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                            @endforeach 
                                </div> 
                                @if ($total > 1)
                                @php
                                $queryParams = '';
                                if (Request::input('term')) {
                                    $queryParams .= '&term=' . urlencode(Request::input('term'));
                                }
                                if (Request::input('category')) {
                                    $queryParams .= '&category=' . (Request::input('category'));
                                }
                                if (Request::input('city')) {
                                    $queryParams .= '&city=' . urlencode(Request::input('city'));
                                }
                                @endphp
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pagination">
                                            @include('frontend._partials.pagination')
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>                    
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="limit-box fl-wrap"></div>
        @include('frontend._partials.getintouch')
    </div>
</div>



                                               
                                        

@endsection