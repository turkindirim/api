@extends('/frontend/layout')


@section('title')
{{ $offer->name }}
@endsection

@section('content')

<div id="wrapper">
    <!--  content  --> 
    <div class="content">
        <!--  section  --> 
        <section class="parallax-section single-par list-single-section" data-scrollax-parent="true" id="sec1">
         
        <div class="bg par-elem "  data-bg="{{ url('uploads/' . $offer->background) }}" data-scrollax="properties: { translateY: '30%' }"></div>
        
        <div class="overlay"></div>
            <div class="bubble-bg"></div>
            <div class="list-single-header absolute-header fl-wrap">
                <div class="container">
                    <div class="list-single-header-item">
                        <div class="list-single-header-item-opt fl-wrap">
                            <div class="list-single-header-cat fl-wrap">
                            
                                @foreach  ($offer->categories as $category) 
                                <a href="{{url('category/'. $category->id)}}"><i class="{{ $category->code }}"></i> {{ $category->name }}</a> 
                                    @if (!$loop->last)
                                        {!! '&nbsp;&nbsp;' !!}
                                    @endif
                                @endforeach 
                            
                            </div>
                        </div>
                        <h1>{{ $offer->name }}</h1>
                        <span class="section-separator"></span>
                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5">
                            <span>(11 {{__('front.Reviews') }})</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="fl-wrap list-single-header-column">
                                    <div class="share-holder hid-share">
                                        <div class="showshare"><i class="fa fa-share"></i></div>
                                        <div class="share-container  isShare"></div>
                                    </div>
                                    <a class="custom-scroll-link" href="#sec5"><i class="fa fa-hand-o-right"></i></a>
                                    <span class="viewed-counter"><i class="fa fa-eye"></i> {{ $offer->views}} </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="list-single-header-contacts fl-wrap">
                                    <ul>
                                        <li><i class="fa fa-phone"></i><a href="tel:{{ $offer->branches[0]->phone }}">{{parsePhoneNumber ($offer->branches[0]->phone) }}</a></li><br>
                                        <li><i class="fa fa-map-marker"></i> <a href="#">{{ $offer->branches[0]->district['name'] }} {{ $offer->branches[0]->address1 }} {{ $offer->branches[0]->address2 }} {{ $offer->branches[0]->city['name'] }} - {{ $offer->branches[0]->province['name'] }}</a></li><br>
                                        <li><i class="fa fa-envelope-o"></i><a  href="mailto:{{ $offer->branches[0]->email }}">{{ $offer->branches[0]->email }}</a></li><br>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--  section end --> 
        <div class="scroll-nav-wrapper fl-wrap">
            <div class="container">
                <nav class="scroll-nav scroll-init">
                    <ul>
                        <li><a class="act-scrlink" href="#sec1">{{__('front.Top') }}</a></li>
                        <li><a href="#sec2">{{__('front.Details') }}</a></li>
                        <li><a href="#sec3">{{__('front.Gallery') }}</a></li>
                        <li><a href="#sec4">{{__('front.Reviews') }}</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <!--  section  --> 
        <section class="gray-section no-top-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="list-single-main-wrapper fl-wrap" id="sec2">
                            <div class="breadcrumbs gradient-bg  fl-wrap"><a href="#">{{__('front.Home') }}</a><a href="#">{{__('front.Offers') }}</a><span>{{ $offer['name'] }}</span></div>
                            
                            <a href="{{ url('uploads/' . $offer->header) }}" class="gal-link popup-image"><img class="offer-image" src="{{ url('uploads/' . $offer->header) }}" alt=""></a>

                            <div class="list-single-main-item fl-wrap">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>{{__('front.About') }} </h3>
                                </div>
                                <p>{!! $offer['description'] !!}</p>   
                                
                                
                            </div>
                            <!--<div class="accordion">
                                <a class="toggle act-accordion" href="#"> Details option   <i class="fa fa-angle-down"></i></a>
                                <div class="accordion-inner visible">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                </div>
                                <a class="toggle" href="#"> Details option 2  <i class="fa fa-angle-down"></i></a>
                                <div class="accordion-inner">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                </div>
                                
                            </div>-->
                            <div class="list-single-main-item fl-wrap" id="sec3">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>{{__('front.Gallery') }} - {{__('front.Photos') }}</h3>
                                </div>
                                <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                @foreach ($offer->media as $media)
                                    <div class="gallery-item">
                                        <div class="grid-item-holder">
                                            <div class="box-item">
                                                <img  src="{{ url('uploads/' . $media['name']) }}"   alt="">
                                                <a href="{{ url('uploads/' . $media['name']) }}" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach                                   
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box-widget-wrap">
                            <div class="box-widget-item fl-wrap">
                                <div class="box-widget-item-header">
                                    <h3> {{__('front.About Company') }} : </h3>
                                </div>
                                <div class="box-widget list-author-widget">
                                    <div class="list-author-widget-header shapes-bg-small  color-bg fl-wrap">
                                        <span class="list-author-widget-link"><a href="{{ url('company/'.$offer->branches[0]->id.'/'.urlencode($offer->branches[0]->name)) }}">{{ $offer->name }}</a></span>
                                        <a href="{{ url('company/'.$offer->branches[0]->id.'/'.urlencode($offer->branches[0]->name)) }}">
                                            <img src="{{ url('uploads/' . $offer->branches[0]->logo) }}" alt="">
                                        </a>
                                    </div>
                                    <div class="box-widget-content">
                                        <div class="list-author-widget-text">
                                            <div class="list-author-widget-contacts">
                                                <ul>
                                                    <li><span><i class="fa fa-phone"></i> </span> <a href="tel:{{ $offer->branches[0]->phone }}"> {{parsePhoneNumber ($offer->branches[0]->phone) }}</a></li>
                                                    <li><span><i class="fa fa-mobile"></i> </span> <a href="tel:{{ $offer->branches[0]->mobile }}"> {{parsePhoneNumber ($offer->branches[0]->mobile) }}</a></li>
                                                    <li><span><i class="fa fa-envelope-o"></i> </span> <a href="mailto:{{ $offer->branches[0]->email }}">{{ $offer->branches[0]->email }}</a></li>
                                                    <li><span><i class="fa fa-globe"></i> </span> <a href="{{ $offer->branches[0]->seller->website }}" target="_blank">{{ $offer->branches[0]->seller->website }}</a></li>
                                                    <li><span><i class="fa fa-map-marker"></i> </span> <a href="#">{{ $offer->branches[0]->district['name'] }} {{ $offer->branches[0]->address1 }} {{ $offer->branches[0]->address2 }} {{ $offer->branches[0]->city['name'] }} - {{ $offer->branches[0]->province['name'] }}</a></li>
                                                </ul>
                                            </div>
                                            <div class="list-widget-social">
                                            <ul>
                                                <li><a href="{{$offer->branches[0]->seller->facebook}}" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="{{$offer->branches[0]->seller->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="{{$offer->branches[0]->seller->instagram}}" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                                                <li><a href="{{$offer->branches[0]->seller->youtube}}" target="_blank" ><i class="fab fa-youtube"></i></a></li>
                                                @if ($offer->branches[0]->mobile)
                                                    <li><a href="https://wa.me/{{$offer->branches[0]->mobile}} " target="_blank" ><i class="fa fa-whatsapp"></i></a></li>
                                                @endif
                                            </ul>
                                        </div>
                                        <a href="{{ url('company/'.$offer->branches[0]->id.'/'.urlencode($offer->branches[0]->name)) }}" class="btn transparent-btn">{{__('front.View Profile') }} <i class="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-widget-item fl-wrap">
                                <div class="box-widget-item-header">
                                    <h3>{{__('front.Location') }} : </h3>
                                </div>
                                <div class="box-widget">
                                    
                                <div id="map-main" style="height:300px"></div>
                            
                            </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="list-single-main-wrapper fl-wrap" id="sec2">
                            <div class="list-single-main-item fl-wrap" id="sec4">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>{{__('front.Item Reviews') }} -  <span> {{ count($offer->reviews) }} </span></h3>
                                </div>
                                <div class="reviews-comments-wrap">  
                                    @if ($offer->reviews)
                                        @foreach ($offer->reviews as $review)
                                            <div>{{ $review->review . ' - ' . $review->rate . ' - ' . $review->user->name }}</div>
                                        @endforeach
                                    @endif                                                             
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box-widget-wrap box-widget comment-side">
                            <form class="add-comment custom-form" action="/review/offer" method="post">
                                @csrf
                                <input type="hidden" name="offer_id" value="{{ $offer->id }}">
                                <div class="box-widget-item-header">
                                    <h3>{{__('front.Add Reviews & Rate item') }} </h3>
                                </div>
                                <div id="add-review" class="add-review-box">
                                    <div class="leave-rating-wrap">
                                        <span class="leave-rating-title">{{__('front.Your rating  for this Offer') }} : </span>
                                        <div class="leave-rating">
                                            <input type="radio" name="rate" id="rating-1" value="5"/>
                                            <label for="rating-1" class="fa fa-star-o"></label>
                                            <input type="radio" name="rate" id="rating-2" value="4"/>
                                            <label for="rating-2" class="fa fa-star-o"></label>
                                            <input type="radio" name="rate" id="rating-3" value="3"/>
                                            <label for="rating-3" class="fa fa-star-o"></label>
                                            <input type="radio" name="rate" id="rating-4" value="2"/>
                                            <label for="rating-4" class="fa fa-star-o"></label>
                                            <input type="radio" name="rate" id="rating-5" value="1"/>
                                            <label for="rating-5" class="fa fa-star-o"></label>
                                        </div>
                                    </div>
                                    <fieldset>
                                        <textarea name="review" cols="40" rows="3" placeholder="{{__('front.Your Review') }}"></textarea>
                                    </fieldset>
                                    <button class="btn  big-btn  color-bg flat-btn">{{__('front.Submit Review') }} <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                </div>
                            </form>                                                                        
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="limit-box fl-wrap"></div>
        @include('frontend._partials.getintouch')
    </div>
</div>

                                   
                                        

@endsection

@section('after_script')
<script>
showMap({{ $offer->branches[0]->latitude}}, {{ $offer->branches[0]->longitude}}, '{{ $offer->branches[0]->name}}');
</script>
            
@endsection