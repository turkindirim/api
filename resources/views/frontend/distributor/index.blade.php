@extends('/frontend/layout')

@section('title')
{{ __('front.Distributors') }}
@endsection

@section('content')
<style>
.card-listing > div {margin-bottom:30px}
</style>
    <div id="wrapper">
        <div class="content">
            <section class="parallax-section" data-scrollax-parent="true">
                <div class="bg par-elem "  data-bg="{{ asset('images/banner.png') }}" data-scrollax="properties: { translateY: '30%' }"></div>
                <div class="container">
                    <div class="section-title center-align">
                        <h2><span>{{__('front.Distributors') }}</span></h2>
                    </div>
                </div>
            </section>
            <section class="gray-bg" id="sec1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="list-main-wrap fl-wrap card-listing ">
                                <div class="row">
                                @foreach ($distributors as $distributor)
                                @if ($loop->index > 0 && $loop->index % 4 == 0)
                                    </div><div class="row">
                                @endif
                                <div class="col-md-6 col-lg-3">
                                    <article class="geodir-category-listing fl-wrap">
                                        <div class="geodir-category-img">
                                            <a href="{{url('/distributor/' . $distributor->id. '/'. urlencode($distributor->name) )}}"><img src="{{ $distributor->logo ? url('uploads/' . $distributor->logo) : '' }}" alt="{{ ($distributor->name) }}"></a>
                                            <a href="#"><div class="overlay"></div></a>
                                        </div>
                                        <div class="geodir-category-content fl-wrap">
                                            <h4><a href="{{url('/distributor/' . $distributor->id. '/'. urlencode($distributor->name) )}}">{{ $distributor->name }}</a></h4>
                                            <div class="">
                                                <div class="geodir-category-location"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $distributor->city['name'] }} - {{ $distributor->province['name'] }}</div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                @endforeach      
                                </div>                   
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    @if ($total > 1)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pagination">
                                @include('frontend._partials.pagination')
                            </div>
                        </div>
                    </div>
                    @endif
                </div>   
            </section>
            @include('frontend._partials.getintouch')
        </div>
    </div>
@endsection