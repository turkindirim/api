@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Create Language') }}</h2>

<form action="{{ route('language.store') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }}</label>
                    <input type="text" value="{{ old('name')}}" class="input-count md-input" name="name" id="input_counter" maxlength="255">
                </div>
            </div>

            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Code') }}</label>
                    <input type="text" class="md-input lowercase" name="code" required="required" value="{{ old('code') }}" maxlength="2">
                </div>
            </div>

            <div class="uk-row-first  uk-margin-large-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled "><label>{{ __('admin.Direction') }}</label></div>
                <select name="direction" class="uk-width-1-1"  data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Direction') }}">
                    <option value="1">{{ __('admin.Left to Right') }}</option>
                    <option value="0">{{ __('admin.Right to left') }}</option>
                </select>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled "><label>{{ __('admin.Active') }}</label></div>
                <select name="active" class="uk-width-1-1"  data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Active') }}">
                    <option value="1">{{ __('admin.Active') }}</option>
                    <option value="0">{{ __('admin.Inactive') }}</option>
                </select>
            </div>
            
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Create Language') }}</button>
    </div>
    @csrf
</form>


@endsection