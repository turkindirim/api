@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Edit Language') }}</h2>

<form action="{{ route('language.update', ['id' => $language->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }}</label>
                    <input type="text" value="{{ $language->name }}" class="input-count md-input" name="name" id="input_counter" maxlength="255">
                </div>
            </div>

            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Code') }}</label>
                    <input type="text" class="md-input lowercase" name="code" required="required" value="{{ $language->code }}" maxlength="2">
                </div>
            </div>

            <div class="uk-row-first uk-margin-large-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled "><label>{{ __('admin.Direction') }}</label></div>
                <select name="direction" class="uk-width-1-1"  data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Direction') }}">
                    <option value="1" {{ $language->direction == 1 ? 'selected="selected"' : '' }}>{{ __('admin.Left to Right') }}</option>
                    <option value="0" {{ $language->direction == 0 ? 'selected="selected"' : '' }}>{{ __('admin.Right to left') }}</option>
                </select>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled "><label>{{ __('admin.Active') }}</label></div>
                <select name="active" class="uk-width-1-1"  data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Active') }}">
                    <option value="1" {{ $language->active == 1 ? 'selected="selected"' : '' }}>{{ __('admin.Active') }}</option>
                    <option value="0" {{ $language->active == 0 ? 'selected="selected"' : '' }}>{{ __('admin.Inactive') }}</option>
                </select>
            </div>
            
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Language') }}</button>
    </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection