@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Languages') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Language') }}</th>
                    <th>{{ __('admin.Code') }}</th>
                    <th>{{ __('admin.Direction') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($languages as $language)
                    <tr>
                        <td>{{ $language['id'] }}</td>
                        <td>{{ $language['name'] }}</td>
                        <td>{{ $language['code'] }}</td>
                        <td>{{ $language['direction'] == '1' ? __('admin.Left to Right') : __('admin.Right to left') }}</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ window.location.href='{{ route('language.destroy', ['id' => $language['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <a href="{{ route('language.edit', ['id' => $language['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>




<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('language.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection