<aside id="sidebar_main">
        
    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="#" class="sSidebar_hide sidebar_logo_large">
                <img class="logo_regular" src="{{ asset('assets/img/logo_main.png') }}" alt="" height="15" width="71"/>
                <img class="logo_light" src="{{ asset('assets/img/logo_main_white.png') }}" alt="" height="15" width="71"/>
            </a>
            <a href="#" class="sSidebar_show sidebar_logo_small">
                <img class="logo_regular" src="{{ asset('assets/img/logo_main_small.png') }}" alt="" height="32" width="32"/>
                <img class="logo_light" src="{{ asset('assets/img/logo_main_small_light.png') }}" alt="" height="32" width="32"/>
            </a>
        </div>
    </div>
    
    <div class="menu_section">
        <ul>
            <li class="{{ (\Request::route()->getName() == 'admin.dashboard') ? 'current_section' : '' }}" title="{{ __('admin.Dashboard') }}">
                <a href="{{ route('admin.dashboard') }}">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">{{ __('Dashboard') }}</span>
                </a>
            </li>
            <li class="{{ (request()->is('seller*')) ? 'current_section' : '' }}" title="{{ __('admin.Sellers') }}">
                <a href="{{ route('seller.index') }}">
                    <span class="menu_icon"><i class="material-icons">attach_money</i></span>
                    <span class="menu_title">{{ __('admin.Sellers') }}</span>
                </a>
            </li>
            <li class="{{ (request()->is('branch*')) ? 'current_section' : '' }}" title="{{ __('admin.Branches') }}">
                <a href="{{ route('branch.index') }}">
                    <span class="menu_icon"><i class="material-icons">location_city</i></span>
                    <span class="menu_title">{{ __('admin.Branches') }}</span>
                </a>
            </li>
            <li class="{{ (request()->is('distributor*')) ? 'current_section' : '' }}" title="{{ __('admin.Distributors') }}">
                <a href="{{ route('distributor.index') }}">
                    <span class="menu_icon"><i class="material-icons">bubble_chart</i></span>
                    <span class="menu_title">{{ __('admin.Distributors') }}</span>
                </a>
            </li>
            <li class="{{ (request()->is('users*')) ? 'current_section' : '' }}" title="{{ __('admin.Users') }}">
                <a href="{{ route('users.index') }}">
                    <span class="menu_icon"><i class="material-icons">person</i></span>
                    <span class="menu_title">{{ __('admin.Users') }}</span>
                </a>
            </li>
            @if (Auth::user()->superadmin == '1')
            <li class="{{ (request()->is('admin*')) ? 'current_section' : '' }}" title="{{ __('admin.Admins') }}">
                <a href="{{ route('admins.index') }}">
                    <span class="menu_icon"><i class="material-icons">account_circle</i></span>
                    <span class="menu_title">{{ __('admin.Admins') }}</span>
                </a>
            </li>
            @endif
            
            
            <li class="{{ (request()->is('transactions*')) ? 'current_section' : '' }}" title="{{ __('admin.Transactions') }}">
                <a href="{{ route('transactions.index') }}">
                    <span class="menu_icon"><i class="material-icons">swap_horiz</i></span>
                    <span class="menu_title">{{ __('admin.Transactions') }}</span>
                </a>
            </li>
            <li class="{{ (request()->is('newsfeed*')) ? 'current_section' : '' }}" title="{{ __('admin.Newsfeed') }}">
                <a href="{{ route('newsfeed.index') }}">
                    <span class="menu_icon"><i class="material-icons">rss_feed</i></span>
                    <span class="menu_title">{{ __('admin.Newsfeed') }}</span>
                </a>
            </li>
            <li class="{{ (request()->is('post*')) ? 'current_section' : '' }}" title="{{ __('admin.Post') }}">
                <a href="{{ route('post.index') }}">
                    <span class="menu_icon"><i class="material-icons">short_text</i></span>
                    <span class="menu_title">{{ __('admin.Post') }}</span>
                </a>
            </li>
            <li title="{{ __('admin.Cards') }}">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">credit_card</i></span>
                    <span class="menu_title">{{ __('admin.Cards') }}</span>
                </a>
                <ul>
                    <li class="{{ (request()->is('types')) ? 'act_item' : '' }}"><a href="{{ route('types.index') }}">{{ __('admin.Types') }}</a></li>
                    <li class="{{ (request()->is('cards') || request()->is('cards/log*')) ? 'act_item' : '' }}"><a href="{{ route('cards.index') }}">{{ __('admin.Cards') }}</a></li>
                    <li class="{{ (request()->is('cards/barcode')) ? 'act_item' : '' }}"><a href="{{ route('cards.barcode') }}">{{ __('admin.Set Distributor') }}</a></li>
                </ul>
            </li>
            <li class="{{ (request()->is('offers*')) ? 'current_section' : '' }}" title="{{ __('admin.Offers') }}">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">receipt</i></span>
                    <span class="menu_title">{{ __('admin.Offers') }}</span>
                </a>
                <ul>
                    <li class="{{ (request()->is('tags*')) ? 'act_item' : '' }}"><a href="{{ route('tags.index') }}">{{ __('admin.Tags') }}</a></li>
                    <li class="{{ (request()->is('categories*')) ? 'act_item' : '' }}"><a href="{{ route('categories.index') }}">{{ __('admin.Categories') }}</a></li>
                    <li class="{{ (request()->is('offers*')) ? 'act_item' : '' }}"><a href="{{ route('offers.index') }}">{{ __('admin.Offers') }}</a></li>
                </ul>
            </li>
            <li class="{{ (request()->is('locations*')) ? 'current_section' : '' }}" title="{{ __('admin.Locations') }}">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">public</i></span>
                    <span class="menu_title">{{ __('admin.Locations') }}</span>
                </a>
                <ul>
                    <li class="{{ (request()->is('countries*')) ? 'act_item' : '' }}"><a href="{{ route('country.index') }}">{{ __('admin.Countries') }}</a></li>
                    <li class="{{ (request()->is('provinces*')) ? 'act_item' : '' }}"><a href="{{ route('province.index') }}">{{ __('admin.Provinces') }}</a></li>
                    <li class="{{ (request()->is('cities*')) ? 'act_item' : '' }}"><a href="{{ route('city.index') }}">{{ __('admin.Cities') }}</a></li>
                    <li class="{{ (request()->is('districts*')) ? 'act_item' : '' }}"><a href="{{ route('district.index') }}">{{ __('admin.Districts') }}</a></li>
                </ul>
            </li>
            <li class="{{ (request()->is('language*')) ? 'current_section' : '' }}" title="{{ __('admin.Languages') }}">
                <a href="{{ route('language.index') }}">
                    <span class="menu_icon"><i class="material-icons">language</i></span>
                    <span class="menu_title">{{ __('admin.Languages') }}</span>
                </a>
            </li>
            <li class="{{ (request()->is('slides*')) ? 'current_section' : '' }}" title="Slides">
                <a href="{{ route('slides.index') }}">
                    <span class="menu_icon"><i class="material-icons">collections</i></span>
                    <span class="menu_title">Slides</span>
                </a>
            </li>
            <li title="{{ __('admin.Trash') }}">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">delete</i></span>
                    <span class="menu_title">{{ __('admin.Trash') }}</span>
                </a>
                <ul>
                    <li class="{{ (request()->is('trash/sellers*')) ? 'act_item' : '' }}"><a href="{{ route('sellers.trash') }}">{{ __('admin.Sellers') }}</a></li>
                    <li class="{{ (request()->is('trash/branches*')) ? 'act_item' : '' }}"><a href="{{ route('branches.trash') }}">{{ __('admin.Branches') }}</a></li>
                    <li class="{{ (request()->is('trash/distributors*')) ? 'act_item' : '' }}"><a href="{{ route('distributors.trash') }}">{{ __('admin.Distributors') }}</a></li>
                    <li class="{{ (request()->is('trash/users*')) ? 'act_item' : '' }}"><a href="{{ route('users.trash') }}">{{ __('admin.Users') }}</a></li>
                    <li class="{{ (request()->is('trash/newsfeed*')) ? 'act_item' : '' }}"><a href="{{ route('newsfeed.trash') }}">{{ __('admin.Newsfeed') }}</a></li>
                    <li class="{{ (request()->is('trash/languages*')) ? 'act_item' : '' }}"><a href="{{ route('languages.trash') }}">{{ __('admin.Languages') }}</a></li>
                    <li class="{{ (request()->is('trash/cards*')) ? 'act_item' : '' }}"><a href="{{ route('cards.trash') }}">{{ __('admin.Cards') }}</a></li>
                    <li class="{{ (request()->is('trash/types*')) ? 'act_item' : '' }}"><a href="{{ route('types.trash') }}">{{ __('admin.Types') }}</a></li>
                    <li class="{{ (request()->is('trash/offers*')) ? 'act_item' : '' }}"><a href="{{ route('offers.trash') }}">{{ __('admin.Offers') }}</a></li>
                    <li class="{{ (request()->is('trash/categories*')) ? 'act_item' : '' }}"><a href="{{ route('categories.trash') }}">{{ __('admin.Categories') }}</a></li>
                    <li class="{{ (request()->is('trash/countries*')) ? 'act_item' : '' }}"><a href="{{ route('countries.trash') }}">{{ __('admin.Countries') }}</a></li>
                    <li class="{{ (request()->is('trash/provinces*')) ? 'act_item' : '' }}"><a href="{{ route('provinces.trash') }}">{{ __('admin.Provinces') }}</a></li>
                    <li class="{{ (request()->is('trash/cities*')) ? 'act_item' : '' }}"><a href="{{ route('cities.trash') }}">{{ __('admin.Cities') }}</a></li>
                    <li class="{{ (request()->is('trash/districts*')) ? 'act_item' : '' }}"><a href="{{ route('districts.trash') }}">{{ __('admin.Districts') }}</a></li>
                </ul>
            </li>
            <li title="{{ __('admin.Log') }}">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">notes</i></span>
                    <span class="menu_title">{{ __('admin.Log') }}</span>
                </a>
                <ul>
                    <li class="{{ (request()->is('log/admins*')) ? 'act_item' : '' }}"><a href="{{ route('log.admins') }}">{{ __('admin.Admins') }}</a></li>
                    <li class="{{ (request()->is('log/branches*')) ? 'act_item' : '' }}"><a href="{{ route('log.branches') }}">{{ __('admin.Branches') }}</a></li>
                    <li class="{{ (request()->is('log/distributors*')) ? 'act_item' : '' }}"><a href="{{ route('log.distributors') }}">{{ __('admin.Distributors') }}</a></li>
                    <li class="{{ (request()->is('log/users*')) ? 'act_item' : '' }}"><a href="{{ route('log.users') }}">{{ __('admin.Users') }}</a></li>
                </ul>
            </li>
            <li title="{{ __('admin.Settings') }}">
                <a href="{{ route('settings') }}">
                    <span class="menu_icon"><i class="material-icons">settings</i></span>
                    <span class="menu_title">{{ __('admin.Settings') }}</span>
                </a>
            </li>
        </ul>
    </div>
</aside>