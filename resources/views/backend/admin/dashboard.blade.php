@extends('/backend/admin/layout')

@section('content')

<?php //dd(); ?>

<div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
    <div>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_visitors peity_data">5,3,9,6,5,9,7</span></div>
                <span class="uk-text-muted uk-text-small">Visitors (last 7d)</span>
                <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>12456</noscript></span></h2>
            </div>
        </div>
    </div>
    <div>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_sale peity_data">5,3,9,6,5,9,7,3,5,2</span></div>
                <span class="uk-text-muted uk-text-small">Sale</span>
                <h2 class="uk-margin-remove">$<span class="countUpMe">0<noscript>142384</noscript></span></h2>
            </div>
        </div>
    </div>
    <div>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">64/100</span></div>
                <span class="uk-text-muted uk-text-small">Orders Completed</span>
                <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>64</noscript></span>%</h2>
            </div>
        </div>
    </div>
    <div>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_live peity_data">5,3,9,6,5,9,7,3,5,2,5,3,9,6,5,9,7,3,5,2</span></div>
                <span class="uk-text-muted uk-text-small">Visitors (live)</span>
                <h2 class="uk-margin-remove" id="peity_live_text">46</h2>
            </div>
        </div>
    </div>
</div>



<div class="uk-grid">
    <div class="uk-width-1-1">
        <div class="md-card">
            <div class="md-card-toolbar">
                <div class="md-card-toolbar-actions">
                    <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                    <i class="md-icon material-icons">&#xE5D5;</i>
                    <div class="md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                        <i class="md-icon material-icons">&#xE5D4;</i>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav">
                                <li><a href="#">Action 1</a></li>
                                <li><a href="#">Action 2</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h3 class="md-card-toolbar-heading-text">
                    Chart
                </h3>
            </div>
            <div class="md-card-content">
                <div class="mGraph-wrapper">
                    <div id="mGraph_sale" class="mGraph" data-uk-check-display></div>
                </div>
                <div class="md-card-fullscreen-content">
                    <div class="uk-overflow-container">
                        <table class="uk-table uk-table-no-border uk-text-nowrap">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Best Seller</th>
                                <th>Total Sale</th>
                                <th>Change</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>January 2014</td>
                                <td>Non suscipit qui fuga.</td>
                                <td>$3 234 162</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>February 2014</td>
                                <td>Voluptatem et sint at.</td>
                                <td>$3 771 083</td>
                                <td class="uk-text-success">+2.5%</td>
                            </tr>
                            <tr>
                                <td>March 2014</td>
                                <td>Et magnam mollitia.</td>
                                <td>$2 429 352</td>
                                <td class="uk-text-danger">-4.6%</td>
                            </tr>
                            <tr>
                                <td>April 2014</td>
                                <td>Doloribus fugiat blanditiis quam.</td>
                                <td>$4 844 169</td>
                                <td class="uk-text-success">+7%</td>
                            </tr>
                            <tr>
                                <td>May 2014</td>
                                <td>Qui magnam ipsum dolor.</td>
                                <td>$5 284 318</td>
                                <td class="uk-text-success">+3.2%</td>
                            </tr>
                            <tr>
                                <td>June 2014</td>
                                <td>Reprehenderit voluptas non.</td>
                                <td>$4 688 183</td>
                                <td class="uk-text-danger">-6%</td>
                            </tr>
                            <tr>
                                <td>July 2014</td>
                                <td>Non repellendus maiores.</td>
                                <td>$4 353 427</td>
                                <td class="uk-text-success">-5.3%</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <p class="uk-margin-large-top uk-margin-small-bottom heading_list uk-text-success">Some Info:</p>
                    <p class="uk-margin-top-remove">Dolorem quia tempora maxime et occaecati qui aliquid sed eum earum nisi ut cumque earum labore et facere vel quis totam a nulla qui deleniti et nobis accusamus illo numquam repudiandae debitis ut nostrum debitis aliquam qui omnis minima animi fugiat dolorem optio facilis in tempore modi tempora est optio ratione reiciendis qui magni ipsum sed eum aspernatur cumque natus eum iste et autem doloribus rerum ea architecto nulla sequi est est nostrum architecto animi porro quasi voluptates sit iure autem numquam debitis dicta corrupti facere aperiam tempore assumenda et assumenda dolor molestias ut veritatis voluptatem placeat nobis enim expedita ratione.</p>
                </div>
            </div>
        </div>
    </div>
</div>



            <!-- circular charts -->
            <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center uk-sortable sortable-handler" id="dashboard_sortable_cards" data-uk-sortable data-uk-grid-margin>
                <div>
                    <div class="md-card md-card-hover md-card-overlay">
                        <div class="md-card-content">
                            <div class="epc_chart" data-percent="76" data-bar-color="#03a9f4">
                                <span class="epc_chart_icon"><i class="material-icons">&#xE0BE;</i></span>
                            </div>
                        </div>
                        <div class="md-card-overlay-content">
                            <div class="uk-clearfix md-card-overlay-header">
                                <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                                <h3>
                                    User Messages
                                </h3>
                            </div>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias consectetur.
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card md-card-hover md-card-overlay">
                        <div class="md-card-content uk-flex uk-flex-center uk-flex-middle">
                            <span class="peity_conversions_large peity_data">5,3,9,6,5,9,7</span>
                        </div>
                        <div class="md-card-overlay-content">
                            <div class="uk-clearfix md-card-overlay-header">
                                <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                                <h3>
                                    Conversions
                                </h3>
                            </div>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card md-card-hover md-card-overlay md-card-overlay-active">
                        <div class="md-card-content" id="canvas_1">
                            <div class="epc_chart" data-percent="37" data-bar-color="#9c27b0">
                                <span class="epc_chart_icon"><i class="material-icons">&#xE85D;</i></span>
                            </div>
                        </div>
                        <div class="md-card-overlay-content">
                            <div class="uk-clearfix md-card-overlay-header">
                                <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                                <h3>
                                    Tasks List
                                </h3>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            <button class="md-btn md-btn-primary">More</button>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card md-card-hover md-card-overlay">
                        <div class="md-card-content">
                            <div class="epc_chart" data-percent="53" data-bar-color="#009688">
                                <span class="epc_chart_text"><span class="countUpMe">53</span>%</span>
                            </div>
                        </div>
                        <div class="md-card-overlay-content">
                            <div class="uk-clearfix md-card-overlay-header">
                                <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                                <h3>
                                    Orders
                                </h3>
                            </div>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card md-card-hover md-card-overlay">
                        <div class="md-card-content">
                            <div class="epc_chart" data-percent="37" data-bar-color="#607d8b">
                                <span class="epc_chart_icon"><i class="material-icons">&#xE7FE;</i></span>
                            </div>
                        </div>
                        <div class="md-card-overlay-content">
                            <div class="uk-clearfix md-card-overlay-header">
                                <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                                <h3>
                                    User Registrations
                                </h3>
                            </div>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </div>
                    </div>
                </div>
            </div>



<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-2">
        <div class="md-card">
            <div class="md-card-content">
                <h4 class="heading_c uk-margin-bottom">{{ __('admin.Users') }}</h4>
                <div id="users_by_type" class="chartist"></div>
            </div>
        </div>
    </div>
    <div class="uk-width-large-1-2">
        <div class="md-card">
            <div class="md-card-content">
                <h4 class="heading_c uk-margin-bottom">{{ __('admin.Users Provinces Top 5') }}</h4>
                <div id="users_by_province" class="chartist"></div>
            </div>
        </div>
    </div>

</div>

<div class="uk-grid" data-uk-grid-margin>
    
    <div class="uk-width-large-1-2">
        <div class="md-card">
            <div class="md-card-content">
                <h4 class="heading_c uk-margin-bottom">{{ __('admin.Users By Location Top 5') }}</h4>
                <div id="users_by_location" class="chartist"></div>
            </div>
        </div>
    </div>
    <div class="uk-width-large-1-2">
        <div class="md-card">
            <div class="md-card-content">
                <h4 class="heading_c uk-margin-bottom">{{ __('admin.Users By Nationality Top 5') }}</h4>
                <div id="users_by_nationality" class="chartist"></div>
            </div>
        </div>
    </div>
</div>

<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-2">
        <div class="md-card">
            <div class="md-card-content">
                <h4 class="heading_c uk-margin-bottom">{{ __('admin.Cards') }}</h4>
                <div id="cards" class="chartist"></div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@section('footer_script')
<script>
var ch_distributed_series = new Chartist.Bar('#users_by_type', {
    labels: ['{{ __('admin.Admins')}}','{{ __('admin.Branches')}}','{{ __('admin.Distributors')}}','{{ __('admin.Sellers')}}','{{ __('admin.Users')}}'],
    series: [
        [{{ $users_by_type['admins'] }},{{ $users_by_type['branches'] }},{{ $users_by_type['distributors'] }},{{ $users_by_type['sellers'] }},{{ $users_by_type['users'] }}]        
    ]
}, {
    plugins: [
        Chartist.plugins.tooltip({
            // appendToBody: true
        })
    ]
});
var ch_distributed_series = new Chartist.Bar('#users_by_province', {
    labels: [
        @foreach($users_by_province['provinces'] as $province)
            {!! "'".$province['name']."'" !!}
            @if ($loop->index == 4)
                break;
            @endif
            @if (!$loop->last)
                {{ ',' }}
            @endif
        @endforeach
    ],
    series: [
        [
            @foreach($users_by_province['users'] as $user)
                {{ $user->user_count }}
                @if ($loop->index == 4)
                    break;
                @endif

                @if (!$loop->last)
                    {{ ',' }}
                @endif
            @endforeach
        ]        
    ]
}, {
    plugins: [
        Chartist.plugins.tooltip({
            // appendToBody: true
        })
    ]
});

var ch_distributed_series = new Chartist.Bar('#users_by_location', {
    labels: [
        @foreach($users_by_location['locations'] as $location)
            {!! "'".$location['name']."'" !!}
            @if ($loop->index == 4)
                break;
            @endif
            @if (!$loop->last)
                {{ ',' }}
            @endif
        @endforeach
    ],
    series: [
        [
            @foreach($users_by_location['users'] as $user)
                {{ $user->user_count }}
                @if ($loop->index == 4)
                    break;
                @endif

                @if (!$loop->last)
                    {{ ',' }}
                @endif
            @endforeach
        ]        
    ]
}, {
    plugins: [
        Chartist.plugins.tooltip({
            // appendToBody: true
        })
    ]
});

var ch_distributed_series = new Chartist.Bar('#users_by_nationality', {
    labels: [
        @foreach($users_by_nationality['nationalities'] as $nationality)
            {!! "'".$nationality['name']."'" !!}
            @if ($loop->index == 4)
                break;
            @endif
            @if (!$loop->last)
                {{ ',' }}
            @endif
        @endforeach
    ],
    series: [
        [
            @foreach($users_by_nationality['users'] as $user)
                {{ $user->user_count }}
                @if ($loop->index == 4)
                    break;
                @endif

                @if (!$loop->last)
                    {{ ',' }}
                @endif
            @endforeach
        ]        
    ]
}, {
    plugins: [
        Chartist.plugins.tooltip({
            // appendToBody: true
        })
    ]
});

var ch_distributed_series = new Chartist.Bar('#cards', {
    labels: [
        '{{__('admin.Cards')}}',
        '{{__('admin.Active Cards')}}',
        '{{__('admin.Inactive Cards')}}',
    ],
    series: [
        [
            {{$cards['count']}},
            {{$cards['active']}},
            {{$cards['inactive']}},
        ]        
    ]
}, {
    plugins: [
        Chartist.plugins.tooltip({
            // appendToBody: true
        })
    ]
});
</script>
@endsection