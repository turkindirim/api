@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Cities') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.City') }}</th>
                    <th>{{ __('admin.Province') }}</th>
                    <th>{{ __('admin.Districts') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($cities as $city)
                    <tr>
                        <td>{{ $city['id'] }}</td>
                        <td>{{ $city['name'] }}</td>
                        <td>{{ $city['province']['name'] }}</td>
                        <td>{{ count($city['districts']) }}</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ window.location.href='{{ route('city.destroy', ['id' => $city['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <a href="{{ route('city.edit', ['id' => $city['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>




<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('city.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection