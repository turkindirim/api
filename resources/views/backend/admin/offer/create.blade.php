@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Create Offer') }}</h2>

<form action="{{ route('offers.store') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input required type="text" value="{{ old('name_' . $language['code'])}}" class="input-count md-input" name="name_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Description') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom">
                        <textarea  class="ckeditor" name="description_{{ $language['code'] }}">{{ old('description_' . $language['code'])}}</textarea>
                    </div>
                </div>
                
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Excerpt') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom">
                        <textarea class="md-input" name="excerpt_{{ $language['code'] }}">{{ old('excerpt_' . $language['code'])}}</textarea>
                    </div>
                </div>
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Images') }} - {{ $language['name'] }}</label></div>
                
                
                <div class="uk-grid uk-grid-divider uk-margin-large-bottom">
                    <div class="uk-width-medium-1-4 uk-row-first">
                        <p>{{ __('admin.Thumbnail') }}</p>
                        <input type="file" class="maxFileSize" name="thumb_{{ $language['code'] }}" accept=".jpg,.png,.bmp,.gif">
                    </div>
                    <div class="uk-width-medium-1-4">
                        <p>{{ __('admin.Background') }}</p>
                        <input type="file" class="maxFileSize" name="background_{{ $language['code'] }}" accept=".jpg,.png,.bmp,.gif">
                    </div>
                    <div class="uk-width-medium-1-4">
                        <p>{{ __('admin.Header') }}</p>
                        <input type="file" class="maxFileSize" name="header_{{ $language['code'] }}" accept=".jpg,.png,.bmp,.gif">
                    </div>
                    <div class="uk-width-medium-1-4">
                        <p>{{ __('admin.Header') }}</p>
                        <input type="file" class="maxFileSize" name="mobile_thumb_{{ $language['code'] }}" accept=".jpg,.png,.bmp,.gif">
                    </div>
                </div>

            @endforeach

            <h4 class="uk-margin-large-top">{{ __('admin.Status') }}</h4>
            <div class="uk-width-medium-1-3">
                <input type="checkbox" value="1" data-switchery checked="checked" name="active" id="switch_demo_1" />
                <label for="switch_demo_1" class="inline-label">{{ __('admin.Active Offer') }}</label>
            </div>
            <h4 class="uk-margin-large-top">{{ __('admin.Featured') }}</h4>
            <div class="uk-width-medium-1-3">
                <input type="checkbox" value="1" data-switchery name="featured" />
                <label  class="inline-label">{{ __('admin.Featured Offer') }}</label>
            </div>

            <h4 class="uk-margin-large-top">{{ __('admin.Media') }}</h4>
            <div class="uk-row-first ">
                <div><input type="file" multiple accept=".jpg,.png,.bmp,.gif,.mp4" name="media[]" class="dropify maxFileSize" /></div>
            </div>
            

            <h4 class="uk-margin-large-top">{{ __('admin.Categories') }}</h4>
            <div class="uk-row-first">
                <select id="category" multiple name="category[]" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Categories') }}">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <h4 class="uk-margin-large-top">{{ __('admin.Tags') }}</h4>
            <div class="uk-row-first ">
                <select id="tag" multiple="multiple" name="tag[]" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Tags') }}">
                    @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                    @endforeach
                </select>
            </div>
            <h4 class=" uk-margin-large-top">{{ __('admin.Branches') }}</h4>
            <div class="uk-row-first">
                <div data-dynamic-fields="field_template_b"></div>
            
                <script id="field_template_b" type="text/x-handlebars-template">
                    <div class="uk-grid form_section autocomplete-wrapper">
                        <div class="uk-width-1-1">
                            <div class="uk-input-group">
                            <input type="text" data-id="branch{{ '{'.'{counter}'.'}' }}" data-user="branch" class="userSelector md-input" placeholder="{{ __('admin.Type name here') }}">
                            <input type="hidden" id="branch{{ '{'.'{counter}'.'}' }}" name="branch[]">
                            <span class="uk-input-group-addon">
                                    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
                                </span>
                            </div>
                        </div>
                        <div class="uk-dropdown" aria-expanded="true">
                            <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results md-list">
                            </ul>
                        </div>
                    </div>
                </script>
            </div>
            
            
            <h4 class="uk-margin-large-top">{{ __('admin.Period') }}</h4>
            <div class="uk-grid ">
                <div class="uk-width-large-1-2 uk-width-1-1">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_start">{{ __('admin.Valid From') }}</label>
                        <input name="valid_from" class="md-input" type="text" id="uk_dp_start" required>
                    </div>
                </div>
                <div class="uk-width-large-1-2 uk-width-medium-1-1">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_end">{{ __('admin.Valid To') }}</label>
                        <input name="valid_to" class="md-input" type="text" id="uk_dp_end" required>
                    </div>
                </div>
            </div>
        </div>
    </div>
          
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Create Offer') }}</button>
    </div>
    @csrf
</form>


@endsection