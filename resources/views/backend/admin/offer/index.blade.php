@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Filter') }}</h2>


<form action="{{ route('offers.filter') }}" method="post">
    @csrf
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-grid">
                
                <div class="uk-width-large-1-6 uk-width-medium-1-2">
                    <div class="uk-input-group">
                        <label>Title</label>
                        <input type="text" class="md-input" name="name" value="{{ request()->get('name') }}" />
                    </div>
                </div>
                <div class="uk-width-large-1-6 uk-width-medium-1-2">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_1">Valid in</label>
                        <input class="md-input" name="valid_in" value="{{ request()->get('valid_in') }}" type="text" id="uk_dp_1" data-uk-datepicker="{format:'YYYY-MM-DD'}">
                    </div>
                </div>
                <div class="uk-width-large-1-6 uk-width-1-2">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_start">Published (From)</label>
                        <input name="created_at_from" class="md-input" value="{{ request()->get('created_at_from') }}" type="text" id="uk_dp_start">
                    </div>
                </div>
                <div class="uk-width-large-1-6 uk-width-medium-1-2">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_end">Published (To)</label>
                        <input name="created_at_to" class="md-input" value="{{ request()->get('created_at_to') }}" type="text" id="uk_dp_end">
                    </div>
                </div>

                <div class="uk-width-large-1-6 uk-width-medium-1-2">
                    <div class="uk-input-group">
                        <select class="" name="categories" id="" data-md-select2>
                            <option value="">Category</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}" {{ $category->id == request()->get('categories') ? 'selected="selected"':'' }}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>                    
                </div>
                
                <div class="uk-width-large-1-6 uk-width-medium-1-2">
                    <div class="uk-input-group">
                        <select class="" name="offer_status" id="" data-md-select2>
                            <option value="">Status</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>                    
                </div>
                <div class="uk-row-first">
                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Filter') }}</button>
                </div>
            </div>
        </div>
    </div>
</form>

<h2 class=" uk-margin-bottom">{{ __('admin.Offers') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Sort</th>
                    <th>{{ __('admin.Name') }}</th>
                    <th>{{ __('admin.Period') }}</th>
                    <th>{{ __('admin.Publish Date') }}</th>
                    <th>{{ __('admin.Branches') }}</th>
                    <th>{{ __('admin.Categories') }}</th>
                    <th>{{ __('admin.Status') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($offers as $offer)
                    <tr>
                        <td>{{ $offer['id'] }}</td>
                        <td>
                            <span class="real_sort">{{ $offer['sort'] }}</span>
                            <input type="number" min="1" max="10000000" class="sort_field" data-id="{{ $offer['id'] }}" data-model="offer" value="{{ $offer['sort'] }}" style="width:70px;">
                        </td>
                        <td>{{ $offer['name'] }}</td>
                        <td>
                            <b style="color:#{{ ($offer['valid_from'] > date('Y-m-d')) ? 'e5b20b' : ($offer['valid_to'] < date('Y-m-d')) ? 'af1313':'37af13' }};">{{ $offer['valid_from'] . ' | ' . $offer['valid_to'] }}</b>
                        </td>
                        <td>{{ $offer['created_at']->format('Y-m-d') }}</td>
                        <td>
                            @if ($offer['branches']->count() > 6)
                                {{ $offer['branches']->count() }}
                            @else
                                @foreach ($offer['branches'] as $branch)
                                    {{ $branch['name'] }}
                                    @if (!$loop->last)
                                        {{ ' - ' }}
                                    @endif
                                @endforeach
                            @endif    
                        </td>
                        <td>
                            @if ($offer['categories']->count() > 6)
                                {{ $offer['categories']->count() }}
                            @else
                                @foreach ($offer['categories'] as $category)
                                    {{ $category['name'] }}
                                    @if (!$loop->last)
                                        {{ ' - ' }}
                                    @endif
                                @endforeach
                            @endif
                        </td>
                        <td>{!! $offer['active'] ? '<span style="color:#37af13;">'.__('admin.Active').'</span>' : '<span style="color:#e5b20b;">'.__('admin.Inactive').'</span>' !!}</td>
                        <!-- 
                        <td><button class="md-btn" onclick="getOfferMedia( {{ $offer['id'] }})" data-uk-modal="{target:'#media_modal'}">{{ __('admin.Open') }}</button></td>
                        //-->
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ window.location.href='{{ route('offers.destroy', ['id' => $offer['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <a href="{{ route('offers.edit', ['id' => $offer['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                            <a href="{{ route('offers.reviews', ['id' => $offer['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-comment"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@include('backend/admin/pagination')

<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('offers.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection