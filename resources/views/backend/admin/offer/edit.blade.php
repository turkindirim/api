@extends('/backend/admin/layout')
@section('content')

<h2 class=" uk-margin-bottom">{{ __('admin.Edit Offer') }}</h2>
<form action="{{ route('offers.update', ['id' => $offer->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ $offer->translate($language['code']) ? $offer->translate($language['code'])->name : ''}}" class="input-count md-input" name="name_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Description') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom"><textarea  class="ckeditor" name="description_{{ $language['code'] }}">{{ $offer->translate($language['code']) ? $offer->translate($language['code'])->description : ''}}</textarea></div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Excerpt') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom">
                        <textarea class="form-control md-input" name="excerpt_{{ $language['code'] }}">{{ $offer->translate($language['code']) ? $offer->translate($language['code'])->excerpt : ''}}</textarea>
                    </div>
                </div>
                
                <div class="uk-grid uk-grid-divider uk-margin-large-bottom">
                    <div class="uk-width-medium-1-4 uk-row-first">
                        <p>{{ __('admin.Thumbnail') }}</p>
                        <img src="{{ url('uploads/'.$offer->translate($language['code'])->thumb) }}" alt="" class="img-responsive">
                        <input class="maxFileSize" type="file" name="thumb_{{ $language['code'] }}" accept=".jpg,.jpeg.png,.bmp,.gif">
                    </div>
                    <div class="uk-width-medium-1-4">
                        <p>{{ __('admin.Background') }}</p>
                        <img src="{{ url('uploads/'.$offer->translate($language['code'])->background) }}" alt="" class="img-responsive">
                        <input class="maxFileSize" type="file" name="background_{{ $language['code'] }}" accept=".jpg,.jpeg.png,.bmp,.gif">
                    </div>
                    <div class="uk-width-medium-1-4">
                        <p>{{ __('admin.Header') }}</p>
                        <img src="{{ url('uploads/'.$offer->translate($language['code'])->header) }}" alt="" class="img-responsive">
                        <input class="maxFileSize" type="file" name="header_{{ $language['code'] }}" accept=".jpg,.jpeg,.png,.bmp,.gif">
                    </div>
                    <div class="uk-width-medium-1-4">
                        <p>{{ __('admin.Mobile Thumb') }}</p>
                        <img src="{{ url('uploads/'.$offer->translate($language['code'])->mobile_thumb) }}" alt="" class="img-responsive">
                        <input class="maxFileSize" type="file" name="mobile_thumb_{{ $language['code'] }}" accept=".jpg,.jpeg,.png,.bmp,.gif">
                    </div>
                </div>
            @endforeach

            
            <h4 class="uk-margin-large-top">{{ __('admin.Status') }}</h4>
            <div class="uk-width-medium-1-3">
                <input type="checkbox" value="1" data-switchery {{ ($offer->active == 1) ? 'checked' : '' }} name="active" id="switch_demo_1" />
                <label for="switch_demo_1" class="inline-label">{{ __('admin.Active Offer') }}</label>
            </div>
            <h4 class="uk-margin-large-top">{{ __('admin.Featured') }}</h4>
            <div class="uk-width-medium-1-3">
                <input type="checkbox" value="1" data-switchery {{ ($offer->featured == 1) ? 'checked' : '' }} name="featured" />
                <label  class="inline-label">{{ __('admin.Featured Offer') }}</label>
            </div>

            <h4 class="uk-margin-large-top">{{ __('admin.Categories') }}</h4>
            <div class="uk-row-first">
                <select id="category" multiple name="category[]" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Categories') }}">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}" {{ $offer->categories->contains($category->id) ? 'selected="selected"':''}}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>

            <h4 class="uk-margin-large-top">{{ __('admin.Tags') }}</h4>
            <div class="uk-row-first ">
                <select id="tag" multiple name="tag[]" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Tags') }}">
                    @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}" {{ $offer->tags->contains($tag->id) ? 'selected="selected"':''}}>{{ $tag->name }}</option>
                    @endforeach
                </select>
            </div>
            

            <h4 class="uk-margin-large-top">{{ __('admin.Period') }}</h4>
            <div class="uk-grid">
                <div class="uk-width-large-1-2 uk-width-1-1">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_start">{{ __('admin.Valid From') }}</label>
                        <input value="{{ $offer->valid_from }}" name="valid_from" class="md-input" type="text" id="uk_dp_start" required>
                    </div>
                </div>
                <div class="uk-width-large-1-2 uk-width-medium-1-1">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_end">{{ __('admin.Valid To') }}</label>
                        <input value="{{ $offer->valid_to }}" name="valid_to" class="md-input" type="text" id="uk_dp_end" required>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="md-card">
        <div class="md-card-content">
            <h4 class="uk-margin-small-top">{{ __('admin.Media') }}</h4>
            <!-- id="dragula_sortable" //-->
            <div  class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-5 uk-margin-bottom" data-uk-grid-margin>
                @foreach ($offer->media as $media)
                <div id="media{{ $media['id'] }}">
                    <div class="md-card">
                        @if ($media['type'] == 'image')
                        <div class="md-card-head head_background" style="background-image: url('{{ url('uploads/'.$media['name']) }}')"></div>
                        @else
                        <div class="md-card-head head_background">
                            <video width="320" height="240" controls>
                                <source src="{{ url('uploads/' . $media['name']) }}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                        @endif
                        
                        <div class="md-card-content">
                        <button type="button" onclick="mediaDestroy('{{ route('media.destroy', ['id' => $media['id']]) }}', 'media{{$media['id']}}')" class="md-btn md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Delete') }}</button>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="uk-row-first ">
                <div><input type="file" multiple accept=".jpg,.png,.bmp,.gif,.mp4" name="media[]" class="dropify" /></div>
            </div>
        </div>
    </div>

    <div class="md-card">
        <div class="md-card-content">
            <h4 class=" uk-margin-small-top">{{ __('admin.Branches') }}</h4>
            @if ($offer->adder)
                {{ $offer->adder->name . ' - ' . $offer->adder->seller->name }}
            @else
                <table id="dt_default" class="uk-table table_check">
                    <thead>
                        <tr>
                            <th><input type="checkbox" data-md-icheck class="check_all"></th>
                            <th>{{ __('admin.Branch') }}</th>
                        </tr>
                    </thead>
                    <tbody class="toDeleteBranches">
                        @foreach ($offer->branches as $branch)
                            <tr data-id="{{ $branch['id'] }}" id="row{{ $branch['id'] }}">
                                <td><input type="checkbox" data-md-icheck class="check_row"></td>
                                <td>{{ $branch['name'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <button type="button" onclick="deleteOfferBranches({{$offer->id}});" class="md-btn md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Delete Branches') }}</button>

                <h5 class="uk-margin-top">{{ __('admin.Add') }}</h5>
                <div class="uk-row-first uk-margin-top">
                    <div data-dynamic-fields="field_template_b"></div>
                
                    <script id="field_template_b" type="text/x-handlebars-template">
                        <div class="uk-grid form_section autocomplete-wrapper">
                            <div class="uk-width-1-1">
                                <div class="uk-input-group">
                                <input type="text" data-id="branch{{ '{'.'{counter}'.'}' }}" data-user="branch" class="userSelector md-input" placeholder="{{ __('admin.Type name here') }}">
                                <input type="hidden" id="branch{{ '{'.'{counter}'.'}' }}" name="branch[]">
                                <span class="uk-input-group-addon">
                                        <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
                                    </span>
                                </div>
                            </div>
                            <div class="uk-dropdown" aria-expanded="true">
                                <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results md-list">
                                </ul>
                            </div>
                        </div>
                    </script>
                </div>
            @endif
            
    
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Offer') }}</button>
    </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection