@extends('/backend/admin/layout')
@section('content')

<h2 class=" uk-margin-bottom">{{ __('admin.Profile') }}</h2>
<form action="{{ route('admin.account.update') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Current Password') }}</label>
                    <input type="password" class="md-input" name="current_password" required="required" value="">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Name') }}</label>
                    <input type="text" class="md-input" name="name" required="required" value="{{ Auth::user()->name ?: '' }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Email') }}</label>
                    <input type="email" class="md-input" name="email" required="required" value="{{ Auth::user()->email ?: '' }}">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.New Password') }}</label>
                    <input type="password" class="md-input" name="password" value="">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.New Password Confirmation') }}</label>
                    <input type="password" class="md-input" name="password_confirmation" value="">
                </div>
            </div>
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Default Language') }}</label></div>
                <select id="default_language" name="default_language" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Default Language') }}">
                    @foreach ($languages as $language)
                        <option value="{{ $language->code }}" {{ Auth::user()->default_language == $language->code ? 'selected="selected"' : '' }}>{{ $language->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Profile Picture') }}</label></div>
                <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo" id="input-file-a" class="dropify" data-default-file="{{ Auth::user()->logo ? url('uploads/' . Auth::user()->logo) : '' }}" /></div>
            </div> 
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Profile') }}</button>
    </div>
    @csrf
</form>
@endsection