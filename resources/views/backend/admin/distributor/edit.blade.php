@extends('/backend/admin/layout')
@section('content')

<?php //dd($provinces); ?>

<h2 class=" uk-margin-bottom">{{ __('admin.Edit Distributor') }}</h2>

<form action="{{ route('distributor.update', ['id' => $distributor->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ $distributor->translate($language['code']) ? $distributor->translate($language['code'])->name : ''}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
            @endforeach
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Email') }}</label>
                    <input type="email" class="md-input" name="email" required="required" value="{{ $distributor->email }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Password') }}</label>
                    <input type="password" class="md-input" name="password">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Password Confirmation') }}</label>
                    <input type="password" class="md-input" name="password_confirmation">
                </div>
            </div>
        </div>
    </div>
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Phone') }}</label>
                    <input type="text" class="md-input" name="phone" value="{{ $distributor->phone }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Mobile') }}</label>
                    <input type="text" class="md-input" name="mobile" value="{{ $distributor->mobile }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Website') }}</label>
                    <input type="url" class="md-input" name="website" value="{{ $distributor->website }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Logo') }}</label></div>
                <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo" id="input-file-a" class="dropify" data-default-file="{{ $distributor->logo ? url('uploads/' . $distributor->logo) : '' }}" /></div>
            </div>
        </div>
    </div>
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Select Country') }}</label></div>
                <select onchange="countryChanged(this.options[this.selectedIndex].value, true, true);" id="country" name="country" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Select Country') }}">
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}" {{ ($country->id == $distributor->country_id) ? 'selected="selected"':'' }}>{{ $country->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Select Province') }}</label></div>
                <select onchange="provinceChanged(this.options[this.selectedIndex].value);" id="province" name="province_id" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Select Province') }}">
                    @foreach ($provinces as $province)
                        <option value="{{ $province->id }}" {{ ($province->id == $distributor->province_id) ? 'selected="selected"':'' }}>{{ $province->name }}</option>
                    @endforeach
                </select>
            </div>

            
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Select City') }}</label></div>
                <select onchange="cityChanged(this.options[this.selectedIndex].value);" id="city" name="city_id" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Select City') }}">
                    @foreach ($cities as $city)
                        <option value="{{ $city->id }}" {{ ($city->id == $distributor->city_id) ? 'selected="selected"':'' }}>{{ $city->name }}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Select District') }}</label></div>
                <select id="district" name="district_id" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Select District') }}">
                    @foreach ($districts as $district)
                        <option value="{{ $district->id }}" {{ ($district->id == $distributor->district_id) ? 'selected="selected"':'' }}>{{ $district->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Address 1') }}</label>
                    <input type="text" class="md-input" name="address1" value="{{ $distributor->address1 }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Address 2') }}</label>
                    <input type="text" class="md-input" name="address2" value="{{ $distributor->address2 }}">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Latitude') }}</label>
                    <input type="text" class="md-input" name="latitude" id="latitude" value="{{ $distributor->latitude }}">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Longitude') }}</label>
                    <input type="text" class="md-input" name="longitude" id="longitude" value="{{ $distributor->longitude }}">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Map') }}</label></div>
                <div id="map" class="gmap" style="width:100%;height:400px;"></div>

            </div>
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Distributor') }}</button>
    </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection