@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Edit Country') }}</h2>

<form action="{{ route('country.update', ['id' => $country->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ $country->translate($language['code']) ? $country->translate($language['code'])->name : ''}}" class="input-count md-input" name="name_{{ $language['code'] }}" required="required" maxlength="255">
                    </div>
                </div>
            @endforeach
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Dial Code') }}</label>
                    <input type="number" value="{{ $country->dial_code ?: ''}}" class="input-count md-input" name="dial_code" id="" maxlength="3">
                </div>
            </div>
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Country') }}</button>
    </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection