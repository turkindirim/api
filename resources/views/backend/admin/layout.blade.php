@include('/backend/admin/header')

<div class="wrapper">
    @include('/backend/admin/sidebar')

        
    <div id="page_content">
        <div id="page_content_inner">

            @if ($errors->any())

                <div class="uk-margin-top uk-alert uk-alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (\Session::has('success'))
                <div class="uk-alert uk-alert-success uk-margin-top">
                    <span>{!! \Session::get('success') !!}</span>
                </div>
            @endif

            @yield('content')

        </div>
    </div>
</div>

@include('backend/admin/footer')