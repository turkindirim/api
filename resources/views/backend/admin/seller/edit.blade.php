@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Edit Seller') }}</h2>

<form action="{{ route('seller.update', ['id' => $seller->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class=" uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ $seller->translate($language['code']) ? $seller->translate($language['code'])->name : ''}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter" maxlength="255">
                    </div>
                </div>
            @endforeach

            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Website') }}</label>
                    <input type="url" class="md-input" name="website" value="{{ $seller->website }}">
                </div>
            </div>

            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.facebook') }}</label>
                    <input type="url" class="md-input" name="facebook" value="{{ $seller->facebook }}">
                </div>
            </div>

            
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.twitter') }}</label>
                    <input type="url" class="md-input" name="twitter" value="{{ $seller->twitter }}">
                </div>
            </div>

            
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.instagram') }}</label>
                    <input type="url" class="md-input" name="instagram" value="{{ $seller->instagram }}">
                </div>
            </div>

            
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.youtube') }}</label>
                    <input type="url" class="md-input" name="youtube" value="{{ $seller->youtube }}">
                </div>
            </div>

            <div class="uk-row-first uk-margin-large-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled "><label>{{ __('admin.Featured') }}</label></div>
                <select name="featured" class="uk-width-1-1"  data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Featured') }}">
                    <option value="1" {{ $seller->featured == 1 ? 'selected="selected"' : '' }}>{{ __('admin.Featured') }}</option>
                    <option value="0" {{ $seller->featured == 0 ? 'selected="selected"' : '' }}>{{ __('admin.Normal') }}</option>
                </select>
            </div>
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Logo') }}</label></div>
                <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo" id="input-file-a" class="dropify" data-default-file="{{ $seller->logo ? url('uploads/' . $seller->logo) : '' }}" /></div>
            </div>
            
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
            <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Seller') }}</button>
        </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection