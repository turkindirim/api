@extends('/backend/admin/layout')
@section('content')



<h2 class=" uk-margin-bottom">{{ __('admin.Filter') }}</h2>


<form action="{{ route('seller.filter') }}" method="post">
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-grid">
                <div class="uk-width-large-1-5 uk-width-medium-1-2 uk-row-first">
                    <label>{{ __('admin.ID') }}</label>
                    <input type="number" min="1" class="md-input" name="id" value="{{ array_key_exists('id', $fields) ? $fields['id'] : '' }}" />
                </div>
                <div class="uk-width-large-1-5 uk-width-medium-1-2">
                    <label>{{ __('admin.Name') }}</label>
                    <input type="text" class="md-input" name="name" value="{{ array_key_exists('name', $fields) ? $fields['name'] : '' }}" />
                </div>
                <div class="uk-width-large-1-5 uk-width-1-2">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_start">{{ __('admin.From') }}</label>
                        <input name="created_at_from" class="md-input" type="text" id="uk_dp_start" value="{{ array_key_exists('created_at_from', $fields) ? $fields['created_at_from'] : '' }}">
                    </div>
                </div>
                <div class="uk-width-large-1-5 uk-width-medium-1-2">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_end">{{ __('admin.To') }}</label>
                        <input name="created_at_to" class="md-input" type="text" id="uk_dp_end" value="{{ array_key_exists('created_at_to', $fields) ? $fields['created_at_to'] : '' }}">
                    </div>
                </div>
                <div class="uk-row-first">
                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Filter') }}</button>
                </div>
            </div>
        </div>
    </div>
    @csrf
</form>


<h2 class=" uk-margin-bottom">{{ __('admin.Sellers') }}</h2>

<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Titles') }}</th>
                    <th>{{ __('admin.Logo') }}</th>
                    <th>{{ __('admin.Website') }}</th>
                    <th>{{ __('admin.Branches') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($sellers as $seller)
                    <tr>
                        <td>{{ $seller['id'] }}</td>
                        <td>{{ $seller['name'] }}</td>
                        <td><img class="logo" src="{{ url('uploads/' . $seller['logo']) }}" alt=""></td>
                        <td><a href="{{ $seller['website'] }}" target="_blank">{{ $seller['website'] }}</a></td>
                        <td>@php echo count($seller['branches']) @endphp</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ window.location.href='{{ route('seller.destroy', ['id' => $seller['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <a href="{{ route('seller.edit', ['id' => $seller['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('backend/admin/pagination')


<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('seller.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection