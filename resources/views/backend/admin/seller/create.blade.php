@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Create Seller') }}</h2>

<form action="{{ route('seller.store') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ old('name_' . $language['code'])}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
            @endforeach

            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Website') }}</label>
                    <input type="url" class="md-input" name="website" value="{{ old('website') }}">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Facebook') }}</label>
                    <input type="url" class="md-input" name="facebook" value="{{ old('facebook') }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.twitter') }}</label>
                    <input type="url" class="md-input" name="twitter" value="{{ old('twitter') }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.instagram') }}</label>
                    <input type="url" class="md-input" name="instagram" value="{{ old('instagram') }}">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.youtube') }}</label>
                    <input type="url" class="md-input" name="youtube" value="{{ old('youtube') }}">
                </div>
            </div>
            <div class="uk-row-first uk-margin-large-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled "><label>{{ __('admin.Featured') }}</label></div>
                <select name="featured" class="uk-width-1-1"  data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Featured') }}">
                    <option value="1">{{ __('admin.Featured') }}</option>
                    <option value="0">{{ __('admin.Normal') }}</option>
                </select>
            </div>

            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Logo') }}</label></div>
                <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo" id="input-file-a" class="dropify" /></div>
            </div>            
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Create Seller') }}</button>
    </div>
    @csrf
</form>


@endsection