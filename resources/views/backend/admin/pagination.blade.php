@if (!empty($last_page))
    @if ($last_page > 1)
        @if ($current_page > 1)
            <a href="{{ $previousPageUrl }}" class="md-btn md-btn-wave waves-effect waves-button">{{ __('admin.Previous') }}</a>
        @endif
        <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
            <button class="md-btn">{{ __('admin.Go to page') }} ({{$current_page}})<i class="material-icons">&#xE313;</i></button>
            <div class="uk-dropdown uk-dropdown-scrollable">
                <ul class="uk-nav uk-nav-dropdown">
                    <?php
                    $qs = '';
                    if (isset($_GET['categories'])) {
                        if (is_numeric($_GET['categories'])) {
                            $qs .= '&categories='.$_GET['categories'];
                        }

                    }    
                    ?>
                    @for ($i = 0; $i < $last_page; $i++)
                        <li>
                            <a href="?page={{ ($i + 1) . $qs }}" class="{{ (($i + 1) == $current_page) ? ' md-btn-primary' : ''}}">{{ ($i + 1) }}</a>
                        </li>
                    @endfor
                </ul>
            </div>
        </div>
        @if ($last_page > $current_page)
            <a href="{{ $nextPageUrl }}" class="md-btn md-btn-wave waves-effect waves-button">{{ (__('admin.Next')) }}</a>
        @endif
    @endif
@endif