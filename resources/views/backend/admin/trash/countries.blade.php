@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Countries') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Country') }}</th>
                    <th>{{ __('admin.Provinces') }}</th>
                    <th>{{ __('admin.Dial Code') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($countries as $country)
                    <tr>
                        <td>{{ $country['id'] }}</td>
                        <td>{{ $country['name'] }}</td>
                        <td>{{ count($country['provinces']) }}</td>
                        <td>{{ $country['dial_code'] }}</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Permanently Delete Item') }}', function(){ window.location.href='{{ route('country.forcedelete', ['id' => $country['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <button class="md-btn md-btn-primary" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Restore Item') }}', function(){ window.location.href='{{ route('country.restore', ['id' => $country['id']]) }}'; });">
                                <i class="material-icons">launch </i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection