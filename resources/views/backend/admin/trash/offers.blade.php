@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Offers') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Name') }}</th>
                    <th>{{ __('admin.Period') }}</th>
                    <th>{{ __('admin.Publish Date') }}</th>
                    <th>{{ __('admin.Branches') }}</th>
                    <th>{{ __('admin.Categories') }}</th>
                    <!-- <th>{{ __('admin.Media') }}</th> //-->
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($offers as $offer)
                    <tr>
                        <td>{{ $offer['id'] }}</td>
                        <td>{{ $offer['name'] }}</td>
                        <td>{{ $offer['valid_from'] . ' | ' . $offer['valid_to'] }}</td>
                        <td>{{ $offer['created_at']->format('Y-m-d') }}</td>
                        <td>
                            @if ($offer['branches']->count() > 6)
                                {{ $offer['branches']->count() }}
                            @else
                                @foreach ($offer['branches'] as $branch)
                                    {{ $branch['name'] }}
                                    @if (!$loop->last)
                                        {{ ' - ' }}
                                    @endif
                                @endforeach
                            @endif    
                        </td>
                        <td>
                            @if ($offer['categories']->count() > 6)
                                {{ $offer['categories']->count() }}
                            @else
                                @foreach ($offer['categories'] as $category)
                                    {{ $category['name'] }}
                                    @if (!$loop->last)
                                        {{ ' - ' }}
                                    @endif
                                @endforeach
                            @endif
                        </td>
                        <!-- 
                        <td><button class="md-btn" onclick="getOfferMedia( {{ $offer['id'] }})" data-uk-modal="{target:'#media_modal'}">{{ __('admin.Open') }}</button></td>
                        //-->
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Permanently Delete Item') }}', function(){ window.location.href='{{ route('offers.forcedelete', ['id' => $offer['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <button class="md-btn md-btn-primary" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Restore Item') }}', function(){ window.location.href='{{ route('offers.restore', ['id' => $offer['id']]) }}'; });">
                                <i class="material-icons">launch </i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection