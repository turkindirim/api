@extends('/backend/admin/layout')
@section('content')

<?php //dd($distributors); ?>

<h2 class=" uk-margin-bottom">{{ __('admin.Distributors') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Titles') }}</th>
                    <th>{{ __('admin.Logo') }}</th>
                    <th>{{ __('admin.Email') }}</th>
                    <th>{{ __('admin.Phone') }}</th>
                    <th>{{ __('admin.Mobile') }}</th>
                    <th>{{ __('admin.Website') }}</th>
                    <th>{{ __('admin.Address') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($distributors as $distributor)
                    <tr>
                        <td>{{ $distributor['id'] }}</td>
                        <td>{{ $distributor['name'] }}</td>
                        <td><img class="logo" src="{{ url('uploads/' . $distributor['logo']) }}" alt=""></td>
                        <td>{{ $distributor['email'] }}</td>
                        <td>{{ $distributor['phone'] }}</td>
                        <td>{{ $distributor['mobile'] }}</td>
                        <td><a href="{{ $distributor['website'] }}" target="_blank">{{ $distributor['website'] }}</a></td>
                        <td>
                            <p>
                                {{ $distributor['province']['name'] . ' - ' . $distributor['city']['name'] . ' - ' . $distributor['district']['name'] }}
                                <br>
                                {{ $distributor['address1'] }}
                                <br>
                                {{ $distributor['address2'] }}
                                @if ($distributor['longitude'] && $distributor['latitude'])
                                    <a href="{{ 'https://www.google.com/maps/@' . $distributor['latitude'] . ',' . $distributor['longitude']}},12z" target="_blank"><i class="material-icons">location_on</i></a>
                                @endif
                            </p>
                            
                        </td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Permanently Delete Item') }}', function(){ window.location.href='{{ route('distributor.forcedelete', ['id' => $distributor['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <button class="md-btn md-btn-primary" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Restore Item') }}', function(){ window.location.href='{{ route('distributor.restore', ['id' => $distributor['id']]) }}'; });">
                                <i class="material-icons">launch </i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection