@extends('/backend/admin/layout')
@section('content')



<h2 class=" uk-margin-bottom">{{ __('admin.Branches') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Titles') }}</th>
                    <th>{{ __('admin.Seller') }}</th>
                    <th>{{ __('admin.Email') }}</th>
                    <th>{{ __('admin.Phone') }}</th>
                    <th>{{ __('admin.Mobile') }}</th>
                    <th>{{ __('admin.Address') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($branches as $branch)
                    <tr>
                        <td>{{ $branch['id'] }}</td>
                        <td>{{ $branch['name'] }}</td>
                        <td>
                            <img class="mini-logo" src="{{ url('uploads/' . $branch['logo']) }}" alt="">
                            <p>{{ $branch['sellername'] }}</p>
                        </td>
                        <td>{{ $branch['email'] }}</td>
                        <td>{{ $branch['phone'] }}</td>
                        <td>{{ $branch['mobile'] }}</td>
                        
                        <td>
                            <p>
                                {{ $branch['province']['name'] . ' - ' . $branch['city']['name'] . ' - ' . $branch['district']['name'] }}
                                <br>
                                {{ $branch['address1'] }}
                                <br>
                                {{ $branch['address2'] }}
                                @if ($branch['longitude'] && $branch['latitude'])
                                    <a href="{{ 'https://www.google.com/maps/@' . $branch['latitude'] . ',' . $branch['longitude']}},12z" target="_blank"><i class="material-icons">location_on</i></a>
                                @endif
                            </p>
                            
                        </td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Permanently Delete Item') }}', function(){ window.location.href='{{ route('branch.forcedelete', ['id' => $branch['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <button class="md-btn md-btn-primary" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Restore Item') }}', function(){ window.location.href='{{ route('branch.restore', ['id' => $branch['id']]) }}'; });">
                                <i class="material-icons">launch </i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection