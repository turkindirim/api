@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Provinces') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Province') }}</th>
                    <th>{{ __('admin.Country') }}</th>
                    <th>{{ __('admin.Cities') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($provinces as $province)
                    <tr>
                        <td>{{ $province['id'] }}</td>
                        <td>{{ $province['name'] }}</td>
                        <td>{{ $province['country']['name'] }}</td>
                        <td>{{ count($province['cities']) }}</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Permanently Delete Item') }}', function(){ window.location.href='{{ route('province.forcedelete', ['id' => $province['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <button class="md-btn md-btn-primary" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Restore Item') }}', function(){ window.location.href='{{ route('province.restore', ['id' => $province['id']]) }}'; });">
                                <i class="material-icons">launch </i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection