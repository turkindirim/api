@extends('/backend/admin/layout')
@section('content')

<h2 class=" uk-margin-bottom">{{ __('admin.Cards') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table table_check">
                <thead>
                <tr>
                    <th class="uk-width-1-10 uk-text-center small_col"><input type="checkbox" data-md-icheck class="check_all"></th>
                    <th>{{ __('admin.Card') }}</th>
                    <th>{{ __('admin.Flag') }}</th>
                    <th>{{ __('admin.Type') }}</th>
                    <th>{{ __('admin.Distributor') }}</th>
                    <th>{{ __('admin.User') }}</th>
                    <th>{{ __('admin.Generated') }}</th>
                    <th>{{ __('admin.Sold') }}</th>
                    <th>{{ __('admin.Used') }}</th>
                    <th>{{ __('admin.Period') }}</th>
                    <th>{{ __('admin.Valid Dates') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($cards as $card)
                    <tr data-id="{{ $card['id'] }}">
                        <td class="uk-table-middle small_col"><input type="checkbox" data-md-icheck class="check_row"></td>
                        <td>{{ $card['number'] }}</td>
                        <td>{{ $card['flag'] }}</td>
                        <td>{{ $card['type']['name'] }}</td>
                        <td>{{ $card['distributor']['name'] }}</td>
                        <td>{{ $card['user']['name'] }}</td>
                        <td>{{ $card['created_at']->format('Y-m-d')}}</td>
                        <td>{{ $card['sold_at'] ? $card['sold_at']->format('Y-m-d') : '-' }}</td>
                        <td>{{ $card['used_at'] ? $card['used_at']->format('Y-m-d') : '-' }}</td>
                        
                        <td>{{ $card['period'] }}</td>
                        <td>
                            {!!
                                $card['used_at'] ? 
                                '<div style="color:#239938;">'.$card['used_at']->format('Y-m-d') .'</div><div style="color:#992222;">'. $card['used_at']->addDays($card['period'])->format('Y-m-d').'</div>' :
                                ''
                            !!}</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Permanently Delete Item') }}', function(){ window.location.href='{{ route('cards.forcedelete', ['id' => $card['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <button class="md-btn md-btn-primary" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Restore Item') }}', function(){ window.location.href='{{ route('cards.restore', ['id' => $card['id']]) }}'; });">
                                <i class="material-icons">launch </i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection