@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Sellers') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Titles') }}</th>
                    <th>{{ __('admin.Logo') }}</th>
                    <th>{{ __('admin.Website') }}</th>
                    <th>{{ __('admin.Branches') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($sellers as $seller)
                    <tr>
                        <td>{{ $seller['id'] }}</td>
                        <td>{{ $seller['name'] }}</td>
                        <td><img class="logo" src="{{ url('uploads/' . $seller['logo']) }}" alt=""></td>
                        <td><a href="{{ $seller['website'] }}" target="_blank">{{ $seller['website'] }}</a></td>
                        <td>@php echo count($seller['branches']) @endphp</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Permanently Delete Item') }}', function(){ window.location.href='{{ route('seller.forcedelete', ['id' => $seller['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <button class="md-btn md-btn-primary" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Restore Item') }}', function(){ window.location.href='{{ route('seller.restore', ['id' => $seller['id']]) }}'; });">
                                <i class="material-icons">launch </i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection