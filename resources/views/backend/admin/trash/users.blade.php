@extends('/backend/admin/layout')

@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Users') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Name') }}</th>
                    <th>{{ __('admin.Email') }}</th>
                    <th>{{ __('admin.Profile Picture') }}</th>
                    <th>{{ __('admin.Cards') }}</th>
                    <th>{{ __('admin.Nationality') }}</th>
                    <th>{{ __('admin.Passport') }}</th>
                    <th>{{ __('admin.Mobile') }}</th>
                    <th>{{ __('admin.Address') }}</th>
                    <th>{{ __('admin.Registration Date') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user['id'] }}</td>
                            <td>{{ $user['name'] }}</td>
                            <td>{{ $user['email'] }}</td>
                            <td>{{ $user['logo'] ? '<img class="mini-logo" src="'.url('uploads/' . $user['logo']).'">' : '' }}</td>
                            <td>{{ $user['cards']->count() }}</td>
                            <td>{{ $user['getNationality']['name'] }}</td>
                            <td>{{ $user['passport'] ? '<a target="_blank" href="'.url('uploads/'.$user['passport']).'">'.__('admin.View').'</a>': '' }}</td>
                            <td>{{ $user['mobile'] }}</td>
                            <td>{{ $user['country']['name'] .', '.$user['province']['name'] .', '. $user['address1'] .','. $user['address2'] }}</td>
                            <td>{{ $user['created_at']->format('Y-m-d') }}</td>
                            <td>
                                <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Permanently Delete Item') }}', function(){ window.location.href='{{ route('users.forcedelete', ['id' => $user['id']]) }}'; });">
                                    <i class="uk-icon-close"></i>
                                </button>
                                <button class="md-btn md-btn-primary" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Restore Item') }}', function(){ window.location.href='{{ route('users.restore', ['id' => $user['id']]) }}'; });">
                                    <i class="material-icons">launch </i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection