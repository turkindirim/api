@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Offer Categories') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Title') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                    <tr>
                        <td>{{ $category['id'] }}</td>
                        <td>{{ $category['name'] }}</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Permanently Delete Item') }}', function(){ window.location.href='{{ route('categories.forcedelete', ['id' => $category['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <button class="md-btn md-btn-primary" type="button" onclick="UIkit.modal.confirm('{{ __('admin.Restore Item') }}', function(){ window.location.href='{{ route('categories.restore', ['id' => $category['id']]) }}'; });">
                                <i class="material-icons">launch </i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection