@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Create Cards') }}</h2>

<form action="{{ route('cards.store') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Count') }}</label>
                    <input type="number" value="{{ old('count') ?: '1' }}" class="md-input" name="count" max="10000" min="1">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Flag') }}</label>
                    <input type="text" value="{{ old('flag') }}" class="input-count md-input" name="flag" id="flag" maxlength="32">
                </div>
            </div>
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Type') }}</label></div>
                <select name="type" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Type') }}">
                    @foreach ($types as $type)
                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="uk-row-first  autocomplete-wrapper">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.User') }}</label>
                    <input type="text" data-id="user" data-user="user" class="userSelector md-input">
                    <input type="hidden" id="user" name="user" value="{{ old('user') }}">
                </div>
                <div class="uk-dropdown" aria-expanded="true">
                    <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results md-list">
                    </ul>
                </div>
            </div>
            <div class="uk-row-first autocomplete-wrapper">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Distributor') }}</label>
                    <input type="text" data-id="distributor" data-user="distributor" class="userSelector md-input">
                    <input type="hidden" id="distributor" name="distributor" value="{{ old('distributor') }}">
                </div>
                <div class="uk-dropdown" aria-expanded="true">
                    <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results md-list">
                    </ul>
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Period') }}</label>
                    <input type="number" value="{{ old('period') }}" class="md-input" name="period" max="1000" min="1">
                </div>
            </div>
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Create Cards') }}</button>
    </div>
    @csrf
</form>


@endsection