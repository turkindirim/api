@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Set Distributor') }}</h2>



<div class="md-card">
    <div class="md-card-content">
        <form action="#" method="POST" id="setDistributorForm">
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Card Number') }}</label>
                    <input type="text" value="" class="md-input" id="cardNumber">
                </div>
            </div>
            @csrf
        </form>
    </div>
</div>


<div class="md-card">
    <div class="md-card-content">
        <form action="{{ route('cards.setDistributor') }}" method="post">
            <div class="uk-grid uk-grid-divider">
                <div class="uk-width-medium-3-4 uk-row-first">
                    <h4>{{ __('admin.Cards List') }}</h4>
                    <table class="uk-table uk-margin-large-bottom">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ __('admin.Card Number') }}</th>
                                <th>{{ __('admin.Generated') }}</th>
                                <th>{{ __('admin.Options') }}</th>
                            </tr>
                        </thead>
                        <tbody id="cards_list">

                        </tbody>
                    </table>
                </div>
                <div class="uk-width-medium-1-5">
                    <h4>{{ __('admin.Distributor') }}</h4>
                    <div class="uk-row-first autocomplete-wrapper">
                        <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Distributor') }}</label>
                            <input type="text" data-id="distributor" data-user="distributor" class="userSelector md-input" required>
                            <input type="hidden" id="distributor" name="distributor" value="{{ old('distributor') }}">
                        </div>
                        <div class="uk-dropdown" aria-expanded="true">
                            <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results md-list">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="uk-grid">
                <div class="uk-margin-top uk-row-first">
                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Set Distributor') }}</button>
                </div>
            </div>
            @csrf
        </form>
    </div>
</div>

@endsection