@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Edit Card') }}</h2>

<form action="{{ route('cards.update', ['id' => $card->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Flag') }}</label>
                    <input type="text" value="{{ $card->flag }}" class="input-count md-input" name="flag" id="flag" maxlength="32">
                </div>
            </div>
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Type') }}</label></div>
                <select name="type" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Type') }}">
                    @foreach ($types as $type)
                        <option value="{{ $type->id }}" {{ $type->id == $card->type_id ? 'selected="selected"' : '' }}>{{ $type->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="uk-row-first autocomplete-wrapper">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Distributor') }}</label>
                    <input type="text" data-id="distributor" value="{{ $card->distributor ? $card->distributor->name : '' }}" data-user="distributor" class="userSelector md-input">
                    <input type="hidden" id="distributor" name="distributor" value="{{ $card->distributor_id ?: '' }}">
                </div>
                <div class="uk-dropdown" aria-expanded="true">
                    <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results md-list">
                    </ul>
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Period') }}</label>
                    <input type="number" value="{{ $card->period }}" class="md-input" name="period" max="1000" min="1">
                </div>
            </div>
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Card') }}</button>
    </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection