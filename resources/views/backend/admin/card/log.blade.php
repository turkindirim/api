@extends('/backend/admin/layout')
@section('content')

<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <table class="uk-table">
            <thead>
                <tr>
                    <th>{{ __('admin.ID') }}</th>
                    <th>{{ __('admin.Card') }}</th>
                    <th>{{ __('admin.Generated') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $card->id }}</td>
                    <td>{{ $card->number }}</td>
                    <td>{{ $card->created_at->format('Y-m-d') }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <table class="uk-table" id="dt_tableExport">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.User') }}</th>
                    <th>{{ __('admin.Action') }}</th>
                    <th>{{ __('admin.Description') }}</th>
                    <th>{{ __('admin.Date') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($log as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>
                            @php
                                $user_type = explode('\\', $item->userable_type);
                            @endphp
                            {!! '<b>' . end($user_type) . ':</b> ' !!}
                            {{ $item->userable['name'] }}
                        </td>
                        <td>{{ $item->action }}</td>
                        <td>
                            @switch($item->action)
                                @case('Activation Failed')
                                    {{ __('admin.' . $item->description) }}
                                    @break
                                @default
                                    @if (\strlen($item->description) > 2)
                                        @php $params = json_decode($item->description, true); @endphp
                                        @foreach ($params as $key => $value)
                                            {!! '<b>' . $key . ':</b> ' . $value !!}
                                            @if (!$loop->last) 
                                                {!! '<br>' !!}
                                            @endif
                                        @endforeach
                                    @else
                                        
                                    @endif
                                    
                            @endswitch
                        </td>
                        <td>{{ $item->created_at->format('Y-m-d | H:i') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@foreach ($log as $item)
    
@endforeach

@endsection