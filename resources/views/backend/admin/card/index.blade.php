@extends('/backend/admin/layout')
@section('header_script')
<script>
var DT_Ajax_URL = "{{ route('cards.search') }}",
    DT_Cols_Arr = [
        { "data": "checkbox", "orderable":false  },
        { "data": "id" },
        { "data": "number", render: function(x) {
            return '' + x;
        } },
        { "data": "activation_code" },
        { "data": "flag" },
        { "data": "type" },
        { "data": "distributor.name" },
        { "data": "user.name" },
        { "data": "created_at" },
        { "data": "activated_at.date" },
        { "data": "sold_at.date" },
        { "data": "used_at.date" },
        { "data": "period" },
        { "data": "valid_dates" },
        { "data": "options", "orderable":false }
    ],
    extra_data = () => {
        return {
            'active_status' : document.getElementById('active_status').value,
            'used_status' : document.getElementById('used_status').value,
            'sold_status' : document.getElementById('sold_status').value,
            'expired_status' : document.getElementById('expired_status').value,
        }
    }
    ;

</script>
@endsection
@section('content')

<h2 class=" uk-margin-bottom">{{ __('admin.Filter') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-grid">
            <div class="uk-width-large-1-5 uk-width-medium-1-2">
                <select id="sold_status" class="uk-width-1-1" data-md-select2 data-allow-clear="true" data-placeholder="{{ __('admin.Sold') }}">
                    <option value="">{{ __('admin.All') }}</option>
                    <option value="1">{{ __('admin.Sold Only') }}</option>
                    <option value="0">{{ __('admin.Unsold Only') }}</option>
                </select>
            </div>
            <div class="uk-width-large-1-5 uk-width-medium-1-2 uk-row-first">
                <select id="active_status" class="uk-width-1-1" data-md-select2 data-allow-clear="true" data-placeholder="{{ __('admin.Active') }}">
                    <option value="">{{ __('admin.All') }}</option>
                    <option value="1">{{ __('admin.Activated Only') }}</option>
                    <option value="0">{{ __('admin.Inactivated Only') }}</option>
                </select>
            </div>

            <div class="uk-width-large-1-5 uk-width-medium-1-2">
                <select id="used_status" class="uk-width-1-1" data-md-select2 data-allow-clear="true" data-placeholder="{{ __('admin.Used') }}">
                    <option value="">{{ __('admin.All') }}</option>
                    <option value="1">{{ __('admin.Used Only') }}</option>
                    <option value="0">{{ __('admin.Unused Only') }}</option>
                </select>
            </div>
            <div class="uk-width-large-1-5 uk-width-medium-1-2">
                <select id="expired_status" class="uk-width-1-1" data-md-select2 data-allow-clear="true" data-placeholder="{{ __('admin.Expired') }}">
                    <option value="">{{ __('admin.All') }}</option>
                    <option value="1">{{ __('admin.Expired Only') }}</option>
                    <option value="0">{{ __('admin.Unexpired Only') }}</option>
                </select>
            </div>
            
            <div class="uk-row-first">
                <button type="button" class="updateAjaxTable md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Filter') }}</button>
            </div>
        </div>
    </div>
</div>

<h2 class=" uk-margin-bottom">{{ __('admin.Cards') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableAjax" class="uk-table table_check uk-table-align-vertical">
                <thead>
                <tr>
                    <th class="uk-width-1-10  small_col"><input type="checkbox" data-md-icheck class="check_all"></th>
                    <th>#</th>
                    <th>{{ __('admin.Card') }}</th>
                    <th>{{ __('admin.Activation Code') }}</th>
                    <th>{{ __('admin.Flag') }}</th>
                    <th>{{ __('admin.Type') }}</th>
                    <th>{{ __('admin.Distributor') }}</th>
                    <th>{{ __('admin.User') }}</th>
                    <th>{{ __('admin.Generated') }}</th>
                    <th>{{ __('admin.Activated') }}</th>
                    <th>{{ __('admin.Sold') }}</th>
                    <th>{{ __('admin.Used') }}</th>
                    <th>{{ __('admin.Period') }}</th>
                    <th>{{ __('admin.Expired') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<h2 class=" uk-margin-large-top">{{ __('admin.Bulk Actions') }}</h2>
<form action="{{ route('cards.bulk') }}" method="post" class="bulk-submit" data-table="dt_tableAjax">
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-grid">
                <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-row-first">
                    <label>{{ __('admin.Period') }}</label>
                    <input type="text" class="md-input" name="period" />
                </div>
                <div class="uk-width-large-1-4 uk-width-medium-1-2">
                    <label>{{ __('admin.Flag') }}</label>
                    <input type="text" class="md-input" name="flag" />
                </div>
                <div class="uk-width-large-1-4 uk-width-medium-1-2  autocomplete-wrapper">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Distributor') }}</label>
                        <input type="text" data-id="distributor" data-user="distributor" class="userSelector md-input">
                        <input type="hidden" id="distributor" name="distributor" value="{{ old('distributor') }}">
                    </div>
                    <div class="uk-dropdown" aria-expanded="true">
                        <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results md-list">
                        </ul>
                    </div>
                </div>
                <div class="uk-width-large-1-4 uk-width-medium-1-2">
                    <select name="type" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Type') }}">
                        <option value="">{{ __('admin.Type') }}</option>
                        @foreach ($types as $type)
                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="uk-row-first">
                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Apply') }}</button>
                </div>
            </div>
        </div>
    </div>
    @csrf
    <input type="hidden" name="cards" class="ids" value="">
</form>


<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('cards.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection