@extends('/backend/admin/layout')
@section('header_script')
<script>
var DT_Ajax_URL = "{{ route('transactions.search') }}",
    DT_Cols_Arr = [
        { "data": "id" },
        { "data": "card.number" },
        { "data": "branch.name"},
        { "data": "user.name" },
        { "data": "created_at", render: function(data, type, row){
            console.log(type, row);
            return row.created_at.date + ' ' + row.created_at.time;
        }}
    ],
    extra_data = () => {return {}};
</script>
@endsection
@section('content')

<h2 class=" uk-margin-bottom">{{ __('admin.Transactions') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableAjax" class="uk-table table_check uk-table-align-vertical">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Card') }}</th>
                    <th>{{ __('admin.Branch') }}</th>
                    <th>{{ __('admin.User') }}</th>
                    <th>{{ __('admin.Generated') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection