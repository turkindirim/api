@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Filter') }}</h2>


<form action="{{ route('branch.filter') }}" method="post">
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-grid">
                <div class="uk-width-large-1-10 uk-width-medium-1-2 uk-row-first">
                    <label>{{ __('admin.ID') }}</label>
                    <input type="number" min="1" class="md-input" name="id" value="{{ array_key_exists('id', $fields) ? $fields['id'] : '' }}" />
                </div>
                <div class="uk-width-large-2-5 uk-width-medium-1-2">
                    <label>{{ __('admin.Name').', '.__('admin.Email').', '.__('admin.Phone').', '.__('admin.Mobile').', '.__('admin.Address') }}</label>
                    <input type="text" class="md-input" name="name" value="{{ array_key_exists('name', $fields) ? $fields['name'] : '' }}" />
                </div>
                <div class="uk-width-large-1-5 uk-width-1-2">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_start">{{ __('admin.From') }}</label>
                        <input name="created_at_from" class="md-input" type="text" id="uk_dp_start" value="{{ array_key_exists('created_at_from', $fields) ? $fields['created_at_from'] : '' }}">
                    </div>
                </div>
                <div class="uk-width-large-1-5 uk-width-medium-1-2">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="uk_dp_end">{{ __('admin.To') }}</label>
                        <input name="created_at_to" class="md-input" type="text" id="uk_dp_end" value="{{ array_key_exists('created_at_to', $fields) ? $fields['created_at_to'] : '' }}">
                    </div>
                </div>
                <div class="uk-row-first">
                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Filter') }}</button>
                </div>
            </div>
        </div>
    </div>
    @csrf
</form>


<h2 class=" uk-margin-bottom">{{ __('admin.Branches') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Titles') }}</th>
                    <th>{{ __('admin.Seller') }}</th>
                    <th>{{ __('admin.Email') }}</th>
                    <th>{{ __('admin.Phone') }}</th>
                    <th>{{ __('admin.Mobile') }}</th>
                    <th>{{ __('admin.Address') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($branches as $branch)
                    <tr>
                        <td>{{ $branch['id'] }}</td>
                        <td>{{ $branch['name'] }}</td>
                        <td>
                            <img class="mini-logo" src="{{ (isset($branch['logo'])) ? url('uploads/' . $branch['logo']) : '' }}" alt="">
                            <p>{{ $branch['sellername'] }}</p>
                        </td>
                        <td>{{ $branch['email'] }}</td>
                        <td>{{ $branch['phone'] }}</td>
                        <td>{{ $branch['mobile'] }}</td>
                        
                        <td>
                            <p>
                                {{ $branch['province']['name'] . ' - ' . $branch['city']['name'] . ' - ' . $branch['district']['name'] }}
                                <br>
                                {{ $branch['address1'] }}
                                <br>
                                {{ $branch['address2'] }}
                                @if ($branch['longitude'] && $branch['latitude'])
                                    <a href="{{ 'https://www.google.com/maps/@' . $branch['latitude'] . ',' . $branch['longitude']}},12z" target="_blank"><i class="material-icons">location_on</i></a>
                                @endif
                            </p>
                            
                        </td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ window.location.href='{{ route('branch.destroy', ['id' => $branch['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <a href="{{ route('branch.edit', ['id' => $branch['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                            <a href="{{ route('branch.reviews', ['id' => $branch['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-comment"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@include('backend/admin/pagination')


<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('branch.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection