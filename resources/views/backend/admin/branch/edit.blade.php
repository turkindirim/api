@extends('/backend/admin/layout')
@section('content')

<h2 class=" uk-margin-bottom">{{ __('admin.Edit Branch') }}</h2>

<form action="{{ route('branch.update', ['id' => $branch->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Select Seller') }}</label></div>
                <select name="seller_id" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Select Seller') }}">
                    @foreach ($sellers as $seller)
                        <option value="{{ $seller->id }}" {{ $seller->id == $branch->seller_id ? 'selected="selected"':'' }}>{{ $seller->name }}</option>
                    @endforeach
                </select>
            </div>
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ $branch->translate($language['code']) ? $branch->translate($language['code'])->name : ''}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
                
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Description') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom"><textarea  class="ckeditor" name="description_{{ $language['code'] }}">{{ $branch->translate($language['code']) ? $branch->translate($language['code'])->description : ''}}</textarea></div>
                </div>
            @endforeach
            @if (auth()->user()->superadmin)
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Email') }}</label>
                    <input type="email" class="md-input" name="email" required="required" value="{{ $branch->email ?: '' }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Email Confirmation') }}</label>
                    <input type="email" class="md-input" name="email_confirmation" required="required" value="{{ $branch->email ?: '' }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Password') }}</label>
                    <input type="password" class="md-input" name="password">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Password Confirmation') }}</label>
                    <input type="password" class="md-input" name="password_confirmation">
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Phone') }}</label>
                    <input type="text" class="md-input" name="phone" required="required" value="{{ $branch->phone }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Mobile') }}</label>
                    <input type="text" class="md-input" name="mobile" required="required" value="{{ $branch->mobile }}">
                </div>
            </div>
        </div>
    </div>
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Select Country') }}</label></div>
                <select onchange="countryChanged(this.options[this.selectedIndex].value, true, true);" id="country" name="country" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Select Country') }}">
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}" {{ ($country->id == $branch->country_id) ? 'selected="selected"':'' }}>{{ $country->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Select Province') }}</label></div>
                <select onchange="provinceChanged(this.options[this.selectedIndex].value);" id="province" name="province_id" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Select Province') }}">
                    @foreach ($provinces as $province)
                        <option value="{{ $province->id }}" {{ ($province->id == $branch->province_id) ? 'selected="selected"':'' }}>{{ $province->name }}</option>
                    @endforeach
                </select>
            </div>

            
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Select City') }}</label></div>
                <select onchange="cityChanged(this.options[this.selectedIndex].value);" id="city" name="city_id" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Select City') }}">
                    @foreach ($cities as $city)
                        <option value="{{ $city->id }}" {{ ($city->id == $branch->city_id) ? 'selected="selected"':'' }}>{{ $city->name }}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Select District') }}</label></div>
                <select id="district" name="district_id" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Select District') }}">
                    @foreach ($districts as $district)
                        <option value="{{ $district->id }}" {{ ($district->id == $branch->district_id) ? 'selected="selected"':'' }}>{{ $district->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Address 1') }}</label>
                    <input type="text" class="md-input" name="address1" required="required" value="{{ $branch->address1 }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Address 2') }}</label>
                    <input type="text" class="md-input" name="address2" value="{{ $branch->address2 }}">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Latitude') }}</label>
                    <input type="text" class="md-input" name="latitude" id="latitude" required="required" value="{{ $branch->latitude }}">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Longitude') }}</label>
                    <input type="text" class="md-input" name="longitude" id="longitude" required="required" value="{{ $branch->longitude }}">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Map') }}</label></div>
                <div id="map" class="gmap" style="width:100%;height:400px;"></div>

            </div>
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Branch') }}</button>
    </div>
    <input type="hidden" name="_method" value="PUT">
    @csrf
</form>


@endsection

@section('bedore_dasboard')
@if ($branch->latitude)
    <script type="text/javascript">
    var showMapMarker = true, 
    branchLng = {{ $branch->longitude }},
    branchLat = {{ $branch->latitude }},
    branchName = '{{$branch->name}}';
    </script>
@endif
@endsection