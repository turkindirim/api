@extends('/backend/admin/layout')
@section('content')
<h2 class=" uk-margin-bottom">{{ __('admin.Settings') }}</h2>
<form action="{{ route('settings.update') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Default Language') }}</label></div>
                <select id="default_language" name="default_language" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Default Language') }}">
                    @foreach ($languages as $language)
                        <option value="{{ $language->code }}" {{ $settings->default_language == $language->code ? 'selected="selected"' : '' }}>{{ $language->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit') }}</button>
    </div>
    @csrf
</form>
@endsection