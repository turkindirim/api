@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Create Post') }}</h2>

<form action="{{ route('post.store') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input required type="text" value="{{ old('name_' . $language['code'])}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Description') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom"><textarea  class="ckeditor" name="description_{{ $language['code'] }}">{{ old('description_' . $language['code'])}}</textarea></div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Excerpt') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom"><textarea class="md-input autosized" name="excerpt_{{ $language['code'] }}">{{ old('excerpt_' . $language['code'])}}</textarea></div>
                </div>
                <div class="uk-row-first uk-margin-large-bottom">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Logo') }} - {{ $language['name'] }}</label></div>
                    <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo_{{ $language['code'] }}" class="dropify" /></div>
                </div>  
            @endforeach

            <h4 class="uk-margin-large-top">{{ __('admin.Categories') }}</h4>
            <div class="uk-row-first">
                <select id="category" multiple name="category[]" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Categories') }}">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <h4 class="uk-margin-large-top">{{ __('admin.Tags') }}</h4>
            <div class="uk-row-first ">
                <select id="tag" multiple name="tag[]" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Tags') }}">
                    @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                    @endforeach
                </select>
            </div>
            
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Create Post') }}</button>
    </div>
    @csrf
</form>


@endsection