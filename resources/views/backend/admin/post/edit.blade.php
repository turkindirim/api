@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Edit Post') }}</h2>

<form action="{{ route('post.update', ['id' => $post->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ $post->translate($language['code']) ? $post->translate($language['code'])->name : ''}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Description') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom"><textarea  class="ckeditor" name="description_{{ $language['code'] }}">{{ $post->translate($language['code']) ? $post->translate($language['code'])->description : ''}}</textarea></div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Excerpt') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom"><textarea class="md-input autosized" name="excerpt_{{ $language['code'] }}">{{ $post->translate($language['code']) ? $post->translate($language['code'])->excerpt : ''}}</textarea></div>
                </div>
                <div class=" uk-row-first uk-margin-large-bottom">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Logo') }} - {{ $language['name'] }}</label></div>
                    <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo_{{ $language['code'] }}" id="input-file-a" class="dropify" data-default-file="{{ $post->translate($language['code'])->logo ? url('uploads/' . $post->translate($language['code'])->logo) : '' }}" /></div>
                </div>
            @endforeach
            
            <h4 class="uk-margin-large-top">{{ __('admin.Categories') }}</h4>
            <div class="uk-row-first">
                <select id="category" multiple name="category[]" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Categories') }}">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}" {{ $post->categories->contains($category->id) ? 'selected="selected"':''}}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>

            <h4 class="uk-margin-large-top">{{ __('admin.Tags') }}</h4>
            <div class="uk-row-first ">
                <select id="tag" multiple name="tag[]" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Tags') }}">
                    @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}" {{ $post->tags->contains($tag->id) ? 'selected="selected"':''}}>{{ $tag->name }}</option>
                    @endforeach
                </select>
            </div> 
        </div>
    </div>
    
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Post') }}</button>
    </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection