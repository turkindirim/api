@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Post') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Name') }}</th>
                    <th>{{ __('admin.Picture') }}</th>
                    <th>{{ __('admin.Publish Date') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                    <tr>
                        <td>{{ $post['id'] }}</td>
                        <td>{{ $post['name'] }}</td>
                        <td><img class="logo" src="{{ url('uploads/' . $post['logo']) }}" alt=""></td>
                        <td>{{ $post['created_at']->format('Y-m-d') }}</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ window.location.href='{{ route('post.destroy', ['id' => $post['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <a href="{{ route('post.edit', ['id' => $post['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                            <a href="{{ route('post.reviews', ['id' => $post['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-comment"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@include('backend/admin/pagination')


<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('post.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection