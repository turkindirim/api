@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Admins Log') }}</h2>

<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <form action="" method="POST">
                <div class="uk-grid ">
                    <div class="uk-width-large-1-2 uk-width-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_start">{{ __('admin.From') }}</label>
                            <input name="from" class="md-input" type="text" id="uk_dp_start" required>
                        </div>
                    </div>
                    <div class="uk-width-large-1-2 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_end">{{ __('admin.To') }}</label>
                            <input name="to" class="md-input" type="text" id="uk_dp_end" required>
                        </div>
                    </div>
                </div>
                <div class="uk-margin-top uk-row-first">
                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Filter') }}</button>
                </div>
                @csrf
            </form>
        </div>
    </div>
</div>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Admin') }}</th>
                    <th>{{ __('admin.Model') }}</th>
                    <th>{{ __('admin.Action') }}</th>
                    <th>{{ __('admin.Item') }}</th>
                    <th>{{ __('admin.Details') }}</th>
                    <th>{{ __('admin.Date') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($log as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->admin['name'] }}</td>
                            <td>{{ $item->model }}</td>
                            <td>{{ $item->action }}</td>
                            <td>{{ $item->item }}</td>
                            <td>
                                @if (\strlen($item->description) > 2)
                                    @php $params = json_decode($item->description, true); @endphp
                                    @foreach ($params as $key => $value)
                                        {!! '<b>' . $key . ':</b> ' . $value !!}
                                        @if (!$loop->last) 
                                            {!! '<br>' !!}
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection