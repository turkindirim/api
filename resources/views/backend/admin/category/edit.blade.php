@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Edit Category') }}</h2>

<form action="{{ route('categories.update', ['id' => $category->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first margin-medium-top">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ $category->translate($language['code']) ? $category->translate($language['code'])->name : ''}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Description') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom"><textarea  class="ckeditor" name="description_{{ $language['code'] }}">{{ $category->translate($language['code']) ? $category->translate($language['code'])->description : ''}}</textarea></div>
                </div>
                <div class="uk-width-medium-1-4 uk-row-first margin-medium-bottom">
                    <p>{{ __('admin.Thumbnail') }}</p>
                    <img src="{{ url('uploads/'.$category->translate($language['code'])->header) }}" alt="" class="img-responsive">
                    <input type="file" name="header_{{ $language['code'] }}" accept=".jpg,.jpeg.png,.bmp,.gif">
                </div>
            @endforeach 
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Code') }}</label>
                    <input type="text" value="{{ $category->code }}" class="input-count md-input" name="code" id="input_counter" maxlength="255">
                </div>
            </div> 
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Logo') }}</label></div>
                <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo" id="input-file-a" class="dropify" data-default-file="{{ $category->logo ? url('uploads/' . $category->logo) : '' }}" /></div>
            </div>          
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Category') }}</button>
    </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection