@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Offer Categories') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Title') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                    <tr>
                        <td>{{ $category['id'] }}</td>
                        <td>{{ $category['name'] }}</td>
                        <td>
                        <button class="md-btn md-btn-danger" category="button" onclick="UIkit.modal.confirm('{{ __('admin.Confirm Deletetion')}}', function(){ window.location.href='{{ route('categories.destroy', ['id' => $category['id']]) }}'; });"><i class="uk-icon-close"></i></button>
                            <a href="{{ route('categories.edit', ['id' => $category['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>




<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('categories.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection