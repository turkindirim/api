@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Edit Type') }}</h2>

<form action="{{ route('types.update', ['id' => $type->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ $type->translate($language['code']) ? $type->translate($language['code'])->name : ''}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Description') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom"><textarea  class="ckeditor" name="description_{{ $language['code'] }}">{{ $type->translate($language['code']) ? $type->translate($language['code'])->description : ''}}</textarea></div>
                </div>
            @endforeach  
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Code') }}</label>
                    <input type="number" value="{{ $type->code }}" class="input-count md-input" name="code" id="input_counter" maxlength="2">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled">
                    <p>{{ __('admin.Card') }}</p>
                    <input type="file" class="maxFileSize" name="card" accept=".jpg,.png,.bmp,.gif">
                </div>
            </div>      
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Type') }}</button>
    </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection