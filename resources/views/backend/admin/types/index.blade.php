@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Card Types') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table uk-table-align-vertical uk-table-nowrap tablesorter">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Title') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($types as $type)
                    <tr>
                        <td>{{ $type['id'] }}</td>
                        <td>{{ $type['name'] }}</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ window.location.href='{{ route('types.destroy', ['id' => $type['id']]) }}'; });"><i class="uk-icon-close"></i></button>
                            <a href="{{ route('types.edit', ['id' => $type['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>




<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('types.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection