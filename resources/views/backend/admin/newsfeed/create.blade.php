@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Create Newsfeed') }}</h2>

<form action="{{ route('newsfeed.store') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input required type="text" value="{{ old('name_' . $language['code'])}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Description') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom"><textarea  class="ckeditor" name="description_{{ $language['code'] }}">{{ old('name_' . $language['code'])}}</textarea></div>
                </div>
                <div class="uk-row-first uk-margin-large-bottom">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Logo') }} - {{ $language['name'] }}</label></div>
                    <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo_{{ $language['code'] }}" class="dropify" /></div>
                </div>  
            @endforeach

            <div class="uk-row-first autocomplete-wrapper">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Offer') }}</label>
                    <input type="text" data-id="offer" data-user="offer" class="userSelector md-input" required>
                    <input type="hidden" id="offer" name="offer" value="{{ old('offer') }}">
                </div>
                <div class="uk-dropdown" aria-expanded="true">
                    <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results md-list">
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Create Newsfeed') }}</button>
    </div>
    @csrf
</form>


@endsection