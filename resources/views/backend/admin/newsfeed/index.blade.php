@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Newsfeed') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Name') }}</th>
                    <th>{{ __('admin.Picture') }}</th>
                    <th>{{ __('admin.Publish Date') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($newsfeed as $feed)
                    <tr>
                        <td>{{ $feed['id'] }}</td>
                        <td>{{ $feed['name'] }}</td>
                        <td><img class="logo" src="{{ url('uploads/' . $feed['logo']) }}" alt=""></td>
                        <td>{{ $feed['created_at']->format('Y-m-d') }}</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ window.location.href='{{ route('newsfeed.destroy', ['id' => $feed['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <a href="{{ route('newsfeed.edit', ['id' => $feed['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@include('backend/admin/pagination')


<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('newsfeed.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection