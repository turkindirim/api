@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Edit Newsfeed') }}</h2>

<form action="{{ route('newsfeed.update', ['id' => $newsfeed->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ $newsfeed->translate($language['code']) ? $newsfeed->translate($language['code'])->name : ''}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Description') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom"><textarea  class="ckeditor" name="description_{{ $language['code'] }}">{{ $newsfeed->translate($language['code']) ? $newsfeed->translate($language['code'])->description : ''}}</textarea></div>
                </div>
                <div class=" uk-row-first uk-margin-large-bottom">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Logo') }} - {{ $language['name'] }}</label></div>
                    <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo_{{ $language['code'] }}" id="input-file-a" class="dropify" data-default-file="{{ $newsfeed->translate($language['code'])->logo ? url('uploads/' . $newsfeed->translate($language['code'])->logo) : '' }}" /></div>
                </div>
            @endforeach 
            <div class="uk-row-first autocomplete-wrapper">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Offer') }}</label>
                <input type="text" data-id="offer" value="{{ $newsfeed->offer->name }}" data-user="offer" class="userSelector md-input" required>
                    <input type="hidden" id="offer" name="offer" value="{{ $newsfeed->offer_id }}">
                </div>
                <div class="uk-dropdown" aria-expanded="true">
                    <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results md-list">
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit Newsfeed') }}</button>
    </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection