@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('Slides') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Name') }}</th>
                    <th>{{ __('admin.Picture') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($slides as $slide)
                    <tr>
                        <td>{{ $slide['id'] }}</td>
                        <td>{{ $slide['name'] }}</td>
                        <td>{!! $slide['logo'] ? '<img src="'.url('uploads/' . $slide['logo']) . '" class="mini-logo">' : '' !!}</td>
                        <td>
                            <button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ window.location.href='{{ route('slides.destroy', ['id' => $slide['id']]) }}'; });">
                                <i class="uk-icon-close"></i>
                            </button>
                            <a href="{{ route('slides.edit', ['id' => $slide['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>




<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('slides.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection