@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Create Slide') }}</h2>

<form action="{{ route('slides.store') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ old('name_' . $language['code'])}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Description') }} - {{ $language['name'] }}</label></div>
                    <div class="uk-margin-top uk-margin-bottom">
                        <textarea  class="ckeditor" name="description_{{ $language['code'] }}">{{ old('description_' . $language['code'])}}</textarea>
                    </div>
                </div>
            @endforeach

            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Logo') }}</label></div>
                <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo" id="input-file-a" class="dropify" /></div>
            </div>     
            <div class="uk-row-first uk-margin-top">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>Thumb</label></div>
                <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="thumb" id="input-file-a" class="dropify" /></div>
            </div>            
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">Create Slide</button>
    </div>
    @csrf
</form>


@endsection