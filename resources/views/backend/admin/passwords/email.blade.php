<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <title>{{ config('app.name') }} - {{ __('Reset Password') }}</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

    <!-- uikit -->
<link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}"/>

    <!-- altair admin login page -->
    <link rel="stylesheet" href="{{ asset('assets/css/login_page.min.css') }}" />

</head>
<body class="login_page">
    @if (session('status'))
    <div class="uk-alert uk-alert-success uk-margin-top">
        {{ session('status') }}
    </div>
@endif

@if ($errors->has('email'))
<div class="uk-alert uk-alert-danger uk-margin-top">
    <strong>{{ $errors->first('email') }}</strong>
</span>
@endif


    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding" id="login_password_reset">
                <a href="{{ route('admin.login') }}" class="uk-position-top-right uk-close uk-margin-right uk-margin-top"></a>
                <h2 class="heading_a uk-margin-large-bottom">{{ __('Reset Password') }}</h2>
                <form method="POST" action="{{ route('admin.password.email') }}">
                    <div class="uk-form-row">
                        <label for="login_email_reset">{{ __('Email Address') }}</label>
                        <input class="{{ $errors->has('email') ? ' is-invalid' : '' }} md-input" type="email" id="login_email_reset" value="{{ old('email') }}" name="email" required />
                    </div>
                    <div class="uk-margin-medium-top">
                        <button type="submit" class="md-btn md-btn-primary md-btn-block">{{ __('Send Password Reset Link') }}</button>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>

    <!-- common functions -->
    <script src="{{ asset('assets/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('assets/js/uikit_custom.min.js') }}"></script>
    <!-- altair core functions -->
    <script src="{{ asset('assets/js/altair_admin_common.min.js') }}"></script>

    <!-- altair login page functions -->
    <script src="{{ asset('assets/js/pages/login.min.js') }}"></script>
</body>
</html>