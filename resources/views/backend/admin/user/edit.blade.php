@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Edit User') }}</h2>

<form action="{{ route('users.update', ['id' => $user->id]) }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.First Name') }}</label>
                    <input type="text" class="md-input" name="firstname" required="required" value="{{ $user->firstname }}">
                </div>
            </div>
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Last Name') }}</label>
                    <input type="text" class="md-input" name="lastname" required="required" value="{{ $user->lastname }}">
                </div>
            </div>
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Email') }}</label>
                    <input type="email" class="md-input" name="email" required="required" value="{{ $user->email }}">
                </div>
            </div>
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Mobile') }}</label>
                    <input type="text" class="md-input" name="mobile" value="{{ $user->mobile }}">
                </div>
            </div>
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Nationality') }}</label></div>
                <select id="nationality" name="nationality" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Nationality') }}">
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}" {{ ($country->id == $user->nationality) ? 'selected="selected"':'' }}>{{ $country->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Passport') }}</label></div>
                <div><input type="file" accept=".jpg,.png,.pdf" name="passport"  class="dropify" data-default-file="{{ $user->passport ? url('uploads/' . $user->possport) : '' }}" /></div>
            </div>
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Passport Number') }}</label>
                    <input type="text" class="md-input" name="passport_number" value="{{ $user->passport_number }}">
                </div>
            </div>
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Country') }}</label></div>
                <select onchange="countryChanged(this.options[this.selectedIndex].value, false, false);" id="country_id" name="country_id" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Country') }}">
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}" {{ ($country->id == $user->country_id) ? 'selected="selected"':'' }}>{{ $country->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Province') }}</label></div>
                <select id="province" name="province_id" class="uk-width-1-1" data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Province') }}">
                    @foreach ($provinces as $province)
                        <option value="{{ $province->id }}" {{ ($province->id == $user->province_id) ? 'selected="selected"':'' }}>{{ $province->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Address 1') }}</label>
                    <input type="text" class="md-input" name="address1" required="required" value="{{ $user->address1 }}">
                </div>
            </div>
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Address 2') }}</label>
                    <input type="text" class="md-input" name="address2" value="{{ $user->address2 }}">
                </div>
            </div>
            <div class=" uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Profile Picture') }}</label></div>
                <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo" class="dropify" data-default-file="{{ $user->logo ? url('uploads/' . $user->logo) : '' }}" /></div>
            </div>
            <div class="uk-margin-top uk-row-first">
                <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Edit User') }}</button>
            </div>
        </div>
    </div>
    @csrf
    <input type="hidden" name="_method" value="PUT">
</form>


@endsection