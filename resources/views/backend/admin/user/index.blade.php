@extends('/backend/admin/layout')
@section('header_script')
<script>
var DT_Ajax_URL = "{{ route('users.search') }}",
    DT_Cols_Arr = [
        { "data": "id" },
        { "data": "name" },
        { "data": "email" },
        { "data": "logo", "orderable":false },
        { "data": "cards_count" },
        { "data": "gender", render:function (gender) {
            if (gender == '0') {
                return 'Female';
            } else if (gender == '1') {
                return 'Male';
            }
            return '';
        } },
        { "data": "nationality", render: function(country){
            if (country != null) {
                return country.name;
            }
            return '';
        }, "orderable":false },
        { "data": "passport", "orderable":false },
        { "data": "mobile", "orderable":false },
        { "data": "address", "orderable":false },
        { "data": "created_at" },
        { "data": "options", "orderable":false }
    ],
    extra_data = () => {
        return {}
    }
    ;
</script>
@endsection
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Users') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableAjax" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Name') }}</th>
                    <th>{{ __('admin.Email') }}</th>
                    <th>{{ __('admin.Profile Picture') }}</th>
                    <th>{{ __('admin.Cards') }}</th>
                    <th>{{ __('admin.Gender') }}</th>
                    <th>{{ __('admin.Nationality') }}</th>
                    <th>{{ __('admin.Passport') }}</th>
                    <th>{{ __('admin.Mobile') }}</th>
                    <th>{{ __('admin.Address') }}</th>
                    <th>{{ __('admin.Registration Date') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('users.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection