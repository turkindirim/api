@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Create Admin') }}</h2>

<form action="{{ route('admins.store') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">

            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Name') }}</label>
                    <input type="text" class="md-input" name="name" required="required" value="{{ old('name') }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Email') }}</label>
                    <input type="email" class="md-input" name="email" required="required" value="{{ old('email') }}">
                </div>
            </div>
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Password') }}</label>
                    <input type="password" class="md-input" name="password" required="required" value="{{ old('password') }}">
                </div>
            </div>
            
            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Password Confirmation') }}</label>
                    <input type="password" class="md-input" name="password_confirmation" required="required" value="{{ old('password_confirmation') }}">
                </div>
            </div>
            <div class="uk-row-first uk-margin-bottom">
                <input type="checkbox" value="1" name="superadmin" data-switchery  id="switch_demo_1" />
                <label for="switch_demo_1" class="inline-label">{{ __('admin.Super Administrator') }}</label>
            </div>
            <div class="uk-row-first uk-margin-medium-top">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Profile Picture') }}</label></div>
                <div><input type="file" accept=".jpg,.png,.bmp,.gif" name="logo" id="input-file-a" class="dropify" /></div>
            </div>            
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Create Admin') }}</button>
    </div>
    @csrf
</form>


@endsection