@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Create District') }}</h2>

<form action="{{ route('district.store') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ old('name_' . $language['code'])}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
            @endforeach

            <div class="uk-row-first">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Zip Code') }}</label>
                    <input type="text" value="{{ old('zip_code')}}" class="input-count md-input" name="zip_code" id="input_counter" maxlength="8">
                </div>
            </div>


            <div class="uk-row-first uk-margin-bottom">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled "><label>{{ __('admin.Country') }}</label></div>
                <select name="country_id" class="uk-width-1-1"  data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Country') }}">
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="uk-row-first uk-margin-top">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled "><label>{{ __('admin.Province') }}</label></div>
                <select onchange="provinceChanged(this.options[this.selectedIndex].value);" name="province_id" class="uk-width-1-1"  data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.Province') }}">
                    @foreach ($provinces as $province)
                        <option value="{{ $province->id }}">{{ $province->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="uk-row-first uk-margin-top">
                <div class="md-input-wrapper md-input-wrapper-count md-input-filled "><label>{{ __('admin.Province') }}</label></div>
                <select id="city" name="city_id" class="uk-width-1-1"  data-md-select2 data-allow-clear="false" data-placeholder="{{ __('admin.City') }}">

                </select>
            </div>
            
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Create District') }}</button>
    </div>
    @csrf
</form>


@endsection