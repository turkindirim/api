@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Tags') }}</h2>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table id="dt_default" class="uk-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('admin.Title') }}</th>
                    <th>{{ __('admin.Options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($tags as $tag)
                    <tr>
                        <td>{{ $tag['id'] }}</td>
                        <td>{{ $tag['name'] }}</td>
                        <td>
                        <button class="md-btn md-btn-danger" tag="button" onclick="UIkit.modal.confirm('{{ __('admin.Confirm Deletetion')}}', function(){ window.location.href='{{ route('tags.destroy', ['id' => $tag['id']]) }}'; });"><i class="uk-icon-close"></i></button>
                            <a href="{{ route('tags.edit', ['id' => $tag['id']]) }}" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>




<div class="md-fab-wrapper md-fab-speed-dial">
    <a class="md-fab md-fab-primary" href="{{ route('tags.create') }}">
        <i class="material-icons">add</i>
    </a>
</div>

@endsection