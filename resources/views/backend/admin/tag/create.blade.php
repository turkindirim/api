@extends('/backend/admin/layout')
@section('content')


<h2 class=" uk-margin-bottom">{{ __('admin.Create Tag') }}</h2>

<form action="{{ route('tags.store') }}" method="POST" enctype="multipart/form-data">
    <div class="md-card">
        <div class="md-card-content">
            @foreach (Config::get('app.languages'); as $language)
                <div class="uk-row-first">
                    <div class="md-input-wrapper md-input-wrapper-count md-input-filled"><label>{{ __('admin.Title') }} - {{ $language['name'] }}</label>
                        <input type="text" value="{{ old('name_' . $language['code'])}}" class="input-count md-input" name="name_{{ $language['code'] }}" id="input_counter_{{ $language['code'] }}" maxlength="255">
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="uk-margin-top uk-row-first">
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('admin.Create Tag') }}</button>
    </div>
    @csrf
</form>


@endsection