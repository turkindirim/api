
<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="app_theme_e"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon-16x16.png') }}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon-32x32.png') }}" sizes="32x32">

    <title>{{ config('app.name', 'Turk Indirim') }} | {{ __('Admin Panel') }}</title>

    <link rel="stylesheet" href="{{ asset('assets/skins/dropify/css/dropify.css') }}">

    <!-- additional styles for plugins -->
    <!-- weather icons -->
    <link rel="stylesheet" href="{{ asset('bower_components/weather-icons/css/weather-icons.min.css') }}" media="all">
    <!-- metrics graphics (charts) -->
    <link rel="stylesheet" href="{{ asset('bower_components/metrics-graphics/dist/metricsgraphics.css') }}">
    <!-- chartist -->
    <link rel="stylesheet" href="{{ asset('bower_components/chartist/dist/chartist.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">

    <!-- Data Table //-->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.css') }}">
    <!-- uikit -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.css') }}" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="{{ asset('assets/icons/flags/flags.min.css') }}" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="{{ asset('assets/css/style_switcher.min.css') }}" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="{{ asset('assets/css/themes/themes_combined.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('assets/css/themes/themes_combined.min.css') }}" media="all">

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="{{ asset('bower_components/matchMedia/matchMedia.js') }}"></script>
        <script type="text/javascript" src="{{ asset('bower_components/matchMedia/matchMedia.addListener.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('assets/css/ie.css') }}" media="all">
    <![endif]-->
    <style>
    /* Google maps, places selector */
    #pac-input,.pac-card{background-color:#fff}#map{height:400px}body,html{height:100%;margin:0;padding:0}#description{font-family:Roboto;font-size:15px;font-weight:300}#infowindow-content .title{font-weight:700}#pac-input,.pac-controls label{font-family:Roboto;font-weight:300}#infowindow-content{display:none}#map #infowindow-content{display:inline}.pac-card{margin:10px 10px 0 0;border-radius:2px 0 0 2px;box-sizing:border-box;-moz-box-sizing:border-box;outline:0;box-shadow:0 2px 6px rgba(0,0,0,.3);font-family:Roboto}#pac-container{padding-bottom:12px;margin-right:12px}.pac-controls{display:inline-block;padding:5px 11px}.pac-controls label{font-size:13px}#pac-input{font-size:15px;margin-left:12px;padding:0 11px 0 13px;text-overflow:ellipsis;width:400px}#pac-input:focus{border-color:#4d90fe}#title{color:#fff;background-color:#4d90fe;font-size:25px;font-weight:500;padding:6px 12px}

    .logo {
        max-width:100px
    }
    .mini-logo {
        max-width:60px;
    }
    .nostyle-table td {
        border-bottom:0
    }
    table, td, .uk-table td {
        vertical-align: text-bottom
    }
    .md-list-li-item {
        cursor: pointer;
        transition: all .2s linear;
    }
    .md-list .uk-nestable-list > li, .md-list > li {
        padding: 8px 20px 8px 4px;
    }
    .md-list-li-item:hover {
        background-color:#eee 
    }
    .chartist-tooltip {
  position: absolute;
  display: inline-block;
  opacity: 0;
  min-width: 5em;
  padding: .5em;
  background: #F4C63D;
  color: #453D3F;
  font-family: Oxygen,Helvetica,Arial,sans-serif;
  font-weight: 700;
  text-align: center;
  pointer-events: none;
  z-index: 1;
  -webkit-transition: opacity .2s linear;
  -moz-transition: opacity .2s linear;
  -o-transition: opacity .2s linear;
  transition: opacity .2s linear; }
  .chartist-tooltip:before {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    width: 0;
    height: 0;
    margin-left: -15px;
    border: 15px solid transparent;
    border-top-color: #F4C63D; }
  .chartist-tooltip.tooltip-show {
    opacity: 1; }

.ct-area, .ct-line {
  pointer-events: none; }


  input[type="search"].md-input {
      width: unset
  }
  .simple_numbers {
      margin-top:30px
  }
  .uk-pagination {
      text-align: right
  }
    </style>
    <script>
    var InvalidCardNumber = "{{ __('admin.Invalid Card Number') }}",
    NoRowSelected = "{{ __('admin.No Row Selected') }}",
    processing = "{{ __('admin.Processing') }}",
    selectCity = "{{ __('admin.Select City') }}",
    selectDistrict = "{{ __('admin.Select District') }}",
    selectProvince = "{{ __('admin.Select Province') }}",
    Success = "{{ __('admin.Success') }}",
    uploadURL = "{{ url('uploads/') }}",
    cardAlreadyUsedOrSetToDistributor = "{{ __('admin.CardAlreadyUsedOrSetToDistributor') }}"
    
    ;

    </script>
    @yield('header_script')
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                                
                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>
                
                <!-- secondary sidebar switch -->
                <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>

                <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false">
                        <a title="{{ __('admin.Dashboard') }}" href="/" class="top_menu_toggle"><i class="material-icons md-24">dashboard</i></a>
                    </div>
                </div>

                <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false">
                        <a title="{{ __('admin.Index') }}" href="/{{ Request::segment(1) }}" class="top_menu_toggle"><i class="material-icons md-24">drag_indicator</i></a>
                    </div>
                </div>
                

                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">fullscreen</i></a></li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image"><img class="md-user-image" src="{{ Auth::user()->logo ? url('uploads/' . Auth::user()->logo) : asset('assets/img/avatars/avatar_11_tn.png') }}" alt=""/></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                    <li><a href="{{ route('admin.account') }}">{{ __('admin.Profile') }}</a></li>
                                    <li><a href="{{ route('admin.logout') }}">{{ __('admin.Logout') }}</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>