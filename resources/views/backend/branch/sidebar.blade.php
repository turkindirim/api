<aside id="sidebar_main">
        
    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="#" class="sSidebar_hide sidebar_logo_large">
                <img class="logo_regular" src="{{ asset('assets/img/logo_main.png') }}" alt="" height="15" width="71"/>
                <img class="logo_light" src="{{ asset('assets/img/logo_main_white.png') }}" alt="" height="15" width="71"/>
            </a>
            <a href="#" class="sSidebar_show sidebar_logo_small">
                <img class="logo_regular" src="{{ asset('assets/img/logo_main_small.png') }}" alt="" height="32" width="32"/>
                <img class="logo_light" src="{{ asset('assets/img/logo_main_small_light.png') }}" alt="" height="32" width="32"/>
            </a>
        </div>
    </div>
    
    <div class="menu_section">
        <ul>
            <li class="{{ (request()->is('transactions*')) ? 'current_section' : '' }}" title="{{ __('admin.Transactions') }}">
                <a href="/transactions">
                    <span class="menu_icon"><i class="material-icons">swap_horiz</i></span>
                    <span class="menu_title">{{ __('admin.Transactions') }}</span>
                </a>
            </li>
            <li class="{{ (request()->is('offers*')) ? 'current_section' : '' }}" title="{{ __('admin.Offers') }}">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">receipt</i></span>
                    <span class="menu_title">{{ __('admin.Offers') }}</span>
                </a>
                <ul>
                    <li class="{{ (request()->is('offers*')) ? 'act_item' : '' }}"><a href="{{ route('categories.index') }}"><a href="{{ route('offers.index') }}">{{ __('admin.Offers') }}</a></li>
                </ul>
            </li>
            <!--
            <li title="{{ __('admin.Settings') }}">
                <a href="{{ route('settings') }}">
                    <span class="menu_icon"><i class="material-icons">settings</i></span>
                    <span class="menu_title">{{ __('admin.Settings') }}</span>
                </a>
            </li>
            //-->
        </ul>
    </div>
</aside>