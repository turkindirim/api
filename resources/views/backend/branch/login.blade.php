
<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <title>{{ config('app.name') }} - {{ __('Login') }}</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

    <!-- uikit -->
<link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}"/>

    <!-- altair admin login page -->
    <link rel="stylesheet" href="{{ asset('assets/css/login_page.min.css') }}" />

</head>
<body class="login_page">

    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
                    <div class="user_avatar"></div>
                </div>
                <form method="POST" action="{{ route('branch.login.submit') }}">
                    <div class="uk-form-row">
                        <label for="email">{{ __('admin.Email') }}</label>
                        <input class="md-input" type="text" id="email" name="email" value="{{ old('email')}}" />
                        <small class="uk-text-danger">{{ $errors->first('email') ?: '' }}</small>
                    </div>
                    <div class="uk-form-row">
                        <label for="password">{{ __('admin.Password') }}</label>
                        <input class="md-input" type="password" id="password" name="password" value="{{ old('password')}}" />
                        <small class="uk-text-danger">{{ $errors->first('password') ?: '' }}</small>
                    </div>
                    <div class="uk-margin-medium-top">
                        <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">{{ __('admin.Sign In') }}</button>
                    </div>
                    <div class="uk-margin-top">
                    <a href="{{ route('branch.password.request') }}" class="uk-float-right">{{ __('admin.Reset Password') }}</a>
                        <span class="icheck-inline">
                            <input type="checkbox" name="login_page_stay_signed" id="login_page_stay_signed" data-md-icheck />
                            <label for="login_page_stay_signed" class="inline-label">Stay signed in</label>
                        </span>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>

    <!-- common functions -->
    <script src="{{ asset('assets/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('assets/js/uikit_custom.min.js') }}"></script>
    <!-- altair core functions -->
    <script src="{{ asset('assets/js/altair_admin_common.min.js') }}"></script>

    <!-- altair login page functions -->
    <script src="{{ asset('assets/js/pages/login.min.js') }}"></script>
</body>
</html>