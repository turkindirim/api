<script>
    var API = '{{ env("API_URL") }}',
    AcceptLanguage = '{{ Config::get("app.locale") }}';
    WebFontConfig = {
        google: {
            families: [
                'Source+Code+Pro:400,700:latin',
                'Roboto:400,300,500,700,400italic:latin'
            ]
        }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>

<!-- common functions -->
<script src="{{ asset('assets/js/common.min.js') }}"></script>
<!-- uikit functions -->
<script src="{{ asset('assets/js/uikit_custom.min.js') }}"></script>
<!-- altair common functions/helpers -->
<script src="{{ asset('assets/js/altair_admin_common.min.js') }}"></script>

<!-- page specific plugins -->
    <!-- d3 -->
    <script src="{{ asset('bower_components/d3/d3.min.js') }}"></script>
    <!-- metrics graphics (charts) -->
    <script src="{{ asset('bower_components/metrics-graphics/dist/metricsgraphics.min.js') }}"></script>
    <!-- chartist (charts) -->
    <script src="{{ asset('bower_components/chartist/dist/chartist.min.js') }}"></script>


    <!-- maplace (google maps) -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIMjfEw0gDpISenqbXPS5oepcPot5muRk&libraries=places"></script>
    <script src="{{ asset('bower_components/maplace-js/dist/maplace.min.js') }}"></script>
    <!-- peity (small charts) -->
    <script src="{{ asset('bower_components/peity/jquery.peity.min.js') }}"></script>
    <!-- easy-pie-chart (circular statistics) -->
    <script src="{{ asset('bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <!-- countUp -->
    <script src="{{ asset('bower_components/countUp.js/dist/countUp.min.js') }}"></script>
    <!-- handlebars.js -->
    <script src="{{ asset('bower_components/handlebars/handlebars.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/handlebars_helpers.min.js') }}"></script>
    <!-- CLNDR -->
    <script src="{{ asset('bower_components/clndr/clndr.min.js') }}"></script>
    <script src="{{ asset('bower_components/select2/dist/js/select2.min.js') }}"></script>

    
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/dataTables.buttons.js') }}"></script>
    <script src="{{ asset('assets/js/custom/datatables/buttons.uikit.js') }}"></script>
    <script src="{{ asset('bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('bower_components/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.colVis.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.html5.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.print.js') }}"></script>

    <!-- datatables custom integration -->
    <script src="{{ asset('assets/js/custom/datatables/datatables.uikit.min.js') }}"></script>
    <!--  datatables functions -->
    <script src="{{ asset('assets/js/pages/plugins_datatables.js') }}"></script>
    <script src="{{ asset('bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms_file_input.min.js') }}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms_wysiwyg.js') }}"></script>

    <!-- handlebars.js -->
    <script src="{{ asset('bower_components/handlebars/handlebars.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/handlebars_helpers.min.js') }}"></script>
    <!-- parsley (validation) -->
    <script>
    // load parsley config (altair_admin_common.js)
    altair_forms.parsley_validation_config();
    </script>
    <script src="{{ asset('bower_components/parsleyjs/dist/parsley.min.js') }}"></script>

    <script src="{{ asset('bower_components/dragula.js/dist/dragula.min.js') }}"></script>

    <!-- tablesorter
    <script src="{{ asset('bower_components/tablesorter/dist/js/jquery.tablesorter.min.js') }}"></script>
    <script src="{{ asset('bower_components/tablesorter/dist/js/jquery.tablesorter.widgets.min.js') }}"></script>
    <script src="{{ asset('bower_components/tablesorter/dist/js/widgets/widget-alignChar.min.js') }}"></script>
    <script src="{{ asset('bower_components/tablesorter/dist/js/extras/jquery.tablesorter.pager.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/pages_issues.js') }}"></script>
    -->

    <!--  sortable functions -->
    <script src="{{ asset('assets/js/pages/components_sortable.min.js') }}"></script>

    <script src="{{ asset('assets/js/pages/forms_advanced.js') }}"></script>
    <!--  dashbord functions -->
    <script src="{{ asset('assets/js/pages/dashboard.min.js') }}"></script>
    <!--  kendoui functions -->
    <script src="{{ asset('assets/js/kendoui_custom.js') }}"></script>
    <script src="{{ asset('bower_components/d3/d3.min.js') }}"></script>
    <!-- metrics graphics (charts) -->
    <script src="{{ asset('bower_components/metrics-graphics/dist/metricsgraphics.min.js') }}"></script>
    <!-- c3.js (charts) -->
    <script src="{{ asset('bower_components/c3js-chart/c3.min.js') }}"></script>
    <!-- chartist -->
    <script src="{{ asset('bower_components/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/plugins_charts.min.js') }}"></script>


    
    <!-- Functions by Feras //-->
    <script src="{{ asset('assets/js/chartist-plugin-tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/js/dashboard.js') }}"></script>
    @yield('footer_script')
</body>
</html>
