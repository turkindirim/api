<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;
use DB;

class Offer extends Model
{
    
    use Translatable, SoftDeletes;
    protected $fillable = ['valid_from', 'valid_to', 'active', 'by_branch', 'views', 'featured', 'sort'];
    public $translatedAttributes = ['name', 'description', 'excerpt', 'thumb', 'background', 'header', 'mobile_thumb'];
    protected $perPage = 50;
    protected $hidden = ['translations'];
    protected $appends = ['average_rate'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('sort', function ($builder) {
            $builder->orderBy('sort', 'asc');
        });
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_offer', 'offer_id', 'category_id');
    }
    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'offer_tag', 'offer_id', 'tag_id');
    }
    
    public function branches()
    {
        return $this->belongsToMany('App\Branch', 'branch_offer', 'offer_id', 'branch_id');
    }
    public function media()
    {
        return $this->hasMany('App\Media')->withTrashed();
    }
    public function newsfeed()
    {
        return $this->hasMany('App\Newsfeed');
    }

    public function adder()
    {
        return $this->belongsTo('App\Branch', 'by_branch');
    }
    public function reviews()
    {
        return $this->morphMany('App\Review', 'reviewable');
    }

    public function avgRate()
    {
        $x = $this->reviews()->avg('rate');
        return round($x, 1);
    }
    public function getAverageRateAttribute()
    {
        return round(($this->avgRate()));  
    }
    
}
