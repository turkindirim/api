<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use Translatable, SoftDeletes;
    protected $fillable = ['code', 'logo'];
    public $translatedAttributes = ['name', 'description', 'header'];
    public $hidden = ['deleted_at', 'created_at', 'updated_at'];
    public $with = [];

    public function users()
    {
        return $this->belongsToMany('App\User', 'category_user', 'category_id', 'user_id');
    }
    
    public function offers()
    {
        return $this->belongsToMany('App\Offer', 'category_offer', 'category_id', 'offer_id');
    }
    public function posts()
    {
        return $this->belongsToMany('App\Post', 'category_post', 'category_id', 'post_id');
    }
}
