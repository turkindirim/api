<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferTranslation extends Model
{
    public $timestamps = false;
    public $fillable = ['name', 'description', 'thumb', 'background', 'header', 'mobile_thumb'];
}
