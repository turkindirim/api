<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogCard extends Model
{
    public $fillable = ['card_id', 'userable_id', 'userable_type', 'action', 'description'];

    public function card()
    {
        return $this->belongsTo('App\Card');
    }

    public function userable()
    {
        return $this->morphTo();
    }

}
