<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributorTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
}
