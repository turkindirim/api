<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
}
