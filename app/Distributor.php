<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;


class Distributor extends Authenticatable
{
    use Notifiable, HasMultiAuthApiTokens, SoftDeletes, Translatable;
    
    protected $table = 'distributors';
    public $translatedAttributes = ['name'];
    
    protected $perPage = 100;
    
    protected $fillable = [
        'email', 'password', 'logo', 'website', 'phone', 'mobile', 'province_id', 'city_id', 'district_id', 'address1', 'address2', 'longitude', 'latitude',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cards()
    {
        return $this->hasMany('App\Card');
    }
    public function province()
    {
        return $this->belongsTo('App\Province');
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function district()
    {
        return $this->belongsTo('App\District');
    }
    public function cardLogs()
    {
        return $this->morphMany('App\LogCard', 'userable');
    }
    public function logDistributor()
    {
        return $this->hasMany('App\LogDistributor');
    }
}
