<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use Translatable, SoftDeletes;
    public $translatedAttributes = ['name'];
    
    public function offers()
    {
        return $this->belongsToMany('App\Offer', 'offer_tag', 'tag_id', 'offer_id');
    }
    public function posts()
    {
        return $this->belongsToMany('App\Post', 'post_tag', 'tag_id', 'post_id');
    }
}
