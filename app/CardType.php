<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class CardType extends Model
{
    use Translatable, SoftDeletes;

    protected $fillable = ['code', 'logo'];
    public $translatedAttributes = ['name', 'description'];

    public function cards()
    {
        return $this->hasMany('App\Card');
    }
}
