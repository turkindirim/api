<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;

class Newsfeed extends Model
{
    use SoftDeletes, Translatable;
    protected $perPage = 100;

    public $translatedAttributes = ['name', 'description', 'logo'];

    public function offer()
    {
        return $this->belongsTo('App\Offer');
    }
}
