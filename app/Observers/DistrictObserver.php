<?php

namespace App\Observers;

use App\District;
use App\LogAdmin;
use Auth;

class DistrictObserver
{
    /**
     * Handle the District "created" event.
     *
     * @param  \App\District  $district
     * @return void
     */
    public function created(District $district)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'District',
            'action' => 'Create',
            'item' => $district->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the District "updated" event.
     *
     * @param  \App\District  $district
     * @return void
     */
    public function updated(District $district)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'District',
            'action' => 'Update',
            'item' => $district->id,
            'description' => \json_encode($district->getDirty())
        ]);
    }

    /**
     * Handle the District "deleted" event.
     *
     * @param  \App\District  $district
     * @return void
     */
    public function deleted(District $district)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'District',
            'action' => 'Delete',
            'item' => $district->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the District "restored" event.
     *
     * @param  \App\District  $district
     * @return void
     */
    public function restored(District $district)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'District',
            'action' => 'Restore',
            'item' => $district->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the District "force deleted" event.
     *
     * @param  \App\District  $district
     * @return void
     */
    public function forceDeleted(District $district)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'District',
            'action' => 'Permanent Delete',
            'item' => $district->id,
            'description' => \json_encode(['name' => $district->name])
        ]);
    }
}
