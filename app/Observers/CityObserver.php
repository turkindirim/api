<?php

namespace App\Observers;

use App\City;
use App\LogAdmin;
use Auth;

class CityObserver
{
    /**
     * Handle the City "created" event.
     *
     * @param  \App\City  $city
     * @return void
     */
    public function created(City $city)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'City',
            'action' => 'Create',
            'item' => $city->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the City "updated" event.
     *
     * @param  \App\City  $city
     * @return void
     */
    public function updated(City $city)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'City',
            'action' => 'Update',
            'item' => $city->id,
            'description' => \json_encode($city->getDirty())
        ]);
    }

    /**
     * Handle the City "deleted" event.
     *
     * @param  \App\City  $city
     * @return void
     */
    public function deleted(City $city)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'City',
            'action' => 'Delete',
            'item' => $city->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the City "restored" event.
     *
     * @param  \App\City  $city
     * @return void
     */
    public function restored(City $city)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'City',
            'action' => 'Restore',
            'item' => $city->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the City "force deleted" event.
     *
     * @param  \App\City  $city
     * @return void
     */
    public function forceDeleted(City $city)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'City',
            'action' => 'Permanent Delete',
            'item' => $city->id,
            'description' => \json_encode(['name' => $city->name])
        ]);
    }
}
