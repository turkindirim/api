<?php

namespace App\Observers;

use App\Seller;
use App\LogAdmin;
use Auth;

class SellerObserver
{
    /**
     * Handle the seller "created" event.
     *
     * @param  \App\Seller  $seller
     * @return void
     */
    public function created(Seller $seller)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Seller',
            'action' => 'Create',
            'item' => $seller->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the seller "updated" event.
     *
     * @param  \App\Seller  $seller
     * @return void
     */
    public function updated(Seller $seller)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Seller',
            'action' => 'Update',
            'item' => $seller->id,
            'description' => \json_encode($seller->getDirty())
        ]);
    }

    /**
     * Handle the seller "deleted" event.
     *
     * @param  \App\Seller  $seller
     * @return void
     */
    public function deleted(Seller $seller)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Seller',
            'action' => 'Delete',
            'item' => $seller->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the seller "restored" event.
     *
     * @param  \App\Seller  $seller
     * @return void
     */
    public function restored(Seller $seller)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Seller',
            'action' => 'Restore',
            'item' => $seller->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the seller "force deleted" event.
     *
     * @param  \App\Seller  $seller
     * @return void
     */
    public function forceDeleted(Seller $seller)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Seller',
            'action' => 'Permanent Delete',
            'item' => $seller->id,
            'description' => \json_encode(['name' => $seller->name])
        ]);
    }
}
