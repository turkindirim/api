<?php

namespace App\Observers;

use App\Country;
use App\LogAdmin;
use Auth;

class CountryObserver
{
    /**
     * Handle the Country "created" event.
     *
     * @param  \App\Country  $country
     * @return void
     */
    public function created(Country $country)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Country',
            'action' => 'Create',
            'item' => $country->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Country "updated" event.
     *
     * @param  \App\Country  $country
     * @return void
     */
    public function updated(Country $country)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Country',
            'action' => 'Update',
            'item' => $country->id,
            'description' => \json_encode($country->getDirty())
        ]);
    }

    /**
     * Handle the Country "deleted" event.
     *
     * @param  \App\Country  $country
     * @return void
     */
    public function deleted(Country $country)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Country',
            'action' => 'Delete',
            'item' => $country->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Country "restored" event.
     *
     * @param  \App\Country  $country
     * @return void
     */
    public function restored(Country $country)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Country',
            'action' => 'Restore',
            'item' => $country->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Country "force deleted" event.
     *
     * @param  \App\Country  $country
     * @return void
     */
    public function forceDeleted(Country $country)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Country',
            'action' => 'Permanent Delete',
            'item' => $country->id,
            'description' => \json_encode(['name' => $country->name])
        ]);
    }
}
