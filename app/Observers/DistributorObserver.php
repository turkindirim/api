<?php

namespace App\Observers;

use App\Distributor;
use App\LogAdmin;
use App\LogDistributor;
use Auth;

class DistributorObserver
{
    /**
     * Handle the Distributor "created" event.
     *
     * @param  \App\Distributor  $distributor
     * @return void
     */
    public function created(Distributor $distributor)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Distributor',
            'action' => 'Create',
            'item' => $distributor->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Distributor "updated" event.
     *
     * @param  \App\Distributor  $distributor
     * @return void
     */
    public function updated(Distributor $distributor)
    {
        $params = [
            'model' => 'Distributor',
            'action' => 'Update',
            'item' => $distributor->id,
            'description' => \json_encode($distributor->getDirty())
        ];

        if (Auth::user() instanceof Distributor) {
            $params['distributor_id'] = Auth::user()->id;
            LogDistributor::create($params);
        } else {
            $params['admin_id'] = Auth::user()->id;
            LogAdmin::create($params);
        }

    }

    /**
     * Handle the Distributor "deleted" event.
     *
     * @param  \App\Distributor  $distributor
     * @return void
     */
    public function deleted(Distributor $distributor)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Distributor',
            'action' => 'Delete',
            'item' => $distributor->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Distributor "restored" event.
     *
     * @param  \App\Distributor  $distributor
     * @return void
     */
    public function restored(Distributor $distributor)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Distributor',
            'action' => 'Restore',
            'item' => $distributor->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Distributor "force deleted" event.
     *
     * @param  \App\Distributor  $distributor
     * @return void
     */
    public function forceDeleted(Distributor $distributor)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Distributor',
            'action' => 'Permanent Delete',
            'item' => $distributor->id,
            'description' => \json_encode(['name' => $distributor->name])
        ]);
    }
}
