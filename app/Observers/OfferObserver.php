<?php

namespace App\Observers;

use App\Admin;
use App\Offer;
use App\LogAdmin;
use App\LogBranch;
use Auth;

class OfferObserver
{
    /**
     * Handle the Offer "created" event.
     *
     * @param  \App\Offer  $offer
     * @return void
     */
    public function created(Offer $offer)
    {
        if (Auth::user() instanceof Admin) {
            LogAdmin::create([
                'admin_id' => Auth::user()->id,
                'model' => 'Offer',
                'action' => 'Create',
                'item' => $offer->id,
                'description' => ''
            ]);
        } else {
            LogBranch::create([
                'branch_id' => Auth::user()->id,
                'model' => 'Offer',
                'action' => 'Create',
                'item' => $offer->id,
                'description' => ''
            ]);
        }
        
    }

    /**
     * Handle the Offer "updated" event.
     *
     * @param  \App\Offer  $offer
     * @return void
     */
    public function updated(Offer $offer)
    {
        if (Auth::user() instanceof Admin) {
            LogAdmin::create([
                'admin_id' => Auth::user()->id,
                'model' => 'Offer',
                'action' => 'Update',
                'item' => $offer->id,
                'description' => \json_encode($offer->getDirty())
            ]);
        } else {
            LogBranch::create([
                'branch_id' => Auth::user()->id,
                'model' => 'Offer',
                'action' => 'Update',
                'item' => $offer->id,
                'description' => \json_encode($offer->getDirty())
            ]);
        }
    }

    /**
     * Handle the Offer "deleted" event.
     *
     * @param  \App\Offer  $offer
     * @return void
     */
    public function deleted(Offer $offer)
    {
        if (Auth::user() instanceof Admin) {
            LogAdmin::create([
                'admin_id' => Auth::user()->id,
                'model' => 'Offer',
                'action' => 'Delete',
                'item' => $offer->id,
                'description' => ''
            ]);
        } else {
            LogBranch::create([
                'branch_id' => Auth::user()->id,
                'model' => 'Offer',
                'action' => 'Delete',
                'item' => $offer->id,
                'description' => ''
            ]);
        }
    }

    /**
     * Handle the Offer "restored" event.
     *
     * @param  \App\Offer  $offer
     * @return void
     */
    public function restored(Offer $offer)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Offer',
            'action' => 'Restore',
            'item' => $offer->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Offer "force deleted" event.
     *
     * @param  \App\Offer  $offer
     * @return void
     */
    public function forceDeleted(Offer $offer)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Offer',
            'action' => 'Permanent Delete',
            'item' => $offer->id,
            'description' => \json_encode(['name' => $offer->name])
        ]);
    }
}
