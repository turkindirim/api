<?php

namespace App\Observers;

use App\Category;
use App\LogAdmin;
use Auth;

class CategoryObserver
{
    /**
     * Handle the category "created" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function created(Category $category)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Category',
            'action' => 'Create',
            'item' => $category->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the category "updated" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function updated(Category $category)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Category',
            'action' => 'Update',
            'item' => $category->id,
            'description' => \json_encode($category->getDirty())
        ]);
    }

    /**
     * Handle the category "deleted" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function deleted(Category $category)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Category',
            'action' => 'Delete',
            'item' => $category->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the category "restored" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function restored(Category $category)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Category',
            'action' => 'Restore',
            'item' => $category->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the category "force deleted" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function forceDeleted(Category $category)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Category',
            'action' => 'Permanent Delete',
            'item' => $category->id,
            'description' => \json_encode(['name' => $category->name])
        ]);
    }
}
