<?php

namespace App\Observers;

use App\Language;
use App\LogAdmin;
use Auth;

class LanguageObserver
{
    /**
     * Handle the Language "created" event.
     *
     * @param  \App\Language  $language
     * @return void
     */
    public function created(Language $language)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Language',
            'action' => 'Create',
            'item' => $language->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Language "updated" event.
     *
     * @param  \App\Language  $language
     * @return void
     */
    public function updated(Language $language)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Language',
            'action' => 'Update',
            'item' => $language->id,
            'description' => \json_encode($language->getDirty())
        ]);
    }

    /**
     * Handle the Language "deleted" event.
     *
     * @param  \App\Language  $language
     * @return void
     */
    public function deleted(Language $language)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Language',
            'action' => 'Delete',
            'item' => $language->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Language "restored" event.
     *
     * @param  \App\Language  $language
     * @return void
     */
    public function restored(Language $language)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Language',
            'action' => 'Restore',
            'item' => $language->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Language "force deleted" event.
     *
     * @param  \App\Language  $language
     * @return void
     */
    public function forceDeleted(Language $language)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Language',
            'action' => 'Permanent Delete',
            'item' => $language->id,
            'description' => \json_encode(['name' => $language->name])
        ]);
    }
}
