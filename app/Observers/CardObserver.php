<?php

namespace App\Observers;

use App\Admin;
use App\Card;
use App\LogAdmin;
use App\LogCard;
use Auth;

class CardObserver
{
    /**
     * Handle the card "created" event.
     *
     * @param  \App\Card  $card
     * @return void
     */
    public function created(Card $card)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Card',
            'action' => 'Create',
            'item' => $card->id,
            'description' => ''
        ]);
        LogCard::create([
            'card_id' => $card->id,
            'userable_id' => Auth::user()->id,
            'userable_type' => \get_class(Auth::user()),
            'action' => 'Create',
            'description' => ''
        ]);
    }

    /**
     * Handle the card "updated" event.
     *
     * @param  \App\Card  $card
     * @return void
     */
    public function updated(Card $card)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Card',
            'action' => 'Update',
            'item' => $card->id,
            'description' => \json_encode($card->getDirty())
        ]);
        if (Auth::user() instanceof Admin) {
            LogCard::create([
                'card_id' => $card->id,
                'userable_id' => Auth::user()->id,
                'userable_type' => \get_class(Auth::user()),
                'action' => 'Update',
                'description' => \json_encode($card->getDirty())
            ]);
        }
        
    }

    /**
     * Handle the card "deleted" event.
     *
     * @param  \App\Card  $card
     * @return void
     */
    public function deleted(Card $card)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Card',
            'action' => 'Delete',
            'item' => $card->id,
            'description' => ''
        ]);
        LogCard::create([
            'card_id' => $card->id,
            'userable_id' => Auth::user()->id,
            'userable_type' => \get_class(Auth::user()),
            'action' => 'Delete',
            'description' => ''
        ]);
    }

    /**
     * Handle the card "restored" event.
     *
     * @param  \App\Card  $card
     * @return void
     */
    public function restored(Card $card)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Card',
            'action' => 'Restore',
            'item' => $card->id,
            'description' => ''
        ]);
        LogCard::create([
            'card_id' => $card->id,
            'userable_id' => Auth::user()->id,
            'userable_type' => \get_class(Auth::user()),
            'action' => 'Restore',
            'description' => ''
        ]);
    }

    /**
     * Handle the card "force deleted" event.
     *
     * @param  \App\Card  $card
     * @return void
     */
    public function forceDeleted(Card $card)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Card',
            'action' => 'Permanent Delete',
            'item' => $card->id,
            'description' => \json_encode(['name' => $card->number])
        ]);
        LogCard::create([
            'card_id' => $card->id,
            'userable_id' => Auth::user()->id,
            'userable_type' => \get_class(Auth::user()),
            'action' => 'Permanent Delete',
            'description' => ''
        ]);
    }
}
