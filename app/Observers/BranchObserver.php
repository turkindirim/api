<?php

namespace App\Observers;

use App\Branch;
use App\LogAdmin;
use App\LogBranch;
use Auth;

class BranchObserver
{
    /**
     * Handle the branch "created" event.
     *
     * @param  \App\branch  $branch
     * @return void
     */
    public function created(Branch $branch)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Branch',
            'action' => 'Create',
            'item' => $branch->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Branch "updated" event.
     *
     * @param  \App\Branch  $branch
     * @return void
     */
    public function updated(Branch $branch)
    {
        $params = [
            'model' => 'Branch',
            'action' => 'Update',
            'item' => $branch->id,
            'description' => \json_encode($branch->getDirty())
        ];

        if (Auth::user() instanceof Branch) {
            $params['branch_id'] = Auth::user()->id;
            LogBranch::create($params);
        } else {
            $params['admin_id'] = Auth::user()->id;
            LogAdmin::create($params);
        }
        
    }

    /**
     * Handle the Branch "deleted" event.
     *
     * @param  \App\Branch  $branch
     * @return void
     */
    public function deleted(Branch $branch)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Branch',
            'action' => 'Delete',
            'item' => $branch->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Branch "restored" event.
     *
     * @param  \App\Branch  $branch
     * @return void
     */
    public function restored(Branch $branch)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Branch',
            'action' => 'Restore',
            'item' => $branch->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Branch "force deleted" event.
     *
     * @param  \App\Branch  $branch
     * @return void
     */
    public function forceDeleted(Branch $branch)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Branch',
            'action' => 'Permanent Delete',
            'item' => $branch->id,
            'description' => \json_encode(['name' => $branch->name])
        ]);
    }
}
