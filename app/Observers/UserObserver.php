<?php

namespace App\Observers;

use App\User;
use App\LogAdmin;
use App\LogUser;
use Auth;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        LogAdmin::create([
            'admin_id' => $user->id,
            'model' => 'User',
            'action' => 'Create',
            'item' => $user->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        $params = [
            'model' => 'User',
            'action' => 'Update',
            'item' => $user->id,
            'description' => \json_encode($user->getDirty())
        ];

        if (Auth::user() instanceof User) {
            $params['user_id'] = Auth::user()->id;
            LogUser::create($params);
        } elseif (Auth::user() instanceof Admin) {
            $params['admin_id'] = Auth::user()->id;
            LogAdmin::create($params);
        }
        
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'User',
            'action' => 'Delete',
            'item' => $user->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'User',
            'action' => 'Restore',
            'item' => $user->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'User',
            'action' => 'Permanent Delete',
            'item' => $user->id,
            'description' => \json_encode(['name' => $user->name])
        ]);
    }
}
