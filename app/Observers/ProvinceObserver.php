<?php

namespace App\Observers;

use App\Province;
use App\LogAdmin;
use Auth;

class ProvinceObserver
{
    /**
     * Handle the Province "created" event.
     *
     * @param  \App\Province  $province
     * @return void
     */
    public function created(Province $province)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Province',
            'action' => 'Create',
            'item' => $province->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Province "updated" event.
     *
     * @param  \App\Province  $province
     * @return void
     */
    public function updated(Province $province)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Province',
            'action' => 'Update',
            'item' => $province->id,
            'description' => \json_encode($province->getDirty())
        ]);
    }

    /**
     * Handle the Province "deleted" event.
     *
     * @param  \App\Province  $province
     * @return void
     */
    public function deleted(Province $province)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Province',
            'action' => 'Delete',
            'item' => $province->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Province "restored" event.
     *
     * @param  \App\Province  $province
     * @return void
     */
    public function restored(Province $province)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Province',
            'action' => 'Restore',
            'item' => $province->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Province "force deleted" event.
     *
     * @param  \App\Province  $province
     * @return void
     */
    public function forceDeleted(Province $province)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Province',
            'action' => 'Permanent Delete',
            'item' => $province->id,
            'description' => \json_encode(['name' => $province->name])
        ]);
    }
}
