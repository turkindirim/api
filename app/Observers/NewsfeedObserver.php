<?php

namespace App\Observers;

use App\Newsfeed;
use App\LogAdmin;
use Auth;

class NewsfeedObserver
{
    /**
     * Handle the Newsfeed "created" event.
     *
     * @param  \App\Newsfeed  $newsfeed
     * @return void
     */
    public function created(Newsfeed $newsfeed)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Newsfeed',
            'action' => 'Create',
            'item' => $newsfeed->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Newsfeed "updated" event.
     *
     * @param  \App\Newsfeed  $newsfeed
     * @return void
     */
    public function updated(Newsfeed $newsfeed)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Newsfeed',
            'action' => 'Update',
            'item' => $newsfeed->id,
            'description' => \json_encode($newsfeed->getDirty())
        ]);
    }

    /**
     * Handle the Newsfeed "deleted" event.
     *
     * @param  \App\Newsfeed  $newsfeed
     * @return void
     */
    public function deleted(Newsfeed $newsfeed)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Newsfeed',
            'action' => 'Delete',
            'item' => $newsfeed->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Newsfeed "restored" event.
     *
     * @param  \App\Newsfeed  $newsfeed
     * @return void
     */
    public function restored(Newsfeed $newsfeed)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Newsfeed',
            'action' => 'Restore',
            'item' => $newsfeed->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Newsfeed "force deleted" event.
     *
     * @param  \App\Newsfeed  $newsfeed
     * @return void
     */
    public function forceDeleted(Newsfeed $newsfeed)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Newsfeed',
            'action' => 'Permanent Delete',
            'item' => $newsfeed->id,
            'description' => \json_encode(['name' => $newsfeed->name])
        ]);
    }
}
