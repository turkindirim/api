<?php

namespace App\Observers;

use App\Post;
use App\LogAdmin;
use Auth;

class PostObserver
{
    /**
     * Handle the Post "created" event.
     *
     * @param  \App\Post  $Post
     * @return void
     */
    public function created(Post $post)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Post',
            'action' => 'Create',
            'item' => $post->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Post "updated" event.
     *
     * @param  \App\Post  $post
     * @return void
     */
    public function updated(Post $post)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Post',
            'action' => 'Update',
            'item' => $post->id,
            'description' => \json_encode($post->getDirty())
        ]);
    }

    /**
     * Handle the Post "deleted" event.
     *
     * @param  \App\Post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Post',
            'action' => 'Delete',
            'item' => $post->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Post "restored" event.
     *
     * @param  \App\Post  $post
     * @return void
     */
    public function restored(Post $post)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Post',
            'action' => 'Restore',
            'item' => $post->id,
            'description' => ''
        ]);
    }

    /**
     * Handle the Post "force deleted" event.
     *
     * @param  \App\Post  $post
     * @return void
     */
    public function forceDeleted(Post $post)
    {
        LogAdmin::create([
            'admin_id' => Auth::user()->id,
            'model' => 'Post',
            'action' => 'Permanent Delete',
            'item' => $post->id,
            'description' => \json_encode(['name' => $post->name])
        ]);
    }
}
