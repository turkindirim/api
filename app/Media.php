<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
    use SoftDeletes;

    protected $fillable = ['offer_id', 'name', 'type'];

    public function offer()
    {
        return $this->belongsTo('App\Offer');
    }
}
