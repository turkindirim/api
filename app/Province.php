<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;

class Province extends Model
{
    use Translatable, SoftDeletes;

    public $translatedAttributes = ['name'];
    
    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'translations'];
    
    public function country()
    {
        return $this->belongsTo('App\Country');
    }
    public function cities()
    {
        return $this->hasMany('App\City');
    }
    
    public function branch()
    {
        return $this->hasMany('App\Branch');
    }
    public function distributor()
    {
        return $this->hasMany('App\Distributor');
    }
}
