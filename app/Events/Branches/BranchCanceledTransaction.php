<?php

namespace App\Events\Branches;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BranchCanceledTransaction
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $branch, $transaction;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $branch, int $transaction)
    {
        $this->branch = $branch;
        $this->transaction = $transaction;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
