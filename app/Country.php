<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;

class Country extends Model
{
    use Translatable, SoftDeletes; 
    
    public $translatedAttributes = ['name'];
    protected $fillable = ['dial_code'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'translations'];
    
    public function provinces()
    {
        return $this->hasMany('App\Province');
    }
    public function users_nationality()
    {
        return $this->hasMany('App\User', 'nationality', 'id');
    }
    public function users()
    {
        return $this->hasMany('App\User', 'country_id', 'id');
    }
}
