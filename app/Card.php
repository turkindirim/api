<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Card extends Model
{
    use SoftDeletes;
    public $fillable = ['type_id', 'user_id', 'distributor_id', 'number', 'flag', 'activation_code', 'period', 'sold_at', 'used_at', 'activated_at', 'expired_at'];
    protected $dates = ['sold_at', 'used_at', 'activated_at', 'expired_at'];
    protected $perPage = 100;

    public function distributor()
    {
        return $this->belongsTo('App\Distributor');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function type()
    {
        return $this->belongsTo('App\CardType');
    }
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }
    public function getExpirationDate()
    {
        
    }
}
