<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardTypeTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'description'];
}
