<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogDistributor extends Model
{
    protected $fillable = ['distributor_id', 'model', 'action', 'item', 'description'];
    
    public function distributor()
    {
        return $this->belongsTo('App\Distributor');
    }
}
