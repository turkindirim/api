<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogBranch extends Model
{
    protected $fillable = ['branch_id', 'model', 'action', 'item', 'description'];
    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }
}
