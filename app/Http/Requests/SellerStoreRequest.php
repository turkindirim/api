<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Config;

class SellerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'logo' => 'sometimes|image',
            'website' => 'nullable|url'
        ];
        foreach (Config::get('app.languages') as $language) {
            $rules['name_' . $language['code']] = 'required|max:255';
        }
        return $rules;
    }

    public function filters()
    {
        return [
            'website' => 'trim|lowercase',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'No Name',
        ];
    }
}
