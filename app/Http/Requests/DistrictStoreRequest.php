<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Config;

class DistrictStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'city_id' => 'required|exists:cities,id',
            'zip_code' => 'required|min:4'
        ];

        foreach (Config::get('app.languages') as $language) {
            $rules['name_' . $language['code']] = 'required|max:255';
        }

        return $rules;
    }
}
