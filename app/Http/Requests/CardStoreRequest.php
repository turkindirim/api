<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CardStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'count' => 'required|numeric',
            'period' => 'required|numeric',
            'distributor' => 'nullable|numeric|exists:distributors,id',
            'user' => 'nullable|numeric|exists:users,id',
            'flag' => 'required',
            'type' => 'required|exists:card_types,id'
        ];
    }
}
