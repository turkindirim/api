<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Config;
class BranchStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|confirmed|email|unique:branches',
            'password' => 'required|confirmed|min:6',
            'seller_id' => 'required|exists:sellers,id',
            'phone' => 'nullable',
            'mobile' => 'required',
            'province_id' => 'required|exists:provinces,id',
            'city_id' => 'required|exists:cities,id',
            'district_id' => 'required|exists:districts,id',
            'address1' => 'required',
            'address2' => 'nullable',
            'longitude' => 'nullable',
            'latitude' => 'nullable'
        ];

        foreach (Config::get('app.languages') as $language) {
            $rules['name_' . $language['code']] = 'required|max:255';
        }

        return $rules;
    }
    
}
