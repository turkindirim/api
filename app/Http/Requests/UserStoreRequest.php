<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string|min:2',
            'lastname' => 'nullable|string|min:2',
            'logo' => 'sometimes|image',
            'nationality' => 'sometimes|exists:countries,id',
            'passport' => 'nullable',
            'passport_number' => 'nullable',
            'mobile' => 'sometimes|unique:users',
            'country_id' => 'sometimes|exists:countries,id',
            'province_id' => 'sometimes|exists:provinces,id',
            'address1' => 'sometimes|string',
            'address2' => 'nullable',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'dob' => 'nullable|date_format:"Y-m-d"',
            'gender' => 'nullable'
        ];
    }
}
