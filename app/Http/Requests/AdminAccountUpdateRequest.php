<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class AdminAccountUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required',
            'email' => 'required|email|unique:admins,email,' . Auth::user()->id,
            'name' => 'required|string',
            'logo' => 'sometimes|image',
            'password' => 'nullable|confirmed|min:6',
        ];
    }
}
