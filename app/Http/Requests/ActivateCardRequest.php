<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ActivateCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fields = [
            'number' => 'required|exists:cards,number',
            'activation_code' => 'required|min:5|max:5',
            'mobile' => 'required|unique:users,mobile,' . $user->id
        ];

        return $fields;
    }
}
