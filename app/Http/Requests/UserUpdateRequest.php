<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $emailRule = $mobileRule = '';
        if (Auth::check()) {
            $emailRule = '|unique:users,email,' . auth()->user()->id;
            $mobileRule = '|unique:users,mobile,' . auth()->user()->id;
        }
        return [
            'firstname' => 'required|string|min:2',
            'lastname' => 'required|string|min:2',
            'logo' => 'nullable|image',
            'nationality' => 'nullable|exists:countries,id',
            'passport' => 'nullable|file|mimes:jpeg,bmp,png,pdf',
            'passport_number' => 'nullable',
            'mobile' => 'nullable' . $mobileRule,
            'country_id' => 'nullable|exists:countries,id',
            'province_id' => 'nullable|exists:provinces,id',
            'address1' => 'nullable|string',
            'address2' => 'nullable',
            'email' => 'required|email' . $emailRule,
            'dob' => 'nullable|date_format:"Y-m-d"',
            'gender' => 'nullable',
        ];
    }
}
