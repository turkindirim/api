<?php

namespace App\Http\Middleware;

use Closure;
use App\Language;
use App\Setting;
use Crypt, Config, Cookie, Auth;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $language = false;
        $user = false;
        if (Auth::user()) {
            $user = Auth::user();
        } elseif (Auth::guard('admin')->user()) {
            $user = Auth::guard('admin')->user();
        } elseif (Auth::guard('branch')->user()) {
            $user = Auth::guard('branch')->user();
        } elseif (Auth::guard('distributor')->user()) {
            $user = Auth::guard('distributor')->user();
        } elseif (Auth::guard('api')->user()) {
            $user = Auth::guard('api')->user();
        } elseif (Auth::guard('api-admin')->user()) {
            $user = Auth::guard('api-admin')->user();
        } elseif (Auth::guard('api-branch')->user()) {
            $user = Auth::guard('api-branch')->user();
        } elseif (Auth::guard('api-distributor')->user()) {
            $user = Auth::guard('api-distributor')->user();
        }
        
        if (!$language) {
            //echo '1';
            $language = Cookie::get('language');
            if ($language && strlen($language) > 2) {
                $language = Crypt::decryptString($language);
            }
        }
        if ($user && !$language) {
            
            if ($user->default_language) {
                $language = $user->default_language;
            }
        }
        if (!$language) {            
            if ($request->hasHeader('Accept-Language')) {
                $language = \strtolower(\substr($request->header('Accept-Language'), 0, 2));
            }
        }
        if (!$language) {
            $settings = Setting::where('id', '=', '1')->first();
            $language = $settings->default_language;
        }
        Config::set('app.site_language', Language::where('code', '=', $language)->get()->first());
        app()->setLocale($language);
        return $next($request);
    }
}
