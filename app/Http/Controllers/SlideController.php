<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use Config, File, DB;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $slides = Slide::all();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($slides)
                ->transformWith(new SlideTransformer)
                ->toArray();
        }
        return view('backend.admin.slide.index')->with([
            'slides' => $slides
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.slide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slide = new Slide;
        $sort = (int) (DB::table('slides')->max('sort')) + 1;
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            $slide->logo = $new_name;
        }
        if ($request->hasFile('thumb')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('thumb')->getClientOriginalExtension();
            $request->file('thumb')->move(public_path('uploads'), $new_name);
            $slide->thumb = $new_name;
        }
        $slide->save();
        foreach (Config::get('app.languages') as $language) {
            $slide->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $slide->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
        }
        $slide->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($slide)
                ->transformWith(new SlideTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Slide added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slide = Slide::findOrFail($id);
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            File::delete(public_path('/uploads/' . $slide->logo));
            $slide->logo = $new_name;
        }
        if ($request->hasFile('thumb')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('thumb')->getClientOriginalExtension();
            $request->file('thumb')->move(public_path('uploads'), $new_name);
            File::delete(public_path('/uploads/' . $slide->thumb));
            $slide->thumb = $new_name;
        }
        $slide->save();
        foreach (Config::get('app.languages') as $language) {
            $slide->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $slide->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
        }
        $slide->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($slide)
                ->transformWith(new SlideTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Slide added successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $slide = Slide::findOrFail($id);
        $slide->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('Slide deleted successfully')
        ]);
    }
    public function forcedelete(Request $request, $id)
    {
        $slide = Slide::withTrashed()->where('id', $id);
        $slideItem = $slide->get();
        File::delete(public_path('/uploads/' . $slideItem->first()->thumb));
        File::delete(public_path('/uploads/' . $slideItem->first()->logo));
        $slide->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('Slide deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $slide = Slide::withTrashed()->where('id', $id);
        $slide->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('Slide restored successfully')
        ]);
    }
}
