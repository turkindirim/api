<?php

namespace App\Http\Controllers;

use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;

use App\Events\Branches\BranchMadeTransaction;

use Illuminate\Http\Request;
use App\Http\Requests\BranchStoreRequest;
use App\Http\Requests\BranchUpdateRequest;
use App\Http\Requests\BranchSellRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Transformers\BranchTransformer;
use App\Transformers\TransactionTransformer;
use App\Branch;
use App\Card;
use App\City;
use App\Country;
use App\District;
use App\Province;
use App\Seller;
use App\Transaction;
use Auth, Config;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $branches = $this->getBranches($request);
        $branches = $branches->paginate();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($branches->items())
                ->transformWith(new BranchTransformer)
                ->toArray()
                ;
        }
        return view('backend.admin.branch.index')->with([
            'branches' => $branches->items(),
            'total' => $branches->total(),
            'last_page' => $branches->lastPage(),
            'current_page' => $branches->currentPage(),
            'nextPageUrl' => $branches->nextPageUrl(),
            'previousPageUrl' => $branches->previousPageUrl(),
            'fields' => []
        ]);
    }

    public function filter(Request $request)
    {
        $branches = $this->getBranches($request);
        $fields = [];
        if ($request->id) {
            $fields['id'] = $request->id;
        }
        if ($request->name) {

            $fields['name'] = $request->name;
        }
        if ($request->created_at_from) {
            $fields['created_at_from'] = $request->created_at_from;
        }
        if ($request->created_at_to) {
            $fields['created_at_to'] = $request->created_at_to;
        }

        $branches = $branches->get();

        if ($request->expectsJson()) {
            if ($request->expectsJson()) {
                return fractal()
                    ->collection($branches)
                    ->transformWith(new BranchTransformer)
                    ->toArray()
                    ;
            }
        }
        return view('backend.admin.branch.index')->with([
            'branches' => $branches,
            'fields' => $fields
        ]);

    }


    public function getBranches(Request $request)
    {
        $branches = Branch::where('id' , '>', '0');
        $fields = [];
        if ($request->id) {
            $branches = $branches->where('id', '=', $request->id);
        }
        if ($request->name) {

            $branches = $branches->where(function($query) use ($request) {

                $query->orWhereHas('translations', function ($q) use ($request) {
                    $q->where('name', 'LIKE', '%'.$request->name.'%');
                });
                
                $query->orWhereHas('seller', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                $query->orWhereHas('province', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                $query->orWhereHas('city', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                $query->orWhereHas('district', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                

                $query->orWhere('email', 'like', '%'.$request->name.'%');
                $query->orWhere('phone', 'like', '%'.$request->name.'%');
                $query->orWhere('mobile', 'like', '%'.$request->name.'%');
                $query->orWhere('address1', 'like', '%'.$request->name.'%');
            });
            $fields['name'] = $request->name;
        }
        if ($request->created_at_from) {
            $branches = $branches->where('created_at', '>=', $request->created_at_from . ' 00:00:00');
        }
        if ($request->created_at_to) {
            $branches = $branches->where('created_at', '<=', $request->created_at_to . ' 23:59:59');
        }

        return $branches;
    }


    public function dashboard()
    {
        return view('backend.branch.dashboard');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.branch.create')->with([
            'sellers' => Seller::all(),
            'countries' => Country::all(),
            'provinces' => Province::all(),
            'cities' => City::all(),
            'districts' => District::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchStoreRequest $request)
    {
        $branch_parameters = $request->all();
        $branch_parameters['password'] = Hash::make($request->password);
        $branch = Branch::create($branch_parameters);
        foreach (Config::get('app.languages') as $language) {
            $branch->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $branch->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
        }
        $branch->save();
        if ($request->expectsJson()) {
            return fractal()
                ->item($branch)
                ->transformWith(new BranchTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Branch added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $branch = Branch::findOrFail($id);
        $branch->increment('views');

        if ($request->expectsJson()) {
            return fractal()
                ->item($branch)
                ->transformWith(new BranchTransformer)
                ->includeReviews()
                ->toArray()
                ;
        }
        
        return view('frontend.branch.show')->with([
            'branch' => $branch
        ]);
    }

    

    public function reviews(Request $request, $id)
    {
        
        $branch = Branch::findOrFail($id);
        if ($request->expectsJson()) {

        }
        return view('backend.admin.branch.reviews')->with([
            'branch' => $branch,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::findOrFail($id);
        return view('backend.admin.branch.edit')->with([
            'branch' => $branch,
            'sellers' => Seller::all(),
            'countries' => Country::all(),
            'provinces' => Province::all(),
            'cities' => City::where('province_id', '=', $branch->province_id)->get(),
            'districts' => District::where('city_id', '=', $branch->city_id)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BranchUpdateRequest $request, $id)
    {
        $branch = Branch::findOrFail($id);
        $branch_parameters = $request->all();
        if (is_null($request->password) || (empty($request->password))) {
            unset($branch_parameters['password']);
            unset($branch_parameters['password_confirmation']);
        } else {
            $branch_parameters['password'] = Hash::make($request->password);
        }
        foreach (Config::get('app.languages') as $language) {
            $branch->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $branch->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
        }
        $branch->fill($branch_parameters)->save();
        if ($request->expectsJson()) {
            return fractal()
                ->item($branch)
                ->transformWith(new BranchTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Branch edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $branch = Branch::findOrFail($id);
        if ($branch->offers->count() > 0) {
            if ($request->expectsJson()) {
                return response()->json([
                    __('Branch has offers, please delete them first')
                ]);
            } else {
                return redirect()->back()->withErrors([
                    __('Branch has offers, please delete them first')
                ]);
            }
        }
        $branch->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Branch deleted successfully')
        ]);
    }

    public function forcedelete(Request $request, $id)
    {
        $branch = Branch::withTrashed()->where('id', $id);
        $branch->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Branch deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $branch = Branch::withTrashed()->where('id', $id);
        $branch->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Branch restored successfully')
        ]);
    }


    public function sell(BranchSellRequest $request)
    {
        $error_code = 422;
        $errors = [];
        $card = Card::where('number', '=', $request->card)->get()->first();
        if ($card->sold_at == null) {
            $errors[] = __('admin.Card is not sold yet');
        } elseif ($card->user_id == null) {
            $errors[] = __('admin.Card is not activated yet');
        } elseif ($card->used_at) {
            $expiration_date = ($card->used_at->addDays($card->period)->format('Y-m-d'));
            if (date('Y-m-d') > $expiration_date)
                $errors[] = __('admin.Card is expired on ' . $expiration_date);
        }
        if (count($errors)) {
            return errorsResponse($errors, $error_code);
        }

        if ($card->used_at == null) {
            $card->used_at = now()->timestamp;
            $card->expired_at = $card->used_at->addDays($card->preiod);
            $card->save();
        }
        $transaction = new Transaction;
        $transaction->card_id = $card->id;
        $transaction->user_id = $card->user_id;
        $transaction->branch_id = Auth::user()->id;
        $transaction->save();
        event(new BranchMadeTransaction($card->id, $transaction->id));
        return fractal()
        ->item($transaction)
        ->transformWith(new TransactionTransformer)
        ->toArray()
        ;

    }

    public function transactions()
    {
        $paginator = Transaction::where('branch_id', '=', Auth::user()->id)->paginate();
    
        $transactions = $paginator->getCollection();
        $resource = new Collection($transactions, new TransactionTransformer());
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $manager->createData($resource)->toArray();

        return ($manager->createData($resource)->toArray());
    }

    
    public function account(Request $request)
    {
        $branch = Branch::find(Auth::user()->id);
        if ($request->expectsJson()) {
            return fractal()
                ->item($branch)
                ->transformWith(new BranchTransformer)
                ->toArray()
                ;
        }
    }
    public function api_update(BranchUpdateRequest $request)
    {
        return $this->update($request, Auth::user()->id);
    }
}
