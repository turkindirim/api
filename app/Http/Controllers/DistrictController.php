<?php

namespace App\Http\Controllers;

use App\Transformers\DistrictTransformer;
use Illuminate\Http\Request;
use App\Http\Requests\DistrictStoreRequest;
use App\Country;
use App\Province;
use App\City;
use App\District;
use Config;


class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (is_numeric($request->city_id)) {
            $districts = District::where('city_id', '=', $request->city_id)->get()->sortBy('name');
        } else {
            $districts = District::all()->sortBy('name');
        }
        
        if ($request->expectsJson()) {
            return fractal()
                ->collection($districts)
                ->transformWith(new DistrictTransformer)
                ->toArray();
        }
        return view('backend.admin.district.index')->with([
            'districts' => $districts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.district.create')->with([
            'countries' => Country::all(),
            'provinces' => Province::all()
        ]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DistrictStoreRequest $request)
    {
        $district = new District;
        $district->city_id = $request->city_id;
        $district->zip_code = $request->zip_code;
        $district->save();
        foreach (Config::get('app.languages') as $language) {
            $district->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $district->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($district)
                ->transformWith(new DistrictTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.District added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $district = District::findOrFail($id);
        return view('backend.admin.district.edit')->with([
            'district' => $district,
            'countries' => Country::all(),
            'provinces' => Province::all(),
            'cities' => City::find($district->city_id)->get()
        ]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $district = District::findOrFail($id);
        $district->city_id = $request->city_id;
        $district->zip_code = $request->zip_code;
        $district->save();
        foreach (Config::get('app.languages') as $language) {
            $district->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $district->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($district)
                ->transformWith(new DistrictTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.District edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $district = District::findOrFail($id);
        $district->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.District deleted successfully')
        ]);
    }

    public function forcedelete(Request $request, $id)
    {
        $district = District::withTrashed()->where('id', $id);
        $district->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.District deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $district = District::withTrashed()->where('id', $id);
        $district->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.District restored successfully')
        ]);
    }
    
}
