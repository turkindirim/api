<?php

namespace App\Http\Controllers;

use App\Events\Distributors\DistributorSoldCard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\DistributorStoreRequest;
use App\Http\Requests\DistributorUpdateRequest;
use Illuminate\Support\Facades\Hash;
use App\Transformers\DistributorTransformer;
use App\Transformers\CardTransformer;
use Auth, Config, File;
use App\Card;
use App\City;
use App\Country;
use App\Distributor;
use App\District;
use App\Province;

class DistributorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $distributors = Distributor::paginate();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($distributors->items())
                ->transformWith(new DistributorTransformer)
                ->toArray()
                ;
        }
        return view('backend.admin.distributor.index')->with([
            'distributors' => $distributors->items(),
            'total' => $distributors->total(),
            'last_page' => $distributors->lastPage(),
            'current_page' => $distributors->currentPage(),
            'nextPageUrl' => $distributors->nextPageUrl(),
            'previousPageUrl' => $distributors->previousPageUrl(),
            'fields' => []
        ]);
    }

    
    public function filter(Request $request)
    {
        $distributors = Distributor::where('id' , '>', '0');
        $fields = [];
        if ($request->id) {
            $distributors = $distributors->where('id', '=', $request->id);
            $fields['id'] = $request->id;
        }
        if ($request->name) {

            $distributors = $distributors->where(function($query) use ($request) {

                $query->orWhereHas('translations', function ($q) use ($request) {
                    $q->where('name', 'LIKE', '%'.$request->name.'%');
                });
                
                $query->orWhereHas('province', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                $query->orWhereHas('city', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                $query->orWhereHas('district', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                $query->orWhere('email', 'like', '%'.$request->name.'%');
                $query->orWhere('phone', 'like', '%'.$request->name.'%');
                $query->orWhere('mobile', 'like', '%'.$request->name.'%');
                $query->orWhere('address1', 'like', '%'.$request->name.'%');
            });
            $fields['name'] = $request->name;
        }
        if ($request->created_at_from) {
            $distributors = $distributors->where('created_at', '>=', $request->created_at_from . ' 00:00:00');
            $fields['created_at_from'] = $request->created_at_from;
        }
        if ($request->created_at_to) {
            $distributors = $distributors->where('created_at', '<=', $request->created_at_to . ' 23:59:59');
            $fields['created_at_to'] = $request->created_at_to;
        }

        $distributors = $distributors->get();

        if ($request->expectsJson()) {
            if ($request->expectsJson()) {
                return fractal()
                    ->collection($distributors)
                    ->transformWith(new DistributorTransformer)
                    ->toArray()
                    ;
            }
        }
        return view('backend.admin.distributor.index')->with([
            'distributors' => $distributors,
            'fields' => $fields
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.distributor.create')->with([
            'countries' => Country::all(),
            'provinces' => Province::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DistributorStoreRequest $request)
    {
        $distributor_parameters = $request->all();
        $distributor_parameters['password'] = Hash::make($request->password);
        $distributor_parameters['logo'] = null;
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            $distributor_parameters['logo'] = $new_name;
        }
        $distributor = Distributor::create($distributor_parameters);
        foreach (Config::get('app.languages') as $language) {
            $distributor->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $distributor->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($distributor)
                ->transformWith(new DistributorTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Distributor added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $distributor = Distributor::findOrFail($id);
        if ($request->expectsJson()) {
            return fractal()
                ->item($distributor)
                ->transformWith(new DistributorTransformer)
                ->toArray()
                ;
        } else {
            
            return view('frontend.distributor.show')->with([
                'distributor' => $distributor
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $distributor = Distributor::findOrFail($id);
        return view('backend.admin.distributor.edit')->with([
            'distributor' => $distributor,
            'countries' => Country::all(),
            'provinces' => Province::all(),
            'cities' => City::where('province_id', '=', $distributor->province_id)->get(),
            'districts' => District::where('city_id', '=', $distributor->city_id)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DistributorUpdateRequest $request, $id)
    {
        $distributor = Distributor::findOrFail($id);
        $distributor_parameters = $request->all();
        
        if (is_null($request->password) || (empty($request->password))) {
            unset($distributor_parameters['password']);
            unset($distributor_parameters['password_confirmation']);
        } else {
            $distributor_parameters['password'] = Hash::make($request->password);
        }

        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            File::delete(public_path('/uploads/' . $distributor->logo));
            $distributor_parameters['logo'] = $new_name;
        }
        foreach (Config::get('app.languages') as $language) {
            $distributor->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $distributor->fill($distributor_parameters)->save();
        if ($request->expectsJson()) {
            return fractal()
                ->item($distributor)
                ->transformWith(new DistributorTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Distributor edited successfully')
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $distributor = Distributor::findOrFail($id);
        $distributor->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Distributor deleted successfully')
        ]);
    }

    public function forcedelete(Request $request, $id)
    {
        $distributor = Distributor::withTrashed()->where('id', $id);
        $distributorItem = $distributor->get();
        File::delete(public_path('/uploads/' . $distributorItem->first()->logo));
        $distributor->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Distributor deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $distributor = Distributor::withTrashed()->where('id', $id);
        $distributor->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Distributor restored successfully')
        ]);
    }

    public function cards(Request $request)
    {
        $cards = Card::where('distributor_id', '=', Auth::user()->id)->get();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($cards)
                ->transformWith(new CardTransformer)
                ->addMeta([
                    'cards_count' => $cards->count()
                ])
                ->parseExcludes('distributor')
                ->toArray()
                ;
        }
    }
    public function sellCard(Request $request, $number)
    {
        $errors = [];
        $error_code = 422;
        $card = Card::where('number', '=', $number)->get();

        if ($card->count()) {
            $card = $card->first();
            if ($card->distributor_id != Auth::user()->id) {
                $errors[] = __('admin.Action is not allowed');
                $error_code = 401;
            } else {
                if ($card->user_id) {
                    $errors[] = __('admin.Card is attached to another user');
                } elseif ($card->sold_at) {
                    $errors[] = __('admin.Card is already sold');
                } else {
                    $card->sold_at = now()->timestamp;
                    $card->save();
                    event(new DistributorSoldCard( $card->id, Auth::user()->id));
                    return fractal()
                        ->item($card)
                        ->transformWith(new CardTransformer)
                        ->parseExcludes('distributor')
                        ->toArray()
                        ;
                }
            }     
        } else {
            $errors[] = __('admin.Card number is wrong');
            $error_code = 404;
        }
        
        return errorsResponse($errors, $error_code);
        
    }
    public function account(Request $request)
    {
        $distributor = Distributor::find(Auth::user()->id);
        if ($request->expectsJson()) {
            return fractal()
                ->item($distributor)
                ->transformWith(new DistributorTransformer)
                ->toArray()
                ;
        }
    }
    public function api_update(DistributorUpdateRequest $request)
    {
        return $this->update($request, Auth::user()->id);
    }
}
