<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SellerStoreRequest;
use App\Seller;
use App\Transformers\SellerTransformer;
use Config, File;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sellers = Seller::paginate();
        if ($request->expectsJson()) {
            if ($request->expectsJson()) {
                return fractal()
                    ->collection($sellers->items())
                    ->transformWith(new SellerTransformer)
                    ->toArray()
                    ;
            }
        }
        return view('backend.admin.seller.index')->with([
            'sellers' => $sellers->items(),
            'total' => $sellers->total(),
            'last_page' => $sellers->lastPage(),
            'current_page' => $sellers->currentPage(),
            'nextPageUrl' => $sellers->nextPageUrl(),
            'previousPageUrl' => $sellers->previousPageUrl(),
            'fields' => []
        ]);
    }

    public function filter(Request $request)
    {
        $sellers = Seller::where('id' , '>', '0');
        $fields = [];
        if ($request->id) {
            $sellers = $sellers->where('id', '=', $request->id);
            $fields['id'] = $request->id;
        }
        if ($request->name) {
            $sellers = $sellers->whereHas('translations', function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->name.'%');
            });
            $fields['name'] = $request->name;
        }
        if ($request->created_at_from) {
            $sellers = $sellers->where('created_at', '>=', $request->created_at_from . ' 00:00:00');
            $fields['created_at_from'] = $request->created_at_from;
        }
        if ($request->created_at_to) {
            $sellers = $sellers->where('created_at', '<=', $request->created_at_to . ' 23:59:59');
            $fields['created_at_to'] = $request->created_at_to;
        }

        $sellers = $sellers->get();

        if ($request->expectsJson()) {
            if ($request->expectsJson()) {
                return fractal()
                    ->collection($sellers)
                    ->transformWith(new SellerTransformer)
                    ->toArray()
                    ;
            }
        }
        return view('backend.admin.seller.index')->with([
            'sellers' => $sellers,
            'fields' => $fields
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.seller.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SellerStoreRequest $request)
    {
        $seller = new Seller;
        $seller->website = $request->website;
        $seller->facebook = $request->facebook;
        $seller->instagram = $request->instagram;
        $seller->twitter = $request->twitter;
        $seller->youtube = $request->youtube;
        $seller->featured = $request->featured;
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            $seller->logo = $new_name;
        }
        $seller->save();
        foreach (Config::get('app.languages') as $language) {
            $seller->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $seller->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($seller)
                ->transformWith(new SellerTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Seller added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $seller = Seller::findOrFail($id);
        if ($request->expectsJson()) {
            return fractal()
                ->item($seller)
                ->transformWith(new SellerTransformer)
                ->includeBranches()
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Seller added successfully')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('backend.admin.seller.edit')->with([
            'seller' => Seller::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SellerStoreRequest $request, $id)
    {
        $seller = Seller::findOrFail($id);
        $seller->website = $request->website;
        $seller->facebook = $request->facebook;
        $seller->instagram = $request->instagram;
        $seller->twitter = $request->twitter;
        $seller->youtube = $request->youtube;
        $seller->featured = $request->featured;
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            File::delete(public_path('/uploads/' . $seller->logo));
            $seller->logo = $new_name;
        }
        foreach (Config::get('app.languages') as $language) {
            $seller->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $seller->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($seller)
                ->transformWith(new SellerTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Seller edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $seller = Seller::findOrFail($id);
        if ($seller->branches->count() > 0) {
            if ($request->expectsJson()) {
                return response()->json([
                    __('Seller has branches, please delete them first')
                ]);
            } else {
                return redirect()->back()->withErrors([
                    __('Seller has branches, please delete them first')
                ]);
            }
        }
        $seller->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Seller deleted successfully')
        ]);
    }
    public function forcedelete(Request $request, $id)
    {
        $seller = Seller::withTrashed()->where('id', $id);
        $sellerItem = $seller->get();
        File::delete(public_path('/uploads/' . $sellerItem->first()->logo));
        $seller->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Seller deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $seller = Seller::withTrashed()->where('id', $id);
        $seller->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Seller restored successfully')
        ]);
    }
}
