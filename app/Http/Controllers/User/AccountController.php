<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Events\Users\UserRegistered;
use Illuminate\Auth\Events\Registered;
use App\Services\Cards\CardService;
use App\Services\Auth\UserService;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;

use Illuminate\Support\Facades\Hash;
use App\Country;
use App\User;
use Auth;

class AccountController extends Controller
{
    public $cardService, $userService;

    public function __construct(CardService $cardService, UserService $userService)
    {
        $this->cardService = $cardService;
        $this->userService = $userService;
    }

    public function register(UserStoreRequest $request)
    {
        $user = $this->userService->register($request);
        event(new Registered($user));
        event(new UserRegistered($user->id));

        Auth::login($user, true);
        return redirect()->route('user.cards.index');
    }
    public function updateAccount(UserUpdateRequest $request)
    {
        $user = $this->userService->updateUser($request, auth()->user()->id);
        if ($request->expectsJson()) {
            return fractal()
                ->item($user)
                ->transformWith(new UserTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.User edited successfully')
        ]);
    }

    public function account()
    {
        return view('frontend.account.account')->with([
            'user' => User::findOrFail(auth()->user()->id),
            'countries' => Country::all()
        ]);
    }

    public function updatePasswordWithVerification(Request $request)
    {
        $request->validate([
            'oldPassword' => 'required',
            'password' => 'required|min:6|confirmed'
        ]);
        $user = User::findOrFail(auth()->user()->id);
        if (Hash::check($request->oldPassword, $user->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json([
                'error' => false,
                'data' => $user
            ]);
        } else {
            return response()->json([
                'data' => null,
                'meta' => [
                    'error' => true,
                    'messages' => [__('admin.Old password is wrong')]
                ]
            ], 422);
        }
        
    }

    public function cards()
    {
        return view('frontend.account.cards')->with([
            'cards' => auth()->user()->cards
        ]);
    }
    public function newcard()
    {
        return view('frontend.account.newcard');
    }
}
