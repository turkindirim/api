<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Branch;

class BranchController extends Controller
{
    public function index()
    {
        $branches = Branch::paginate(12);
        return view('frontend.branch.index')->with([
            'branches' => $branches->items(),
            'total' => $branches->total(),
            'last_page' => $branches->lastPage(),
            'current_page' => $branches->currentPage(),
            'nextPageUrl' => $branches->nextPageUrl(),
            'previousPageUrl' => $branches->previousPageUrl()
        ]);
    }
}
