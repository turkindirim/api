<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Distributor;

class DistributorController extends Controller
{
    public function index()
    {
        $distributors = Distributor::paginate(12);
        return view('frontend.distributor.index')->with([
            'distributors' => $distributors->items(),
            'total' => $distributors->total(),
            'last_page' => $distributors->lastPage(),
            'current_page' => $distributors->currentPage(),
            'nextPageUrl' => $distributors->nextPageUrl(),
            'previousPageUrl' => $distributors->previousPageUrl()
        ]);
    }
}
