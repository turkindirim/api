<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LogAdmin;
use App\LogBranch;
use App\LogDistributor;
use App\LogUser;

class LogController extends Controller
{
    public function admins(Request $request)
    {
        $where = [];
        if ($request->from || $request->to) {
            if ($request->from) {
                $where[] = ['created_at', '>=', $request->from . ' 00:00:00'];
            }
            if ($request->to) {
                $where[] = ['created_at', '<=', $request->to . ' 23:59:59'];
            }
        } else {
            $where[] = ['created_at', 'LIKE', date('Y-m-d') . '%'];
        }
        
        $log = LogAdmin::where($where)->get();
        return view('backend.admin.log.admins')->with([
            'log' => $log
        ]);
    }
    public function branches(Request $request)
    {
        $where = [];
        if ($request->from || $request->to) {
            if ($request->from) {
                $where[] = ['created_at', '>=', $request->from . ' 00:00:00'];
            }
            if ($request->to) {
                $where[] = ['created_at', '<=', $request->to . ' 23:59:59'];
            }
        } else {
            $where[] = ['created_at', 'LIKE', date('Y-m-d') . '%'];
        }
        
        $log = LogBranch::where($where)->get();
        return view('backend.admin.log.branches')->with([
            'log' => $log
        ]);
    }
    public function distributors(Request $request)
    {
        $where = [];
        if ($request->from || $request->to) {
            if ($request->from) {
                $where[] = ['created_at', '>=', $request->from . ' 00:00:00'];
            }
            if ($request->to) {
                $where[] = ['created_at', '<=', $request->to . ' 23:59:59'];
            }
        } else {
            $where[] = ['created_at', 'LIKE', date('Y-m-d') . '%'];
        }
        
        $log = LogDistributor::where($where)->get();
        return view('backend.admin.log.distributors')->with([
            'log' => $log
        ]);
    }
    public function users(Request $request)
    {
        $where = [];
        if ($request->from || $request->to) {
            if ($request->from) {
                $where[] = ['created_at', '>=', $request->from . ' 00:00:00'];
            }
            if ($request->to) {
                $where[] = ['created_at', '<=', $request->to . ' 23:59:59'];
            }
        } else {
            $where[] = ['created_at', 'LIKE', date('Y-m-d') . '%'];
        }
        
        $log = LogUser::where($where)->get();
        return view('backend.admin.log.users')->with([
            'log' => $log
        ]);
    }
}
