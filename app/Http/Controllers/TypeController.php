<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CardTypeStoreRequest;
use App\Transformers\TypeTransformer;
use App\CardType;
use Config, Validator;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $types = CardType::all();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($types)
                ->transformWith(new TypeTransformer)
                ->toArray();
        }
        return view('backend.admin.types.index')->with([
            'types' => $types
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CardTypeStoreRequest $request)
    {
        $type = new CardType;
        $type->code = $request->code;
        if ($request->hasFile('card')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('card')->getClientOriginalExtension();
            $request->file('card')->move(public_path('uploads'), $new_name);
            $type->logo = $new_name;
        }
        foreach (Config::get('app.languages') as $language) {
            $type->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $type->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
        }
        $type->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($type)
                ->transformWith(new TypeTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Type added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = CardType::findOrFail($id);
        return view('backend.admin.types.edit')->with([
            'type' => $type
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'code' => 'required|max:2|min:2|unique:card_types,code,'.$id,
        ])->validate();
        $type = CardType::findOrFail($id);
        $type->code = $request->code;
        if ($request->hasFile('card')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('card')->getClientOriginalExtension();
            $request->file('card')->move(public_path('uploads'), $new_name);
            $type->logo = $new_name;
        }
        foreach (Config::get('app.languages') as $language) {
            $type->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $type->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
        }
        $type->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($type)
                ->transformWith(new TypeTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Type edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $type = CardType::findOrFail($id);
        $type->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Type deleted successfully')
        ]);
    }

    
    public function forcedelete(Request $request, $id)
    {
        $type = CardType::withTrashed()->where('id', $id);
        $type->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Type deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $type = CardType::withTrashed()->where('id', $id);
        $type->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Type restored successfully')
        ]);
    }
}
