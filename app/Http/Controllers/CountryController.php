<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transformers\CountryTransformer;
use App\Country;
use Config;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $countries = Country::all()->sortBy('name');

        if ($request->expectsJson()) {
            return fractal()
                ->collection($countries)
                ->transformWith(new CountryTransformer)
                ->toArray();
        }
        return view('backend.admin.country.index')->with([
            'countries' => $countries
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.country.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $country = new Country;
        $country->dial_code = $request->dial_code;
        $country->save();
        foreach (Config::get('app.languages') as $language) {
            $country->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $country->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($country)
                ->transformWith(new CountryTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Country added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::findOrFail($id);
        return view('backend.admin.country.edit')->with([
            'country' => $country
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $country = Country::findOrFail($id);
        $country->dial_code = $request->dial_code;
        foreach (Config::get('app.languages') as $language) {
            $country->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $country->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($country)
                ->transformWith(new CountryTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Country edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $country = Country::findOrFail($id);
        $country->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Country deleted successfully')
        ]);
    }

    public function forcedelete(Request $request, $id)
    {
        $country = Country::withTrashed()->where('id', $id);
        $country->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Country deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $country = Country::withTrashed()->where('id', $id);
        $country->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Country restored successfully')
        ]);
    }
}
