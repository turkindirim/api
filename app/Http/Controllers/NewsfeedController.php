<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Newsfeed;
use App\Transformers\NewsfeedTransformer;
use Config, File;

class NewsfeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $newsfeed = Newsfeed::paginate();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($newsfeed->items())
                ->transformWith(new NewsfeedTransformer)
                ->toArray()
                ;
        }
        return view('backend.admin.newsfeed.index')->with([
            'newsfeed' => $newsfeed->items(),
            'total' => $newsfeed->total(),
            'last_page' => $newsfeed->lastPage(),
            'current_page' => $newsfeed->currentPage(),
            'nextPageUrl' => $newsfeed->nextPageUrl(),
            'previousPageUrl' => $newsfeed->previousPageUrl(),
            'fields' => []
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.newsfeed.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newsfeed = new Newsfeed;
        $newsfeed->offer_id = $request->offer;
        foreach (Config::get('app.languages') as $language) {
            $newsfeed->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $newsfeed->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
            if ($request->hasFile('logo_' . $language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo_' . $language['code'])->getClientOriginalExtension();
                $request->file('logo_' . $language['code'])->move(public_path('uploads'), $new_name);
                $newsfeed->translateOrNew($language['code'])->logo = $new_name;
            }
        }
        $newsfeed->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($newsfeed)
                ->transformWith(new NewsfeedTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Newsfeed added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $newsfeed = Newsfeed::findOrFail($id);
        return view('backend.admin.newsfeed.edit')->with([
            'newsfeed' => $newsfeed,
        ]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newsfeed = Newsfeed::findOrFail($id);
        $newsfeed->offer_id = $request->offer;
        foreach (Config::get('app.languages') as $language) {
            $newsfeed->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $newsfeed->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
            if ($request->hasFile('logo_' . $language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo_' . $language['code'])->getClientOriginalExtension();
                $request->file('logo_' . $language['code'])->move(public_path('uploads'), $new_name);
                File::delete(public_path('/uploads/' . $newsfeed->translate($language['code'])->logo));
                $newsfeed->translateOrNew($language['code'])->logo = $new_name;
            }
        }
        $newsfeed->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($newsfeed)
                ->transformWith(new NewsfeedTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Newsfeed edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $newsfeed = Newsfeed::findOrFail($id);
        $newsfeed->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Newsfeed deleted successfully')
        ]);
    }

    
    public function forcedelete(Request $request, $id)
    {
        $newsfeed = Newsfeed::withTrashed()->where('id', $id);
        $newsfeedItem = $newsfeed->get();
        File::delete(public_path('/uploads/' . $newsfeedItem->first()->logo));
        $newsfeed->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Newsfeed deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $newsfeed = Newsfeed::withTrashed()->where('id', $id);
        $newsfeed->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Newsfeed restored successfully')
        ]);
    }
}
