<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use Illuminate\Http\Request;
use App\Http\Requests\ReviewBranchRequest;
use App\Http\Requests\ReviewPostRequest;
use App\Http\Requests\ReviewOfferRequest;
use App\Branch;
use App\Post;
use App\Offer;
use App\Review;

class ReviewController extends Controller
{

    public function reviewPost(ReviewPostRequest $request)
    {
        $review = new Review([
            'rate' => $request->rate,
            'review' => $request->review,
            'user_id' => auth()->user()->id
        ]);
        $post = Post::findOrFail($request->post_id);
        $post->reviews()->save($review);
        if ($request->expectsJson()) {
            return response()->json([
                'data' => null,
                'meta' => [
                    'error' => false,
                    'messages' => [__('admin.Review added to post')]
                ]
            ], 200);
        } else {
            return redirect()->back()->with([
                'success' => __('admin.Review added to post')
            ]);
        }
    }

    public function reviewOffer(ReviewOfferRequest $request)
    {
        $review = new Review([
            'rate' => $request->rate,
            'review' => $request->review,
            'user_id' => auth()->user()->id
        ]);
        $offer = Offer::findOrFail($request->offer_id);
        $offer->reviews()->save($review);
        if ($request->expectsJson()) {
            return response()->json([
                'data' => null,
                'meta' => [
                    'error' => false,
                    'messages' => [__('admin.Review added to offer')]
                ]
            ], 200);
        } else {
            return redirect()->back()->with([
                'success' => __('admin.Review added to offer')
            ]);
        }
    }

    public function reviewbranch(ReviewBranchRequest $request)
    {
        $review = new Review([
            'rate' => $request->rate,
            'review' => $request->review,
            'user_id' => auth()->user()->id
        ]);
        $branch = Branch::findOrFail($request->branch_id);
        $branch->reviews()->save($review);
        if ($request->expectsJson()) {
            return response()->json([
                'data' => null,
                'meta' => [
                    'error' => false,
                    'messages' => [__('admin.Review added to branch')]
                ]
            ], 200);
        } else {
            return redirect()->back()->with([
                'success' => __('admin.Review added to company')
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy(Request $request, $id)
    {
        $review = Review::findOrFail($id);
        $review->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Item deleted successfully')
        ]);
    }
    public function forcedelete(Request $request, $id)
    {
        $review = Review::withTrashed()->where('id', $id);
        $review->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Item deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $review = Review::withTrashed()->where('id', $id);
        $review->restore();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Item restored successfully')
        ]);
    }
}
