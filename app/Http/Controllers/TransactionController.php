<?php

namespace App\Http\Controllers;
use App\Events\Branches\BranchCanceledTransaction;
use App\Transformers\TransactionTransformer;
use Illuminate\Http\Request;
use App\Transaction;
use Auth;
class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.admin.transactions.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->delete();
        event(new BranchCanceledTransaction(Auth::user()->id, $id));
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Transaction deleted successfully')
        ]);
    }

    

    public function search(Request $request)
    {
        $transactions = Transaction::where('id', '>', '0');
        if ($request->search['value']) {
            $transactions = $transactions->where(function($query) use ($request) {

                $query->orWhere('id', '=', $request->search['value']);
                $query->orWhereHas('user', function ($q) use ($request) {
                    $q->where('name', 'LIKE', '%'.$request->search['value'].'%');
                });
                $query->orWhereHas('card', function ($q) use ($request) {
                    $q->where('number', 'LIKE', '%'.$request->search['value'].'%');
                });
                
                $query->orWhereHas('branch', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->search['value'].'%');
                    });
                });
            });
        }

        // Limit
        $beforeLimitingCount = $transactions->get()->count();
        if (strlen($request->start) && ($request->length > 0)) {
            $transactions = $transactions->offset($request->start)->limit($request->length);
        }

        // Get
        $transactions = $transactions->get();
        
        // Generate JSON
        return \array_merge([
            'draw' => $request->draw + 1,
            'recordsFiltered' => $beforeLimitingCount,
            'recordsTotal' => Transaction::all()->count()
        ], fractal()
            ->collection($transactions)
            ->transformWith(new TransactionTransformer)
            ->toArray()
        );
    }
}
