<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Seller;
use App\Branch;
use App\Distributor;
use App\User;
use App\Language;
use App\Newsfeed;
use App\CardType;
use App\Card;
use App\Category;
use App\Offer;
use App\Country;
use App\Province;
use App\City;
use App\District;

class TrashController extends Controller
{
    public function sellers()
    {
        $sellers = Seller::onlyTrashed()->get();
        return view('backend.admin.trash.sellers')->with([
            'sellers' => $sellers
        ]);
    }
    public function branches()
    {
        $branches = Branch::onlyTrashed()->get();
        return view('backend.admin.trash.branches')->with([
            'branches' => $branches
        ]);
    }
    public function distributors()
    {
        $distributors = Distributor::onlyTrashed()->get();
        return view('backend.admin.trash.distributors')->with([
            'distributors' => $distributors
        ]);
    }
    public function users()
    {
        $users = User::onlyTrashed()->get();
        return view('backend.admin.trash.users')->with([
            'users' => $users
        ]);
    }
    public function newsfeed()
    {
        $newsfeed = Newsfeed::onlyTrashed()->get();
        return view('backend.admin.trash.newsfeed')->with([
            'newsfeed' => $newsfeed
        ]);
    }
    public function languages()
    {
        $languages = Language::onlyTrashed()->get();
        return view('backend.admin.trash.languages')->with([
            'languages' => $languages
        ]);
    }
    public function cards()
    {
        $cards = Card::onlyTrashed()->get();
        return view('backend.admin.trash.cards')->with([
            'cards' => $cards
        ]);
    }
    public function types()
    {
        $types = CardType::onlyTrashed()->get();
        return view('backend.admin.trash.types')->with([
            'types' => $types
        ]);
    }
    public function offers()
    {
        $offers = Offer::onlyTrashed()->get();
        return view('backend.admin.trash.offers')->with([
            'offers' => $offers
        ]);
    }
    public function categories()
    {
        $categories = Category::onlyTrashed()->get();
        return view('backend.admin.trash.categories')->with([
            'categories' => $categories
        ]);
    }
    public function countries()
    {
        $countries = Country::onlyTrashed()->get();
        return view('backend.admin.trash.countries')->with([
            'countries' => $countries
        ]);
    }
    public function provinces()
    {
        $provinces = Province::onlyTrashed()->get();
        return view('backend.admin.trash.provinces')->with([
            'provinces' => $provinces
        ]);
    }
    public function cities()
    {
        $cities = City::onlyTrashed()->get();
        return view('backend.admin.trash.cities')->with([
            'cities' => $cities
        ]);
    }
    public function districts()
    {
        $districts = District::onlyTrashed()->get();
        return view('backend.admin.trash.districts')->with([
            'districts' => $districts
        ]);
    }
}
