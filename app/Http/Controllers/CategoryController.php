<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryStoreRequest;
use App\Transformers\CategoryTransformer;
use App\Category;
use Config, File;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::all();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($categories)
                ->transformWith(new CategoryTransformer)
                ->toArray();
        }
        return view('backend.admin.category.index')->with([
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        $category = new Category;
        $category->code = $request->code;
        foreach (Config::get('app.languages') as $language) {
            $category->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $category->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
            if ($request->hasFile('header_'.$language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('header_'.$language['code'])->getClientOriginalExtension();
                $request->file('header_'.$language['code'])->move(public_path('uploads'), $new_name);
                $offer->translateOrNew($language['code'])->header = $new_name;
            }
        }
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            $category->logo = $new_name;
        }
        
        $category->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($category)
                ->transformWith(new CategoryTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Category added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::with('offers')->find($id);
        return view('frontend.category.show')->with([
            'category' => $category 
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('backend.admin.category.edit')->with([
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryStoreRequest $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->code = $request->code;
        foreach (Config::get('app.languages') as $language) {
            $category->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $category->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
            if ($request->hasFile('header_'.$language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('header_'.$language['code'])->getClientOriginalExtension();
                $request->file('header_'.$language['code'])->move(public_path('uploads'), $new_name);
                File::delete(public_path('/uploads/' . $category->header));
                $category->translateOrNew($language['code'])->header = $new_name;
            }
        }
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            File::delete(public_path('/uploads/' . $category->logo));
            $category->logo = $new_name;
        }
        $category->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($category)
                ->transformWith(new CategoryTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Category edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Category deleted successfully')
        ]);
    }

    public function forcedelete(Request $request, $id)
    {
        $category = Category::withTrashed()->where('id', $id);
        $category->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Category deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $seller = Category::withTrashed()->where('id', $id);
        $seller->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Category restored successfully')
        ]);
    }
}
