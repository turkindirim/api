<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LanguageStoreRequest;
use App\Http\Requests\LanguageUpdateRequest;
use App\Transformers\LanguageTransformer;
use App\Language;
use Auth, Cookie;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $languages = Language::all();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($languages)
                ->transformWith(new LanguageTransformer)
                ->toArray()
                ;
        }
        return view('backend.admin.language.index')->with([
            'languages' => $languages
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.language.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LanguageStoreRequest $request)
    {
        $language = Language::create($request->all());
        if ($request->expectsJson()) {
            return fractal()
                ->item($language)
                ->transformWith(new LanguageTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Language added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('backend.admin.language.edit')->with([
            'language' => Language::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LanguageUpdateRequest $request, $id)
    {
        $language = Language::findOrFail($id);
        $language->fill($request->all())->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($language)
                ->transformWith(new LanguageTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Language edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        $language = Language::findOrFail($id);
        $language->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Language deleted successfully')
        ]);
    }
    
    public function forcedelete(Request $request, $id)
    {
        $language = Language::withTrashed()->where('id', $id);
        $language->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Language deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $language = Language::withTrashed()->where('id', $id);
        $language->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Language restored successfully')
        ]);
    }

    public function switchLanguage(Request $request, $code)
    {
        $language = Language::where('code', '=', $code)->get()->first();
        if (!$language) {
            \abort(404);
        }
        if (Auth::check()) {
            $user = Auth::user();
            $user->default_language = $language->code;
            $user->save();
        }
        Cookie::forever('language', $language->code);

        return redirect()->back()->cookie(
            'language', $language->code, 10000000
        );
    }
}
