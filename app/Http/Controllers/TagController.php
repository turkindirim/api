<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TagStoreRequest;
use App\Transformers\TagTransformer;
use App\Post;
use App\Tag;
use Config, File;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tags = Tag::all();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($tags)
                ->transformWith(new TagTransformer)
                ->toArray();
        }
        return view('backend.admin.tag.index')->with([
            'tags' => $tags
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagStoreRequest $request)
    {
        $tag = new Tag;
        foreach (Config::get('app.languages') as $language) {
            $tag->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        
        $tag->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($tag)
                ->transformWith(new TagTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Tag added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tag = Tag::find($id);
        return view('frontend.tag.show')->with([
            'tag' => $tag 
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        return view('backend.admin.tag.edit')->with([
            'tag' => $tag
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TagStoreRequest $request, $id)
    {
        $tag = Tag::findOrFail($id);
        
        foreach (Config::get('app.languages') as $language) {
            $tag->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $tag->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($tag)
                ->transformWith(new TagTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Tag edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Tag deleted successfully')
        ]);
    }

    public function forcedelete(Request $request, $id)
    {
        $tag = Tag::withTrashed()->where('id', $id);
        $tag->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Tag deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $seller = Tag::withTrashed()->where('id', $id);
        $seller->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Tag restored successfully')
        ]);
    }
}
