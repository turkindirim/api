<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Socialite;
use Auth;
use App\Services\SocialFacebookAccountService;


class SocialAuthFacebookController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function redirectFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function redirectTwitter()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callbackFacebook(SocialFacebookAccountService $service)
    {
        return $this->loginAndRedirect($service->createOrGetUser(Socialite::driver('facebook')->user(), 'facebook'));
    }
    public function callbackTwitter(SocialFacebookAccountService $service)
    {
        return $this->loginAndRedirect($service->createOrGetUser(Socialite::driver('twitter')->user(), 'twitter'));
    }
    public function loginAndRedirect(User $user)
    {
        Auth::login($user, true);
        return redirect()->to('/account');
    }
}
