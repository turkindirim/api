<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class BranchLoginController extends Controller
{
    public function __construct()
    {
        
    }
    public function showLoginForm()
    {
        return view('backend.branch.login');
    }
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
            'remember_me' => 'nullable'
        ]);

        
        if (Auth::guard('branch')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->intended(route('branch.dashboard'));
        }

        return redirect()->back();
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('branch.login');
    }
}
