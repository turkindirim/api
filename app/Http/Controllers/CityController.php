<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\CityStoreRequest;
use App\Transformers\CityTransformer;
use App\Country;
use App\Province;
use App\City;
use Config;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (is_numeric($request->province_id)) {
            $cities = City::where('province_id', '=', $request->province_id)->get()->sortBy('name');
        } else {
            $cities = City::all()->sortBy('name');
        }
        
        if ($request->expectsJson()) {
            return fractal()
                ->collection($cities)
                ->transformWith(new CityTransformer)
                ->toArray();
        }
        return view('backend.admin.city.index')->with([
            'cities' => $cities
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.city.create')->with([
            'countries' => Country::all(),
            'provinces' => Province::all()
        ]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityStoreRequest $request)
    {
        $city = new City;
        $city->province_id = $request->province_id;
        $city->save();
        foreach (Config::get('app.languages') as $language) {
            $city->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $city->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($city)
                ->transformWith(new CityTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.City added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);
        return view('backend.admin.city.edit')->with([
            'city' => $city,
            'countries' => Country::all(),
            'provinces' => Province::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CityStoreRequest $request, $id)
    {
        $city = City::findOrFail($id);
        $city->province_id = $request->province_id;
        foreach (Config::get('app.languages') as $language) {
            $city->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $city->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($city)
                ->transformWith(new CityTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.City edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $city = City::findOrFail($id);
        $city->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.City deleted successfully')
        ]);
    }
    public function forcedelete(Request $request, $id)
    {
        $city = City::withTrashed()->where('id', $id);
        $city->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.City deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $city = City::withTrashed()->where('id', $id);
        $city->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.City restored successfully')
        ]);
    }
}
