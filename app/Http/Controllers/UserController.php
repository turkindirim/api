<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;

use App\Events\Users\UserActivatedCard;
use App\Events\Users\CardActivationFailed;
use App\Events\Users\UserRegistered;
use Illuminate\Auth\Events\Registered;

use App\Services\Auth\UserService;

use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\ActivateCardRequest;
use App\Transformers\CardTransformer;
use App\Transformers\TransactionTransformer;
use App\Transformers\UserTransformer;
use App\Transformers\UserHtmlTransformer;
use Illuminate\Support\Facades\Hash;
use App\Card;
use App\Country;
use App\Province;
use App\Transaction;
use App\User;
use Auth, DB, File;

class UserController extends Controller
{

    public $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::paginate();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($users->items())
                ->transformWith(new UserTransformer)
                ->toArray()
                ;
        }
        return view('backend.admin.user.index')->with([
            'users' => $users->items(),
            'total' => $users->total(),
            'last_page' => $users->lastPage(),
            'current_page' => $users->currentPage(),
            'nextPageUrl' => $users->nextPageUrl(),
            'previousPageUrl' => $users->previousPageUrl(),
            'fields' => []
        ]);
    }

    
    public function filter(Request $request)
    {
        $users = User::where('id' , '>', '0');
        $fields = [];
        if ($request->id) {
            $users = $users->where('id', '=', $request->id);
            $fields['id'] = $request->id;
        }
        if ($request->name) {

            $users = $users->where(function($query) use ($request) {

                $query->orWhereHas('translations', function ($q) use ($request) {
                    $q->where('name', 'LIKE', '%'.$request->name.'%');
                });
                
                $query->orWhereHas('seller', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                $query->orWhereHas('province', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                $query->orWhereHas('city', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                $query->orWhereHas('district', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.$request->name.'%');
                    });
                });
                

                $query->orWhere('email', 'like', '%'.$request->name.'%');
                $query->orWhere('phone', 'like', '%'.$request->name.'%');
                $query->orWhere('mobile', 'like', '%'.$request->name.'%');
                $query->orWhere('address1', 'like', '%'.$request->name.'%');
            });
            $fields['name'] = $request->name;
        }
        if ($request->created_at_from) {
            $users = $users->where('created_at', '>=', $request->created_at_from . ' 00:00:00');
            $fields['created_at_from'] = $request->created_at_from;
        }
        if ($request->created_at_to) {
            $users = $users->where('created_at', '<=', $request->created_at_to . ' 23:59:59');
            $fields['created_at_to'] = $request->created_at_to;
        }

        $users = $users->get();

        if ($request->expectsJson()) {
            if ($request->expectsJson()) {
                return fractal()
                    ->collection($users)
                    ->transformWith(new UserTransformer)
                    ->toArray()
                    ;
            }
        }
        return view('backend.admin.user.index')->with([
            'users' => $users,
            'fields' => $fields
        ]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.user.create')->with([
            'countries' => Country::all(),
            'provinces' => Province::where('country_id', '=', '1')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $user = $this->userService->register($request);
        event(new Registered($user));
        // $user->sendEmailVerificationNotification();
        event(new UserRegistered($user->id));
        if ($request->expectsJson()) {
            return fractal()
                ->item($user)
                ->transformWith(new UserTransformer)
                ->toArray()
                ;
        }        
        
        return redirect()->back()->with([
            'success' => __('admin.User added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('backend.admin.user.edit')->with([
            'user' => $user,
            'countries' => Country::all(),
            'provinces' => Province::where('country_id', '=', $user->country_id)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $user = $this->userService->updateUser($request, $id);
        if ($request->expectsJson()) {
            return fractal()
                ->item($user)
                ->transformWith(new UserTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.User edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.User deleted successfully')
        ]);
    }

    public function search(Request $request)
    {
        $users = User::where('id', '>', '0');
        if ($request->search['value']) {
            $users = $users->where('name', 'LIKE', '%'.$request->search['value'].'%')->orWhere('email', 'LIKE', '%'.$request->search['value'].'%');
        }
        switch ($request->order[0]['column']) {
            case '0':
                $users = $users->orderBy('id', $request->order[0]['dir']);
                break;
            case '1':
                $users = $users->orderBy('name', $request->order[0]['dir']);
                break;
            case '2':
                $users = $users->orderBy('email', $request->order[0]['dir']);
                break;
            case '4':
            
                $users = $users->withCount('cards')->orderBy('cards_count', $request->order[0]['dir']);
                break;
            case '9':
                $users = $users->orderBy('created_at', $request->order[0]['dir']);
                break;
        }
        $beforeLimitingCount = $users->get()->count();
        if (strlen($request->start) && ($request->length > 0)) {
            $users = $users->offset($request->start)->limit($request->length);
        }
        $users = $users->get();
        return \array_merge([
            'draw' => $request->draw + 1,
            'recordsFiltered' => $beforeLimitingCount,
            'recordsTotal' => User::all()->count()
        ], fractal()
            ->collection($users)
            ->transformWith(new UserHtmlTransformer)
            ->toArray()
        );
    }


    
    public function forcedelete(Request $request, $id)
    {
        $user = User::withTrashed()->where('id', $id);
        $userItem = $user->get();
        File::delete(public_path('/uploads/' . $userItem->first()->logo));
        if ($userItem->first()->passport) {
            File::delete(public_path('/uploads/' . $$userItem->first()->passport));
        }
        $user->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.User deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $user = User::withTrashed()->where('id', $id);
        $user->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.User restored successfully')
        ]);
    }

    public function cards(Request $request)
    {
        $cards = Card::where('user_id', '=', Auth::user()->id)->get();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($cards)
                ->transformWith(new CardTransformer)
                ->addMeta([
                    'cards_count' => $cards->count()
                ])
                ->parseExcludes('distributor')
                ->toArray()
                ;
        }
    }
    public function activate(ActivateCardRequest $request)
    {
        $errors = [];
        $error_code = 422;

        $card = Card::where('number', '=', $request->number)->get();

        if ($card->count()) {
            $card = $card->first();
            if ($card->activation_code != $request->activation_code) {
                $errors[] = __('admin.Activation code is wrong');

                event(new CardActivationFailed($card->id, 'Activation code is wrong'));
            } elseif ($card->user_id != null) {
                if ($card->user_id == Auth::user()->id) {
                    $errors[] = __('admin.You have already activated this card');
                    event(new CardActivationFailed($card->id, 'You have already activated this card'));
                } else {
                    $errors[] = __('admin.Unable to activate this card');
                    event(new CardActivationFailed($card->id, 'Unable to activate this card'));
                    $error_code = 403;
                }
            } 
            /*
            elseif ($card->sold_at == null) {
                $errors[] = __('admin.Unable to activate this card');
            }
            */
            else {
                $card->activated_at = now()->timestamp;
                $card->user_id = Auth::user()->id;
                $card->save();

                event(new UserActivatedCard(Auth::user()->id, $card->id));

                if ($request->expectsJson()) {
                    return fractal()->item($card)
                        ->transformWith(new CardTransformer)
                        ->parseExcludes('distributor')->toArray();
                } else {
                    return redirect()->route('user.cards.index');
                }
            }
        } else {
            $errors[] = __('admin.Card number is wrong');
            $error_code = 404;
        }
        if ($request->expectsJson()) {
            return errorsResponse($errors, $error_code);
        }
    }

    public function transactions()
    {
        $paginator = Transaction::where('user_id', '=', Auth::user()->id)->paginate();
    
        $transactions = $paginator->getCollection();
        $resource = new Collection($transactions, new TransactionTransformer());
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $manager->createData($resource)->toArray();

        return ($manager->createData($resource)->toArray());
    }
    public function account(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($request->expectsJson()) {
            return fractal()
                ->item($user)
                ->transformWith(new UserTransformer)
                ->toArray();
        }
    }
    public function api_update(UserUpdateRequest $request)
    {
        return $this->update($request, Auth::user()->id);
    }
}
