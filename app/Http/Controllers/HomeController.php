<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactUsRequest;
use Illuminate\Http\Request;
use App\CardType;


use App\Transformers\OfferTransformer;
use App\Transformers\SellerTransformer;
use App\Transformers\CategoryTransformer;
use App\Transformers\TypeTransformer;
use App\Transformers\SlideTransformer;

use App\Mail\ContactUsMail;
use App\Mail\OrderMail;

use App\Category;
use App\Offer;
use App\Seller;
use App\Setting;
use App\Slide;

use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('welcome')->with([
            'offers' => Offer::where([
                ['valid_from', '<=', date('Y-m-d')],
                ['valid_to', '>=', date('Y-m-d')],
                ['active', '=', '1'],
                ['featured', '=', '1'],
            ])->orderBy('id', 'desc')->limit(16)->get(),
            'featured_sellers' => Seller::where('featured', '=', '1')->get(),
        ]);
    }
    public function contact()
    {
        return view('frontend.contact');
    }
    
    public function cards()
    {
        return view('frontend.cards');
    }
    public function contactus(ContactUsRequest $request)
    {
        $setting = Setting::find(1);
        Mail::to($setting->contactusemail)->send(new ContactUsMail([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
        ]));
    }

    public function order(ContactUsRequest $request)
    {
        $setting = Setting::find(1);
        Mail::to($setting->contactusemail)->send(new OrderMail([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
        ]));
    }

    public function homeApi(Request $request)
    {
        $featured_sellers = Seller::where('featured', '=', '1')->limit(10)->get();
        $popular_offers = Offer::where([
            ['valid_from', '<=', date('Y-m-d')],
            ['valid_to', '>=', date('Y-m-d')],
            ['active', '=', '1'],
        ])->orderBy('views', 'desc')->limit(10)->get();
        $featured_offers = Offer::where([
            ['valid_from', '<=', date('Y-m-d')],
            ['valid_to', '>=', date('Y-m-d')],
            ['active', '=', '1'],
            ['featured', '=', '1'],
        ])->orderBy('id', 'desc')->limit(10)->get();

        return \response()->json([
            'featured_offers' => fractal()
                ->collection($featured_offers)
                ->transformWith(new OfferTransformer)
                ->toArray(),
            'featured_sellers' => fractal()
                ->collection($featured_sellers)
                ->transformWith(new SellerTransformer)
                ->toArray(),
            'categories' => fractal()
                ->collection(Category::all())
                ->transformWith(new CategoryTransformer)
                ->toArray(),
            'cards' => fractal()
                ->collection(CardType::all())
                ->transformWith(new TypeTransformer)
                ->toArray(),
            'slides' => fractal()
                ->collection(Slide::all())
                ->transformWith(new SlideTransformer)
                ->toArray(),
            'popular_offers' => fractal()
            ->collection($popular_offers)
            ->transformWith(new OfferTransformer)
            ->toArray()
        ], 200);

    }

}
