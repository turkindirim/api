<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProvinceStoreRequest;
use App\Transformers\ProvinceTransformer;
use App\Country;
use App\Province;
use Config;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if (is_numeric($request->country_id)) {
            $provinces = Province::where('country_id', '=', $request->country_id)->get()->sortBy('name');
        } else {
            $provinces = Province::all()->sortBy('name');
        }
        if ($request->expectsJson()) {
            return fractal()
                ->collection($provinces)
                ->transformWith(new ProvinceTransformer)
                ->toArray();
        }
        return view('backend.admin.province.index')->with([
            'provinces' => $provinces
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.province.create')->with([
            'countries' => Country::all()
        ]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProvinceStoreRequest $request)
    {
        $province = new Province;
        $province->country_id = $request->country_id;
        $province->save();
        foreach (Config::get('app.languages') as $language) {
            $province->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $province->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($province)
                ->transformWith(new ProvinceTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Province added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $province = Province::findOrFail($id);
        return view('backend.admin.province.edit')->with([
            'province' => $province
        ])->with([
            'countries' => Country::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProvinceStoreRequest $request, $id)
    {
        $province = Province::findOrFail($id);
        $province->country_id = $request->country_id;
        foreach (Config::get('app.languages') as $language) {
            $province->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
        }
        $province->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($province)
                ->transformWith(new ProvinceTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Province edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $province = Province::findOrFail($id);
        $province->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Province deleted successfully')
        ]);
    }

    public function forcedelete(Request $request, $id)
    {
        $province = Province::withTrashed()->where('id', $id);
        $province->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Province deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $province = Province::withTrashed()->where('id', $id);
        $province->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Province restored successfully')
        ]);
    }
}
