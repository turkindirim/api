<?php

namespace App\Http\Controllers;
use App\Transformers\BranchTransformer;
use App\Transformers\DistributorTransformer;
use App\Transformers\OfferTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\AdminStoreRequest;
use App\Http\Requests\AdminUpdateRequest;
use App\Http\Requests\AdminAccountUpdateRequest;

use App\Admin;
use App\Branch;
use App\Card;
use App\Country;
use App\Distributor;
use App\Language;
use App\Offer;
use App\Province;
use App\Seller;
use App\Setting;
use App\User;

use Auth, File, DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('SuperAdmin')->only([
            'index',
            'create',
            'store',
            'edit',
            'update',
            'delete',
        ]);
    }

    public function index(Request $request)
    {
        $admins = Admin::all();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($admins)
                ->transformWith(new AdminTransformer)
                ->toArray()
                ;
        }
        return view('backend.admin.admin.index')->with([
            'admins' => $admins,
        ]);
    }
    public function create()
    {
        return view('backend.admin.admin.create');
    }
    public function store(AdminStoreRequest $request)
    {
        $admin = new Admin;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->superadmin = $request->superadmin;
        $admin->password = Hash::make($request->password);
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            $admin->logo = $new_name;
        }
        $admin->save();
        if ($request->expectsJson()) {
            return fractal()
                ->item($admin)
                ->transformWith(new AdminTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Admin added successfully')
        ]);
    }
    public function edit($id)
    {
        $admin = Admin::findOrFail($id);
        return view('backend.admin.admin.edit')->with([
            'admin' => $admin
        ]);
    }
    public function update(AdminUpdateRequest $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->superadmin = $request->superadmin;
        if ($request->password) {
            $admin->password = Hash::make($request->password);
        }
        
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            File::delete(public_path('/uploads/' . $admin->logo));
            $admin->logo = $new_name;
        }
        $admin->save();
        if ($request->expectsJson()) {
            return fractal()
                ->item($admin)
                ->transformWith(new AdminTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Admin edited successfully')
        ]);
        
    }
    public function destroy(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $admin->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Admin deleted successfully')
        ]);
    }

    public function dashboard()
    {
        $users_by_province = DB::table('users')
            ->select(DB::raw('count(*) as user_count, province_id'))
            ->groupBy('province_id')
            ->get()->toArray();
        
        $provinces = [];
        foreach ($users_by_province as $province) {
            $provinces[] = $province->province_id;
        }


        $users_by_location = DB::table('users')
        ->select(DB::raw('count(*) as user_count, country_id'))
        ->groupBy('country_id')
        ->get()->toArray();

        $locations = [];
        foreach ($users_by_location as $location) {
            $locations[] = $location->country_id;
        }
            
        $users_by_nationality = DB::table('users')
        ->select(DB::raw('count(*) as user_count, nationality'))
        ->groupBy('nationality')
        ->get()->toArray();

        $nationalities = [];
        foreach ($users_by_nationality as $nationality) {
            $nationalities[] = $nationality->nationality;
        }

        $active_cards = Card::whereNull('user_id')->get()->count();
        $all_cards = Card::all()->count();
            
        return view('backend.admin.dashboard')->with([
            'users_by_type' => [
                'admins' => Admin::all()->count(),
                'branches' => Branch::all()->count(),
                'distributors' => Distributor::all()->count(),
                'sellers' => Seller::all()->count(),
                'users' => User::all()->count(),
            ],
            'users_by_province' => [
                'users' => $users_by_province,
                'provinces' => Province::find($provinces)->toArray()
            ],
            'users_by_location' => [
                'users' => $users_by_location,
                'locations' => Country::find($locations)
            ],
            'users_by_nationality' => [
                'users' => $users_by_nationality,
                'nationalities' => Country::find($nationalities)
            ],
            'cards' => [
                'count' => $all_cards,
                'active' => $active_cards,
                'inactive' => ($all_cards - $active_cards)
            ]
        ]);
    }
    public function settings()
    {
        return view('backend.admin.settings')->with([
            'languages' => Language::all(),
            'settings' => Setting::where('id', '=', '1')->first()
        ]);
    }
    public function update_settings(Request $request)
    {
        $settings = Setting::where('id', '=', '1')->first();
        $settings->default_language = $request->default_language;
        $settings->save();
        
        return redirect()->back()->with([
            'success' => __('admin.Settings edited successfully')
        ]);
    }
    public function account()
    {
        return view('backend.admin.account')->with([
            'languages' => Language::all()
        ]);
    }
    public function updateAccount(AdminAccountUpdateRequest $request)
    {
        $admin = Admin::find(Auth::user()->id);
        if (Hash::check($request->current_password, $admin->password)) {
            $admin->email = $request->email;
            $admin->name = $request->name;
            $admin->default_language = $request->default_language;
            if (strlen($request->password)) {
                $admin->password = Hash::make($request->password);
            }
            if ($request->hasFile('logo')) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
                $request->file('logo')->move(public_path('uploads'), $new_name);
                $admin->logo = $new_name;
            }
            $admin->save();
            return redirect()->back()->with([
                'success' => __('admin.Profile edited successfully')
            ]);
        } else {
            if ($request->expectsJson()) {
                return errorsResponse([__('admin.Current password is invalid')], 403);
            }
            return redirect()->back()->withErrors([__('admin.Current password is invalid')]);
        }
    }
    public function getBranch(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'branch' => 'required|string|min:2',
        ]);
        if ($validator->fails()) {
            return errorsResponse([$validator->messages()->first()]);
        } 
        $branches = Branch::whereHas('translations', function ($query) use ($request) {
           $query->where('name', 'LIKE', '%'.$request->branch.'%');
        })->get();
        if ($branches->count()) {
            return fractal()
                ->collection($branches)
                ->transformWith(new BranchTransformer)
                ->addMeta(['error' => false])
                ->toArray();
        }

        return errorsResponse([__('admin.Branch Not Found')]);
    }
    
    public function getUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required|string|min:2',
        ]);
        if ($validator->fails()) {
            return errorsResponse([$validator->messages()->first()]);
       } 
       $users = User::where('name', 'LIKE', '%'.$request->user.'%')->get();
       if ($users->count()) {
            return fractal()
                ->collection($users)
                ->transformWith(new UserTransformer)
                ->addMeta(['error' => false])
                ->toArray();
       }

        return errorsResponse([__('admin.User Not Found')]);
    }
    public function getDistributor(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'distributor' => 'required|string|min:2',
        ]);
        if ($validator->fails()) {
            return errorsResponse([$validator->messages()->first()]);
        } 
        $distributors = Distributor::whereHas('translations', function ($query) use ($request) {
            $query->where('name', 'LIKE', '%'.$request->distributor.'%');
        })->get();


        if ($distributors->count()) {
            return fractal()
                ->collection($distributors)
                ->transformWith(new DistributorTransformer)
                ->addMeta(['error' => false])
                ->toArray();
        }
        return errorsResponse([__('admin.Distributor Not Found')]);
    }


    public function getOffer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offer' => 'required|string|min:2',
        ]);
        if ($validator->fails()) {
            return errorsResponse([$validator->messages()->first()]);
        } 
        $offers = Offer::whereHas('translations', function ($query) use ($request) {
            $query->where('name', 'LIKE', '%'.$request->offer.'%')->orWhere('description', 'LIKE', '%'.$request->offer.'%');
        })->get();


        if ($offers->count()) {
            return fractal()
                ->collection($offers)
                ->transformWith(new OfferTransformer)
                ->addMeta(['error' => false])
                ->toArray();
        }
        return errorsResponse([__('admin.Offer Not Found')]);
    }

    public function setsort(Request $request, $model)
    {
        switch ($model) {
            case 'offer':
                $offer = Offer::findOrFail($request->id);
                $offer->sort = $request->sort;
                $offer->save();
                break;
            default:
        }
    }
}
