<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\OfferStoreRequest;
use App\Transformers\OfferTransformer;
use Illuminate\Support\Facades\Input;
use App\Admin;
use App\Branch;
use App\Category;
use App\Media;
use App\Offer;
use App\Tag;
use Illuminate\Support\Collection;

use Auth, Config, Validator, File;

class OfferController extends Controller
{
    protected $newslug = '';
    protected $images = ['jpg','jpeg','png','bmp','gif'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $offers = $this->getOffers($request);
        if ($request->expectsJson()) {
            return fractal()
                ->collection($offers->items())
                ->transformWith(new OfferTransformer)
                ->includeBrief()
                ->toArray()
                ;
        }
        return view('backend.admin.offer.index')->with([
            'offers' => $offers->items(),
            'total' => $offers->total(),
            'last_page' => $offers->lastPage(),
            'current_page' => $offers->currentPage(),
            'nextPageUrl' => $offers->nextPageUrl(),
            'previousPageUrl' => $offers->previousPageUrl(),
            'categories' => Category::all(),
            'fields' => []
        ]);
    }
    public function indexFront(Request $request)
    {
        $offers = $this->getOffers($request, 12);
        return view('frontend.offer.index')->with([
            'offers' => $offers->items(),
            'total' => $offers->total(),
            'last_page' => $offers->lastPage(),
            'current_page' => $offers->currentPage(),
            'nextPageUrl' => $offers->nextPageUrl(),
            'previousPageUrl' => $offers->previousPageUrl(),
            'fields' => []
        ]);
    }
    public function indexMap(Request $request)
    {
        $offers = $this->getOffers($request, 12);
        return view('frontend.offer.map')->with([
            'offers' => $offers->items(),
            'total' => $offers->total(),
            'last_page' => $offers->lastPage(),
            'current_page' => $offers->currentPage(),
            'nextPageUrl' => $offers->nextPageUrl(),
            'previousPageUrl' => $offers->previousPageUrl(),
            'fields' => []
        ]);
    }

    function getOffers(Request $request, $perPage = false)
    {
        if (Auth::user() instanceof Admin) {
            $category = Input::get('category');
            $offers = Offer::where('id', '>', '0');
            if (!empty($request->valid_in)) {
                $offers = $offers->where([
                    ['valid_from', '<=', $request->valid_in],
                    ['valid_to', '>=', $request->valid_in],
                ]);
            }
            if (!empty($request->created_at_from)) {
                $offers = $offers->where([
                    ['created_at', '>=', $request->created_at_from]
                ]);
            }
            if (!empty($request->created_at_to)) {
                $offers = $offers->where([
                    ['created_at', '<=', $request->created_at_to]
                ]);
            }
            if (!empty($request->name)) {
                $offers = $offers->Where(function ($aaa) use ($request) {
                    $aaa->WhereHas('translations', function ($query) use ($request) {
                        $query->where('name', 'LIKE', '%'.$request->name.'%');
                    });

                    $aaa->orWhereHas('branches', function($q) use($request) {
                        $q->WhereHas('translations', function ($query) use ($request) {
                            $query->where('name', 'LIKE', '%'.$request->name.'%');
                        });
                    });
                });
            }
            //dd($offers->toSql());
            if (strlen($request->offer_status)) {
                $offers = $offers->where('active', '=', $request->offer_status);
            }
        } else {
            $offers = Offer::where([
                ['valid_from', '<=', date('Y-m-d')],
                ['valid_to', '>=', date('Y-m-d')],
                ['active', '=', '1']
            ]);
            $category = Input::get('category');
            $city = Input::get('city');
            $term = Input::get('term');
            
            if (is_numeric($city)) {
                $offers = $offers->whereHas('branches', function ($query) use ($city) {
                    $query->whereHas('city', function ($q) use ($city) {
                        $q->where('id', '=', $city);
                    });
                });
            }
            if (strlen($term)) {
                $offers = $offers->where(function($query) use ($term) {
                    $query->orWhereHas('translations', function ($q) use ($term) {
                        $q->where('name', 'LIKE', '%'.$term.'%');
                    });
                    $query->orWhereHas('translations', function ($q) use ($term) {
                        $q->where('description', 'LIKE', '%'.$term.'%');
                    });
                });
            }
        }
        if ($request->branches) {
            $branches = \explode(',', $request->branches);
            $offers = $offers->whereHas('branches', function($q) use($branches) {
                $q->whereIn('branch_id', $branches);
            });
        }
        
        if ($request->categories) {
            $categories = \explode(',', $request->categories);
            $offers = $offers->whereHas('categories', function($q) use($categories) {
                $q->whereIn('category_id', $categories);
            });
        }
        if (is_numeric($category)) {
            $offers = $offers->whereHas('categories', function ($query) use ($category) {
                $query->where('category_id', '=', $category);
            });
        }

        return $perPage ? $offers->paginate($perPage) : $offers->paginate();
    }


    public function showOffersByTag(Request $request, $id)
    {
        $offers = Offer::where([
            ['valid_from', '<=', date('Y-m-d')],
            ['valid_to', '>=', date('Y-m-d')],
            ['active', '=', '1']
        ])->whereHas('tags', function ($query) use ($id) {
            $query->where('tags.id', '=', $id);
        })->paginate();
        return view('frontend.offer.index')->with([
            'offers' => $offers->items(),
            'total' => $offers->total(),
            'last_page' => $offers->lastPage(),
            'current_page' => $offers->currentPage(),
            'nextPageUrl' => $offers->nextPageUrl(),
            'previousPageUrl' => $offers->previousPageUrl(),
            'fields' => []
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.offer.create')->with([
            'categories' => Category::all(),
            'tags' => Tag::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $offer = new Offer;
        $offer->valid_from = $request->valid_from;
        $offer->valid_to = $request->valid_to;
        $offer->featured = 0;
        if (Auth::user() instanceof Admin) {
            $offer->featured = $request->featured ? 1 : 0;
            $offer->active =  $request->active;
        } elseif (Auth::user() instanceof Branch) {
            $offer->active =  0;
            $offer->by_branch = Auth::user()->id;
        }
        $offer->save();

        if ($request->hasFile('media')) {
            $files = $request->file('media');
            foreach ($files as $file) {
                $ext = \strtolower($file->getClientOriginalExtension());
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $ext;
                $file->move(public_path('uploads'), $new_name);
                Media::create([
                    'offer_id' => $offer->id,
                    'name' => $new_name,
                    'type' => in_array($ext, $this->images) ? 'image' : 'video',
                ]);
            }
        }

        if (!is_null($request->category)) {
            $categories = Category::find($request->category);
            $offer->categories()->attach($categories);
        }
        if (!is_null($request->tag)) {
            $tags = Tag::find($request->tag);
            $offer->tags()->attach($tags);
        }
        if (Auth::user() instanceof Admin) {
            if (!is_null($request->branch)) {
                $branches = Branch::find($request->branch);
                $offer->branches()->attach($branches);
            }
        } else {
            $offer->branches()->attach(Auth::user());
        }
        foreach (Config::get('app.languages') as $language) {
            $offer->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $offer->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
            $offer->translateOrNew($language['code'])->excerpt = $request['excerpt_' . $language['code']];
            
            if ($request->hasFile('thumb_'.$language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('thumb_'.$language['code'])->getClientOriginalExtension();
                $request->file('thumb_'.$language['code'])->move(public_path('uploads'), $new_name);
                $offer->translateOrNew($language['code'])->thumb = $new_name;
            }
            if ($request->hasFile('background_'.$language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('background_'.$language['code'])->getClientOriginalExtension();
                $request->file('background_'.$language['code'])->move(public_path('uploads'), $new_name);
                $offer->translateOrNew($language['code'])->background = $new_name;
            }
            if ($request->hasFile('header_'.$language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('header_'.$language['code'])->getClientOriginalExtension();
                $request->file('header_'.$language['code'])->move(public_path('uploads'), $new_name);
                $offer->translateOrNew($language['code'])->header = $new_name;
            }     
            if ($request->hasFile('mobile_thumb_'.$language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('mobile_thumb_'.$language['code'])->getClientOriginalExtension();
                $request->file('mobile_thumb_'.$language['code'])->move(public_path('uploads'), $new_name);
                $offer->translateOrNew($language['code'])->mobile_thumb = $new_name;
            }            
        }
        $offer->save();
        
        if ($request->expectsJson()) {
            return fractal()
                ->item($offer)
                ->transformWith(new OfferTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Offer added successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $offer = Offer::findOrFail($id);
        $offer->increment('views');
        if ($request->expectsJson()) {
            return fractal()
                ->item($offer)
                ->transformWith(new OfferTransformer)
                ->includeBranches()
                ->includeReviews()
                ->toArray()
                ;
        }
        return view('frontend.offer.show')->with([
            'offer' => $offer
        ]);
    }

    public function reviews(Request $request, $id)
    {
        
        $offer = Offer::findOrFail($id);
        if ($request->expectsJson()) {

        }
        return view('backend.admin.offer.reviews')->with([
            'offer' => $offer,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offer = Offer::findOrFail($id);
        return view('backend.admin.offer.edit')->with([
            'offer' => $offer,
            'categories' => Category::all(),
            'tags' => Tag::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $offer = Offer::findOrFail($id);
        $offer->valid_from = $request->valid_from;
        $offer->valid_to = $request->valid_to;
        $offer->active = $request->active;
        
        //dd($request->hasFile('thumb_en'));
        foreach (Config::get('app.languages') as $language) {
            $offer->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $offer->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
            $offer->translateOrNew($language['code'])->excerpt = $request['excerpt_' . $language['code']];
            // $offer->translateOrNew($language['code'])->slug = $this->slugger($this->slug($request['name_' . $language['code']]));
            if ($request->hasFile('thumb_'.$language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('thumb_'.$language['code'])->getClientOriginalExtension();
                $request->file('thumb_'.$language['code'])->move(public_path('uploads'), $new_name);
                File::delete(public_path('/uploads/' . $offer->thumb));
                $offer->translateOrNew($language['code'])->thumb = $new_name;
            }
            if ($request->hasFile('background_'.$language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('background_'.$language['code'])->getClientOriginalExtension();
                $request->file('background_'.$language['code'])->move(public_path('uploads'), $new_name);
                File::delete(public_path('/uploads/' . $offer->background));
                $offer->translateOrNew($language['code'])->background = $new_name;
            }
            if ($request->hasFile('header_'.$language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('header_'.$language['code'])->getClientOriginalExtension();
                $request->file('header_'.$language['code'])->move(public_path('uploads'), $new_name);
                File::delete(public_path('/uploads/' . $offer->header));
                $offer->translateOrNew($language['code'])->header = $new_name;
            }
            if ($request->hasFile('mobile_thumb_'.$language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('mobile_thumb_'.$language['code'])->getClientOriginalExtension();
                $request->file('mobile_thumb_'.$language['code'])->move(public_path('uploads'), $new_name);
                File::delete(public_path('/uploads/' . $offer->mobile_thumb));
                $offer->translateOrNew($language['code'])->mobile_thumb = $new_name;
            }
        }
        if ($request->hasFile('media')) {
            $files = $request->file('media');
            foreach ($files as $file) {
                $ext = \strtolower($file->getClientOriginalExtension());
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $ext;
                $file->move(public_path('uploads'), $new_name);
                Media::create([
                    'offer_id' => $offer->id,
                    'name' => $new_name,
                    'type' => in_array($ext, $this->images) ? 'image' : 'video',
                ]);
            }
        }
        
        if (!is_null($request->category)) {
            $categories = Category::find($request->category);
            $offer->categories()->sync($categories);
        }
        if (!is_null($request->tag)) {
            $tags = [];
            foreach ($request->tag as $tag) {
                if (is_numeric($tag)) {
                    $tags[] = $tag;
                } else {
                    $the_tag = new Tag;
                    $the_tag->save();
                    foreach (Config::get('app.languages') as $language) {
                        $the_tag->translateOrNew($language['code'])->name = $tag;
                    }
                    $the_tag->save();
                    $tags[] = $the_tag->id;
                }
            }
            $offer->tags()->sync($tags);
        } else {
            $offer->tags()->sync([]);
        }
        if (Auth::user() instanceof Admin) {
            $offer->featured = $request->featured ? 1 : 0;
            if (!is_null($request->branch)) {
                $branches = Branch::find($request->branch);
                $offer->branches()->syncWithoutDetaching($branches);
            }
        }
        $offer->save();
        if ($request->expectsJson()) {
            return fractal()
                ->item($offer)
                ->transformWith(new OfferTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Offer edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $offer = Offer::findOrFail($id);
        if (Auth::user() instanceof Admin) {
            $offer->delete();
        } elseif ($offer->by_branch != Auth::user()->id) {
            return response()->json([
                'data' => null,
                'meta' => [
                    'error' => true,
                    'messages' => [__('admin.Forbidden Action')]
                ]
            ], 403);
        }
        
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Offer deleted successfully')
        ]);
    }

    
    public function forcedelete(Request $request, $id)
    {
        $offer = Offer::withTrashed()->where('id', $id);
        $offerItem = Offer::withTrashed()->where('id', $id)->get();
        foreach ($offerItem as $o) {
            foreach ($o->media as $file) {
                File::delete(public_path('/uploads/' . $file->name));
            }            
        }
        $offer->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Offer deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $offer = Offer::withTrashed()->where('id', $id);
        $offer->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Offer restored successfully')
        ]);
    }

    public function deleteOfferBranches(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offer' => 'required|numeric',
            'ids' => 'required'
        ]);
        if ($validator->fails()) {
            return errorsResponse([$validator->messages()->first()]);
        }
        $offer = Offer::find($request->offer);
        $branches = \explode(',', $request->ids);
        $offer->branches()->detach($branches);
    }
    
    public function slugger($slug, $counter = '')
    {
        if ($counter != '') {
            $this->newslug = $slug . '-' . $counter;
            $next = (int)($counter) + 1;
        } else {
            $this->newslug = $slug;
            $next = 2;
        }
        $offer = Offer::whereHas('translations', function ($query) {
            $query->where('name', '=', $this->newslug);
        })->get();

        if (!$offer->isEmpty()) {
            return $this->slugger($slug, $next);
        }
        return $this->newslug;
    }
    public function slug($string)
    {
        return urlencode(mb_strtolower(str_replace(' ', '-', trim($string))));
    }
}
