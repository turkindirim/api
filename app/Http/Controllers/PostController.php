<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;
use App\Tag;
use App\Transformers\PostTransformer;
use Config, File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::withoutGlobalScope('id')->paginate();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($posts->items())
                ->transformWith(new PostTransformer)
                ->toArray()
                ;
        }
        return view('backend.admin.post.index')->with([
            'posts' => $posts->items(),
            'total' => $posts->total(),
            'last_page' => $posts->lastPage(),
            'current_page' => $posts->currentPage(),
            'nextPageUrl' => $posts->nextPageUrl(),
            'previousPageUrl' => $posts->previousPageUrl(),
            'fields' => []
        ]); 
    }

    
    public function indexFront(Request $request)
    {
        $posts = Post::paginate(12);
        if ($request->expectsJson()) {
            return fractal()
                ->collection($posts->items())
                ->transformWith(new PostTransformer)
                ->toArray()
                ;
        }
        return view('frontend.post.index')->with([
            'recentPosts' => Post::take(6)->orderBy('id', 'desc')->get(),
            'posts' => $posts->items(),
            'total' => $posts->total(),
            'last_page' => $posts->lastPage(),
            'current_page' => $posts->currentPage(),
            'nextPageUrl' => $posts->nextPageUrl(),
            'previousPageUrl' => $posts->previousPageUrl(),
            'tags' => Tag::all(),
            'categories' => Category::all()
        ]); 
    }


    public function showPostsByTag(Request $request, $id)
    {
        $posts = Post::whereHas('tags', function ($query) use ($id) {
            $query->where('tags.id',$id);
        })->paginate();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($posts->items())
                ->transformWith(new PostTransformer)
                ->toArray()
                ;
        }
        return view('frontend.post.index')->with([
            'recentPosts' => Post::take(6)->orderBy('id', 'desc')->get(),
            'tags' => Tag::all(),
            'posts' => $posts->items(),
            'total' => $posts->total(),
            'last_page' => $posts->lastPage(),
            'current_page' => $posts->currentPage(),
            'nextPageUrl' => $posts->nextPageUrl(),
            'previousPageUrl' => $posts->previousPageUrl(),
            'fields' => []
        ]); 
    }
    public function showPostsByCategory(Request $request, $id)
    {
        $posts = Post::whereHas('categories', function ($query) use ($id) {
            $query->where('categories.id',$id);
        })->paginate();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($posts->items())
                ->transformWith(new PostTransformer)
                ->toArray()
                ;
        }
        return view('frontend.post.index')->with([
            'recentPosts' => Post::take(6)->orderBy('id', 'desc')->get(),
            'tags' => Tag::all(),
            'posts' => $posts->items(),
            'total' => $posts->total(),
            'last_page' => $posts->lastPage(),
            'current_page' => $posts->currentPage(),
            'nextPageUrl' => $posts->nextPageUrl(),
            'previousPageUrl' => $posts->previousPageUrl(),
            'fields' => []
        ]); 
    }

    public function aboutPage()
    {
        return view('frontend.about')->with([
            'post' => Post::withoutGlobalScope('id')->where('id', '1')->get()->first()
        ]);
    }
    public function privacyPage()
    {
        return view('frontend.privacy')->with([
            'post' => Post::withoutGlobalScope('id')->where('id', '7')->get()->first()
        ]);
    }
    public function termsPage()
    {
        return view('frontend.terms')->with([
            'post' => Post::withoutGlobalScope('id')->where('id', '6')->get()->first()
        ]);
    }
    public function supportPage()
    {
        return view('frontend.support')->with([
            'post' => Post::withoutGlobalScope('id')->where('id', '9')->get()->first()
        ]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.post.create')->with([
            'categories' => Category::all(),
            'tags' => Tag::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post;
        foreach (Config::get('app.languages') as $language) {
            $post->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $post->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
            $post->translateOrNew($language['code'])->excerpt = $request['excerpt_' . $language['code']];
            if ($request->hasFile('logo_' . $language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo_' . $language['code'])->getClientOriginalExtension();
                $request->file('logo_' . $language['code'])->move(public_path('uploads'), $new_name);
                $post->translateOrNew($language['code'])->logo = $new_name;
            }
        }
        

        if (!is_null($request->category)) {
            $categories = Category::find($request->category);
            $post->categories()->attach($categories);
        }
        if (!is_null($request->tag)) {
            $tags = Tag::find($request->tag);
            $post->tags()->attach($tags);
        }

        $post->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($post)
                ->transformWith(new PostTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Post added successfully')
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $title)
    {
        $post = Post::whereHas('translations', function ($query) use ($title) {
            $query->where('locale', config('app.site_language')->code)
            ->where('name', urldecode($title)); 
        })->first();

        if (!$post) {
            abort(404);
        }
        $post->increment('views');
        
        return view('frontend.post.show')->with([
            'recentPosts' => Post::take(6)->orderBy('id', 'desc')->get(),
            'post' => $post,
            'tags' => Tag::all(),
            'categories' => Category::all()
        ]);
    }

    public function showApi($id)
    {
        $post = Post::findOrFail($id);
        return fractal()
            ->item($post)
            ->transformWith(new PostTransformer)
            ->includeReviews()
            ->toArray()
            ;
        
    }
    public function reviews(Request $request, $id)
    {
        
        $post = Post::findOrFail($id);
        if ($request->expectsJson()) {

        }
        return view('backend.admin.post.reviews')->with([
            'post' => $post,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::withoutGlobalScope('id')->findOrFail($id);
        return view('backend.admin.post.edit')->with([
            'post' => $post,
            'categories' => Category::all(),
            'tags' => Tag::all()
        ]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::withoutGlobalScope('id')->findOrFail($id);
        foreach (Config::get('app.languages') as $language) {
            $post->translateOrNew($language['code'])->name = $request['name_' . $language['code']];
            $post->translateOrNew($language['code'])->description = $request['description_' . $language['code']];
            $post->translateOrNew($language['code'])->excerpt = $request['excerpt_' . $language['code']];
            if ($request->hasFile('logo_' . $language['code'])) {
                $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo_' . $language['code'])->getClientOriginalExtension();
                $request->file('logo_' . $language['code'])->move(public_path('uploads'), $new_name);
                File::delete(public_path('/uploads/' . $post->translate($language['code'])->logo));
                $post->translateOrNew($language['code'])->logo = $new_name;
            }
        }
        
        $categories = Category::find($request->category);
        $post->categories()->sync($categories);
        
        $tags = Tag::find($request->tag);
        $post->tags()->sync($tags);
        $post->save();

        if ($request->expectsJson()) {
            return fractal()
                ->item($post)
                ->transformWith(new PostTransformer)
                ->toArray()
                ;
        }
        return redirect()->back()->with([
            'success' => __('admin.Post edited successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Post deleted successfully')
        ]);
    }

    
    public function forcedelete(Request $request, $id)
    {
        $post = Post::withTrashed()->where('id', $id);
        $postItem = $post->get();
        File::delete(public_path('/uploads/' . $postItem->first()->logo));
        $post->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Post deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $post = Post::withTrashed()->where('id', $id);
        $post->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Post restored successfully')
        ]);
    }
}
