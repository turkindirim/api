<?php

namespace App\Http\Controllers;
use App\Http\Requests\CardStoreRequest;
use App\Http\Requests\CardUpdateRequest;
use App\Http\Requests\SetDistributorRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Services\Cards\CardService;
use App\Card;
use App\CardType;
use App\Distributor;
use App\LogCard;
use App\Transformers\CardTransformer;
use App\Transformers\CardHtmlTransformer;
use Auth;

class CardController extends Controller
{
    public $cardService;

    public function __construct(CardService $cardService)
    {
        $this->cardService = $cardService;
    }

    public function index(Request $request)
    {
        $cards = Card::paginate();
        if ($request->expectsJson()) {
            return fractal()
                ->collection($cards)
                ->transformWith(new CardTransformer)
                ->toArray();
        }
        return view('backend.admin.card.index')->with([
            'types' => CardType::all(),
            'cards' => $cards->items(),
            'total' => $cards->total(),
            'last_page' => $cards->lastPage(),
            'current_page' => $cards->currentPage(),
            'nextPageUrl' => $cards->nextPageUrl(),
            'previousPageUrl' => $cards->previousPageUrl(),
            'fields' => []
        ]);
    }
    
    public function create()
    {
        return view('backend.admin.card.create')->with([
            'types' => CardType::all(),
        ]);
    }

    public function store(CardStoreRequest $request)
    {
        $card_type = CardType::findOrFail($request->type);
        $count = (int)($request->count);
        for ($i = 0; $i < $count; $i++) {
            $this->cardService->createCard(
                $this->generateCardNumber($card_type->code),
                $request->type,
                $request->flag,
                $request->distributor,
                $request->user,
                $request->period
            );
        }
        if ($request->expectsJson()) {
            
        }
        return redirect()->back()->with([
            'success' => __('admin.Cards added successfully')
        ]);
    }

    public function show(Request $request, $id)
    {
        $card = Card::where('number', $id)->get();
        if ($card->count()) {
            if ($request->expectsJson()) {
                return fractal()
                ->item($card->first())
                ->transformWith(new CardTransformer)
                ->toArray(); 
            }
            
        } else {
            if ($request->expectsJson()) {
                return errorsResponse([ __('admin.Card Not Found') ], 404);
            }
        }
    }
    public function edit($id)
    {
        $card = Card::findOrFail($id);
        return view('backend.admin.card.edit')->with([
            'types' => CardType::all(),
            'card' => $card
        ]);
    }

    public function update(CardUpdateRequest $request, $id)
    {
        $card = Card::findOrFail($id);
        $card->period = $request->period ?: $card->period;
        $card->flag = $request->flag ?: $card->flag;
        $card->distributor_id = $request->distributor;
        $card->type_id = $request->type ?: $card->type_id;
        $card->save();
        if ($request->expectsJson()) {
            
        }
        return redirect()->back()->with([
            'success' => __('admin.Card edited successfully')
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $card = Card::findOrFail($id);
        $card->delete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Card deleted successfully')
        ]);
    }

    public function search(Request $request)
    {
        // Query
        $cards = Card::where('id', '>', '0');
        if ($request->search['value']) {
            $cards = $cards->where(function($query) use ($request) {
                $query->orWhereHas('user', function ($q) use ($request) {
                    $q->where('name', 'LIKE', '%'.urldecode($request->search['value']).'%');
                });
                $query->orWhereHas('type', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.urldecode($request->search['value']).'%');
                    });
                });
                $query->orWhereHas('distributor', function ($q) use ($request) {
                    $q->whereHas('translations', function ($a) use ($request) {
                        $a->where('name', 'LIKE', '%'.urldecode($request->search['value']).'%');
                    });
                });

                $query->orWhere('number', 'like', '%'.urldecode($request->search['value']).'%');
                $query->orWhere('cartoon_code', 'like', '%'.urldecode($request->search['value']).'%');
                $query->orWhere('serial_code', 'like', '%'.urldecode($request->search['value']).'%');
                $query->orWhere('flag', 'like', '%'.urldecode($request->search['value']).'%');
            });
        }

        if ($request->active_status === '1') {
            $cards = $cards->whereNotNull('activated_at');
        } elseif ($request->active_status === '0') {
            $cards = $cards->whereNull('activated_at');
        }        
        if ($request->used_status === '1') {
            $cards = $cards->whereNotNull('used_at');
        } elseif ($request->used_status === '0') {
            $cards = $cards->whereNull('used_at');
        }
        if ($request->sold_status === '1') {
            $cards = $cards->whereNotNull('sold_at');
        } elseif ($request->sold_status === '0') {
            $cards = $cards->whereNull('sold_at');
        }
        if ($request->expired_status === '1') {
            $cards = $cards->where('expired_at', '<', date('Y-m-d'));
        } elseif ($request->expired_status === '0') {
            $cards = $cards->where('expired_at', '>=', date('Y-m-d'));
        }
        
            
        // Limit
        $beforeLimitingCount = $cards->get()->count();
        if (strlen($request->start) && ($request->length > 0)) {
            $cards = $cards->offset($request->start)->limit($request->length);
        }

        // Get
        $cards = $cards->get();


        // Sort
        switch ($request->order[0]['column']) {
            case '1':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('id') : $cards->sortByDesc('id');
                break;
            case '2':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('number') : $cards->sortByDesc('number');
                break;
            case '4':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('flag') : $cards->sortByDesc('flag');
                break;
            case '5':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('type') : $cards->sortByDesc('type');
                break;            
            case '6':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('distributor') : $cards->sortByDesc('distributor');
                break;
            case '7':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('user') : $cards->sortByDesc('user');
                break;
            case '8':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('created_at') : $cards->sortByDesc('created_at');
                break;
            case '9':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('activated_at') : $cards->sortByDesc('activated_at');
                break;
            case '10':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('sold_at') : $cards->sortByDesc('sold_at');
                break;
            case '11':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('used_at') : $cards->sortByDesc('used_at');
                break;
            case '12':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('period') : $cards->sortByDesc('period');
                break;
            case '13':
                $cards = ($request->order[0]['dir'] == 'asc') ? $cards->sortBy('expired_at') : $cards->sortByDesc('expired_at');
                break;
        }

        // Generate JSON
        return \array_merge([
            'draw' => $request->draw + 1,
            'recordsFiltered' => $beforeLimitingCount,
            'recordsTotal' => Card::all()->count()
        ], fractal()
            ->collection($cards)
            ->transformWith(new CardHtmlTransformer)
            ->toArray()
        );
    }

    protected function generateCardNumber($code)
    {
        $new_number = date('Y') . rand(1000, 9999) . $code . date('m') . rand(1000, 9999) . '';
        $exists = Card::where('number', $new_number)->get();
        if ($exists->count()) {
            return $this->generateCardNumber($code);
        }
        return $new_number;
    }
    public function bulk(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cards' => 'required|min:1',
        ]);
        if ($validator->fails()) {
            if ($request->expectsJson()) {
                return errorsResponse([$validator->messages()->first()]);
            }
            return redirect()->back()->with([
                'errors' => $validator->messages()->first()
            ]);
        }
        $cardIDs = \explode(',', $request->cards);
        foreach ($cardIDs as $key => $value) {
            $card = Card::find($value);
            if ($request->period) {
                if ($card->used_at) {
                    $card->expired_at = $card['used_at']->addDays($request->period)->format('Y-m-d H:i:s');
                }
                $card->period = $request->period ?: $card->period;
            }
            
            $card->flag = $request->flag ?: $card->flag;
            $card->distributor_id = $request->distributor ?: $card->distributor_id;
            $card->type_id = $request->type ?: $card->type_id;
            $card->save();

            LogCard::create([
                'card_id' => $card->id,
                'userable_id' => Auth::user()->id,
                'userable_type' => \get_class(Auth::user()),
                'action' => 'Bulk Update',
                'description' => \json_encode($card->getDirty())
            ]);

        }
        if ($request->expectsJson()) {
            
        }
        return redirect()->back()->with([
            'success' => __('admin.Card edited successfully')
        ]);
    }

    public function barcode(Request $request)
    {
        return view('backend.admin.card.barcode');
    }
    public function setDistributor(SetDistributorRequest $request)
    {
        foreach ($request->cards as $key => $value) {
            
            $card = Card::find($value);
            $card->distributor_id = $request->distributor ?: $card->distributor_id;
            $card->save();

            LogCard::create([
                'card_id' => $card->id,
                'userable_id' => Auth::user()->id,
                'userable_type' => \get_class(Auth::user()),
                'action' => 'Bulk Update',
                'description' => \json_encode($card->getDirty())
            ]);

        }
        if ($request->expectsJson()) {
            
        }
        return redirect()->back()->with([
            'success' => __('admin.Card edited successfully') . ' (' . count($request->cards) . ')'
        ]);
    }
    public function log(Request $request, $id)
    {
        $card = Card::find($id);
        $log = LogCard::where('card_id', $id)->orderBy('id', 'asc')->get();
        return view('backend.admin.card.log')->with([
            'log' => $log,
            'card' => $card
        ]);
    }

    
    public function forcedelete(Request $request, $id)
    {
        $card = Card::withTrashed()->where('id', $id);
        $card->forceDelete();
        if ($request->expectsJson()) {
            return deleteItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Card deleted successfully')
        ]);
    }
    public function restore(Request $request, $id)
    {
        $card = Card::withTrashed()->where('id', $id);
        $card->restore();
        if ($request->expectsJson()) {
            return restoreItemResponse();
        }
        return redirect()->back()->with([
            'success' => __('admin.Card restored successfully')
        ]);
    }
}
