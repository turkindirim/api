<?php

namespace App\Listeners\Users;

use App\Events\Users\UserActivatedCard;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\LogCard;
use App\LogUser;
use Auth;

class LogCardActivation
{
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  UserActivatedCard  $event
     * @return void
     */
    public function handle(UserActivatedCard $event)
    {
        LogUser::create([
            'user_id' => $event->user,
            'model' => 'Card',
            'action' => 'Activate',
            'item' => $event->card,
            'description' => ''
        ]);
        LogCard::create([
            'card_id' => $event->card,
            'userable_id' => Auth::user()->id,
            'userable_type' => \get_class(Auth::user()),
            'action' => 'Activate',
            'description' => ''
        ]);
    }
}
