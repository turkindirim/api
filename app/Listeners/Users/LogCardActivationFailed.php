<?php

namespace App\Listeners\Users;

use App\Events\Users\CardActivationFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\LogCard;
use Auth;

class LogCardActivationFailed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CardActivationFailed  $event
     * @return void
     */
    public function handle(CardActivationFailed $event)
    {
        LogCard::create([
            'card_id' => $event->card,
            'userable_id' => Auth::user()->id,
            'userable_type' => \get_class(Auth::user()),
            'action' => 'Activation Failed',
            'description' => $event->reason
        ]);
    }
}
