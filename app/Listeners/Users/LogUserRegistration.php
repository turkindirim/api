<?php

namespace App\Listeners\Users;

use App\Events\Users\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\LogUser;

class LogUserRegistration
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        LogUser::create([
            'user_id' => $event->user,
            'model' => 'User',
            'action' => 'Register',
            'item' => $event->user,
            'description' => ''
        ]);
    }
}
