<?php

namespace App\Listeners\Branches;

use App\Events\Branches\BranchCanceledTransaction;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\LogBranch;

class LogTransactionCancellation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BranchCanceledTransaction  $event
     * @return void
     */
    public function handle(BranchCanceledTransaction $event)
    {
        LogBranch::create([
            'branch_id' => $event->branch,
            'model' => 'Transaction',
            'action' => 'Cancel',
            'item' => $event->transaction,
            'description' => ''
        ]);
    }
}
