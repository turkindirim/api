<?php

namespace App\Listeners\Branches;

use App\Events\Branches\BranchMadeTransaction;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\LogBranch;
use App\LogCard;
use Auth;

class LogTransaction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BranchMadeTransaction  $event
     * @return void
     */
    public function handle(BranchMadeTransaction $event)
    {
        LogBranch::create([
            'branch_id' => Auth::user()->id,
            'model' => 'Transaction',
            'action' => 'Create',
            'item' => $event->transaction,
            'description' => \json_encode(['card ID' => $event->card])
        ]);
        LogCard::create([
            'card_id' => $event->card,
            'userable_id' => Auth::user()->id,
            'userable_type' => \get_class(Auth::user()),
            'action' => 'Transaction',
            'description' => \json_encode(['Transaction ID' => $event->transaction])
        ]);
    }
}
