<?php

namespace App\Listeners\Distributors;

use App\Events\Distributors\DistributorSoldCard;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\LogCard;
use App\LogDistributor;
use Auth;

class LogCardSelling
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DistributorSoldCard  $event
     * @return void
     */
    public function handle(DistributorSoldCard $event)
    {
        LogDistributor::create([
            'distributor_id' => $event->distributor,
            'model' => 'Card',
            'action' => 'Sell',
            'item' => $event->card,
            'description' => ''
        ]);

        LogCard::create([
            'card_id' => $event->card,
            'userable_id' => Auth::user()->id,
            'userable_type' => \get_class(Auth::user()),
            'action' => 'Selling',
            'description' => ''
        ]);
    }
}
