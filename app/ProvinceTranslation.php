<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvinceTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
}
