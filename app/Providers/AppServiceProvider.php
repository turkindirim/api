<?php

namespace App\Providers;

use App\Observers\BranchObserver;
use App\Observers\CardObserver;
use App\Observers\CategoryObserver;
use App\Observers\CityObserver;
use App\Observers\CountryObserver;
use App\Observers\DistributorObserver;
use App\Observers\DistrictObserver;
use App\Observers\LanguageObserver;
use App\Observers\NewsfeedObserver;
use App\Observers\OfferObserver;
use App\Observers\ProvinceObserver;
use App\Observers\UserObserver;
use App\Observers\SellerObserver;
use App\Branch;
use App\Card;
use App\Category;
use App\City;
use App\Country;
use App\Distributor;
use App\District;
use App\Language;
use App\Newsfeed;
use App\Offer;
use App\Post;
use App\Province;
use App\Seller;
use App\User;
use View;

use Illuminate\Support\ServiceProvider;
use Config;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Branch::observe(BranchObserver::class);
        Card::observe(CardObserver::class);
        Category::observe(CategoryObserver::class);
        City::observe(CityObserver::class);
        Country::observe(CountryObserver::class);
        Distributor::observe(DistributorObserver::class);
        District::observe(DistrictObserver::class);
        Language::observe(LanguageObserver::class);
        Newsfeed::observe(NewsfeedObserver::class);
        Offer::observe(OfferObserver::class);
        Province::observe(ProvinceObserver::class);
        Seller::observe(SellerObserver::class);
        User::observe(UserObserver::class);

        Config::set('app.languages', Language::all()->toArray());
        
        View::share('global', [
            'categories' => Category::all(),
            'cities' => City::all(),
            'provinces' => Province::all(),
            'terms_page' => Post::withoutGlobalScope('id')->find(6),
            'languages' => Language::where('active', '=', '1')->get()
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
