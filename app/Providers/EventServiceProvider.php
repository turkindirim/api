<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\Branches\BranchCanceledTransaction' => [
            'App\Listeners\Branches\LogTransactionCancellation'
        ],
        'App\Events\Branches\BranchMadeTransaction' => [
            'App\Listeners\Branches\LogTransaction'
        ],
        'App\Events\Distributors\DistributorSoldCard' => [
            'App\Listeners\Distributors\LogCardSelling'
        ],
        'App\Events\Users\UserActivatedCard' => [
            'App\Listeners\Users\LogCardActivation'
        ],
        'App\Events\Users\CardActivationFailed' => [
            'App\Listeners\Users\LogCardActivationFailed'
        ],
        'App\Events\Users\UserRegistered' => [
            'App\Listeners\Users\LogUserRegistration'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
