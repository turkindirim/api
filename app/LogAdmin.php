<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAdmin extends Model
{
    protected $fillable = ['admin_id', 'model', 'action', 'item', 'description'];

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
}
