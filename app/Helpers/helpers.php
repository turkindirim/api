<?php

function deleteItemResponse()
{
    return response()->json([
        'data' => null,
        'meta' => [
            'error' => false,
            'messages' => [__('admin.Item Deleted Successfully')]
        ]
    ], 200);
}

function restoreItemResponse()
{
    return response()->json([
        'data' => null,
        'meta' => [
            'error' => false,
            'messages' => [__('admin.Item Restored Successfully')]
        ]
    ], 200);
}

function errorsResponse($errors = [], $error_code = 422)
{
    return response()->json([
        'data' => null,
        'meta' => [
            'error' => true,
            'messages' => $errors
        ]
    ], $error_code);
}

function parsePhoneNumber($phone)
{
    $phone = str_replace(' ', '', $phone);
    $phone = str_replace('(', '', $phone);
    $phone = str_replace(')', '', $phone);
    $length = strlen($phone);
    if ($length == 10) {
        $phone = '+90' . $phone;
    } elseif ($length == 11) {
        $phone = '+9' . $phone;
    } elseif ($length == 14 && (strpos($phone, '0090') == 0)) {
        $phone = preg_replace('/'.preg_quote('0090', '/').'/', '+90', $phone, 1);
    } elseif ($length == 13 && (strpos($phone, '+90') == 0)) {
        $phone = $phone;
    } else {
        return $phone;
    }
    return substr($phone, 0, 3) . ' (' .  substr($phone, 3, 3) . ') ' . substr($phone, 6, 3) . ' ' . substr($phone, 9, 2) . ' ' . substr($phone, 11, 2);
}
