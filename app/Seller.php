<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;

class Seller extends Authenticatable
{
    use Notifiable, HasMultiAuthApiTokens, Translatable, SoftDeletes;

    protected $table = 'sellers';

    protected $perPage = 100;
    
    public $translatedAttributes = ['name'];
    protected $fillable = ['logo', 'website', 'facebook', 'twitter', 'instagram', 'youtube', 'featured'];
    protected $hidden = ['password', 'remember_token', 'translations'];
    // protected $with = ['translations'];

    public function branches()
    {
        return $this->hasMany('App\Branch', 'seller_id', 'id');
    }
}
