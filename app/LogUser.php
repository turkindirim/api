<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogUser extends Model
{
    protected $fillable = ['user_id', 'model', 'action', 'item', 'description'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
