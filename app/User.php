<?php

namespace App;

use Illuminate\Notifications\Notifiable;

// use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract; // this is an interface
// use Illuminate\Auth\MustVerifyEmail; // this the trait

use Illuminate\Foundation\Auth\User as Authenticatable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable // implements MustVerifyEmailContract
{
    use HasMultiAuthApiTokens, Notifiable, SoftDeletes;  // , MustVerifyEmail
    protected $table = 'users';
    public $perPage = 100;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'name', 'logo', 'email', 'password', 'nationality', 'passport', 'passport_number', 'mobile', 'country_id', 'province_id', 'address1', 'address2','default_language','dob','gender','email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array 
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function cards()
    {
        return $this->hasMany('App\Card');
    }
    public function getNationality()
    {
        return $this->belongsTo('App\Country', 'nationality', 'id');
    }
    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }
    public function province()
    {
        return $this->belongsTo('App\Province');
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function district()
    {
        return $this->belongsTo('App\District');
    }
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_user', 'user_id', 'category_id');
    }
    public function logUser()
    {
        return $this->hasMany('App\LogUser');
    }
    public function cardLogs()
    {
        return $this->morphMany('App\LogCard', 'userable');
    }
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }
    public function reviews()
    {
        return $this->hasMany('App\Review');
    }
}
