<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;


class Branch extends Authenticatable
{
    use Notifiable, HasMultiAuthApiTokens, Translatable, SoftDeletes;

    protected $table = 'branches';
    protected $perPage = 100;
    public $translatedAttributes = ['name', 'description'];
    protected $fillable = ['email', 'password', 'phone', 'mobile', 'seller_id', 'province_id', 'city_id', 'district_id', 'address1', 'address2', 'longitude', 'latitude',];
    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['sellername', 'logo'];

    public function getSellernameAttribute()
    {
        return $this->seller->name;  
    }
    public function getLogoAttribute()
    {
        return $this->seller->logo;  
    }
    public function seller()
    {
        return $this->belongsTo('App\Seller');
    }
    public function province()
    {
        return $this->belongsTo('App\Province');
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function district()
    {
        return $this->belongsTo('App\District');
    }
    public function offers()
    {
        return $this->belongsToMany('App\Offer', 'branch_offer', 'branch_id', 'offer_id');
    }
    public function logBranch()
    {
        return $this->hasMany('App\LogBranch');
    }
    public function cardLogs()
    {
        return $this->morphMany('App\LogCard', 'userable');
    }
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function my_offers()
    {
        return $this->hasMany('App\Branch', 'by_branch', 'id');
    }
    public function reviews()
    {
        return $this->morphMany('App\Review', 'reviewable');
    }
    public function avgRate() {
        $x = $this->reviews()->avg('rate');
        return round($x, 1);
    }
}
