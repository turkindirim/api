<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    protected $perPage = 50;
    protected $fillable = ['branch_id', 'card_id', 'user_id'];

    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }
    public function card()
    {
        return $this->belongsTo('App\Card');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
