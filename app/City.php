<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;

class City extends Model
{
    use Translatable, SoftDeletes;

    public $translatedAttributes = ['name'];
    
    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'translations'];
    
    public function province()
    {
        return $this->belongsTo('App\Province');
    }
    public function districts()
    {
        return $this->hasMany('App\District');
    }
    public function branch()
    {
        return $this->hasMany('App\Branch');
    }
    public function distributor()
    {
        return $this->hasMany('App\Distributor');
    }
}
