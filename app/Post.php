<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;

class Post extends Model
{
    use SoftDeletes, Translatable;
    public $perPage = 12;
    protected $fillable = ['photo', 'user_id', 'views'];
    public $translatedAttributes = ['name', 'description', 'logo', 'excerpt'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('id', function ($builder) {
            $builder->whereNotIn('id', [1, 6, 7, 9]);
        });
    }
    
    
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_post', 'post_id', 'category_id');
    }
    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'post_tag', 'post_id', 'tag_id');
    }
    public function reviews()
    {
        return $this->morphMany('App\Review', 'reviewable');
    }
    
    public function avgRate() {
        $x = $this->reviews()->avg('rate');
        return round($x, 1);
    }
}
