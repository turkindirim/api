<?php

namespace App\Services\Auth;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserService
{
    public function register(UserStoreRequest $request)
    {
        $user_parameters = $request->all();
        $user_parameters['password'] = Hash::make($request->password);
        $user_parameters['name'] = $request->firstname . ' ' . $request->lastname;
        unset($user_parameters['password_confirmation']);
        $user_parameters['logo'] = null;
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            $user_parameters['logo'] = $new_name;
        }
        $user = new User;
        $user->fill($user_parameters);
        $user->save();
        return $user;
    }
    public function updateUser(UserUpdateRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->name = $request->firstname . ' ' . $request->lastname;
        if ($request->hasFile('logo')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('uploads'), $new_name);
            File::delete(public_path('/uploads/' . $user->logo));
            $user->logo = $new_name;
        }
        if ($request->hasFile('passport')) {
            $new_name = date('Y-m-d-H-i-s-') . rand(1000, 9999) . '.' . $request->file('passport')->getClientOriginalExtension();
            $request->file('passport')->move(public_path('uploads'), $new_name);
            File::delete(public_path('/uploads/' . $user->passport));
            $user->passport = $new_name;
        }
        $user->save();
        return $user;
    }
}