<?php

namespace App\Services\Cards;
use App\Card;

class CardService
{
    public function createCard($number, $type, $flag, $distributor, $user, $period)
    {
        $card = new Card;
        $card->number = $number;
        $card->type_id = $type;
        $card->flag = $flag;
        $card->distributor_id = $distributor;
        $card->user_id = $user;
        $card->activation_code = rand(10000, 99999);
        $card->period = $period;
        $card->save();
        return $card;
    }
}