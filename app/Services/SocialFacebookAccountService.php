<?php

namespace App\Services;
use App\SocialFacebookAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialFacebookAccountService
{
    public function createOrGetUser(ProviderUser $providerUser, $provider)
    {
        $account = SocialFacebookAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new SocialFacebookAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $fullName = $providerUser->getName();
                $names = explode(' ', $fullName, 2);
                $user = User::create([
                    'firstname' => $names[0],
                    'lastname' => $names[1],
                    'email' => $providerUser->getEmail(),
                    'name' => $fullName,
                    'password' => bcrypt(rand(1,10000)),
                    'email_verified_at' => date('Y-m-d H:i:s'),
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
}