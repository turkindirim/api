<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Post;

class PostTransformer extends TransformerAbstract
{

    public $availableIncludes = ['reviews'];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Post $post)
    {
        return [
            'id' => $post->id,
            'name' => (string) $post->name,
            'description' => (string) $post->description,
            'logo' => (string) $post->logo ? url('uploads/' . $post->logo) : '',
            'reviews_count' => count($post->reviews),
            'av_rate' => $post->avgRate(),
            'reviews' => (object) []
        ];
    }
    
    public function includeReviews(Post $post)
    {
        return $this->collection($post->reviews, new ReviewTransformer);
    }

}
