<?php

namespace App\Transformers;

use App\District;

class DistrictTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(District $district)
    {
        return [
            'id' => $district->id,
            'name' => (string) $district->name
        ];
    }
}