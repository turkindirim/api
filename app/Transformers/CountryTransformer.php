<?php

namespace App\Transformers;
use App\Country;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Country $country)
    {
        return [
            'id' => $country->id,
            'dial_code' => $country->dial_code,
            'name' => (string) $country->name
        ];
    }
}
