<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Slide;

class SlideTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Slide $slide)
    {
        return [
            'id' => $slide->id,
            'name' => (string) $slide->name,
            'description' => (string) $slide->description,
            'logo' => (string) $slide->logo ? url('uploads/' . $slide->logo) : '',
            'thumb' => (string) $slide->thumb ? url('uploads/' . $slide->thumb) : '',
        ];
    }
}
