<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Language;

class LanguageTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Language $language)
    {
        return [
            'id' => $language->id,
            'name' => (string) $language->name,
            'code' => (string) $language->code,
            'direction' => (string) $language->direction
        ];
    }
}
