<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Newsfeed;
use App\Offer;
use App\Transformers\OfferTransformer;

class NewsfeedTransformer extends TransformerAbstract
{

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Newsfeed $newsfeed)
    {
        return [
            'id' => $newsfeed->id,
            'name' => (string) $newsfeed->name,
            'description' => (string) $newsfeed->description,
            'logo' => (string) $newsfeed->logo ? url('uploads/' . $newsfeed->logo) : '',
            'offer' => $newsfeed->offer_id ? [
                'id' => $newsfeed->offer->id
            ] : null
        ];
    }

}
