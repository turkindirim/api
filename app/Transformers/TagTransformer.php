<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Tag;

class TagTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Tag $tag)
    {
        return [
            'id' => $tag->id,
            'name' => (string) $tag->name
        ];
    }
}
