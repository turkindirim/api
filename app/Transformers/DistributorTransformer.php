<?php

namespace App\Transformers;

use App\Distributor;

class DistributorTransformer extends \League\Fractal\TransformerAbstract
{

    protected $defaultIncludes = ['province', 'city', 'district'];
    protected $availableIncludes = ['cards'];

    public function transform(Distributor $distributor)
    {
        return [
            'id' => $distributor->id,
            'name' => (string) $distributor->name,
            'email' => (string) $distributor->email,
            'logo' => (string) $distributor->logo ? url('uploads/' . $distributor->logo) : '',
            'website' => (string) $distributor->website,
            'phone' => (string) $distributor->phone,
            'mobile' => (string) $distributor->mobile,
            'address1' => (string) $distributor->address1,
            'address2' => (string) $distributor->address2,
            'longitude' => (float) $distributor->longitude,
            'latitude' => (float) $distributor->latitude,
            'created_at' => $distributor->created_at->toDateTimeString(),
            'created_at_human' => $distributor->created_at->diffForHumans()
        ];
    }


    public function includeCity(Distributor $distributor)
    {
        return $this->item($distributor->city, new CityTransformer);
    }
    public function includeDistrict(Distributor $distributor)
    {
        return $this->item($distributor->district, new DistrictTransformer);
    }
    public function includeProvince(Distributor $distributor)
    {
        return $this->item($distributor->province, new ProvinceTransformer);
    }
    public function includeCards(Distributor $distributor)
    {
        return $this->collection($distributor->cards, new CardTransformer);
    }

}