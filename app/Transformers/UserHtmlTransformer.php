<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;
use App\Country;

class UserHtmlTransformer extends TransformerAbstract
{


    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {

        return [
            'id' => $user->id,
            'name' => $user->name,
            'logo' => $user->logo ? '<img class="mini-logo" src="'.url('uploads/' . $user->logo).'">' : '',
            'email' => $user->email,
            'mobile' => $user->mobile,
            'cards_count' => $user->cards->count(),
            'nationality' => $user->getNationality,
            'gender' => $user->gender,
            'country' => $user->country,
            'address' => $user->country['name'] .', '.$user->province['name'] .', '. $user->address1 .','. $user->address2,
            'passport' => $user->passport ? '<a target="_blank" href="'.url('uploads/'.$user->passport).'">'.__('admin.View').'</a>' : '',
            'options' => '<button class="md-btn md-btn-danger" type="button" onclick="UIkit.modal.confirm(\''.__('admin.Delete User').'\', function(){ window.location.href=\''.route('users.destroy', ['id' => $user->id]).'\'; });">
            <i class="uk-icon-close"></i>
        </button>
        <a href="'.route('users.edit', ['id' => $user->id]).'" class="md-btn md-btn-primary"><i class="uk-icon-edit"></i></a>',
            'created_at' => $user->created_at->format('Y-m-d'),
            'created_at_human' => $user->created_at->diffForHumans()
        ];
    }
}
