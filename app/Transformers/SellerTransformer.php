<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Seller;

class SellerTransformer extends TransformerAbstract
{

    protected $availableIncludes = ['branches'];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Seller $seller)
    {
        return [
            'id' => $seller->id,
            'name' => (string) $seller->name,
            'website' => (string) $seller->website,
            'facebook' => (string) $seller->facebook,
            'twitter' => (string) $seller->twitter,
            'logo' => (string) ($seller->logo ? url('uploads/' . $seller->logo) : ''),
            'branches' => (object)[]
        ];
    }
    public function includeBranches(Seller $seller)
    {
        return $this->collection($seller->branches, new BranchTransformer);
    }
}
