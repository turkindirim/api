<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Branch;

class BranchTransformer extends TransformerAbstract
{

    protected $availableIncludes = ['reviews'];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Branch $branch)
    {
        return [
            'id' => $branch->id,
            'name' => (string) $branch->name,
            'prefix' => (string) $branch->seller->name,
            'seller_name' => (string) $branch->seller->name,
            'email' =>(string)  $branch->email,
            'logo' => (string) $branch->seller->logo ? url('uploads/' . $branch->seller->logo) : '',
            'website' => (string) $branch->seller->website,
            'phone' => (string) $branch->phone,
            'mobile' => (string) $branch->mobile,
            'province' => (object) [
                'id' => $branch->province ? $branch->province->id : -1,
                'name' => $branch->province ? $branch->province->name : '',
            ],
            'city' => (object) [
                'id' => $branch->city ? $branch->city->id : -1,
                'name' => $branch->city ? $branch->city->name : '',
            ],
            'district' => (object) [
                'id' => $branch->district ? $branch->district->id: -1,
                'name' => $branch->district ? $branch->district->name : '',
            ],
            'address1' => (string) $branch->address1,
            'address2' => (string) $branch->address2,
            'longitude' => (float) $branch->longitude,
            'latitude' => (float) $branch->latitude,
            'created_at' => $branch->created_at->toDateTimeString(),
            'created_at_human' => $branch->created_at->diffForHumans(),
            'av_rate' => $branch->avgRate(),
            'reviews_count' => count($branch->reviews),
            'reviews' => (object) []
        ];
    }

    public function includeReviews(Branch $branch)
    {
        return $this->collection($branch->reviews, new ReviewTransformer);
    }
}
