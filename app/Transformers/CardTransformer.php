<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Transformers\DistributorTransformer;
use App\Card;

class CardTransformer extends TransformerAbstract
{

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Card $card)
    {
        return [
            'id' => $card->id,
            'number' => (string) $card->number,
            'flag' => (string) $card->flag,
            'period' => (int) $card->period,
            'distributor' => $card->distributor_id ? [
                'id' => $card->distributor['id'],
                'name' => (string) $card->distributor['name'],
                'email' => (string) $card->distributor['email']
            ] : (object) array(),
            'user' => $card->user_id ? [
                'id' => $card->user['id'],
                'name' => $card->user['name'],
                'email' => $card->user['email']
            ] : (object) array(),
            'type' => [
                'id' => $card->type_id,
                'name' => $card->type->name
            ],
            'active' => $card->activated_at ? true : false,
            'logo' => $card->type->logo ? url('uploads/' . $card->type->logo) : '',
            'activated_at' => $card->activated_at ? ['date' => $card->activated_at->format('Y-m-d'), 'time' => $card->activated_at->format('H:i:s')] : null,
            'used' => $card->used_at ? true : false,
            'used_at' => $card->used_at ? ['date' => $card->used_at->format('Y-m-d'), 'time' => $card->used_at->format('H:i:s')] : null,
            'sold' => $card->sold_at ? true : false,
            'sold_at' => $card->sold_at ?  ['date' => $card->sold_at->format('Y-m-d'), 'time' => $card->sold_at->format('H:i:s')] : null,
            'valid' => (date('Y-m-d') > $card->expired_at) ? false : true,
            'valid_until' => $card->expired_at ?  ['date' => $card->expired_at->format('Y-m-d'), 'time' => $card->expired_at->format('H:i:s')] : null,
            'created_at' => ['date' => $card->created_at->format('Y-m-d'), 'time' => $card->created_at->format('H:i:s')],
            'created_at_human' => $card->created_at->diffForHumans()
        ];
    }
}
