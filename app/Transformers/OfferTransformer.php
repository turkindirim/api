<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Offer;

class OfferTransformer extends TransformerAbstract
{
    
    protected $defaultIncludes = ['media', 'categories', 'tags'];
    protected $availableIncludes = ['branches', 'reviews', 'brief'];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Offer $offer)
    {
        return [
            'id' => $offer->id,
            'valid_from' => (string) $offer->valid_from,
            'valid_to' => (string) $offer->valid_to,
            'name' => (string) $offer->name,
            'views' => $offer->views,
            'description' => (string) $offer->description,
            'pending' => (boolean) $offer->pending,
            'active' => (boolean) $offer->active,
            'thumb' => (string) $offer->thumb ? url('uploads/' . $offer->thumb) : '',
            'reviews_count' => count($offer->reviews),
            'av_rate' => $offer->avgRate(),
            'reviews' => (object) []
        ];
    }
    
    public function includeBranches(Offer $offer)
    {
        return $this->collection($offer->branches, new BranchTransformer);
    }
    public function includeBrief(Offer $offer)
    {
        return $this->collection($offer->branches, new BriefBranchTransformer);
    }
    public function includeCategories(Offer $offer)
    {
        return $this->collection($offer->categories, new CategoryTransformer);
    }
    public function includeTags(Offer $offer)
    {
        return $this->collection($offer->tags, new TagTransformer);
    }
    public function includeMedia(Offer $offer)
    {
        return $this->collection($offer->media, new MediaTransformer);
    }
    public function includeReviews(Offer $offer)
    {
        return $this->collection($offer->reviews, new ReviewTransformer);
    }
}
