<?php

namespace App\Transformers;
use App\Province;

class ProvinceTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(Province $province)
    {
        return [
            'id' => $province->id,
            'name' => (string) $province->name
        ];
    }
}