<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\CardType;

class TypeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CardType $type)
    {
        return [
            'id' => $type->id,
            'name' => (string) $type->name,
            'description' => (string) $type->description,
            'logo' => (string) $type->logo ? url('uploads/' . $type->logo) : ''
        ];
    }
}
