<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Transaction;

class TransactionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Transaction $transaction)
    {
        return [
            'id' => $transaction->id,
            'branch' => [
                'id' => $transaction->branch->id,
                'name' => (string) $transaction->branch->name,
                'email' => (string) $transaction->branch->email,
                'logo' => (string) $transaction->branch->logo ? url('uploads/' . $transaction->branch->logo) : ''
            ],
            'card' => [
                'id' => $transaction->card->id,
                'number' => (string) $transaction->card->number
            ],
            'user' => [
                'id' => $transaction->user->id,
                'name' => (string) $transaction->user->name,
                'logo' => (string) $transaction->user->logo ? url('uploads/' . $transaction->user->logo) : ''
            ],
            'created_at' => ['date' => $transaction->created_at->format('Y-m-d'), 'time' => $transaction->created_at->format('H:i:s')],
            'created_at_human' => $transaction->created_at->diffForHumans()
        ];
    }
}
