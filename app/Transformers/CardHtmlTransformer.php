<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Card;

class CardHtmlTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Card $card)
    {
        $user = $card->user_id ? 
        ['id' => $card->user['id'],'name' => $card->user['name'],'email' => $card->user['email']] : 
        ['id' => '','name' =>  '','email' =>  ''];
        
        $distributor = $card->distributor_id ? 
        ['id' => $card->distributor['id'],'name' => $card->distributor['name'],'email' => $card->distributor['email']] : 
        ['id' => '','name' =>  '','email' =>  ''];

        return [
            'id' => $card->id,
            'checkbox' => '<input data-id="'.$card->id.'" type="checkbox" data-md-icheck class="check_row">',
            'number' => $card->number,
            'activation_code' => $card->activation_code,
            'flag' => $card->flag,
            'period' => $card->period,
            'type' => $card->type['name'],
            'distributor' => $distributor,
            'user' => $user,
            'active' => $card->activated_at ? true : false,
            'used' => $card->used_at ? true : false,
            'sold' => $card->sold_at ? true : false,
            'activated_at' => $this->returnDate($card->activated_at),
            'used_at' => $this->returnDate($card->used_at),
            'sold_at' => $this->returnDate($card->sold_at),
            'valid' => (date('Y-m-d') > $card->expired_at) ? false : true,
            'valid_until' => $card->expired_at ? $card->expired_at->format('Y-m-d') : null,
            'created_at' => $card->created_at->format('Y-m-d'),
            'created_at_human' => $card->created_at->diffForHumans(),
            'valid_dates' => $card->expired_at ? ((date('Y-m-d') > $card->expired_at)) ? '<b style="color:#992222;">'. $card->expired_at->format('Y-m-d').'</b>' : $card->expired_at->format('Y-m-d') : '',
            'options' => '
            <div class="md-btn-group">
                <button onclick="UIkit.modal.confirm(\''.__('admin.Delete Item').'\', function(){ window.location.href=\''.route('cards.destroy', ['id' => $card->id]).'\'; });" class="md-btn md-btn-small md-btn-danger md-btn-wave waves-effect waves-button"><i class="uk-icon-close"></i></button>
                <a href="'.route('cards.edit', ['id' => $card->id]).'" class="md-btn md-btn-small md-btn-primary md-btn-wave waves-effect waves-button"><i class="uk-icon-edit"></i></a>
                <a href="'.route('cards.log', ['id' => $card->id]).'" class="md-btn md-btn-small md-btn-warning md-btn-wave waves-effect waves-button"><i class="uk-icon-history"></i></a>
            </div>',
        ];
    }

    public function returnDate($date)
    {
        return $date ? ['date' => $date->format('Y-m-d'), 'time' => $date->format('H:i:s')] : 
            ['date' => '', 'time' => ''];
    }
}
