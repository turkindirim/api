<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Review;

class ReviewTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Review $review)
    {
        return [
            'id' => $review->id,
            'rate' => $review->rate,
            'review' => $review->review,
            'user' => [
                'id' => $review->user->id,
                'name' => $review->user->name,
                'email' => $review->user->email
            ]
        ];
    }
}
