<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Transformers\CardTransformer;
use App\Transformers\CountryTransformer;
use App\Transformers\ProvinceTransformer;
use App\Country;
use App\Province;
use App\User;

class UserTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['nationality','country', 'province'];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => (string)($user->name),
            'logo' => (string) $user->logo ? url('uploads/' . $user->logo) : '',
            'email' => $user->email,
            'mobile' => $user->mobile,
            'nationality' => (object)($user->nationality),
            'country' => (object)($user->country),
            'province' => (object)($user->province),
            'address1' => (string)$user->address1,
            'address2' => (string)$user->address2,
            'passport' => (string)$user->passport ? url('uploads/'.$user->passport) : '',
            'created_at' => $user->created_at ? $user->created_at->format('Y-m-d') : '',
            'created_at_human' => $user->created_at ? $user->created_at->diffForHumans() : '',
            'cards' => []
        ];
    }
    
    public function includeCards(User $user)
    {
        if ($user->cards)
        return $this->collection($user->cards, new CardTransformer);
    }
    public function includeNationality(User $user)
    {
        if ($user->nationality)
            return $this->item(Country::find($user->nationality), new CountryTransformer);
    }
    public function includeCountry(User $user)
    {
        if ($user->country_id)
            return $this->item(Country::find($user->country_id), new CountryTransformer);
    }
    public function includeProvince(User $user)
    {
        if ($user->province_id)
            return $this->item(Province::find($user->province_id), new ProvinceTransformer);
    }
}
