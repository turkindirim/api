<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Admin;
class AdminTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Admin $admin)
    {
        return [
            'id' => $admin->id,
            'name' => (string) $admin->name,
            'email' => (string) $admin->email,
            'logo' => (string) $admin->logo ? url('uploads/' . $admin->logo) : '',
            'created_at' => $admin->created_at,
        ];
    }
}
