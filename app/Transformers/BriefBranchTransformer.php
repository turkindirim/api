<?php

namespace App\Transformers;
use League\Fractal\TransformerAbstract;
use App\Branch;

class BriefBranchTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Branch $branch)
    {
        return [
            'id' => $branch->id,
            'name' => (string) $branch->name,
            'prefix' => (string) $branch->seller->name,
            'seller_name' => (string) $branch->seller->name,
            'email' =>(string)  $branch->email,
            'logo' => (string) $branch->seller->logo ? url('uploads/' . $branch->seller->logo) : '',
            'phone' => (string) $branch->phone,
            'mobile' => (string) $branch->mobile,
            'website' => (string) $branch->seller->website,
            'address1' => (string) $branch->address1,
            'longitude' => (float) $branch->longitude,
            'latitude' => (float) $branch->latitude,
            'av_rate' => $branch->avgRate(),
            'reviews_count' => count($branch->reviews),
        ];
    }
}
