<?php

namespace App\Transformers;

use App\City;

class CityTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(City $city)
    {
        return [
            'id' => $city->id,
            'name' => (string) $city->name
        ];
    }
}