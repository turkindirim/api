<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Media;

class MediaTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Media $media)
    {
        return [
            'id' => $media->id,
            'type' => (string) $media->type,
            'url' => (string) $media->name ? url('uploads/' . $media->name) : '',
        ];
    }
}
