<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Category;

class CategoryTransformer extends TransformerAbstract
{

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'id' => $category->id,
            'name' => (string)  $category->name,
            'description' => (string)  $category->description,
            'logo' => (string) $category->logo ? url('uploads/' . $category->logo) : '',
        ];
    }
}
