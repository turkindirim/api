<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;

class Slide extends Model
{
    use Translatable, SoftDeletes;
    public $fillable = ['logo', 'thumb', 'link', 'sort'];
    public $translatedAttributes = ['name', 'description'];
}
