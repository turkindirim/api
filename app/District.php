<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Dimsav\Translatable\Translatable;

class District extends Model
{
    use Translatable, SoftDeletes;
    
    public $translatedAttributes = ['name'];
    
    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'translations'];
    
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    
    public function branch()
    {
        return $this->hasMany('App\Branch');
    }
    public function distributor()
    {
        return $this->hasMany('App\Distributor');
    }
}
